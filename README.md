Test Environment (CTESW)
=======================

Framework for validation testing of CANopen software library.

Designed to test [ECSS-compliant](https://ecss.nl/standard/ecss-e-st-50-15c-space-engineering-canbus-extension-protocol-1-may-2015/)
version of [lely-core](https://gitlab.com/n7space/canopen/lely-core) (forked from [Lely Industries](https://opensource.lely.com/canopen/))

Each validation test created using Test Environment will consist of two applications, one running on x86 host and second running on ARM dedicated hardware.
Applications will exchange data using CAN bus and CANopen protocol stack.

Test Environment was developed under a programme of, and funded by, the [European Space Agency](https://www.esa.int/).

Licensed under the ESA Public License (ESA-PL) Permissive (Type 3), Version 2.4 (see [LICENSE](./LICENSE) for details).

## Requirements

 * GCC x86 compiler (10.2)
 * GCC ARM compiler (9-2020-q2-update)
 * GDB debugger (multiarch, 9.2)
 * Python 3.10
   with packages:
    - paramiko (3.4.0)
    - pygdbmi (0.11.0.0)
    - pyserial (3.5)
    - scons (4.6.0.post1)
    - scp (0.14.5)
    - timeout_decorator (0.5.0)

Easiest way to reproduce required software environment is to use Docker (>= 25.0.0) with provided images.
[Dockerfile](./docker/Dockerfile) can also be used as a reference of detailed required packages etc.

## Writing tests

The test consist of 3 parts:
 * C code of "Application A",
 * C code of "Application B",
 * Python code (SCons tool definition) for defining the case.

### C code

Below is an example of C code for one of the validation test applications.

```c
void
TestSetup(can_net_t *const net)
{
  CANSW_enable_rpdo_service(net);  // not real CANSW interface, just example
}

void
TestTeardown(void)
{
  CANSW_disable_rpdo_service(net);
}

void
TestMessageReceived(const struct can_msg *msg)
{
  // optional msg validation
}

void
TestMessageSent(const int ret, const struct can_msg *const msg)
{
  // optional msg validation
  LOG("Message sent");
}

void
TestStep(void)
{
  if (CANSW_pdo_received())
  {
    const int32_t value = CANSW_obj_value(TEST_OBJID);
    if (value == TEST_OBJVALUE))
      FINISH_TEST();
    else {
      LOG_UINT32_EXPECTED(“obj value”, TEST_OBJVALUE, value);
      FAIL_TEST("Incorrect obj value");
    }
  }
}
```

See [TestHarness.h](./resources/TestFramework/TestHarness.h) header for more details.

### SConscript

Following is an example of test SCons definition.

```python
Import("env")  # use environment provided by SConstruct

client_dev = env.Dcf2Dev("client.dcf")
server_dev = env.Dcf2Dev("server.dcf")

test_case = env.MakeSymmetricalTestCase("symmetrical-test-case",
                                        ["client.c"] + client_dev,
                                        ["server.c"] + server_dev,
                                        trace={
                                                "traces": ["SRS1", "SRS2"],
                                                "doc": {
                                                    "given": "something",
                                                    "when": "doing",
                                                    "then": "expecting",
                                                },
                                            },
                                        )

Return("test_case")  # This will forward result as result of BuildModule
```

## Running tests

Tests are built and executed using [SCons](https://scons.org/) tool, using provided configuration.

### Configuration

Configuration of the test hardware setup can be provided in the form of INI files.
Example configuration is available at [svf-configs/](./svf-configs) subdirectory.
By default [samv71q21/titan.conf](./svf-configs/samv71q21/titan.conf) is used,
but it can be overwritten by passing option `svf=<filepath>` to the SCons.

### Execution

Tests can be executed by simply calling the [SCons](https://scons.org/) tool: `scons`.
If no test name is provided, all tests will be executed.

Single test can be executed by passing name of the test to the tool `scons <test-name>`.
Supported options can be obtained from the tool by calling `scons -h`.

Easiest way to execute tests in predefined environment is to use Docker. Suggested way of executing:
`docker run -v $PWD:"/path/to/ctesw" --workdir "/the/same/path" --user $(id -u):$(id -g) --rm "CTESW image name" scons`.
This way the container will run with user permissions and will have access to all necessary CTESW files.
Docker configuration can be found in `docker/` directory.

## Testing Test Environment itself

Test Environment contains its own tests, used to validate the framework itself.

They are executed using [cram](https://github.com/brodie/cram) tool, which validates results of called console applications.

All tests can be executed by calling `./run-cram.sh` Bash script.

Easiest way to execute tests in predefined environment is to use Docker.
See above for uggested way of executing docker image.

## Substituting CANSW Git URL

When working with the Test Environment it might be convinient to use different URL for CANSW (CANopen Library) submodule.

Good example of such use-case - use SSH access to the CANSW submodule to contribute to it.

It can be done by "URL substitution" in `~/.gitconfig`:

```
[url "git@gitlab.com:n7space/canopen/lely-core.git"]
        insteadOf = https://gitlab.com/n7space/canopen/lely-core.git
```
