# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


env = CreateEnvironment()

env.AllEnvs.BuildModule("resources/n7-core")
env.AllEnvs.BuildModule("resources/bsp")
env.AllEnvs.BuildModule("resources/LibCANopen")
env.AllEnvs.BuildModule("resources/TestFramework")
env.BuildModule("tests")
env.BuildModule("doc")

Default(None)
Help(
    f"\n\nCTESW - CANopen SW Library Test Environment - {env['N7S_SW_VERSION']}\n",
    append=True,
)
Help(
    "Licensed under European Space Agency Public License (ESA-PL) Permissive (Type 3) – v2.4\n",
    append=True,
)
Help("Copyright N7 Space sp. z o.o. 2020-2024\n\n", append=True)
