#!/bin/bash

if test "$#" -ne 1; then
    echo "Usage $0 <export folder name>"
    exit 1
fi

REGISTRY=registry.gitlab.com/n7space/canopen/lely-core

IMAGES=(
      asan
      python-checks
      clang-format
      sonarcloud
      coverity
      cppcheck-win64
      cppcheck-unix64
      doc
      valgrind
      lcov
      arm-none-eabi
      aarch64-linux-gnu
      arm-linux-gnueabihf
      x86_64-w64-mingw32
      i686-w64-mingw32
      gcc
      cpplint
)

mkdir -p "$1"

for img in "${IMAGES[@]}" ; do
    ITEM="$REGISTRY/$img"

    echo "Downloading $img"
    docker pull -a "$ITEM"

    echo "Exporting $img"
    docker save "$ITEM" | bzip2 > "$1/$img.tar.bz2"
done
