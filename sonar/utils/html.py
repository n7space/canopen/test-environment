# This file is part of the Test Environment CI configuration.
#
# @copyright 2023-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import typing


class TagGuard:
    def __init__(self, output: typing.TextIO, tag: str, attributes: str = ""):
        self.output = output
        self.tag = tag
        self.attributes = attributes
        if len(self.attributes) > 0:
            self.attributes = " " + self.attributes

    def __enter__(self):
        self.output.write(f"<{self.tag}{self.attributes}>\n")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.output.write(f"</{self.tag}>\n")


class NestedCounterNode:
    class Root:
        def item(self, separator: str) -> str:
            return ""

        def level(self) -> int:
            return 0

    def __init__(self, parent: typing.Union["NestedCounterNode", Root] = Root()):
        self.parent = parent
        self.counter = 0

    def next(self):
        self.counter += 1

    def item(self, separator: str) -> str:
        result = self.parent.item(separator)
        if len(result):
            return f"{result}{separator}{self.counter}"
        return str(self.counter)

    def level(self) -> int:
        return self.parent.level() + 1


class NestedCounter:
    def __init__(self):
        self.node = NestedCounterNode()

    def next(self):
        self.node.next()

    def title(self) -> str:
        return self.node.item(".")

    def toc(self) -> str:
        return self.node.item("-")

    def level(self) -> int:
        return self.node.level()

    def down(self):
        self.node = NestedCounterNode(self.node)
        return self

    def up(self):
        self.node = self.node.parent


class SubchapterGuard:
    def __init__(self, counter: NestedCounter, toc: typing.List[str]):
        self.counter = counter
        self.toc = toc

    def __enter__(self):
        self.counter.down()
        self.toc.append(f"<div style='margin-left: {self.counter.level() * 8}pt'>")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.counter.up()
        self.toc.append("</div>")


class HtmlReportBuilder:
    def __init__(self, title: str):
        self.title = title
        self.toc: typing.List[str] = []
        self.contents: typing.List[str] = []
        self.counter = NestedCounter()

    def subchapter(self, title: str) -> SubchapterGuard:
        self.counter.next()
        self.toc.append(
            f'<a href="#toc-{self.counter.toc()}">{self.counter.title()} {title}</a>'
        )
        self.contents.append(
            f'\n<h{self.counter.level()} id="toc-{self.counter.toc()}"> {self.counter.title()} {title}</h{self.counter.level()}>\n'
        )
        return SubchapterGuard(self.counter, self.toc)

    def add_line(self, line: str):
        self.contents.append(line)

    def add_lines(self, lines: typing.List[str]):
        self.contents += lines

    def dump(self, output: typing.TextIO):
        output.write("<!DOCTYPE html>\n")
        with TagGuard(output, "html"):
            with TagGuard(output, "head"):
                output.writelines(
                    [
                        '<meta charset="utf-8" />\n',
                        f"<title>{self.title}</title>\n",
                    ]
                )
                with TagGuard(output, "style"):
                    output.writelines(["ul {", "  list-style-type: none;", "}"])
            with TagGuard(output, "body"):
                output.writelines(
                    [
                        f"<h1>{self.title}</h1>\n",
                        f"Generated: {datetime.datetime.now().astimezone().isoformat()}\n",
                    ]
                )
                with TagGuard(output, "nav"):
                    output.write("\n".join(self.toc))
                output.write("\n".join(self.contents))
