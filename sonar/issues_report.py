#!/bin/env python3

# This file is part of the Test Environment CI configuration.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import html
from collections import defaultdict, namedtuple
from urllib.parse import urljoin

import requests
from utils.html import HtmlReportBuilder

RESOLUTIONS = [
    "UNRESOLVED",
    "WONTFIX",
    "FALSE-POSITIVE",
]

CATEGORIES = {
    "CONSISTENT": "Consistency",
    "INTENTIONAL": "Intentionality",
    "ADAPTABLE": "Adaptability",
    "RESPONSIBLE": "Responsibility",
    "OTHER": "Other",
}


class Sonar:
    def __init__(self, url, token):
        self.token = token
        self.auth = (token, "")
        self.url = url

    def download_issues(self, organization, project, resolved):
        issues = []
        rules = []
        page = 1

        while True:
            args = {
                "organization": organization,
                "componentKeys": project,
                "resolutions": "FALSE-POSITIVE,WONTFIX" if resolved else "",
                "resolved": "yes" if resolved else "no",
                "additionalFields": "comments,rules",
                "ps": 500,
                "p": page,
            }
            page += 1

            response = requests.get(f"{self.url}/issues/search", args, auth=self.auth)
            if response.status_code != 200:
                raise RuntimeError(
                    f"Unable to download Quality Profile rules "
                    f"{response.status_code} {response.content}"
                )

            payload = response.json()

            issues += payload["issues"]
            rules += payload["rules"]

            if args["ps"] * (page - 1) > int(payload["paging"]["total"]):
                break

        return issues, rules


Component = namedtuple("Component", ["file", "line"])


class Grouping:
    def __init__(self, rules, issues):
        self.langs = {rule["key"]: rule["langName"] for rule in rules}
        self.rules = {rule["key"]: rule["name"] for rule in rules}
        self.issues = issues
        # lang -> resolution -> category -> rule -> justification -> list(Component)
        self.groups = defaultdict(
            lambda: defaultdict(
                lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
            )
        )

        self._groupItems()

    def _groupItems(self):
        for issue in self.issues:
            lang = self._extractLanguage(issue)
            resolution = issue.get("resolution", "UNRESOLVED")
            category = issue.get("cleanCodeAttributeCategory", "OTHER")
            rule = self._extractRule(issue)
            justification = self._extractJustification(issue)

            self.groups[lang][resolution][category][rule][justification].append(
                self._extractComponent(issue)
            )

    def _extractLanguage(self, issue):
        return self.langs[issue["rule"]]

    @staticmethod
    def _extractJustification(issue):
        comments = issue["comments"]
        return (
            "\n".join([comment["markdown"] for comment in comments])
            if len(comments) > 0
            else "NONE"
        )

    @staticmethod
    def _extractComponent(issue):
        return Component(
            file=issue["component"].replace(issue["project"] + ":", ""),
            line=issue.get("line", 0),
        )

    def _extractRule(self, issue):
        return self.rules.get(issue["rule"], issue["message"])


class ReportBuilder:
    def __init__(self):
        self.html = HtmlReportBuilder("Sonar Issues")

    def add_language(self, title, issues):
        with self.html.subchapter(title):
            if len(issues.values()):
                for resolution in RESOLUTIONS:
                    self.add_resolution(resolution, issues[resolution])
            else:
                self.html.add_line("None")

    def add_resolution(self, resolution, issues):
        if len(issues) < 1:
            return

        with self.html.subchapter(resolution):
            for category, display in CATEGORIES.items():
                self.add_category(display, issues[category])

    def add_category(self, category, issues):
        with self.html.subchapter(category):
            if len(issues) > 0:
                for rule, rule_issues in sorted(issues.items()):
                    self.add_rule(rule, rule_issues)
            else:
                self.html.add_line("None")

    def add_rule(self, rule, issues):
        with self.html.subchapter(html.escape(rule)):
            if len(issues) > 1:
                self.add_justifications(issues)
            else:
                self.add_justification(list(issues.items())[0])

    def add_justifications(self, justifications):
        for idx, item in enumerate(sorted(justifications.items())):
            with self.html.subchapter(f"Issue Group #{idx + 1}"):
                self.add_justification(item)

    def add_justification(self, item):
        justification, components = item
        components = sorted(set(components))
        self.html.add_lines(
            [
                f"{justification}",
                f"<p><i>{self._components_header(components)}:</i>",
                "<ul>",
            ]
            + [
                f"<li><pre>{component.file}{':' + str(component.line) if component.line > 0 else ''}</pre></li>\n"
                for component in components
            ]
            + [
                "</ul></p>",
            ]
        )

    def dump(self, output):
        self.html.dump(output)

    @staticmethod
    def _components_header(components):
        if len(components) > 1:
            return f"Issues ({len(components)})"
        return "Issue"


def dump_issues(url, token, organization, project, output):
    sonar = Sonar(url, token)
    issues1, rules1 = sonar.download_issues(organization, project, resolved=False)
    issues2, rules2 = sonar.download_issues(organization, project, resolved=True)

    issues = issues1 + issues2
    rules = rules1 + rules2

    groups = Grouping(rules, issues).groups

    builder = ReportBuilder()
    for lang, title in [
        ("C", "C code"),
        ("C++", "C++ code"),
        ("Python", "Python code"),
    ]:
        builder.add_language(title, groups[lang])

    with open(output, "w", encoding="utf8") as out:
        builder.dump(out)


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print(f"Usage {sys.argv[0]} <organization> <project> <output>")
        sys.exit(-1)

    if "SONAR_HOST_URL" not in os.environ or "SONAR_AUTH_TOKEN" not in os.environ:
        print(
            f"{sys.argv[0]} requires SONAR_HOST_URL and SONAR_AUTH_TOKEN environment variables"
        )
        sys.exit(-1)

    dump_issues(
        urljoin(os.environ["SONAR_HOST_URL"], "api"),
        os.environ["SONAR_AUTH_TOKEN"],
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
    )
