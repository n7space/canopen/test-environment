#!/bin/env python3

# This file is part of the Test Environment CI configuration.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import html
import os
from collections import defaultdict, namedtuple
from urllib.parse import urljoin

import requests

from utils.html import HtmlReportBuilder

CATEGORIES = {
    "CONSISTENT": "Consistency",
    "INTENTIONAL": "Intentionality",
    "ADAPTABLE": "Adaptability",
    "RESPONSIBLE": "Responsibility",
    "OTHER": "Other",
}


class Sonar:
    def __init__(self, url, token):
        self.token = token
        self.auth = (token, "")
        self.url = url

    def find_quality_profile(self, organization, project, language):
        args = {
            "organization": organization,
            "project": project,
            "language": language,
        }

        response = requests.get(
            f"{self.url}/qualityprofiles/search", args, auth=self.auth
        )
        if response.status_code != 200:
            raise RuntimeError(
                f"Unable to find Quality Profile "
                f"{response.status_code} {response.content}"
            )

        json = response.json()["profiles"]

        if len(json) != 1:
            raise RuntimeError(f"Bad number of Quality Profiles {len(json)}")

        return json[0]

    def download_rules(self, qualityprofile_key):
        args = {
            "qprofile": qualityprofile_key,
            "activation": "true",
            "f": "name,descriptionSections,htmlDesc,htmlNote,cleanCodeAttribute,lang",
            "ps": 500,
        }

        response = requests.get(f"{self.url}/rules/search", args, auth=self.auth)
        if response.status_code != 200:
            raise RuntimeError(
                f"Unable to download Quality Profile rules "
                f"{response.status_code} {response.content}"
            )

        return response.json()["rules"]


class ReportBuilder:
    def __init__(self):
        self.html = HtmlReportBuilder("Sonar Rules")

    def add_rule(self, rule):
        with self.html.subchapter(rule.name):
            self.html.add_lines(
                [
                    '<div style="margin-left: 40pt; background-color: #f0f0f0f0; padding-left: 10pt; padding-right: 10pt; padding-top: 1pt; padding-bottom: 5pt; width: 800pt; ">',
                    rule.desc,
                    rule.note,
                    "</div>",
                ]
            )

    def add_language(self, title, rules):
        with self.html.subchapter(title):
            for category, display in CATEGORIES.items():
                self.add_category(display, rules[category])

    def add_category(self, category, rules):
        with self.html.subchapter(category):
            if len(rules) > 0:
                for rule in sorted(rules):
                    self.add_rule(rule)
            else:
                self.html.add_line("None")

    def dump(self, output):
        self.html.dump(output)


Rule = namedtuple("Rule", ["name", "desc", "note"])


class Grouping:
    def __init__(self, rules):
        self.rules = rules

        # lang -> category -> list(rule)
        self.groups = defaultdict(lambda: defaultdict(list))

        self._groupItems()

    def _groupItems(self):
        for rule in self.rules:
            lang = rule["lang"]
            category = rule.get("cleanCodeAttributeCategory", "OTHER")
            rule = Rule(
                name=html.escape(rule["name"]),
                desc=rule.get(
                    "htmlDesc", self._extractDesc(rule["descriptionSections"])
                ),
                note=rule.get(
                    "htmlNote", self._extractNote(rule["descriptionSections"])
                ),
            )

            self.groups[lang][category].append(rule)

    @staticmethod
    def _extractDesc(sections):
        root = [s for s in sections if s.get("key") == "root_cause"]
        if root:
            assert len(root) == 1
            return root[0]["content"]
        return ""

    @staticmethod
    def _extractNote(sections):
        resources = [s for s in sections if s.get("key") == "resources"]
        if resources:
            assert len(resources) == 1
            return "<hr/>" + resources[0]["content"]
        return ""


def get_rules(url, token, organization, project):
    sonar = Sonar(url, token)
    rules = []
    for lang in ["c", "cpp", "py"]:
        qprofile = sonar.find_quality_profile(organization, project, lang)
        rules += sonar.download_rules(qprofile["key"])
    return rules


def dump_rules(url, token, organization, project, output):
    rules = get_rules(url, token, organization, project)

    groups = Grouping(rules).groups

    builder = ReportBuilder()
    for lang, title in [
        ("c", "C code"),
        ("cpp", "C++ code"),
        ("py", "Python code"),
    ]:
        builder.add_language(title, groups[lang])

    with open(output, "w", encoding="utf-8") as out:
        builder.dump(out)


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print(f"Usage {sys.argv[0]} <organization> <project> <output>")
        sys.exit(-1)

    if "SONAR_HOST_URL" not in os.environ or "SONAR_AUTH_TOKEN" not in os.environ:
        print(
            f"{sys.argv[0]} requires SONAR_HOST_URL and SONAR_AUTH_TOKEN environment variables"
        )
        sys.exit(-1)

    dump_rules(
        urljoin(os.environ["SONAR_HOST_URL"], "api"),
        os.environ["SONAR_AUTH_TOKEN"],
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
    )
