#!/bin/bash

# This file is part of the Test Environment tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "${DIR}"

export CTESW_DIR="${DIR}"
export SCONS_BUILD_DIR=${SCONS_BUILD_DIR:=build}

trap "echo \nTESTS INTERRUPTED!\n; exit 1" SIGHUP SIGINT SIGTERM

if [ "$CTESW_BUILD_TYPE" = "coverage" ] ; then
    coverage erase --rcfile="${DIR}/coverage.rc"
fi

REPORT_DIR="${SCONS_BUILD_DIR}/${CTESW_BUILD_TYPE:=release}/"

# Test platform must be specified as 1st argument (e.g. samv71q21)
# Test board must be specified as 2nd argument (e.g. titan).
# Both of those arguments should point to configuration file in svf-configs directory.
# If examples above are used, svf-configs/samv71q21/titan.conf file will be chosen.
if [ $# -ge 2 ] ; then
    export CTESW_PLATFORM=$1
    export CTESW_BOARD=$2
    if [[ ${CTESW_PLATFORM} != *_xc32 ]]; then
        export CTESW_PLATFORM_DIR=${CTESW_PLATFORM}
    else
        export CTESW_PLATFORM_DIR=${CTESW_PLATFORM%_xc32}
    fi
    export SCONS_OPTS="${SCONS_OPTS} hwtbPlatformName=${CTESW_PLATFORM} svf=svf-configs/${CTESW_PLATFORM_DIR}/${CTESW_BOARD}.conf"
else
    echo "You must provide the platform to test as a first argument, and board name as a second."
    exit 2
fi

# Remaining arguments are used as a list of tests to run, all of them by default.
TESTS=${@:3}
TESTS=${TESTS:=$( ls tests/*/*.t )}

for tst in ${TESTS} ; do
    echo -e "\nRUNNING ${tst}"
    mkdir -p $(dirname "${REPORT_DIR}/${tst}")
    cram --shell=/bin/bash --xunit-file="${REPORT_DIR}/${tst}-report.xml" "${tst}"
    if [ $? -ne 0 ]
    then
        echo "FAILED ${tst}"
        exit 1
    fi
    echo "PASSED ${tst}"
    echo "-------------------------------------------------"
done

echo -e "\nALL TESTS PASSED!\n"

if [ "$CTESW_BUILD_TYPE" = "coverage" ] ; then
    echo -e "\nGENERATING COVERAGE"
    coverage html --rcfile="${DIR}/coverage.rc"
    echo -e "COVERAGE - DONE!\n"
    echo "-------------------------------------------------"
fi
