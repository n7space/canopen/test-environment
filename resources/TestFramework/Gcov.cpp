/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Gcov.hpp"

#include <cassert>
#include <cstdlib>

#ifndef N7S_TARGET_GCC_HOST
#include <span>

extern "C" {
#include <gcov.h>
}

// cppcheck-suppress [misra-c2012-8.11]
extern const struct gcov_info *const GCOV_INFO_BEGIN[]; // NOLINT
// cppcheck-suppress [misra-c2012-8.11]
extern const struct gcov_info *const GCOV_INFO_END[]; // NOLINT

namespace
{

class InfoFileDumper
{
      public:
	explicit InfoFileDumper(BinaryLogEncoder &encoder_) noexcept
	    : encoder(encoder_), fd(0u), bufferIndex(0u)
	{
		(void)encoder_; // misra...
	}

	~InfoFileDumper() noexcept
	{
		if (bufferIndex > 0u)
			(void)encoder.write(fd, buffer.data(), bufferIndex);
	}

	InfoFileDumper(const InfoFileDumper &) = delete;
	InfoFileDumper(InfoFileDumper &&) = delete;
	InfoFileDumper &operator=(const InfoFileDumper &) = delete;
	InfoFileDumper &operator=(InfoFileDumper &&) = delete;

	void
	open(const char *const filename) noexcept
	{
		assert(fd == 0u);
		fd = encoder.open(filename);
	}

	void
	store(const void *const data, const uint32_t length)
	{
		assert(fd != 0u);
		if ((bufferIndex + length) > buffer.size()) {
			(void)encoder.write(fd, buffer.data(), bufferIndex);
			bufferIndex = 0u;
		}

		assert(length < buffer.size());

		const auto s = std::span(
				static_cast<const uint8_t *>(data), length);
		std::copy(s.begin(), s.end(), buffer.begin() + bufferIndex);
		bufferIndex += length;
	}

      private:
	BinaryLogEncoder &encoder;

	uint32_t fd;
	std::array<uint8_t, 4096u> buffer{};
	uint32_t bufferIndex;
};

void
process_data(const void *data, unsigned length, void *arg)
{
	auto *const dumper = static_cast<InfoFileDumper *>(arg);
	dumper->store(data, length);
}

void
process_filename(const char *const f, void *const arg)
{
	assert(f != nullptr);
	auto *const dumper = static_cast<InfoFileDumper *>(arg);
	dumper->open(f);
}

void *
allocate(unsigned length, void *arg)
{
	(void)arg;
	// cppcheck-suppress [misra-c2012-21.3]
	return malloc(length); // NOLINT
}
} // namespace

#endif

void
GcovDumper::dump() noexcept
{
#ifndef N7S_TARGET_GCC_HOST
	for (const auto *const *info = GCOV_INFO_BEGIN; info != GCOV_INFO_END;
			++info) {
		InfoFileDumper dumper{ encoder };
		__gcov_info_to_gcda(*info, process_filename, process_data,
				allocate, &dumper);
	}
#endif
}
