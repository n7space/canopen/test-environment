/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_TEST_TIME_H
#define CTESW_TEST_TIME_H

#include <stdbool.h>
#include <stdint.h>

/// Default accuracy percent of time intervals comparison on Host.
static const double HOST_TIME_ACCURACY = 0.06;

/// Default accuracy percent of time intervals comparison on HWTB.
static const double HWTB_TIME_ACCURACY = 0.01;

/// Checks if measured time interval is equal to an expected one, given provided accuracy.
///
/// \param [in] measured the measured value.
/// \param [in] expected the expected value.
/// \param [in] accuracy the accepted error percentage.
///
/// \retval true when value is in expected range.
/// \retval false otherwise.
static inline bool
CheckTimeIntervalWithAccuracy(const int32_t measured, const uint32_t expected,
		const double accuracy)
{
	return (measured < (expected * (1 + accuracy)))
			&& (measured > (expected * (1 - accuracy)));
}

/// Checks if measured time interval is equal to an expected one.
/// Assumes default accuracy for the given platform.
///
/// \param [in] measured the measured value.
/// \param [in] expected the expected value.
///
/// \retval true when value is in expected range.
/// \retval false otherwise.
static inline bool
CheckTimeInterval(const int32_t measured, const uint32_t expected)
{
#ifdef CAN_TEST_ENV_HWTB
	return CheckTimeIntervalWithAccuracy(
			measured, expected, HWTB_TIME_ACCURACY);
#elif CAN_TEST_ENV_HOST
	return CheckTimeIntervalWithAccuracy(
			measured, expected, HOST_TIME_ACCURACY);
#else
#warning "CAN_TEST_ENV_* not defined, assuming HOST accuracy"
	return CheckTimeIntervalWithAccuracy(
			measured, expected, HOST_TIME_ACCURACY);
#endif
}

#endif // CTESW_TEST_TIME_H
