/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CanHandshake.h"
#include "Log.h"

#include "HAL/CanInterface.h"
#include "HAL/ClockInterface.h"

#include <lely/compat/string.h>

#define INIT_FRAME_DATA { 'I', 'N', 'I', 'T' }
#define ACK_FRAME_DATA { 'A', 'C', 'K' }

#define SENDER_ID 0x22
#define RECEIVER_ID 0x11

#define SENDER_RETRY_COUNT 10
#define SEND_RECEIVE_DELAY_S 1
#define RECEIVER_TIMEOUT_S 20

static void
setFrame(struct can_msg *const msg, const uint8_t *const data,
		const uint_least8_t dataLength, const uint_least8_t flags,
		const uint_least32_t id)
{
	msg->id = id;
	msg->flags = flags;
	msg->len = dataLength;
	(void)memcpy(msg->data, data, dataLength);
}

static void
setInitFrame(struct can_msg *const msg, const uint_least32_t id)
{
	uint8_t data[] = INIT_FRAME_DATA;
	setFrame(msg, data, sizeof(data), 0, id);
}

static void
setAckFrame(struct can_msg *const msg, const uint_least32_t id)
{
	uint8_t data[] = ACK_FRAME_DATA;
	setFrame(msg, data, sizeof(data), 0, id);
}

static bool
checkFrame(const struct can_msg *const msg, const uint8_t *const data,
		const uint_least8_t dataLength, const uint_least8_t flags,
		const uint_least32_t id)
{
	if (msg->id != id)
		return false;
	if (msg->flags != flags)
		return false;
	if (msg->len != dataLength)
		return false;
	if (memcmp(data, msg->data, dataLength) != 0)
		return false;
	return true;
}

static bool
checkInitFrame(const struct can_msg *const msg, const uint_least32_t id)
{
	const uint8_t data[] = INIT_FRAME_DATA;
	return checkFrame(msg, data, sizeof(data), 0, id);
}

static bool
checkAckFrame(const struct can_msg *const msg, const uint_least32_t id)
{
	const uint8_t data[] = ACK_FRAME_DATA;
	return checkFrame(msg, data, sizeof(data), 0, id);
}

// This helper function checks for potential overflow (could happen if the time source
// is not monotonic) before calculating timeout.
// GCC detects that and reports as strict-overflow warning.
// Timeout is in seconds.
static inline bool
has_timed_out(const struct timespec now, const struct timespec start,
		const time_t timeout)
{
	return (now.tv_sec > start.tv_sec)
			&& ((now.tv_sec - start.tv_sec) > timeout);
}

bool
CanHandshake_senderRoutine(const Can_Bus bus)
{
	for (uint32_t i = SENDER_RETRY_COUNT; i > 0u; --i) {
		struct can_msg msg = { 0 };
		setInitFrame(&msg, SENDER_ID);
		if (!CanSend(bus, &msg))
			return false;

		struct timespec sendTime = { 0 };
		ClockGetTime(&sendTime);

		struct timespec currentTime = { 0 };
		do {
			if (CanRecv(bus, &msg)
					&& checkAckFrame(&msg, RECEIVER_ID)) {
				setAckFrame(&msg, SENDER_ID);
				return CanSend(bus, &msg);
			}

			ClockGetTime(&currentTime);
		} while (!has_timed_out(
				currentTime, sendTime, SEND_RECEIVE_DELAY_S));
	}
	LOG("Handshake: Did not receive CAN ACK response in specified time");
	return false;
}

bool
CanHandshake_receiverRoutine(const Can_Bus bus)
{
	struct can_msg msg = { 0 };
	struct timespec startTime = { 0 };
	ClockGetTime(&startTime);
	for (;;) {
		if (CanRecv(bus, &msg) && checkInitFrame(&msg, SENDER_ID))
			break;

		struct timespec currentTime = { 0 };
		ClockGetTime(&currentTime);
		if (has_timed_out(currentTime, startTime, RECEIVER_TIMEOUT_S)) {
			LOG("Handshake: Did not receive CAN INIT message in specified time");
			return false;
		}
	}

	setAckFrame(&msg, RECEIVER_ID);
	if (!CanSend(bus, &msg))
		return false;

	ClockGetTime(&startTime);

	for (;;) {
		if (CanRecv(bus, &msg) && checkAckFrame(&msg, SENDER_ID))
			break;

		struct timespec currentTime = { 0 };
		ClockGetTime(&currentTime);
		if (has_timed_out(currentTime, startTime, RECEIVER_TIMEOUT_S)) {
			LOG("Handshake: Did not receive CAN ACK message in specified time");
			return false;
		}
	}
	return true;
}
