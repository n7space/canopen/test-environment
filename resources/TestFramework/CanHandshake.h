/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_CAN_HANDSHAKE_H
#define CTESW_CAN_HANDSHAKE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "HAL/CanInterface.h"

/// Initiates handshake sequence for the sender CAN device. If function returns
/// successfully, both devices are synchronized and ready to perform tests.
///
/// \param [in] bus CAN bus to be used for handshake.
///
/// \retval true  On success.
/// \retval false On error, the error message will be printed to `stdout`.
bool CanHandshake_senderRoutine(Can_Bus bus);

/// Initiates handshake sequence for the receiver CAN device. If function returns
/// successfully, both devices are synchronized and ready to perform tests.
///
/// \param [in] bus CAN bus to be used for handshake.
///
/// \retval true  On success.
/// \retval false On error, the error message will be printed to `stdout`.
bool CanHandshake_receiverRoutine(Can_Bus bus);

#ifdef __cplusplus
}
#endif

#endif // CTESW_CAN_HANDSHAKE_H
