/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "TestHarness.h"

#include <assert.h>

#include <lely/can/net.h>
#include <lely/co/dev.h>
#include <lely/compat/stdlib.h>
#include <lely/compat/string.h>
#include <lely/util/error.h>
#include <lely/util/time.h>

#include "HAL/CanInterface.h"
#include "HAL/ClockInterface.h"
#include "HAL/EntryPoint.h"

#include "Allocator.h"
#include "CanHandshake.h"

#ifdef CAN_TEST_ENV_HWTB
#include "HAL/TempMeter.h"
#endif

#ifndef TEST_ENV_DEFAULT_TEST_TIMEOUT_SECONDS
#define TEST_ENV_DEFAULT_TEST_TIMEOUT_SECONDS 30
#endif

typedef enum {
	TestHarnessState_PreOperational,
	TestHarnessState_Running,
	TestHarnessState_Finished,
	TestHarnessState_Error,
} TestHarnessState;

static TestHarnessState testState = TestHarnessState_PreOperational;
static struct timespec testTimeout = { TEST_ENV_DEFAULT_TEST_TIMEOUT_SECONDS,
	0 };

static TestMessageReceivedCallback *receiveCallback[Can_Bus_Count] = { 0 };
static TestMessageIgnoredCallback *ignoredCallback[Can_Bus_Count] = { 0 };
static TestMessageSentCallback *sentCallback[Can_Bus_Count] = { 0 };

static can_net_t *net = NULL;

static void
initTime(can_net_t *net_)
{
	struct timespec now = { 0, 0 };
	const int result = can_net_set_time(net_, &now);
	if (result != 0) {
		LOG_INT32("result", result);
		LOG_INT32("errc", get_errc());
		FAIL_TEST("can_net_set_time failed");
	}
}

static int
sendCallback(const struct can_msg *const msg, uint_least8_t busId, void *data)
{
	(void)data;

	assert(busId < Can_Bus_Count);

	if (CanSend(busId, msg)) {
		sentCallback[busId](msg);
	} else {
		LOG_INT32("errc", get_errc());
		FAIL_TEST("Sending message failed");
		return -1;
	}

	return 0;
}

static can_net_t *
createDefaultNetwork(void)
{
	can_net_t *const net_ = can_net_create(Allocator_init(), Can_Bus_A);
	assert(net_);

	can_net_set_send_func(net_, sendCallback, NULL);

	initTime(net_);

	return net_;
}

static void
startTemperatureMeasurement(void)
{
#ifdef CAN_TEST_ENV_HWTB
	TempMeter_startup();
	LOG_INT32("Test start temperature", TempMeter_getTemp());
#endif
}

static void
stopTemperatureMeasurement(void)
{
#ifdef CAN_TEST_ENV_HWTB
	LOG_INT32("Test end temperature", TempMeter_getTemp());
	TempMeter_shutdown();
#endif
}

static bool
performHandshake(void)
{
#if CAN_TEST_ENV_HOST
	return CanHandshake_senderRoutine(Can_Bus_A);
#elif CAN_TEST_ENV_HWTB
	return CanHandshake_receiverRoutine(Can_Bus_A);
#else
#warning "Can handshake before test is disabled."
	return true;
#endif
}

static void
checkForTimeout(const struct timespec *now)
{
	if (timespec_cmp(now, &testTimeout) > 0) {
		LOG_TIME("Test timeout", &testTimeout);
		FAIL_TEST("TIMED OUT!");
	}
}

static void
receiveOn(Can_Bus bus)
{
	struct can_msg msg = CAN_MSG_INIT;
	if (CanRecv(bus, &msg)) {
		const ssize_t result = can_net_recv(net, &msg, bus);

		if (result == 1)
			receiveCallback[bus](&msg);
		else if (result == 0)
			ignoredCallback[bus](&msg);
		else {
			LOG_INT32("result", result);
			LOG_INT32("errc", get_errc());
			FAIL_TEST("can_net_recv failed");
		}
	}
}

static void
updateTime(const struct timespec *const timestamp)
{
	const int result = can_net_set_time(net, timestamp);
	if (result != 0) {
		LOG_INT32("result", result);
		LOG_INT32("errc", get_errc());
		FAIL_TEST("Time update failed!");
	}
}

int
EntryPoint_main(void)
{
	CanInit();

	net = createDefaultNetwork();

	startTemperatureMeasurement();

	if (!performHandshake())
		return -2;

	SetCanCallbacksBusA(&TestMessageSent, &TestMessageReceived);
	SetCanCallbacksBusB(&FailOnMessageSent, &FailOnMessageReceived);

	SetIgnoredMessageCallbackBusA(NULL);
	SetIgnoredMessageCallbackBusB(NULL);

	testState = TestHarnessState_Running;

	TestSetup(net); // to be filled by test

	ClockStartup();

	LOG("TEST STARTED");

	while (testState == TestHarnessState_Running) {
		struct timespec now = { 0, 0 };
		ClockGetTime(&now);

		updateTime(&now);

		receiveOn(Can_Bus_A);
		receiveOn(Can_Bus_B);

		if (testState == TestHarnessState_Running)
			TestStep(); // to be filled by test

		checkForTimeout(&now);
	}

	TestTeardown(); // to be filled by test

	can_net_destroy(net);

	CanFini();

	LOG_UINT64("Total allocated bytes", Allocator_usedMemory());

	stopTemperatureMeasurement();

	return (testState == TestHarnessState_Error) ? -1 : 0;
}

void
FailTest(const char *message, size_t length)
{
	testState = TestHarnessState_Error;

	Log_Message(message, length);
	LOG("TEST FAILED!");
}

void
FinishTest(void)
{
	if (testState == TestHarnessState_Error) {
		LOG("Unable to mark test as FINISHED, already marked as FAILED");
	} else {
		LOG("TEST FINISHED!");
		testState = TestHarnessState_Finished;
	}
}

void
SetTestTimeout(const struct timespec *timeout)
{
	testTimeout = *timeout;
}

void
GetCurrentTestTime(struct timespec *const tp)
{
	ClockGetTime(tp);
}

void
FailOnMessageReceived(const struct can_msg *msg)
{
	LOG_ARRAY("message", msg->data, msg->len);
	FAIL_TEST("Unexpected message received");
}

void
FailOnMessageSent(const struct can_msg *msg)
{
	LOG_ARRAY("message", msg->data, msg->len);
	FAIL_TEST("Unexpected message sent");
}

static void
setCanCallbacksBus(Can_Bus bus, TestMessageSentCallback sent,
		TestMessageReceivedCallback received)
{
	assert(sent != NULL);
	assert(received != NULL);
	assert(bus < Can_Bus_Count);

	sentCallback[bus] = sent;
	receiveCallback[bus] = received;
}

void
SetCanCallbacksBusA(TestMessageSentCallback sent,
		TestMessageReceivedCallback received)
{
	setCanCallbacksBus(Can_Bus_A, sent, received);
}

void
SetCanCallbacksBusB(TestMessageSentCallback sent,
		TestMessageReceivedCallback received)
{
	setCanCallbacksBus(Can_Bus_B, sent, received);
}

static void
defaultIgnoredMessageCallback(const struct can_msg *const msg)
{
	LOG_ARRAY("message", msg->data, msg->len);
	FAIL_TEST("Message ignored");
}

void
SetIgnoredMessageCallbackBusA(TestMessageIgnoredCallback callback)
{
	ignoredCallback[Can_Bus_A] = (callback != NULL)
			? callback
			: &defaultIgnoredMessageCallback;
}

void
SetIgnoredMessageCallbackBusB(TestMessageIgnoredCallback callback)
{
	ignoredCallback[Can_Bus_B] = (callback != NULL)
			? callback
			: &defaultIgnoredMessageCallback;
}
