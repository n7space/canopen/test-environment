/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_LOG_H
#define CTESW_LOG_H

#include <stddef.h>
#include <stdint.h>

struct timespec;

/// Logs literal message
#define LOG(str) Log_Message((str), sizeof((str)))

/// Logs literal message without prefixing
#define LOG_RAW(str) Log_Raw((str), sizeof((str)))

/// Logs literal name and specified value of int32 parameter
#define LOG_INT32(name, value) LOG_UINT32((name), (uint32_t)(value))
/// Logs literal name and specified value of int64 parameter
#define LOG_INT64(name, value) LOG_UINT64((name), (uint64_t)(value))

/// Logs literal name, current and expected value of int32 parameter
#define LOG_INT32_EXPECTED(name, expected, current) \
	LOG_UINT32_EXPECTED((name), (uint32_t)(expected), (uint32_t)(current))
/// Logs literal name, current,and expected value of int64 parameter
#define LOG_INT64_EXPECTED(name, expected, current) \
	LOG_UINT64_EXPECTED((name), (uint64_t)(expected), (uint64_t)(current))

/// Logs literal name and specified value of uint32 parameter
#define LOG_UINT32(name, value) Log_UInt32((name), sizeof((name)), (value))
/// Logs literal name and specified value of uint64 parameter
#define LOG_UINT64(name, value) Log_UInt64((name), sizeof((name)), (value))

/// Logs literal name, expected and current value of uint32 parameter
#define LOG_UINT32_EXPECTED(name, expected, current) \
	Log_UInt32Expected((name), sizeof((name)), (expected), (current))
/// Logs literal name, expected and current value of uint64 parameter
#define LOG_UINT64_EXPECTED(name, expected, current) \
	Log_UInt64Expected((name), sizeof((name)), (expected), (current))

/// Logs literal name and specified value of time parameter
#define LOG_TIME(name, value) Log_Time((name), sizeof((name)), (value))

/// Logs literal name and contents of the provided array.
#define LOG_ARRAY(name, array, length) \
	Log_Array((name), sizeof((name)), (array), (length))

/// Logs literal name and contents of the provided string parameter (assumes
/// maximum length).
#define LOG_STR(name, str) Log_String((name), sizeof((name)), (str), 256)

/// Logs literal name and contents of the provided string parameter.
#define LOG_STR_N(name, str, size) \
	Log_String((name), sizeof((name)), (str), (size))

#ifdef __cplusplus
extern "C" {
#endif

/// Logs message (null-terminated string)
/// \param [in] str Message to be logged.
/// \param [in] length Length of the message to be logged.
void Log_Message(const char *str, size_t length);

/// Logs message without any time prefix
/// \param [in] str Message to be logged.
/// \param [in] length Length of the message to be logged.
void Log_Raw(const char *str, size_t length);

/// Logs name and specified value of uint32 parameter
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] value Value to be logged.
void Log_UInt32(const char *name, size_t length, uint32_t value);
/// Logs name and specified value of uint64 parameter
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] value Value to be logged.
void Log_UInt64(const char *name, size_t length, uint64_t value);

/// Logs name, current and expected value of uint32 parameter
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] expected Expected value to be logged.
/// \param [in] current Current value to be logged.
void Log_UInt32Expected(const char *name, size_t length, uint32_t expected,
		uint32_t current);
/// Logs name, current and expected value of uint64 parameter
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] expected Expected value to be logged.
/// \param [in] current Current value to be logged.
void Log_UInt64Expected(const char *name, size_t length, uint64_t expected,
		uint64_t current);

/// Logs name and specified value of time parameter
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] value Value to be logged.
void Log_Time(const char *name, size_t length, const struct timespec *value);

/// Logs name and contents of the provided array.
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] array Array to be logged.
/// \param [in] size Size of the array to be logged.
void Log_Array(const char *name, size_t length, const void *array, size_t size);

/// Logs name and contents of the provided string.
/// \param [in] name Name of the value to be logged.
/// \param [in] length Length of the name.
/// \param [in] str String to be logged.
/// \param [in] size Size of the string to be logged.
void Log_String(const char *name, size_t length, const char *str, size_t size);

#ifdef __cplusplus
}
#endif

#endif // !CTESW_LOG_H
