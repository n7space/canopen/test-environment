/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2024-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST_FRAMEWORK_IO_H
#define TEST_FRAMEWORK_IO_H

/// \file TestFrameworkIO.h
/// \brief Test Framework I/O utilities for system calls and logging facilities.

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// \brief Writes multiple bytes to a standard test output.
/// \param bytes Bytes to write.
/// \param length Amount of bytes to write.
void TestFrameworkIO_writeBytes(
		const uint8_t *const bytes, const size_t length);

/// \brief Flushes standard test output.
void TestFrameworkIO_flush(void);

/// \brief Simulates writing to a file using Test Framework's I/O channel.
/// \details Should be used only for dumping coverage.
/// \param fd File descriptor.
/// \param data Data to write.
/// \param count Amount of bytes to be written.
/// \returns Amount of bytes written.
uint32_t TestFrameworkIO_writeToFile(const uint32_t fd, const char *const data,
		const uint32_t count);

/// \brief Writes null-terminated string via Test Framework's I/O channel.
/// \param str String to write.
/// \param length Expected string length.
void TestFrameworkIO_writeString(const char *const str, const size_t length);

/// \brief Writes final messages and summary of the program execution via
///        Test Framework's I/O channel.
/// \details Responsible for dumping coverage data.
/// \param status Exit status of the program.
void TestFrameworkIO_writeProgramFooter(const uint32_t status);

/// \brief Writes a byte value to standard output in ASCII hex format.
/// \details For example, 0x34 will be written as "34", without 0x prefix.
/// \param byte Byte to write
void TestFrameworkIO_writeByteValueAsHex(const uint8_t byte);

/// \brief Transmits raw memory over standard output in ASCII hex format.
/// \param data Memory to transmit.
/// \param length Amount of bytes to send.
void TestFrameworkIO_writeRawDataAsHex(
		const void *const data, const size_t length);

/// \brief Writes a 32-bit value to standard output in ASCII hex format.
/// \details For example, 0x12345678 will be written as "12345678", without
///          0x prefix.
/// \param value Value to write.
void TestFrameworkIO_write32BitValueAsHex(const uint32_t value);

/// \brief Writes a 64-bit value to standard output in ASCII hex format.
/// \details For example, 0x1234567890ABCDEF will be written as
///          "1234567890ABCDEF", without 0x prefix.
/// \param value Value to write.
void TestFrameworkIO_write64BitValueAsHex(const uint64_t value);

/// \brief Writes a 32-bit unsigned integer to standard output in ASCII
///        hex format.
/// \details For example, 0x12345678 will be written as "0x12345678".
/// \param value Value to write.
void TestFrameworkIO_writeUInt32AsHex(const uint32_t value);

/// \brief Writes a 64-bit unsigned integer to standard output in ASCII
///        hex format.
/// \details For example, 0x1234567890ABCDEF will be written as
///          "0x1234567890ABCDEF".
/// \param value Value to write.
void TestFrameworkIO_writeUInt64AsHex(const uint64_t value);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // TEST_FRAMEWORK_IO_H
