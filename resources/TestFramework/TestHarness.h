/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_TEST_HARNESS_H
#define CTESW_TEST_HARNESS_H

#if !LELY_NO_MALLOC || !LELY_NO_CANFD // sanity check
#error "Some LELY_* macros missing, check build configuration (HAVE_CONFIG_H?)"
#endif

#include <stdbool.h>

#include <lely/can/msg.h>
#include <lely/can/net.h>

#include "Log.h"

#define FINISH_TEST() FinishTest()
#define FAIL_TEST(s) FailTest((s), sizeof((s)))

#define REQUIRE(x) \
	do { \
		const int requireLocalResult = (x); \
		if (requireLocalResult != 0) { \
			LOG_UINT32("Result", requireLocalResult); \
			FAIL_TEST("Call failed:" #x); \
		} \
	} while (false)

#define REQUIRE_SIZE(x, expected) \
	do { \
		const size_t requireLocalResult = (x); \
		if (requireLocalResult != (expected)) { \
			LOG_UINT32("Result", requireLocalResult); \
			LOG_UINT32("Expected", (expected)); \
			FAIL_TEST("Call failed:" #x); \
		} \
	} while (false)

#ifdef __cplusplus
extern "C" {
#endif

/// Sets the total timeout for the test. By default tests have 90 seconds of
/// timeout.
///
/// \param [in] timeout timeout interval value (in seconds).
void SetTestTimeout(const struct timespec *timeout);

/// Returns the current test time value.
///
/// \param [out] tp a pointer to `timespec` object where result will be stored.
void GetCurrentTestTime(struct timespec *const tp);

/// Defines the device, creates and setups all necessary CANopen services.
///
/// To be implemented by the test.
///
/// \param [in,out] net a pointer to a CAN network object.
void TestSetup(can_net_t *const net);

/// Stops and destroys CANopen services.
///
/// To be implemented by the test.
void TestTeardown(void);

/// Callback for CAN message reception. It is called after the CANopen library
/// service indication functions.
///
/// To be implemented by the test.
///
/// \param [in] msg a pointer to the received CAN frame.
typedef void TestMessageReceivedCallback(const struct can_msg *msg);

/// Default callback for CAN message reception on bus A.
///
/// To be implemented by the test.
TestMessageReceivedCallback TestMessageReceived;

/// Default callback for CAN message reception on bus B.
/// Fails the test.
TestMessageReceivedCallback FailOnMessageReceived;

/// Callback for CAN message sending. It is called after message was sent by a
/// send handler used by the CANopen library, but before service indication
/// functions.
///
/// To be implemented by the test.
///
/// \param [in] msg a pointer to the sent CAN frame.
typedef void TestMessageSentCallback(const struct can_msg *msg);

/// Default callback for CAN message sending on bus A.
///
/// To be implemented by the test.
TestMessageSentCallback TestMessageSent;

/// Default callback for CAN message sending on bus B.
/// Fails the test.
TestMessageSentCallback FailOnMessageSent;

/// Sets sent/receive callbacks for CAN bus A.
///
/// \param [in] sent Sent callback to be used.
/// \param [in] received Receive callback to be used.
void SetCanCallbacksBusA(TestMessageSentCallback sent,
		TestMessageReceivedCallback received);

/// Sets sent/receive callbacks for CAN bus B.
///
/// \param [in] sent Sent callback to be used.
/// \param [in] received Receive callback to be used.
void SetCanCallbacksBusB(TestMessageSentCallback sent,
		TestMessageReceivedCallback received);

/// Callback for CAN messages ignored by `can_net_recv`.
/// Default implementation fails the test.
///
/// \param [in] msg Ignored message.
typedef void TestMessageIgnoredCallback(const struct can_msg *msg);

/// Sets callback for ignored messages on CAN bus A.
/// Passing NULL resets to default callback, which fails the tests.
///
/// \param [in] callback Callback to be used.
void SetIgnoredMessageCallbackBusA(TestMessageIgnoredCallback callback);

/// Sets callback for ignored messages on CAN bus B.
/// Passing NULL resets to default callback, which fails the tests.
///
/// \param [in] callback Callback to be used.
void SetIgnoredMessageCallbackBusB(TestMessageIgnoredCallback callback);

/// Progresses the test, analyses service status and updates the test state.
///
/// To be implemented by the test.
void TestStep(void);

/// Marks the test as FAILED and returns.
///
/// \param [in] message null-terminated message describing the cause of a failure.
/// \param [in] length maximum length of the message
void FailTest(const char *message, size_t length);

/// Marks the test as finished and returns. This does not necessarily mean that
/// the test succeeded.
void FinishTest(void);

#ifdef __cplusplus
}
#endif

#endif // !CTESW_TEST_HARNESS_H
