CTESW (Test Environment) C API
==============================

TestFramework is a module of CTESW responsible for providing API for creating test application.

Test applications defined using provided API should follow the example shown below:

```c
void
TestSetup(can_net_t *const net)
{
  CANSW_enable_rpdo_service(net);  // not real CANSW interface, just example
}

void
TestTeardown(void)
{
  CANSW_disable_rpdo_service(net);
}

void
TestMessageReceived(const struct can_msg *msg)
{
  // optional msg validation
}

void
TestMessageSent(const int ret, const struct can_msg *const msg)
{
  // optional msg validation
  LOG("Message sent");
}

void
TestStep(void)
{
  if (CANSW_pdo_received())
  {
    const int32_t value = CANSW_obj_value(TEST_OBJID);
    if (value == TEST_OBJVALUE))
      FINISH_TEST();
    else {
      LOG_UINT32_EXPECTED(“obj value”, TEST_OBJVALUE, value);
      FAIL_TEST("Incorrect obj value");
    }
  }
}
```

For details of available methods - see TestHarness.h
