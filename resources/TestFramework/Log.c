/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Log.h"

#include <assert.h>

#include <lely/compat/time.h>

#include "HAL/ClockInterface.h"
#include "TestFrameworkIO.h"

#define OUT_STR(s) TestFrameworkIO_writeString((s), sizeof((s)))

static void
writeTime(const struct timespec *const tp)
{
	TestFrameworkIO_writeUInt64AsHex((uint64_t)tp->tv_sec);
	OUT_STR(" s ");
	TestFrameworkIO_writeUInt32AsHex((uint32_t)tp->tv_nsec);
	OUT_STR(" ns");
}

static void
LogBegin(void)
{
	struct timespec tp = { 0 };
	ClockGetTime(&tp);
	OUT_STR("[");
	writeTime(&tp);
	OUT_STR("] - ");
}

static void
LogEnd(void)
{
	OUT_STR("\n");
	TestFrameworkIO_flush();
}

void
Log_Message(const char *const str, const size_t length)
{
	LogBegin();
	TestFrameworkIO_writeString(str, length);
	LogEnd();
}

void
Log_Raw(const char *const str, const size_t length)
{
	TestFrameworkIO_writeString(str, length);
	LogEnd();
}

void
Log_UInt64(const char *const name, const size_t length, const uint64_t value)
{
	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": ");
	TestFrameworkIO_writeUInt64AsHex(value);
	LogEnd();
}

void
Log_UInt32(const char *const name, const size_t length, const uint32_t value)
{
	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": ");
	TestFrameworkIO_writeUInt32AsHex(value);
	LogEnd();
}

void
Log_Time(const char *const name, const size_t length,
		const struct timespec *const value)
{
	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": ");
	writeTime(value);
	LogEnd();
}

void
Log_UInt32Expected(const char *const name, const size_t length,
		const uint32_t expected, const uint32_t current)
{
	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": expected ");
	TestFrameworkIO_writeUInt32AsHex(expected);
	OUT_STR(" was ");
	TestFrameworkIO_writeUInt32AsHex(current);
	LogEnd();
}

void
Log_UInt64Expected(const char *const name, const size_t length,
		const uint64_t expected, const uint64_t current)
{
	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": expected ");
	TestFrameworkIO_writeUInt64AsHex(expected);
	OUT_STR(" was ");
	TestFrameworkIO_writeUInt64AsHex(current);
	LogEnd();
}

void
Log_Array(const char *name, const size_t length, const void *const array,
		const size_t size)
{
	assert(array);

	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": [ ");
	const uint8_t *ptr = (const uint8_t *)array;
	for (size_t i = 0; i < size; ++i, ++ptr) {
		TestFrameworkIO_writeByteValueAsHex(*ptr);
		OUT_STR(" ");
	}
	OUT_STR("]");
	LogEnd();
}

void
Log_String(const char *name, size_t length, const char *str, size_t size)
{
	LogBegin();
	TestFrameworkIO_writeString(name, length);
	OUT_STR(": \"");
	TestFrameworkIO_writeString(str, size);
	OUT_STR("\"");
	LogEnd();
}
