/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_ALLOCATOR_H
#define CTESW_ALLOCATOR_H

#include <lely/util/memory.h>

/// Initializes the default allocator for the CANopen library.
///
/// \return allocator interface.
///
/// \see can_net_create()
alloc_t *Allocator_init(void);

/// Returns total memory allocated using the default allocator.
/// \return total allocated memory.
size_t Allocator_usedMemory(void);

#endif // CTESW_ALLOCATOR_H
