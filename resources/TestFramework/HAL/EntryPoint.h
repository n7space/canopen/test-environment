/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_HAL_TESTMAIN_H
#define CTESW_HAL_TESTMAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/// Platform independent entry point procedure.
///
/// To be implemented by the test applications.
///
/// \retval 0 On success.
/// \retval !=0 When any test failed.
int EntryPoint_main(void);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // CTESW_HAL_TESTMAIN_H
