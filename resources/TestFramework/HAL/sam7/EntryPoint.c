/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "../EntryPoint.h"
#include "../OutputMarkers.h"
#include "../PlatformIO.h"

#include <TestFramework/Log.h>

#include <lely/compat/stdlib.h>

#include <n7s/bsp/Fpu/Fpu.h>
#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/ChipConfig.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Wdt/Wdt.h>

#ifdef N7S_TARGET_SAMRH71F20
#include <n7s/bsp/Matrix/Matrix.h>
#endif

#if CAN_TEST_ENV_HWTB
void exit(int r) __attribute__((noreturn));
#endif

extern const uint32_t _ram_start; // NOLINT
extern const uint32_t _ram_end; // NOLINT
extern const uint32_t RTT_NOCACHE_SIZE;

#define EXPECTED_RTT_BUFFER_SIZE 1024u
#define RTT_MPU_REGION_SIZE 9u

#ifdef N7S_TARGET_SAMRH71F20
// Workaround for HW bug related to Mcan memory access on SAMRH71F20
// cppcheck-suppress misra-c2012-8.2
static void
performMatrixWorkaround(void)
{
	Matrix matrix = { 0 };
	Matrix_init(&matrix, Matrix_getDeviceBaseAddress());

	Matrix_SlaveRegionProtectionConfig slaveConfig = { 0 };
	for (int32_t i = 0; i < (int32_t)Matrix_ProtectedRegionId_Count; ++i) {
		Matrix_getSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_Flexram0,
				(Matrix_ProtectedRegionId)i, &slaveConfig);
		slaveConfig.isPrivilegedRegionUserReadAllowed = true;
		slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
		slaveConfig.regionSplitOffset = Matrix_Size_128MB;
		slaveConfig.regionOrder =
				Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
		Matrix_setSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_Flexram0,
				(Matrix_ProtectedRegionId)i, &slaveConfig);

		Matrix_getSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_Flexram1,
				(Matrix_ProtectedRegionId)i, &slaveConfig);
		slaveConfig.isPrivilegedRegionUserReadAllowed = true;
		slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
		slaveConfig.regionSplitOffset = Matrix_Size_128MB;
		slaveConfig.regionOrder =
				Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
		Matrix_setSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_Flexram1,
				(Matrix_ProtectedRegionId)i, &slaveConfig);

		Matrix_getSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_Flexram2,
				(Matrix_ProtectedRegionId)i, &slaveConfig);
		slaveConfig.isPrivilegedRegionUserReadAllowed = true;
		slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
		slaveConfig.regionSplitOffset = Matrix_Size_128MB;
		slaveConfig.regionOrder =
				Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
		Matrix_setSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_Flexram2,
				(Matrix_ProtectedRegionId)i, &slaveConfig);

		Matrix_getSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_AhbSlave,
				(Matrix_ProtectedRegionId)i, &slaveConfig);
		slaveConfig.isPrivilegedRegionUserReadAllowed = true;
		slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
		slaveConfig.regionSplitOffset = Matrix_Size_128MB;
		slaveConfig.regionOrder =
				Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
		Matrix_setSlaveRegionProtectionConfig(&matrix,
				Matrix_Slave_AhbSlave,
				(Matrix_ProtectedRegionId)i, &slaveConfig);
	}
}
#endif

static void
configureMPU(void)
{
	Mpu mpu;
	Mpu_init(&mpu);

	const uint32_t ram_start =
			(uint32_t)(&_ram_start); // cppcheck-suppress [misra-c2012-11.4]
	const uint32_t ram_end =
			(uint32_t)(&_ram_end); // cppcheck-suppress [misra-c2012-11.4]
	const uint32_t rtt_size =
			(uint32_t)(&RTT_NOCACHE_SIZE); // cppcheck-suppress [misra-c2012-11.4]

	assert(((ram_end - ram_start) == N7S_HWTB_EXPECTED_RAM_SIZE)
			&& "RAM memory size is not as expected, MPU config below is probably invalid");
	(void)ram_start; // unused in coverage
			//
	assert((rtt_size == EXPECTED_RTT_BUFFER_SIZE)
			&& "Size of non-cacheable block changed, number 10 below needs to be changed as well");

	Mpu_Config mpuConf = { .isEnabled = true,
		.isDefaultMemoryMapEnabled = true,
		.isMpuEnabledInHandlers = false };
	Mpu_setConfig(&mpu, &mpuConf);

	// Mark RTT section (.rtt_nocache) as non-cacheable memory region with strong ordering
	Mpu_RegionConfig mpuRegionConf = {
		.address = ram_end - rtt_size,
		.isEnabled = true,
		.size = RTT_MPU_REGION_SIZE,
		.subregionDisableMask = 0x00,
		.isShareable = true,
		.isExecutable = true,
		.memoryType = Mpu_RegionMemoryType_StronglyOrdered,
		.innerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.outerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.privilegedAccess = Mpu_RegionAccess_ReadWrite,
		.unprivilegedAccess = Mpu_RegionAccess_ReadWrite,
	};
	Mpu_setRegionConfig(&mpu, 0, &mpuRegionConf);
}

static void
configureFpu(void)
{
	Fpu fpu;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
}

static void
disableWatchdog(void)
{
	Wdt wdt;
	Wdt_Config config;
	Wdt_init(&wdt);
	config.isDisabled = true;
	config.isFaultInterruptEnabled = false;
	config.isResetEnabled = false;
	config.counterValue = 0xFFF;
	config.deltaValue = 0xFFF;
	config.isHaltedOnDebug = false;
	config.isHaltedOnIdle = false;
#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	config.doesFaultActivateProcessorReset = true;
#endif
	Wdt_setConfig(&wdt, &config);
}

static void
configureClock(void)
{
	const Pmc_Config config = Pmc_ChipConfig_getConfig();
	Pmc pmc = { 0 };
	uint32_t errCode = 0;

	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	const bool isSettingConfigSuccessful = Pmc_setConfig(&pmc, &config,
			N7S_HWTB_PMC_CONFIGURATION_TIMEOUT, &errCode);
	assert(isSettingConfigSuccessful);
	assert(errCode == 0);
#ifdef N7S_ENABLE_COVERAGE
	// Asserts are disabled, hence this variable is unused and emits an error.
	(void)isSettingConfigSuccessful;
#endif
}

int
main(void)
{
	disableWatchdog();
	configureFpu();
	configureClock();
	configureMPU();
#ifdef N7S_TARGET_SAMRH71F20
	performMatrixWorkaround();
#endif

	// Shutdown is performed at _exit, see Syscalls.c
	PlatformIO_startup();

	LOG_RAW(TEST_OUTPUT_BEGIN_MARKER);
	const int r = EntryPoint_main();
	LOG_RAW(TEST_OUTPUT_END_MARKER);

	exit(r);

	asm volatile("BKPT #0");
	for (;;)
		;
}
