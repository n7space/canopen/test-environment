/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "SystickInterface.h"

#include <assert.h>
#include <stdatomic.h>
#include <stdint.h>

static Systick systick;
static struct timespec currentTime;
static uint32_t systickPeriodNs;
static uint32_t systickPeriodTicks;
static atomic_long nsSinceLastGetTime;

#define NS_IN_SEC 1000000000

void SysTick_Handler(void); // weak link symbol in interrupt table
void
SysTick_Handler(void)
{
	atomic_fetch_add(&nsSinceLastGetTime, systickPeriodNs);
}

static void
normalizeTime(struct timespec *const tp)
{
	while (tp->tv_nsec >= NS_IN_SEC) {
		tp->tv_nsec -= NS_IN_SEC;
		++(tp->tv_sec);
	}
}

static void
updateCurrentTime(void)
{
	currentTime.tv_nsec += (long)atomic_exchange(&nsSinceLastGetTime, 0uL);
	normalizeTime(&currentTime);
}

void
SystickInterface_startup(const uint32_t intervalNs)
{
	systickPeriodNs = intervalNs;

	currentTime.tv_sec = 0;
	currentTime.tv_nsec = 0;

	atomic_init(&nsSinceLastGetTime, 0uL);

	Systick_init(&systick, Systick_getDeviceRegisterStartAddress());

	Systick_Config config = { 0 };
	config.clockSource = Systick_ClockSource_ProcessorClock;
	config.isInterruptEnabled = true;
	config.isEnabled = true;

	const uint64_t systickTicks =
			(N7S_BSP_CORE_CLOCK * (uint64_t)intervalNs)
			/ (uint64_t)NS_IN_SEC;
	assert((systickTicks != 0uLL)
			&& (systickTicks <= ((uint64_t)UINT32_MAX + 1u)));

	config.reloadValue = (uint32_t)(systickTicks - 1uLL);
	systickPeriodTicks = config.reloadValue;
	Systick_setConfig(&systick, &config);
}

void
SystickInterface_getTime(struct timespec *const tp)
{
	updateCurrentTime();

	tp->tv_nsec = currentTime.tv_nsec;
	tp->tv_sec = currentTime.tv_sec;
}
