/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_HAL_ARM_MCAN_INTERFACE_H
#define CTESW_HAL_ARM_MCAN_INTERFACE_H

#include <n7s/bsp/Mcan/Mcan.h>
#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>

#include <lely/can/msg.h>

#if N7S_HWTB_MCAN_MSGRAM_ALIGN < N7S_HWTB_MCAN_MSGRAM_SIZE
#error "Increase MSGRAM alignment"
#endif

#define MSGRAM_STDID_FILTER_OFFSET 0

#define MSGRAM_EXTID_FILTER_OFFSET \
	(MSGRAM_STDID_FILTER_OFFSET + N7S_HWTB_MCAN_MSGRAM_STDID_FILTER_SIZE)

#define MSGRAM_RXFIFO0_OFFSET \
	(MSGRAM_EXTID_FILTER_OFFSET + N7S_HWTB_MCAN_MSGRAM_EXTID_FILTER_SIZE)

#define MSGRAM_RXFIFO1_OFFSET \
	(MSGRAM_RXFIFO0_OFFSET + N7S_HWTB_MCAN_MSGRAM_RXFIFO0_SIZE)

#define MSGRAM_RXBUFFER_OFFSET \
	(MSGRAM_RXFIFO1_OFFSET + N7S_HWTB_MCAN_MSGRAM_RXFIFO1_SIZE)

#define MSGRAM_TXEVENTFIFO_OFFSET \
	(MSGRAM_RXBUFFER_OFFSET + N7S_HWTB_MCAN_MSGRAM_RXBUFFER_SIZE)

#define MSGRAM_TXBUFFER_OFFSET \
	(MSGRAM_TXEVENTFIFO_OFFSET + N7S_HWTB_MCAN_MSGRAM_TXEVENTFIFO_SIZE)

#if (MSGRAM_TXBUFFER_OFFSET + N7S_HWTB_MCAN_MSGRAM_TXBUFFER_SIZE) \
		> N7S_HWTB_MCAN_MSGRAM_SIZE
#error "Invalid layout of MSGRAM; last field leaks out of the buffer"
#endif

/// \brief Configuration struct defining all necessary identifiers to init the
/// hardware
typedef struct {
	Pmc_PeripheralId mcanPmcId; ///< MCAN PMC ID.
	Mcan_Id mcanId; ///< MCAN interface ID.
	Nvic_Irq mcanIrq; ///< MCAN IRQ number.

	Pio_Port pioPort; ///< PIO port.
	Pmc_PeripheralId pioPmcId; ///< PIO PMC ID.
	Pio_Control pioPeripheral; ///< PIO I/O assignment.
	uint32_t pinMask; ///< Pin mask.

#ifdef N7S_HWTB_MCAN_TRANSCIEVER_HAS_ENABLE_PIN
	Pmc_PeripheralId transcieverPioPmcId; ///< CAN transciever PIO PMC ID.
	Pio_Port transcieverPioPort; ///< CAN transciever PIO port.
	uint32_t transcieverPinMask; ///< CAN transciever Pin mask.
	bool transcieverEnabledPinState; ///< Pin state required to enable the transciever.
#endif
} McanInterface_Config;

/// \brief Struct containing all necessry data for the CANopen library
/// interface.
typedef struct __attribute__((
		aligned(N7S_HWTB_MCAN_MSGRAM_ALIGN * sizeof(uint32_t)))) {
	/// CAN message buffer (all offsets must start with the same 0xXXXX____).
	uint32_t msgRam[N7S_HWTB_MCAN_MSGRAM_SIZE];
	Pio pio; ///< Structure representing MCAN PIO port.
#ifdef N7S_HWTB_MCAN_TRANSCIEVER_HAS_ENABLE_PIN
	Pio transcieverPio; ///< Structure representing MCAN transciever's PIO port.
#endif
	Pmc pmc; ///< Structure representing PMC.
	Mcan mcan; ///< Structure representing MCAN.
	McanInterface_Config config; ///< MCAN config.
} McanInterface;

/// \brief Configures and enables peripherals, starts data reception.
/// \param self [in,out] Pointer to the class representation.
/// \param config [in] Configuration of the MCAN device.
void McanInterface_init(McanInterface *const self,
		const McanInterface_Config *const config);

/// \brief Disables peripherals and interrupts.
/// \param self [in,out] Pointer to the class representation.
void McanInterface_shutdown(McanInterface *const self);

/// \brief Checks whether new message arrived and stores it in msg struct.
/// \param self [in,out] Pointer to the class representation.
/// \param msg [out] Pointer to store received message.
/// \retval true When message was received.
/// \retval false When no message was received.
bool McanInterface_receive(
		McanInterface *const self, struct can_msg *const msg);

/// \brief Send handler to be registered with can_net_set_send_func with a data
/// pointer to McanInterface instance.
/// \param msg [in] Message to send.
/// \param self [in,out] Generic pointer to be used by handler.
/// \retval 0 on success.
int McanInterface_sendHandler(const struct can_msg *const msg, void *self);

/// \brief Returns default configuration needed to initialize MCAN0.
/// \returns Configuration of the MCAN device.
const McanInterface_Config *McanInterface_getMcan0DefaultConfig(void);

/// \brief Returns default configuration needed to initialize MCAN1.
/// \returns Configuration of the MCAN device.
const McanInterface_Config *McanInterface_getMcan1DefaultConfig(void);

#endif // CTESW_HAL_ARM_MCAN_INTERFACE_H
