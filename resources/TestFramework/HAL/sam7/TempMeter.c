/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../TempMeter.h"

#if defined(N7S_TARGET_SAMV71Q21)

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>

#include <n7s/bsp/Pmc/Pmc.h>

/// \brief Structure representing AFEC registers.
typedef struct {
	volatile uint32_t cr; ///< Control Register.
	volatile uint32_t mr; ///< Mode Regiseter.
	volatile uint32_t emr; ///< Extended Mode Register.
	volatile uint32_t seq1r; ///< Channel Sequence 1 Register.
	volatile uint32_t seq2r; ///< Channel Sequence 2 Register.
	volatile uint32_t cher; ///< Channel Enable Regiseter.
	volatile uint32_t chdr; ///< Channel Disable Register.
	volatile uint32_t chsr; ///< Channel Status Register.
	volatile uint32_t lcdr; ///< Last Converted Data Register.
	volatile uint32_t ier; ///< Interrupt Enable Register.
	volatile uint32_t idr; ///< Interupt Disable Register.
	volatile uint32_t imr; ///< Interrupt Mask Register.
	volatile uint32_t isr; ///< Interrupt Mask Register.
	volatile uint32_t reserved1[4]; ///< Reserved.
	volatile uint32_t reserved2[2]; ///< Reserved.
	volatile uint32_t over; ///< Overrun Status Register.
	volatile uint32_t cwr; ///< Compare Window Register.
	volatile uint32_t cgr; ///< Channel Gain Register.
	volatile uint32_t reserved3[2]; ///< Reserved.
	volatile uint32_t diffr; ///< Channel Differential Register.
	volatile uint32_t cselr; ///< Channel Selection Register.
	volatile uint32_t cdr; ///< Channel Data Register.
	volatile uint32_t cocr; ///< Channel Offset Compensation Register.
	volatile uint32_t tempmr; ///< Temperature Sensor Mode Register.
	volatile uint32_t tempcwr; ///< Temperature Compare Window Register.
	volatile uint32_t reserved4[7]; ///< Reserved.
	volatile uint32_t acr; ///< Analog Control Register.
	volatile uint32_t reserved5[2]; ///< Reserved.
	volatile uint32_t shmr; ///< Sample & Hold Mode Register.
	volatile uint32_t reserved6[11]; ///< Reserved.
	volatile uint32_t cosr; ///< Correction Select Register.
	volatile uint32_t cvr; ///< Correction Values Register.
	volatile uint32_t cecr; ///< Channel Error Correction Register.
	volatile uint32_t reserved7[2]; ///< Reserved.
	volatile uint32_t wpmr; ///< Write Protection Mode Register.
	volatile uint32_t wpsr; ///< Write Protection Status Register.
} Afec_Registers;

/// \brief Enumeration listing possible Startup Times.
typedef enum {
	Afec_StartupTime_Sut0 = 0, ///< 0 periods of AFE clock.
	Afec_StartupTime_Sut8 = 1, ///< 8 periods of AFE clock.
	Afec_StartupTime_Sut16 = 2, ///< 16 periods of AFE clock.
	Afec_StartupTime_Sut24 = 3, ///< 24 periods of AFE clock.
	Afec_StartupTime_Sut64 = 4, ///< 64 periods of AFE clock.
	Afec_StartupTime_Sut80 = 5, ///< 80 periods of AFE clock.
	Afec_StartupTime_Sut96 = 6, ///< 96 periods of AFE clock.
	Afec_StartupTime_Sut112 = 7, ///< 112 periods of AFE clock.
	Afec_StartupTime_Sut512 = 8, ///< 512 periods of AFE clock.
	Afec_StartupTime_Sut576 = 9, ///< 576 periods of AFE clock.
	Afec_StartupTime_Sut640 = 10, ///< 640 periods of AFE clock.
	Afec_StartupTime_Sut704 = 11, ///< 704 periods of AFE clock.
	Afec_StartupTime_Sut768 = 12, ///< 768 periods of AFE clock.
	Afec_StartupTime_Sut832 = 13, ///< 832 periods of AFE clock.
	Afec_StartupTime_Sut896 = 14, ///< 896 periods of AFE clock.
	Afec_StartupTime_Sut960 = 15, ///< 960 periods of AFE clock.
} Afec_StartupTime;

/// \brief Software Reset register mask.
#define AFEC_CR_SWRST_MASK 0x00000001U
/// \brief Start Conversion register mask.
#define AFEC_CR_START_MASK 0x00000002U

/// \brief Free Run Mode register mask.
#define AFEC_MR_FREERUN_MASK 0x00000080U

/// \brief Startup Time register offset.
#define AFEC_MR_STARTUP_OFFSET 16U
/// \brief Startup Time register mask.
#define AFEC_MR_STARTUP_MASK 0x000F0000U

/// \brief One register mask.
#define AFEC_MR_ONE_MASK 0x00800000U

/// \brief Tracking Time register offset.
#define AFEC_MR_TRACKTIM_OFFSET 24U
/// \brief Tracking Time register mask.
#define AFEC_MR_TRACKTIM_MASK 0x0F000000U

/// \brief Prescaler Rate Selection register offset.
#define AFEC_MR_PRESCAL_OFFSET 8U
/// \brief Prescaler Rate Selection register mask.
#define AFEC_MR_PRESCAL_MASK 0x0000FF00U

/// \brief Transfer Period register offset.
#define AFEC_MR_TRANSFER_OFFSET 28U
/// \brief Transfer Period register mask.
#define AFEC_MR_TRANSFER_MASK 0x30000000U

/// \brief Channel Gain offset (per channel)
#define AFEC_CGR_GAINX_OFFSET 0x02U
/// \brief Channel Gain mask (per channel)
#define AFEC_CGR_GAINX_MASK 0x03U

/// \brief Converted Data register mask.
#define AFEC_CDR_DATA_MASK 0x0000FFFFU

/// \brief PGA0 Enable register mask.
#define AFEC_ACR_PGA0EN_MASK 0x00000004U
/// \brief PGA1 Enable register mask.
#define AFEC_ACR_PGA1EN_MASK 0x00000008U

/// \brief Sample & Hold Unit Correction Select register mask.
#define AFEC_COSR_CSEL_MASK 0x00000001U

/// \brief TAG of the AFEC_LDCR register mask.
#define AFEC_EMR_TAG_MASK 0x01000000U

/// \brief Single Trigger Mode register mask.
#define AFEC_EMR_STM_MASK 0x02000000U

/// \brief AFE Bias Current Control register offset.
#define AFEC_ACR_IBCTL_OFFSET 8U
/// \brief AFE Bias Current Control register mask.
#define AFEC_ACR_IBCTL_MASK 0x00000300U

/// \brief Data Ready Interrupt Enable register mask.
#define AFEC_IER_DRDY_MASK 0x01000000U

/// \brief Data Ready Interrupt register mask.
#define AFEC_ISR_DRDY_MASK 0x01000000U

/// \brief Internal reference voltage
#define VOLTAGE_REF 3300

/// \brief 12-bit converstion max value
#define MAX_VALUE 4095

/// \brief Shift value of ADC readings.
#define ADC_OFFSET 0x200u

/// \brief ACD tempperature channel
#define AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL 11u

/// \brief Cycles to wait for first ADC reading to show up.
#define ADC_TIMEOUT 20000000u

static Afec_Registers *regs = (Afec_Registers *)0x4003C000u;

void
TempMeter_startup(void)
{
	Pmc pmc = { 0 };
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Afec0);
	regs->cr = AFEC_CR_SWRST_MASK;

	regs->mr = AFEC_MR_FREERUN_MASK | AFEC_MR_ONE_MASK
			| ((Afec_StartupTime_Sut64 << AFEC_MR_STARTUP_OFFSET)
					& AFEC_MR_STARTUP_MASK)
			| ((63u << AFEC_MR_PRESCAL_OFFSET)
					& AFEC_MR_PRESCAL_MASK)
			| ((2 << AFEC_MR_TRACKTIM_OFFSET)
					& AFEC_MR_TRACKTIM_MASK)
			| ((1 << AFEC_MR_TRANSFER_OFFSET)
					& AFEC_MR_TRANSFER_MASK);
	regs->emr = AFEC_EMR_TAG_MASK;
	regs->acr = ((1 << AFEC_ACR_IBCTL_OFFSET) & AFEC_ACR_IBCTL_MASK)
			| AFEC_ACR_PGA0EN_MASK | AFEC_ACR_PGA1EN_MASK;
	regs->cosr = 0u;
	regs->cvr = 0u;
	regs->cosr = AFEC_COSR_CSEL_MASK;
	regs->cgr &= ~(AFEC_CGR_GAINX_MASK << (AFEC_CGR_GAINX_OFFSET
				       * AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL));

	regs->ier = AFEC_IER_DRDY_MASK;
	regs->seq1r = 0u;
	regs->seq2r = 0u;
	regs->diffr = 0u;
	regs->cselr = AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL;
	regs->cocr = ADC_OFFSET;
	regs->shmr &= ~(1u << AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL);
	regs->cecr &= ~(1u << AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL);
	regs->cher |= 1u << AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL;
	regs->cr = AFEC_CR_START_MASK;

	for (uint32_t timeout = ADC_TIMEOUT; timeout > 0; --timeout) {
		if ((regs->isr & AFEC_ISR_DRDY_MASK) != 0) {
			return;
		}
	}
	assert(false && "Temperature reading did not show in specified time");
}

int32_t
TempMeter_getTemp(void)
{
	regs->cselr = AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL;
	const int32_t reading = (int32_t)(regs->cdr & AFEC_CDR_DATA_MASK);

	// According to datasheet: the output voltage VT = 0.72V at 27C
	// and the temperature slope dVT/dT = 2.33 mV/C

	return ((((VOLTAGE_REF * reading) / MAX_VALUE) - 720) * 100 / 233) + 27;
}

void
TempMeter_shutdown(void)
{
	Pmc pmc = { 0 };
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	regs->acr &= ~(AFEC_ACR_PGA0EN_MASK | AFEC_ACR_PGA1EN_MASK);
	regs->chdr |= 1 << AFEC_INTERNAL_TEMPERATURE_SENSOR_CHANNEL;
	Pmc_disablePeripheralClk(&pmc, Pmc_PeripheralId_Afec0);
}
#else
void
TempMeter_startup(void)
{
	// No temperature sensor on selected platform.
}

void
TempMeter_shutdown(void)
{
	// No temperature sensor on selected platform.
}

int32_t
TempMeter_getTemp(void)
{
	// No temperature sensor on selected platform.
	return 0x42;
}
#endif
