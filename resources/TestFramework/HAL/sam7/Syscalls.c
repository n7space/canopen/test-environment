/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>
#include <stdint.h>

#include <sys/stat.h>
#include <sys/time.h>

#include <TestFramework/HAL/PlatformIO.h>
#include <TestFramework/TestFrameworkIO.h>

extern uintptr_t _eheap; // NOLINT
extern uintptr_t _sheap; // NOLINT

// cppcheck-suppress [misra-c2012-8.4]
int _fstat(const int file, struct stat *const st); // NOLINT

int
_fstat(const int file, struct stat *const st)
{
	// cppcheck-suppress misra-c2012-11.7
	(void)file;
	// cppcheck-suppress misra-c2012-11.7
	(void)st;
	return -1;
}

// cppcheck-suppress [misra-c2012-8.4]
void *_sbrk(const intptr_t incr); // NOLINT

void *
_sbrk(const intptr_t incr)
{
	static uint8_t *heap = (uint8_t *)&_sheap;

	if ((heap + incr)
			>= (uint8_t *)&_eheap) // cppcheck-suppress [comparePointers,misra-c2012-18.4]
		return (void *)-1;

	uint8_t *const prev_heap = heap;
	heap += incr; // cppcheck-suppress [misra-c2012-10.3,misra-c2012-18.4]

	return prev_heap;
}

// cppcheck-suppress [misra-c2012-8.4]
void _kill(const int pid, const int sig); // NOLINT

void
_kill(const int pid, const int sig)
{
	// cppcheck-suppress misra-c2012-11.7
	(void)pid;
	// cppcheck-suppress misra-c2012-11.7
	(void)sig;
}

// cppcheck-suppress [misra-c2012-8.4]
int _getpid(void); // NOLINT

int
_getpid(void)
{
	return -1;
}

// cppcheck-suppress [misra-c2012-8.4]
int _write(const int fd, const void *const buffer,
		const unsigned int count); // NOLINT

int
_write(const int fd, const void *const buffer, const unsigned int count)
{
	return (int)TestFrameworkIO_writeToFile((uint32_t)fd, buffer, count);
}

// cppcheck-suppress [misra-c2012-8.4]
int _open(const char *const filename, const int oflag); // NOLINT

int
_open(const char *const filename, const int oflag)
{
	(void)filename;
	(void)oflag;
	return -1;
}

// cppcheck-suppress [misra-c2012-8.4]
int _isatty(const int file); // NOLINT

int
_isatty(const int file)
{
	// cppcheck-suppress misra-c2012-11.7
	(void)file;
	return 0;
}

// cppcheck-suppress [misra-c2012-8.4]
int _close(const int file); // NOLINT

int
_close(const int file)
{
	// cppcheck-suppress misra-c2012-11.7
	(void)file;
	return 0;
}

// cppcheck-suppress [misra-c2012-8.4]
int _lseek(const int file, const int ptr, const int dir); // NOLINT

int
_lseek(const int file, const int ptr, const int dir)
{
	// cppcheck-suppress misra-c2012-11.7
	(void)file;
	// cppcheck-suppress misra-c2012-11.7
	(void)ptr;
	// cppcheck-suppress misra-c2012-11.7
	(void)dir;
	return 0;
}

// cppcheck-suppress [misra-c2012-8.4]
int _read(const int fd, void *const buffer, const unsigned int count); // NOLINT

int
_read(const int fd, void *const buffer, const unsigned int count)
{
	// cppcheck-suppress misra-c2012-11.7
	(void)fd;
	// cppcheck-suppress misra-c2012-11.7
	(void)buffer;
	// cppcheck-suppress misra-c2012-11.7
	(void)count;
	return -1;
}

// cppcheck-suppress [misra-c2012-8.4]
void _exit(const int status); // NOLINT

void
_exit(const int status)
{
	TestFrameworkIO_writeProgramFooter((uint32_t)status);
	PlatformIO_shutdown();

	asm volatile("BKPT #0" ::: "memory");
	for (;;)
		;
}

// cppcheck-suppress [misra-c2012-8.4]
int _gettimeofday(struct timeval *tv, void *tzvp); // NOLINT

int
_gettimeofday(struct timeval *tv, void *tzvp)
{
	(void)tzvp;
	tv->tv_sec = 0;
	tv->tv_usec = 0;
	return 0;
}
