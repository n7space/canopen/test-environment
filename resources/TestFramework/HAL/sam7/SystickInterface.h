/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_HAL_ARM_SYSTICK_INTERFACE_H
#define CTESW_HAL_ARM_SYSTICK_INTERFACE_H

#include <lely/compat/time.h>

#include <n7s/bsp/Systick/Systick.h>

/// \brief Starts counter with specified interval in ns.
/// \param [in] intervalNs Interval in nanoseconds.
void SystickInterface_startup(const uint32_t intervalNs);

/// \brief Stores time in tp structure. Time quant is the same as tickInterval.
/// \param [out] tp Structure to store current coarse time.
void SystickInterface_getTime(struct timespec *const tp);

#endif // CTESW_HAL_ARM_SYSTICK_INTERFACE_H
