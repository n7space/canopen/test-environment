/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "McanInterface.h"

#include <assert.h>
#include <stdbool.h>

#include <lely/compat/string.h>
#include <lely/util/error.h>

static void
configureMcanPmc(McanInterface *const self)
{
#if defined(N7S_TARGET_SAMV71Q21)
	Pmc_enablePeripheralClk(&self->pmc, self->config.mcanPmcId);
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)

	const Pmc_PeripheralClkConfig mcanClockConfig = {
		.isPeripheralClkEnabled = true,
		.isGclkEnabled = true,
		.gclkSrc = N7S_HWTB_MCAN_GCLK_SOURCE,
		.gclkPresc = N7S_HWTB_MCAN_GCLK_PRESCALER,
	};
	Pmc_setPeripheralClkConfig(
			&self->pmc, self->config.mcanPmcId, &mcanClockConfig);
#endif
}

static void
deconfigureMcanPmc(McanInterface *const self)
{
#if defined(N7S_TARGET_SAMV71Q21)
	Pmc_disablePeripheralClk(&self->pmc, self->config.mcanPmcId);
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	Pmc_disablePeripheralClk(&self->pmc, self->config.mcanPmcId);
	Pmc_disableGenericClk(&self->pmc, self->config.mcanPmcId);
#endif
}

static void
configureMcan(McanInterface *const self)
{
	configureMcanPmc(self);
	Mcan_init(&self->mcan, Mcan_getDeviceRegisters(self->config.mcanId));

	Mcan_Config config = { 0 };

	config.msgRamBaseAddress = self->msgRam;
	config.mode = Mcan_Mode_Normal;
	config.isFdEnabled = false;
	config.nominalBitTiming.bitRatePrescaler =
			N7S_HWTB_MCAN_NOMINAL_BIT_TIMING_BIT_RATE_PRESCALER;
	config.nominalBitTiming.synchronizationJump =
			N7S_HWTB_MCAN_NOMINAL_BIT_TIMING_SYNC_JUMP;
	config.nominalBitTiming.timeSegmentAfterSamplePoint =
			N7S_HWTB_MCAN_NOMINAL_BIT_TIMING_TIME_SEG_AFTER_SAMPLE;
	config.nominalBitTiming.timeSegmentBeforeSamplePoint =
			N7S_HWTB_MCAN_NOMINAL_BIT_TIMING_TIME_SEG_BEFORE_SAMPLE;
	config.dataBitTiming.bitRatePrescaler =
			N7S_HWTB_MCAN_DATA_BIT_TIMING_BIT_RATE_PRESCALER;
	config.dataBitTiming.synchronizationJump =
			N7S_HWTB_MCAN_DATA_BIT_TIMING_SYNC_JUMP;
	config.dataBitTiming.timeSegmentAfterSamplePoint =
			N7S_HWTB_MCAN_DATA_BIT_TIMING_TIME_SEG_AFTER_SAMPLE;
	config.dataBitTiming.timeSegmentBeforeSamplePoint =
			N7S_HWTB_MCAN_DATA_BIT_TIMING_TIME_SEG_BEFORE_SAMPLE;
	config.transmitterDelayCompensation.isEnabled = false;
	config.timestampClk = Mcan_TimestampClk_Internal;
	config.timestampTimeoutPrescaler = 14;
	config.timeout.isEnabled = false;
	config.standardIdFilter.isIdRejected = false;
	config.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo0;
	config.standardIdFilter.filterListSize = 0;
	config.extendedIdFilter.isIdRejected = true;
	config.rxFifo0.isEnabled = true;
	config.rxFifo0.startAddress = &self->msgRam[MSGRAM_RXFIFO0_OFFSET];
	config.rxFifo0.size =
			N7S_HWTB_MCAN_MSGRAM_RXFIFO0_SIZE / sizeof(uint32_t);
	config.rxFifo0.mode = Mcan_RxFifoOperationMode_Blocking;
	config.rxFifo0.elementSize = Mcan_ElementSize_8;
	config.rxFifo1.isEnabled = false;
	config.rxBuffer.startAddress = &self->msgRam[MSGRAM_RXBUFFER_OFFSET];
	config.rxBuffer.elementSize = Mcan_ElementSize_8;
	config.txBuffer.isEnabled = true;
	config.txBuffer.startAddress = &self->msgRam[MSGRAM_TXBUFFER_OFFSET];
	config.txBuffer.bufferSize = 0;
	config.txBuffer.queueSize =
			N7S_HWTB_MCAN_MSGRAM_TXBUFFER_SIZE / sizeof(uint32_t);
	config.txBuffer.queueType = Mcan_TxQueueType_Fifo;
	config.txBuffer.elementSize = Mcan_ElementSize_8;
	config.txEventFifo.isEnabled = false;
	config.wdtCounter = 0;

	ErrorCode errCode = ErrorCode_NoError;
	Mcan_setConfig(&self->mcan, &config,
			N7S_HWTB_MCAN_CONFIGURATION_TIMEOUT, &errCode);
	assert(errCode == ErrorCode_NoError);
	(void)errCode; // coverage
}

static void
configureMcanPins(McanInterface *const self)
{
#if defined(N7S_TARGET_SAMV71Q21)
	const Pio_Pin_Config pinConfig = {
		.control = self->config.pioPeripheral,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Up,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	const Pio_Pin_Config pinConfig = {
		.control = self->config.pioPeripheral,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Up,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_8m,
		.isSchmittTriggerDisabled = false,
	};
#else
#error "Unsupported platform detected!"
#endif

	ErrorCode errCode = ErrorCode_NoError;

	Pmc_enablePeripheralClk(&self->pmc, self->config.pioPmcId);
	const bool isInitSuccessful =
			Pio_init(self->config.pioPort, &self->pio, &errCode);

	assert(isInitSuccessful);
	assert(errCode == ErrorCode_NoError);
	(void)isInitSuccessful; // coverage

	const bool isConfigSet = Pio_setPinsConfig(
			&self->pio, self->config.pinMask, &pinConfig, &errCode);
	assert(isConfigSet);
	assert(errCode == ErrorCode_NoError);
	(void)isConfigSet; // coverage
	(void)errCode; // coverage
}

#ifdef N7S_HWTB_MCAN_TRANSCIEVER_HAS_ENABLE_PIN
static void
configureMcanTransciever(McanInterface *const self)
{
#if defined(N7S_TARGET_SAMV71Q21)
	const Pio_Pin_Config pinConfig = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_None,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	const Pio_Pin_Config pinConfig = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_None,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_8m,
		.isSchmittTriggerDisabled = false,
	};
#else
#error "Unsupported platform detected!"
#endif

	ErrorCode errCode = ErrorCode_NoError;

	Pmc_enablePeripheralClk(&self->pmc, self->config.transcieverPioPmcId);
	const bool isTcPioInitSuccessful =
			Pio_init(self->config.transcieverPioPort,
					&self->transcieverPio, &errCode);

	assert(isTcPioInitSuccessful);
	assert(errCode == ErrorCode_NoError);
	(void)isTcPioInitSuccessful; // coverage

	const bool isConfigSet = Pio_setPinsConfig(&self->transcieverPio,
			self->config.transcieverPinMask, &pinConfig, &errCode);
	assert(isConfigSet);
	assert(errCode == ErrorCode_NoError);
	(void)isConfigSet; // coverage
	(void)errCode; // coverage

	if (self->config.transcieverEnabledPinState) {
		Pio_setPins(&self->transcieverPio,
				self->config.transcieverPinMask);
	} else {
		Pio_resetPins(&self->transcieverPio,
				self->config.transcieverPinMask);
	}
}
#endif

void
McanInterface_init(McanInterface *const self,
		const McanInterface_Config *const config)
{
	self->config = *config;
	memset(self->msgRam, 0, sizeof(self->msgRam));

	Pmc_init(&self->pmc, Pmc_getDeviceRegisterStartAddress());

	configureMcanPins(self);
#ifdef N7S_HWTB_MCAN_TRANSCIEVER_HAS_ENABLE_PIN
	configureMcanTransciever(self);
#endif
	configureMcan(self);
}

int
McanInterface_sendHandler(const struct can_msg *const msg, void *self)
{
	assert(self != NULL);
	assert(msg != NULL);
	assert(msg->len <= 8);

	uint8_t dataBuffer[8];
	memcpy(dataBuffer, msg->data, msg->len);

	Mcan_TxElement txElement = { 0 };
	txElement.idType = (msg->flags & CAN_FLAG_IDE) > 0
			? Mcan_IdType_Extended
			: Mcan_IdType_Standard;
	txElement.frameType = (msg->flags & CAN_FLAG_RTR) > 0
			? Mcan_FrameType_Remote
			: Mcan_FrameType_Data;
	txElement.id = msg->id;
	txElement.marker = 0;
	txElement.dataSize = msg->len;
	txElement.data = dataBuffer;

	McanInterface *interface = (McanInterface *)self;
	uint8_t pushIndex;

	size_t txQueueTimer = 0;
	while (txQueueTimer < N7S_HWTB_MCAN_TXQUEUE_TIMER_MAX_WAIT) {
		const bool ret = Mcan_txQueuePush(
				&interface->mcan, txElement, &pushIndex, NULL);
		if (!ret)
			++txQueueTimer;
		else
			break;
	}

	if (txQueueTimer >= N7S_HWTB_MCAN_TXQUEUE_TIMER_MAX_WAIT) {
		set_errnum(ERRNUM_IO);
		return -1;
	}

	return 0;
}

bool
McanInterface_receive(McanInterface *const self, struct can_msg *const msg)
{
	assert(self != NULL);
	assert(msg != NULL);

	Mcan_RxElement rxElement = { 0 };
	memcpy(msg->data, rxElement.data, rxElement.dataSize);
	rxElement.data = msg->data;

	if (!Mcan_rxFifoPull(&((McanInterface *)self)->mcan, Mcan_RxFifoId_0,
			    &rxElement, NULL)) {
		set_errnum(ERRNUM_NODATA);
		return false;
	}

	assert(!rxElement.isCanFdFormatEnabled);
	assert(!rxElement.isBitRateSwitchingEnabled);

	msg->len = rxElement.dataSize;
	msg->id = rxElement.id;

	msg->flags = 0;
	if (rxElement.idType == Mcan_IdType_Extended)
		msg->flags |= CAN_FLAG_IDE;
	if (rxElement.frameType == Mcan_FrameType_Remote)
		msg->flags |= CAN_FLAG_RTR;

	return true;
}

void
McanInterface_shutdown(McanInterface *const self)
{
	while (!Mcan_isTxFifoEmpty(&self->mcan))
		asm volatile("nop");

	deconfigureMcanPmc(self);
}

const McanInterface_Config *
McanInterface_getMcan0DefaultConfig(void)
{
	static const McanInterface_Config config = {
		.mcanPmcId = N7S_HWTB_MCAN0_PMC_ID,
		.mcanId = N7S_HWTB_MCAN0_ID,
		.mcanIrq = N7S_HWTB_MCAN0_IRQ,

		.pioPort = N7S_HWTB_MCAN0_PIO_PORT,
		.pioPmcId = N7S_HWTB_MCAN0_PIO_PMC_ID,
		.pioPeripheral = N7S_HWTB_MCAN0_PIO_PERIPHERAL,
		.pinMask = N7S_HWTB_MCAN0_PIN_TX | N7S_HWTB_MCAN0_PIN_RX,

#ifdef N7S_HWTB_MCAN_TRANSCIEVER_HAS_ENABLE_PIN
		.transcieverPioPort = N7S_HWTB_MCAN0_TRANSCIEVER_PIO_PORT,
		.transcieverPioPmcId = N7S_HWTB_MCAN0_TRANSCIEVER_PIO_PMC_ID,
		.transcieverPinMask = N7S_HWTB_MCAN0_TRANSCIEVER_PIO_PIN,
		.transcieverEnabledPinState =
				N7S_HWTB_MCAN0_TRANSCIEVER_PIO_ENABLED_STATE
#endif
	};
	return &config;
}

const McanInterface_Config *
McanInterface_getMcan1DefaultConfig(void)
{
	static const McanInterface_Config config = {
		.mcanPmcId = N7S_HWTB_MCAN1_PMC_ID,
		.mcanId = N7S_HWTB_MCAN1_ID,
		.mcanIrq = N7S_HWTB_MCAN1_IRQ,

		.pioPort = N7S_HWTB_MCAN1_PIO_PORT,
		.pioPmcId = N7S_HWTB_MCAN1_PIO_PMC_ID,
		.pioPeripheral = N7S_HWTB_MCAN1_PIO_PERIPHERAL,
		.pinMask = N7S_HWTB_MCAN1_PIN_TX | N7S_HWTB_MCAN1_PIN_RX,

#ifdef N7S_HWTB_MCAN_TRANSCIEVER_HAS_ENABLE_PIN
		.transcieverPioPort = N7S_HWTB_MCAN1_TRANSCIEVER_PIO_PORT,
		.transcieverPioPmcId = N7S_HWTB_MCAN1_TRANSCIEVER_PIO_PMC_ID,
		.transcieverPinMask = N7S_HWTB_MCAN1_TRANSCIEVER_PIO_PIN,
		.transcieverEnabledPinState =
				N7S_HWTB_MCAN1_TRANSCIEVER_PIO_ENABLED_STATE
#endif
	};
	return &config;
}
