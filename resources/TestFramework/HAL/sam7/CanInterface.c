/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "../CanInterface.h"
#include "McanInterface.h"

#ifndef CTESW_CAN_A_MCAN
#error "Missing CTESW_CAN_A_MCAN define"
#endif

#ifndef CTESW_CAN_B_MCAN
#error "Missing CTESW_CAN_B_MCAN define"
#endif

static __attribute__((
		section(".ramdata.mcan"))) McanInterface mcan[Can_Bus_Count];

void
CanInit(void)
{
	const McanInterface_Config *configs[2] = { 0 };
	configs[N7S_HWTB_MCAN0_ID] = McanInterface_getMcan0DefaultConfig();
	configs[N7S_HWTB_MCAN1_ID] = McanInterface_getMcan1DefaultConfig();

	McanInterface_init(&mcan[Can_Bus_A], configs[CTESW_CAN_A_MCAN]);
	McanInterface_init(&mcan[Can_Bus_B], configs[CTESW_CAN_B_MCAN]);
}

void
CanFini(void)
{
	for (size_t i = 0; i < Can_Bus_Count; ++i)
		McanInterface_shutdown(&mcan[i]);
}

bool
CanRecv(const Can_Bus bus, struct can_msg *const msg)
{
	return McanInterface_receive(&mcan[bus], msg);
}

bool
CanSend(const Can_Bus bus, const struct can_msg *const msg)
{
	return McanInterface_sendHandler(msg, &mcan[bus]) == 0;
}
