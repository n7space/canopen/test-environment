/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/compat/stdlib.h>
#include <lely/compat/string.h>

#include <TestFramework/Log.h>

#undef memset
#undef memcpy

void _exit(int r) __attribute__((__noreturn__));

// used by default assert implementation (libc header)
void __assert_func(const char *file, int line, const char *func,
		const char *failedexpr);
void
__assert_func(const char *file, int line, const char *func,
		const char *failedexpr)
{
	LOG("ASSERTION FAILED!");
	LOG_STR("File", file);
	LOG_INT32("Line", line);
	LOG_STR("Function", func);
	LOG_STR("Failed assertion", failedexpr);

	_exit(1);
}

void __assert_fail(const char *failedexpr, const char *file, unsigned line,
		const char *func);
void
__assert_fail(const char *failedexpr, const char *file, unsigned line,
		const char *func)
{
	LOG("ASSERTION FAILED!");
	LOG_STR("File", file);
	LOG_INT32("Line", line);
	LOG_STR("Function", func);
	LOG_STR("Failed assertion", failedexpr);

	_exit(1);
}

// wrapper for calls generated directly by compiler
void *memset(void *s, int c, size_t n);
void *
memset(void *s, int c, size_t n)
{
	return lely_compat_memset(s, c, n);
}

// wrapper for calls generated directly by compiler
void *memcpy(void *dest, const void *src, size_t n);
void *
memcpy(void *dest, const void *src, size_t n)
{
	return lely_compat_memcpy(dest, src, n);
}

void exit(int r);
void
exit(int r)
{
	_exit(r);
}

void abort(void);
void
abort(void)
{
	LOG("PROGRAM ABORTED");
	_exit(2);
}
