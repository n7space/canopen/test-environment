/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>

#include "../PlatformIO.h"

#if defined(USE_RTT_IO)
#include <SEGGER_RTT.h>

static inline void
synchronizeWithRTT(void)
{
	asm volatile("dmb");
	asm volatile("dsb");
}

void
PlatformIO_startup(void)
{
	SEGGER_RTT_Init();
	synchronizeWithRTT();
}

void
PlatformIO_shutdown(void)
{
	PlatformIO_flush();
}

void
PlatformIO_flush(void)
{
	synchronizeWithRTT();
	while (SEGGER_RTT_HasDataUp(0u) != 0u)
		;
}

size_t
PlatformIO_writeBytes(const uint8_t *const bytes, const size_t count)
{
	return (size_t)SEGGER_RTT_Write(0u, bytes, count);
}

#else
#error "Usage of stdio would result in a crash, as low level IO interface was not selected with proper #define"
#endif
