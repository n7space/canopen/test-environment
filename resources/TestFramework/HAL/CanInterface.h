/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_HAL_CAN_INTERFACE_H
#define CTESW_HAL_CAN_INTERFACE_H

#include <stdbool.h>

#include <lely/can/msg.h>
#include <lely/can/net.h>

#ifdef __cplusplus
extern "C" {
#endif

/// Enumeration listing available physical CAN buses.
typedef enum {
	Can_Bus_A = 0, ///< CAN A bus.
	Can_Bus_B, ///< CAN B bus.
	Can_Bus_Count, ///< Count of available CAN buses (keep as last item in the enum).
} Can_Bus;

/// Initializes the CAN interface, implementation is platform-specific.
void CanInit(void);

/// Shuts the CAN interface down, implementation is platform-specific.
void CanFini(void);

/// Retrieves a CAN message received by the device. This function is
/// non-blocking.
///
/// \param [in]  bus an identifier of the CAN bus to be used to receive a messsage.
/// \param [out] msg a pointer to the CAN frame object to store received message
///
/// \retval true  When message was available and was written to <b>msg</b>.
/// \retval false When there was no message or an error occurred.
///               The error number can be obtained with `get_errnum()`.
///               If there was no message `errnum` will be set to `ERRNUM_NODATA`.
bool CanRecv(Can_Bus bus, struct can_msg *msg);

/// Sends the CAN message through the CAN interface.
///
/// \param [in] bus a CAN bus to be used to send the messsage.
/// \param [in] msg a pointer to the CAN frame to be sent.
///
/// \retval true  On success.
/// \retval false On error, the error number can be obtained with `get_errnum()`.
bool CanSend(Can_Bus bus, const struct can_msg *msg);

#ifdef __cplusplus
}
#endif

#endif // CTESW_HAL_CAN_INTERFACE_H
