/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../CanInterface.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "TcpCanInterface.h"

#define CTESW_MAX_ENV_VAR_LEN 1024u

static TcpCanInterface can[Can_Bus_Count];

static void
getEnvVar(const char *const varName, char *const output)
{
	const char *const var = getenv(varName);
	if (var == NULL) {
		(void)printf("Missing environment variable: %s\n", varName);
		(void)strncpy(output, varName, CTESW_MAX_ENV_VAR_LEN);
	} else {
		(void)strncpy(output, var, CTESW_MAX_ENV_VAR_LEN);
	}
}

void
CanInit(void)
{
	char hostA[CTESW_MAX_ENV_VAR_LEN] = { 0 };
	getEnvVar("CTESW_CAN_A_ADDRESS", hostA);
	char portA[CTESW_MAX_ENV_VAR_LEN] = { 0 };
	getEnvVar("CTESW_CAN_A_PORT", portA);

	char hostB[CTESW_MAX_ENV_VAR_LEN] = { 0 };
	getEnvVar("CTESW_CAN_B_ADDRESS", hostB);
	char portB[CTESW_MAX_ENV_VAR_LEN] = { 0 };
	getEnvVar("CTESW_CAN_B_PORT", portB);

	TcpCanInterface_init(&can[Can_Bus_A], hostA, portA);
	TcpCanInterface_init(&can[Can_Bus_B], hostB, portB);
}

void
CanFini(void)
{
	for (size_t i = 0; i < Can_Bus_Count; ++i)
		TcpCanInterface_shutdown(&can[i]);
}

bool
CanRecv(const Can_Bus bus, struct can_msg *const msg)
{
	return TcpCanInterface_receive(&can[bus], msg);
}

bool
CanSend(const Can_Bus bus, const struct can_msg *const msg)
{
	return TcpCanInterface_sendHandler(msg, &can[bus]);
}
