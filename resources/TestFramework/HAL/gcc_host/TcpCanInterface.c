/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "TcpCanInterface.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <lely/can/socket.h>
#include <lely/util/error.h>

#define CAN_ID_STANDARD_ID_MASK 0x000003FFu
#define CAN_ID_MASK 0x1FFFFFFFu

static int
findConnection(const struct addrinfo *address)
{
	for (; address != NULL; address = address->ai_next) {
		const int fd = socket(address->ai_family, address->ai_socktype,
				address->ai_protocol);
		if (fd == -1)
			continue;

		if (connect(fd, address->ai_addr, address->ai_addrlen) != -1)
			return fd;

		(void)close(fd);
	}
	assert("Connection error" && false);
	return -1;
}

static int
openConnection(const char *const address, const char *const service)
{
	const struct addrinfo hints = {
		.ai_flags = 0,
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_STREAM,
		.ai_protocol = 0,
		.ai_addrlen = 0,
		.ai_addr = NULL,
		.ai_canonname = NULL,
		.ai_next = NULL,
	};

	struct addrinfo *result = NULL;

	if (getaddrinfo(address, service, &hints, &result) != 0) {
		assert("Name resolution error" && false);
		return -1;
	}

	const int fd = findConnection(result);

	freeaddrinfo(result);

	return fd;
}

void
TcpCanInterface_init(TcpCanInterface *const self, const char *const address,
		const char *const port)
{
	self->socketFd = openConnection(address, port);
	assert(self->socketFd >= 0);
}

bool
TcpCanInterface_sendHandler(const struct can_msg *const msg, void *const data)
{
	assert(data != NULL);
	assert(msg != NULL);

	const TcpCanInterface *self = (TcpCanInterface *)data;

	struct can_frame socketFrame = { 0 };
	if (can_msg2can_frame(msg, &socketFrame) != 0) {
		set_errnum(ERRNUM_BADMSG);
		(void)printf("Conversion from can_msg to socketcan can_frame failed\n");
		return false;
	}

	if (write(self->socketFd, &socketFrame, sizeof(struct can_frame)) < 0) {
		set_errnum(ERRNUM_IO);
		(void)printf("Writing message to the socket failed\n");
		return false;
	}

	return true;
}

bool
TcpCanInterface_receive(TcpCanInterface *const self, struct can_msg *const msg)
{
	assert(self != NULL);
	assert(msg != NULL);

	struct can_frame socketFrame = { 0 };
	ssize_t bytesToRead = sizeof(struct can_frame);
	uint8_t *const frameEndAddress =
			(uint8_t *)&socketFrame + sizeof(struct can_frame);

	while (bytesToRead != 0) {
		const ssize_t size = recv(((TcpCanInterface *)self)->socketFd,
				frameEndAddress - bytesToRead,
				(size_t)bytesToRead, MSG_DONTWAIT);
		if ((size < 0) && (bytesToRead == sizeof(struct can_frame))) {
			set_errnum(ERRNUM_NODATA);
			return false;
		}

		if (size < 0)
			continue;

		bytesToRead -= size;
		assert(bytesToRead >= 0);
	}

	if (can_frame2can_msg(&socketFrame, msg) != 0) {
		set_errnum(ERRNUM_BADMSG);
		(void)printf("Conversion from socketcan can_frame to can_msg failed\n");
		return false;
	}
	return true;
}

void
TcpCanInterface_shutdown(TcpCanInterface *const self)
{
	const int result = close(self->socketFd);
	self->socketFd = -1;
	(void)result;
	assert(result == 0 && "Error occurred when closing can socket");
}
