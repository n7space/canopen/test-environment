/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../ClockInterface.h"

#include <time.h>

static struct timespec init_tp;

void
ClockStartup(void)
{
	(void)timespec_get(&init_tp, TIME_UTC);
}

void
ClockGetTime(struct timespec *const tp)
{
	(void)timespec_get(tp, TIME_UTC);

	tp->tv_sec -= init_tp.tv_sec;
	tp->tv_nsec -= init_tp.tv_nsec;

	if (tp->tv_nsec < 0) {
		tp->tv_sec--;
		tp->tv_nsec += 1000000000L;
	}
}
