/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTESW_HAL_X86_TCP_CAN_INTERFACE_H
#define CTESW_HAL_X86_TCP_CAN_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

#include <lely/can/msg.h>

/// \brief Structure representing TCP CAN interface
typedef struct {
	int socketFd; ///< Local socket descriptor.
} TcpCanInterface;

/// \brief Initializes TcpCanInterface structure.
/// \param self [in,out] Pointer to the class representation.
/// \param address TCP address (symbolic) of the CAN socket.
/// \param port TCP port (number or getaddrinfo() style service) of the CAN socket.
void TcpCanInterface_init(TcpCanInterface *const self, const char *address,
		const char *port);

/// \brief Closes socket.
/// \param self [in,out] Pointer to the class representation.
void TcpCanInterface_shutdown(TcpCanInterface *const self);

/// \brief Checks whether new message arrived and stores it in msg struct.
/// \param self [in,out] Pointer to the class representation.
/// \param msg [out] Pointer to store received message.
/// \retval true When message was received.
/// \retval false When no message was received.
bool TcpCanInterface_receive(
		TcpCanInterface *const self, struct can_msg *const msg);

/// \brief Send handler to be registered with can_net_set_send_func with a data
/// pointer to TcpCanInterface instance.
/// \param msg [in] Message to send.
/// \param self [in,out] Pointer to the class representation.
/// \retval true on success.
bool TcpCanInterface_sendHandler(
		const struct can_msg *const msg, void *const self);

#ifdef __cplusplus
}
#endif

#endif // CTESW_HAL_X86_TCP_CAN_INTERFACE_H
