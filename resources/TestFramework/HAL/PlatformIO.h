/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PLATFORM_IO_H
#define PLATFORM_IO_H

/// \file PlatformIO.h
/// \brief I/O functions prototypes for hardware initialization and communication with test host.

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// \brief Performs a hardware setup procedure of TestFramework I/O.
void PlatformIO_startup(void);

/// \brief Performs a hardware shutdown procedure of TestFramework I/O.
void PlatformIO_shutdown(void);

/// \brief Flushes the I/O buffers.
void PlatformIO_flush(void);

/// \brief Writes provided bytes to substituted standard output.
/// \param bytes Bytes array to be written.
/// \param count Amount of bytes to be written.
/// \returns Amount of bytes written.
size_t PlatformIO_writeBytes(const uint8_t *const bytes, const size_t count);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // PLATFORM_IO_H
