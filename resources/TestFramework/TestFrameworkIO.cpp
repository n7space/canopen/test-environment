/**@file
 * This file is part of the Test Framework of the Test Environment.
 *
 * @copyright 2024-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "TestFrameworkIO.h"

#include "HAL/PlatformIO.h"

#include <array>
#include <cassert>

#ifdef N7S_ENABLE_COVERAGE
#include "Gcov.hpp"
#endif

namespace
{
uint8_t
nibbleToHex(const uint8_t value)
{
	switch (value) {
	case 0xF: return 'F';
	case 0xE: return 'E';
	case 0xD: return 'D';
	case 0xC: return 'C';
	case 0xB: return 'B';
	case 0xA: return 'A';
	default: return static_cast<uint8_t>(value + '0');
	}
}

void __attribute__((noinline))
rawWrite(const uint8_t *const bytes, const size_t length)
{
	const size_t writtenBytes = PlatformIO_writeBytes(bytes, length);
	assert(writtenBytes == length);
	(void)writtenBytes; // to silence warnings with asserts disabled (coverage)
}

template <std::size_t N>
void
outStr(const char (&str)[N])
{
	rawWrite(reinterpret_cast<const uint8_t *>(str), N - 1u); // -1 for '\0'
}
} // namespace

void
TestFrameworkIO_flush(void)
{
	PlatformIO_flush();
}

void
TestFrameworkIO_writeBytes(const uint8_t *const bytes, const size_t length)
{
	assert(bytes);
	assert(length > 0);
	rawWrite(bytes, length);
}

uint32_t
TestFrameworkIO_writeToFile(
		const uint32_t fd, const char *const data, const uint32_t count)
{
	if ((fd != 1u) && (fd != 2u)) // stdout, stderr
	{
		assert(false && "Only stdout and stderr are supported!");
		return 0u;
	}

	const auto *const bytes = reinterpret_cast<const uint8_t *>(data);
	TestFrameworkIO_writeBytes(bytes, count);
	return count;
}

void
TestFrameworkIO_writeProgramFooter(const uint32_t status)
{
	TestFrameworkIO_flush();
	outStr("\n>> EXIT STATUS: ");
	TestFrameworkIO_write32BitValueAsHex(status);

#ifdef N7S_ENABLE_COVERAGE
	outStr("\n>> COVERAGE RESULT - BEGIN <<\n");
	{
		GcovDumper dumper{};
		dumper.dump();
	}
	outStr("\n>> COVERAGE RESULT - END <<\n");
#endif

	outStr("\n");
}

void
TestFrameworkIO_writeString(const char *const str, const size_t length)
{
	assert(str);
	assert(length > 0);

	// std::find does not fit on RH707
	size_t size = 0u;
	for (; size < length; ++size)
		if (str[size] == '\0')
			break;

	rawWrite(reinterpret_cast<const uint8_t *>(str), size);
}

void
TestFrameworkIO_writeByteValueAsHex(const uint8_t byte)
{
	const std::array<uint8_t, 2u> arr = {
		nibbleToHex((byte >> 4u) & 0x0Fu),
		nibbleToHex(byte & 0x0Fu),
	};
	rawWrite(arr.data(), arr.size());
}

void
TestFrameworkIO_writeRawDataAsHex(const void *const data, const size_t length)
{
	const auto *const dataBytes = reinterpret_cast<const uint8_t *>(data);
	for (size_t i = 0; i < length; i++) {
		TestFrameworkIO_writeByteValueAsHex(dataBytes[i]);
	}
}

void
TestFrameworkIO_write32BitValueAsHex(const uint32_t value)
{
	TestFrameworkIO_writeByteValueAsHex(
			static_cast<uint8_t>((value >> 24u) & 0xFFu));
	TestFrameworkIO_writeByteValueAsHex(
			static_cast<uint8_t>((value >> 16u) & 0xFFu));
	TestFrameworkIO_writeByteValueAsHex(
			static_cast<uint8_t>((value >> 8u) & 0xFFu));
	TestFrameworkIO_writeByteValueAsHex(
			static_cast<uint8_t>(value & 0xFFu));
}

void
TestFrameworkIO_write64BitValueAsHex(const uint64_t value)
{
	TestFrameworkIO_write32BitValueAsHex(
			static_cast<uint32_t>((value >> 32u) & 0xFFFFFFFFu));
	TestFrameworkIO_write32BitValueAsHex(
			static_cast<uint32_t>(value & 0xFFFFFFFFu));
}

void
TestFrameworkIO_writeUInt32AsHex(const uint32_t value)
{
	outStr("0x");
	TestFrameworkIO_write32BitValueAsHex(value);
}

void
TestFrameworkIO_writeUInt64AsHex(const uint64_t value)
{
	outStr("0x");
	TestFrameworkIO_write64BitValueAsHex(value);
}
