#!/bin/env python3
# This file is part of the Test Environment.
#
# @copyright 2023-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import configparser
import sys
import time


def _get_config(path):
    config = configparser.ConfigParser()
    config.optionxform = lambda option: option
    if len(config.read([path])) != 1:
        return None

    return config


def _connect_to_egse(config):
    sys.path += [sys.path[0] + "/../py"]
    from libs.EgseControl import EgseControl

    return EgseControl.from_config(config)


def main():
    if len(sys.argv) != 2:
        print(f"Usage: python3 {sys.argv[0]} <path_to_hw_config>")
        sys.exit(0)

    config = _get_config(sys.argv[1])
    if config is None:
        print("Could not open configuration file.")
        sys.exit(1)

    egse = _connect_to_egse(config)

    egse.disablePowerSupply()
    time.sleep(3)
    egse.executeOnEgse("sudo reboot")
    time.sleep(1)


if __name__ == "__main__":
    main()
