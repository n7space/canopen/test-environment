/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BinaryLogEncoder.hpp"

#include <cstring>

#include "TestsRuntime.h"

namespace
{
uint8_t
nibbleToHex(const uint8_t value) noexcept
{
	switch (value) {
	case 0xF: return 'F';
	case 0xE: return 'E';
	case 0xD: return 'D';
	case 0xC: return 'C';
	case 0xB: return 'B';
	case 0xA: return 'A';
	default: return static_cast<uint8_t>(value + '0');
	}
}
} // namespace

BinaryLogEncoder::~BinaryLogEncoder() noexcept { flushOutputBuffer(); }

uint32_t
BinaryLogEncoder::open(const char *const filePath) noexcept
{
	const auto fd = nextFreeFd;
	nextFreeFd++;

	const auto pathLength = static_cast<uint32_t>(std::strlen(filePath));

	writeBufferedByte(BINARY_FILEIO_CMD_OPEN);
	writeBufferedData(&fd, sizeof(fd));
	writeBufferedData(&pathLength, sizeof(pathLength));
	writeBufferedData(filePath, pathLength);

	return fd;
}

uint32_t
BinaryLogEncoder::write(const uint32_t fd, const uint8_t *const data,
		const uint32_t count) noexcept
{
	if (count == 1u) {
		writeBufferedByte(BINARY_FILEIO_CMD_BYTE);
		writeBufferedData(&fd, sizeof(fd));
		writeBufferedByte(*data);
		return count;
	}

	writeBufferedByte(BINARY_FILEIO_CMD_DATA);
	writeBufferedData(&fd, sizeof(fd));
	writeBufferedData(&count, sizeof(count));
	writeBufferedData(data, count);

	return count;
}

void
BinaryLogEncoder::flushOutputBuffer() noexcept
{
	if (bufferPos > 0u) {
		TestsRuntime_writeData(buffer.data(), bufferPos);
		resetBuffer();
		const auto eol = static_cast<uint8_t>('\n');
		TestsRuntime_writeData(&eol, sizeof(eol));
	}
}

void
BinaryLogEncoder::writeBufferedByte(const uint8_t data) noexcept
{
	appendByteAsHex(data);

	if (bufferPos >= buffer.size())
		flushOutputBuffer();
}

void
BinaryLogEncoder::writeBufferedData(
		const void *const data, const size_t count) noexcept
{
	const auto *const bytes = static_cast<const uint8_t *>(data);
	for (size_t i = 0; i < count; ++i)
		writeBufferedByte(bytes[i]);
}

void
BinaryLogEncoder::appendByteAsHex(const uint8_t data) noexcept
{
	constexpr size_t nibbleOffset = 4u;
	constexpr size_t nibbleMask = 0x0Fu;

	appendToBuffer(nibbleToHex(static_cast<uint8_t>(data >> nibbleOffset)
			& nibbleMask));
	appendToBuffer(nibbleToHex(data & nibbleMask));
}

void
BinaryLogEncoder::resetBuffer() noexcept
{
	bufferPos = 0u;
}

void
BinaryLogEncoder::appendToBuffer(const uint8_t data) noexcept
{
	buffer[bufferPos] = data;
	bufferPos++;
}
