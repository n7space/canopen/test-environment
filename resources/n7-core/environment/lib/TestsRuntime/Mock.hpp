/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTSRUNTIME_MOCKUTILS_H
#define N7S_CORE_TESTSRUNTIME_MOCKUTILS_H

#include <cassert>
#include <cstddef>

template <typename Mock, typename Object> struct MockBase {
	Object obj{};

	static Mock &
	mock(Object *const self)
	{
		// cppcheck-suppress [misra-c2012-10.4]
		static_assert(offsetof(MockBase, obj) == 0u,
				"pointer to mocked object should be the pointer to the mock itself");
		return *static_cast<Mock *>(reinterpret_cast<MockBase *>(self));
	}

	static Mock &
	cmock(const Object *const self)
	{
		return mock(const_cast<Object *>(self)); // NOLINT
	}
};

#endif // N7S_CORE_TESTSRUNTIME_MOCKUTILS_H
