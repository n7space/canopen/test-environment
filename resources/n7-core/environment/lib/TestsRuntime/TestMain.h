/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTSRUNTIME_TESTMAIN_H
#define N7S_CORE_TESTSRUNTIME_TESTMAIN_H

/// \file  TestMain.h
/// \brief Tests utility separating tests execution from platform.
/// \details Provides main procedure, should not be included in headers!

#include "Io.h"
#include "TestMainIface.h"
#include "TestsRuntime.h"

#ifdef __cplusplus
extern "C" {
#endif

int
main(void)
{
	TestsRuntime_StartupArg arg = { .disableWatchdog = true };

#ifdef N7S_TESTS_DONT_DISABLE_WATCHDOG
	arg.disableWatchdog = false;
#endif

	TestsRuntime_startup(&arg);
	TestsRuntime_Io_init();

	return test_main();
	// exit() system call should be wrapped by the platform specific runtime correctly.
}

#ifdef __cplusplus
} // extern "C"
#endif

#endif // N7S_CORE_TESTSRUNTIME_TESTMAIN_H
