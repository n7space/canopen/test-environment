/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2023-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTS_RUNTIME_IO_H
#define N7S_CORE_TESTS_RUNTIME_IO_H

/// \file  Io.h
/// \brief Tests Runtime I/O handling.

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// \brief Initializes Tests Runtime I/O module.
void TestsRuntime_Io_init(void);

/// \brief Writes final messages and summary of the program execution.
/// \details Responsible for dumping coverage data.
/// \param [in] status Exit status of the program.
void TestsRuntime_Io_writeProgramFooter(const uint32_t status);

/// \brief Simulates writing to a file using I/O channel.
/// \details Should be used only for dumping coverage.
/// \param [in] fd File descriptor.
/// \param [in] data Data to write.
/// \param [in] count Data bytes count.
/// \returns Written bytes.
uint32_t TestsRuntime_Io_writeToFile(const uint32_t fd, const char *const data,
		const uint32_t count);

/// \brief Logs text.
/// \param [in] text Text to be logged.
/// \param [in] textSize Text size.
void TestsRuntime_Io_logText(const char *const text, const size_t textSize);

/// \brief Logs unsigned 32bit value, prefixed with given name.
/// \param [in] prefix Prefix to be used.
/// \param [in] prefixSize Prefix size.
/// \param [in] value Value to be logged.
void TestsRuntime_Io_logUInt32Value(const char *const prefix,
		const size_t prefixSize, const uint32_t value);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // N7S_CORE_TESTS_RUNTIME_IO_H
