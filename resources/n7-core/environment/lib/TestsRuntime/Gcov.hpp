/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTS_RUNTIME_GCOV_HPP
#define N7S_CORE_TESTS_RUNTIME_GCOV_HPP

/// \file  Gcov.hpp
/// \brief GCOV support.

#include "BinaryLogEncoder.hpp"

class GcovDumper
{
      public:
	void dump() noexcept;

      private:
	BinaryLogEncoder encoder{};
};

#endif // N7S_CORE_TESTS_RUNTIME_GCOV_HPP
