/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTSRUNTIME_TESTSRUNTIME_H
#define N7S_CORE_TESTSRUNTIME_TESTSRUNTIME_H

/// \file TestsRuntime.h
/// \brief Basic hardware initialization and C Runtime implementation for tests.

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// \brief Startup argument structure.
typedef struct {
	bool disableWatchdog; ///< disable Watchdog
} TestsRuntime_StartupArg;

/// \brief Performs a hardware setup procedure of TestsRuntime module.
void TestsRuntime_startup(const TestsRuntime_StartupArg *const arg);

/// \brief Writes provided bytes to substituted standard output.
/// \param [in,out] data Data to be written.
/// \param [in] count Length of data buffer to be written.
void TestsRuntime_writeData(const uint8_t *data, const uint32_t count);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // N7S_CORE_TESTSRUNTIME_TESTSRUNTIME_H
