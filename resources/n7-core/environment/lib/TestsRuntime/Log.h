/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright -2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTS_RUNTIME_LOG_H
#define N7S_CORE_TESTS_RUNTIME_LOG_H

/// \file  Log.h
/// \brief Logging to I/O utilities for tests.

#include "Io.h"

#ifdef __cplusplus
extern "C" {
#endif

/// \brief Calculates literal length.
/// \param [in] text literal
#define LOG_LITERAL_LENGTH(text) (sizeof((text)) - 1u)

/// \brief Logs provided text literal.
/// \param [in] text Literal to be logged.
#define LOG(text) TestsRuntime_Io_logText((text), LOG_LITERAL_LENGTH((text)))

/// \brief Logs uint32_t value prefixed with text literal.
/// \param [in] prefix Text literal (e.g. name of the value).
/// \param [in] value Value to be logged.
#define LOG_UINT32(prefix, value) \
	TestsRuntime_Io_logUInt32Value( \
			(prefix), LOG_LITERAL_LENGTH((prefix)), (value))

#ifdef __cplusplus
} // extern "C"
#endif

#endif // N7S_CORE_TESTS_RUNTIME_LOG_H
