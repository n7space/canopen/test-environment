/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_CORE_TESTS_RUNTIME_BINARYLOGENCODER_H
#define N7S_CORE_TESTS_RUNTIME_BINARYLOGENCODER_H

/// \file BinaryLogEncoder.hpp
/// \brief Utility to encode binary (coverage) data.

#include <array>
#include <cstdint>

class BinaryLogEncoder
{
      public:
	explicit BinaryLogEncoder() noexcept = default;

	BinaryLogEncoder(const BinaryLogEncoder &) = delete;
	BinaryLogEncoder(BinaryLogEncoder &&) = delete;
	BinaryLogEncoder &operator=(const BinaryLogEncoder &) = delete;
	BinaryLogEncoder &operator=(BinaryLogEncoder &&) = delete;

	~BinaryLogEncoder() noexcept;

	uint32_t open(const char *const filePath) noexcept;
	uint32_t write(const uint32_t fd, const uint8_t *const data,
			const uint32_t count) noexcept;

      private:
	constexpr static uint8_t BINARY_FILEIO_CMD_OPEN =
			static_cast<uint8_t>('O');
	constexpr static uint8_t BINARY_FILEIO_CMD_BYTE =
			static_cast<uint8_t>('B');
	constexpr static uint8_t BINARY_FILEIO_CMD_DATA =
			static_cast<uint8_t>('W');

	void flushOutputBuffer() noexcept;
	void writeBufferedByte(const uint8_t data) noexcept;
	void writeBufferedData(const void *const data,
			const std::size_t count) noexcept;
	void appendByteAsHex(const uint8_t data) noexcept;
	void resetBuffer() noexcept;
	void appendToBuffer(const uint8_t data) noexcept;

	uint32_t nextFreeFd = 3u; // after stderr
	std::array<uint8_t, 64u> buffer{};
	uint32_t bufferPos = 0u;
};

#endif // N7S_CORE_TESTS_RUNTIME_BINARYLOGENCODER_H
