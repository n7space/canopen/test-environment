/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2023-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Io.h"

#include <array>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string_view>

#include "TestsRuntime.h"

#ifdef N7S_ENABLE_COVERAGE
#include "Gcov.hpp"
#endif

namespace
{

void
writeString(const std::string_view v) noexcept
{
	TestsRuntime_writeData(reinterpret_cast<const uint8_t *>(v.data()),
			static_cast<uint32_t>(v.size()));
}

uint8_t
nibbleToHex(const uint8_t value) noexcept
{
	switch (value) {
	case 0xF: return 'F';
	case 0xE: return 'E';
	case 0xD: return 'D';
	case 0xC: return 'C';
	case 0xB: return 'B';
	case 0xA: return 'A';
	default: return static_cast<uint8_t>(value + '0');
	}
}

std::array<uint8_t, 2u>
byteAsHexString(const uint8_t data) noexcept
{
	constexpr size_t nibbleOffset = 4u;
	constexpr size_t nibbleMask = 0x0Fu;

	const std::array<uint8_t, 2u> result = {
		nibbleToHex(static_cast<uint8_t>(data >> nibbleOffset)
				& nibbleMask),
		nibbleToHex(data & nibbleMask),
	};
	return result;
}

void
writeByteAsHexString(const uint8_t data) noexcept
{
	const auto buffer = byteAsHexString(data);
	TestsRuntime_writeData(buffer.data(), buffer.size());
}

void
writeIntAsHexString(const uint32_t data) noexcept
{
	writeByteAsHexString(static_cast<uint8_t>((data >> 24u) & 0xFFu));
	writeByteAsHexString(static_cast<uint8_t>((data >> 16u) & 0xFFu));
	writeByteAsHexString(static_cast<uint8_t>((data >> 8u) & 0xFFu));
	writeByteAsHexString(static_cast<uint8_t>(data & 0xFFu));
}

void
dumpCoverage() noexcept
{
#ifdef N7S_ENABLE_COVERAGE
	writeString("\n>> COVERAGE RESULT - BEGIN <<\n");
	{
		GcovDumper gcov{};
		gcov.dump();
	}
	writeString("\n>> COVERAGE RESULT - END <<\n");
#endif
}

void
writeProgramFooter(const uint32_t status) noexcept
{
	writeString("\n>> EXIT STATUS: ");
	writeIntAsHexString(status);
	writeString("\n");

	dumpCoverage();

	writeString("\n>> OUTPUT END MARKER <<\n");
}

} // namespace

void
TestsRuntime_Io_init()
{
	writeString("\n>> TestsRuntime I/O inited.\n");
}

void
TestsRuntime_Io_writeProgramFooter(const uint32_t status)
{
	writeProgramFooter(status);
	(void)std::fflush(stdout);
}

uint32_t
TestsRuntime_Io_writeToFile(
		const uint32_t fd, const char *const data, const uint32_t count)
{
	(void)fd;
	const auto *const bytes = reinterpret_cast<const uint8_t *>(data);

	TestsRuntime_writeData(bytes, count);
	return count;
}

void
TestsRuntime_Io_logUInt32Value(const char *const prefix,
		const size_t prefixSize, const uint32_t value)
{
	writeString({ prefix, prefixSize });
	writeString(": 0x");
	writeIntAsHexString(value);
	writeString("\n");
}

void
TestsRuntime_Io_logText(const char *const text, const size_t textSize)
{
	writeString({ text, textSize });
	writeString("\n");
}
