/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2023-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <CppUTest/TestOutput.h>
#include <CppUTest/TestRegistry.h>
#include <CppUTest/TestResult.h>

#include <TestsRuntime/SuiteFixture.h>
#include <TestsRuntime/TestMainIface.h>

static size_t
cpputest_main()
{
	ConsoleTestOutput output{};
	output.verbose(TestOutput::level_verbose);
	output.color();

	TestResult result(output);
	TestRegistry::getCurrentRegistry()->runAllTests(result);

	return result.getFailureCount();
}

int
test_main(void)
{
	SuiteFixture_setup();
	const auto failureCount = cpputest_main();
	SuiteFixture_teardown();

	return static_cast<int>(failureCount);
}
