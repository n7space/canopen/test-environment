# N7 Core

Project to be used as base for other N7S projects.

## Includes
 * Docker repository with development environments for various platforms.
 * Common C Utils (fifo etc.)

## Usage (as SCons project base)

The recommended way of using this as base SCons project is:

0. Prepare user variables
1. Create new SCons environment

```python
from SCons.Script import Environment

env = Environment()
```

2. Prepare the environment for n7-core, for example by setting the project title

```python
env["N7S_SW_TITLE_SHORT"] = "N7-Core-Example"
```

3. Import `n7s.core-std-env` tool

```python
env.Tool("n7s.core-std-env")
```

4. Prepare subenvironments for used platforms, add custom methods, tools, variables, etc.

```python
env.CreateStdSubEnvs(["samv71q21", "samrh71f20"])
for subenv in env.AllEnvs.Items():
    __setup_environment(subenv)
```

For details about available methods, configuration variables, etc. consult the tools code.
Core utilities for building projects are provided by `env_builder` tool.
Subenvironment management is provided by `multi_platform` tool.
SCons module builders are provided by `modules_builder` tool.
