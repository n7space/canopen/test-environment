# This file is part of the Test Environment resources.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from __future__ import annotations

import argparse
import json
import shutil
import uuid
import xml.dom.minidom as minidom
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path

DEFAULT_XC32_TOOLCHAIN_VERSION = "4.40"

DEFAULT_C32_CONFIG = {
    "additional-warnings": "true",
    "make-warnings-into-errors": "true",
    "optimization-level": "",
    "preprocessor-macros": "",
}


DEFAULT_C32LD_CONFIG = {
    "exclude-standard-libraries": "true",
    "optimization-level": "",
    "preprocessor-macros": "",
}

DEFAULT_C32CPP_CONFIG = {
    "additional-warnings": "true",
    "make-warnings-into-errors": "true",
    "exceptions": "false",
    "optimization-level": "",
    "preprocessor-macros": "",
    "rtti": "false",
}

DEFAULT_C32GLOBAL_CONFIG = {
    "common-include-directories": "",
}


@dataclass
class CompileCommand:
    directory: Path
    file: Path
    output: Path
    command: str
    arguments: list[str]

    @staticmethod
    def from_json(json: dict) -> CompileCommand:
        if "command" in json and "arguments" not in json:
            json["arguments"] = json["command"].split(" ")

        if "arguments" in json and "command" not in json:
            json["command"] = " ".join(json["arguments"])

        return CompileCommand(
            directory=Path(json["directory"]),
            file=Path(json["file"]),
            output=Path(json["output"]),
            command=json["command"],
            arguments=json["arguments"],
        )


def load_compile_commands_json(compile_commands_path: Path) -> list[CompileCommand]:
    if not compile_commands_path.is_file():
        raise RuntimeError(f"Error: File '{compile_commands_path}' does not exist.")

    try:
        with open(compile_commands_path, "r") as compile_commands:
            commands_json = json.load(compile_commands)
            return [CompileCommand.from_json(cmd) for cmd in commands_json]
    except json.JSONDecodeError as e:
        raise RuntimeError(
            f"Error: Failed to decode JSON file '{compile_commands_path}'. {e}"
        )


def load_file(path: Path, should_exist: bool = True) -> str:
    if should_exist and not path.exists():
        raise RuntimeError(f"File {path} does not exist!")

    with open(path, "r") as file:
        return file.read()


@dataclass
class MplabFile:
    path: Path
    xml_path: Path
    build_flags: list[str]

    @staticmethod
    def from_compile_command(
        command: CompileCommand,
        base_path: str | None = None,
        base_xml_path: str | None = None,
    ) -> MplabFile:
        path = (
            command.file
            if base_path is None
            else get_relative_path(command.file, base_path)
        )
        xml_path = (
            command.file
            if base_xml_path is None
            else get_relative_path(command.file, base_xml_path)
        )

        if path is None:
            raise RuntimeError(f"'{base_path}' not found in '{command.file}'")
        if xml_path is None:
            raise RuntimeError(f"'{base_xml_path}' not found in '{command.file}'")

        return MplabFile(
            path=path,
            xml_path=xml_path.parent,
            # first argument is compiler exec, last three are -O out.o in.XXX
            build_flags=command.arguments[1:-3],
        )


def create_mplab_logical_folder(
    name: str, display_name: str | None = None
) -> ET.Element:
    if display_name is None:
        display_name = name

    return ET.Element(
        "logicalFolder",
        attrib={
            "name": name,
            "displayName": display_name,
            "projectFiles": "true",
        },
    )


def insert_mplab_logical_folder(
    parent: ET.Element, name: str, display_name: str | None = None
) -> ET.Element:
    if display_name is None:
        display_name = name

    return ET.SubElement(
        parent,
        "logicalFolder",
        attrib={
            "name": name,
            "displayName": display_name,
            "projectFiles": "true",
        },
    )


def insert_mplab_file(folder: ET.Element, path: str) -> ET.Element:
    file = ET.SubElement(folder, "itemPath")
    file.text = path.replace("\\", "/")
    return file


def find_existing_mplab_logical_folder(
    root: ET.Element, folder_name: str
) -> ET.Element | None:
    existing_file_subdirs = [
        subdir
        for subdir in root.findall("logicalFolder")
        if subdir.attrib["name"] == folder_name
    ]
    return existing_file_subdirs[0] if len(existing_file_subdirs) > 0 else None


def add_file_to_mplab_logical_folder(folder: ET.Element, file: MplabFile) -> ET.Element:
    cwd = folder
    for file_subdir in file.xml_path.parts:
        if (
            found_subdir := find_existing_mplab_logical_folder(cwd, file_subdir)
        ) is not None:
            cwd = found_subdir
        else:
            cwd = insert_mplab_logical_folder(cwd, file_subdir)

    return insert_mplab_file(cwd, str(file.path))


def generate_mplab_project_folder_structure(
    sources: list[MplabFile], headers: list[MplabFile]
) -> ET.Element:
    root = create_mplab_logical_folder("root")
    external_files = insert_mplab_logical_folder(
        root, "ExternalFiles", "Important Files"
    )
    source_files = insert_mplab_logical_folder(root, "SourceFiles", "Source Files")
    header_files = insert_mplab_logical_folder(root, "HeaderFiles", "Header Files")

    for source in sources:
        add_file_to_mplab_logical_folder(source_files, source)

    for header in headers:
        add_file_to_mplab_logical_folder(header_files, header)

    add_file_to_mplab_logical_folder(
        external_files,
        MplabFile(path=Path("Makefile"), xml_path=Path(""), build_flags=[]),
    )

    return root


def generate_mplab_config_toolset(
    target_device: str,
    development_server: str = "localhost",
    target_header: str = "",
    target_plugin_board: str = "",
    platform_tool: str = "noID",
    language_toolchain: str = "XC32",
    language_toolchain_version: str = DEFAULT_XC32_TOOLCHAIN_VERSION,
    platform: int = 3,
):
    tools_set = ET.Element("toolsSet")
    ET.SubElement(tools_set, "developmentServer").text = development_server
    ET.SubElement(tools_set, "targetDevice").text = target_device.upper()
    ET.SubElement(tools_set, "targetHeader").text = target_header
    ET.SubElement(tools_set, "targetPluginBoard").text = target_plugin_board
    ET.SubElement(tools_set, "platformTool").text = platform_tool
    ET.SubElement(tools_set, "languageToolchain").text = language_toolchain
    ET.SubElement(
        tools_set, "languageToolchainVersion"
    ).text = language_toolchain_version
    ET.SubElement(tools_set, "platform").text = str(platform)
    return tools_set


def generate_mplab_config_properties(
    name: str,
    default_properties: dict[str, str],
    override_properties: dict[str, str] = {},
    additional_flags: list[str] = [],
) -> ET.Element:
    default_properties.update(override_properties)
    props = ET.Element(name)
    for key, value in default_properties.items():
        ET.SubElement(
            props,
            "property",
            attrib={
                "key": key,
                "value": value if value is not None and len(value) > 0 else "",
            },
        )
    if len(additional_flags) > 0:
        ET.SubElement(props, "appendMe", attrib={"value": " ".join(additional_flags)})
    return props


def generate_mplab_project_configuration(
    name: str,
    target_device: str,
    defines: list[str] = [],
    compiler_flags: list[str] = [],
    include_directories: list[str] = [],
    optimalization_level: str = "",
    additional_compiler_flags: list[str] = [],
) -> ET.Element:
    config = ET.Element("conf", attrib={"name": name, "type": "3"})
    config.append(generate_mplab_config_toolset(target_device))

    config.append(
        generate_mplab_config_properties(
            "C32",
            DEFAULT_C32_CONFIG,
            {
                "optimization-level": optimalization_level,
                "preprocessor-macros": ";".join(defines),
            },
            additional_compiler_flags,
        )
    )

    config.append(
        generate_mplab_config_properties(
            "C32-LD",
            DEFAULT_C32LD_CONFIG,
            {
                "optimization-level": optimalization_level,
                "preprocessor-macros": ";".join(defines),
            },
        )
    )

    config.append(
        generate_mplab_config_properties(
            "C32CPP",
            DEFAULT_C32CPP_CONFIG,
            {
                "optimization-level": optimalization_level,
                "preprocessor-macros": ";".join(defines),
            },
            additional_compiler_flags,
        )
    )

    config.append(
        generate_mplab_config_properties(
            "C32Global",
            DEFAULT_C32GLOBAL_CONFIG,
            {"common-include-directories": ";".join(include_directories)},
        )
    )

    return config


def generate_mplab_root_configurations_element():
    return ET.Element("configurationDescriptor", version="65")


def get_relative_path(path: Path | str, base_subpath: str) -> Path | None:
    path = str(path)
    subpath_index = path.find(base_subpath)
    if subpath_index >= 0:
        return Path(path[subpath_index + len(base_subpath) :])
    return None


def filter_build_flags(flags: list[str], filtered_prefixes: list[str]) -> list[str]:
    return [
        flag
        for flag in flags
        if not any(flag.startswith(prefix) for prefix in filtered_prefixes)
    ]


def find_build_flags(flags: list[str], prefixes_to_look_for: list[str]) -> list[str]:
    return [
        flag
        for flag in flags
        if any(flag.startswith(prefix) for prefix in prefixes_to_look_for)
    ]


def generate_mplab_project_configurations_xml(
    sources: list[MplabFile],
    headers: list[MplabFile],
    name: str,
    target_device: str,
) -> ET.Element:
    # generate file list
    config_root = generate_mplab_root_configurations_element()
    config_root.append(
        generate_mplab_project_folder_structure(sources=sources, headers=headers)
    )
    source_root_list = ET.SubElement(config_root, "sourceRootList")
    ET.SubElement(source_root_list, "Elem").text = "."
    ET.SubElement(config_root, "projectmakefile").text = "Makefile"

    # generate configurations, assume all files use the same compilation flags
    # list(set()) filters out duplicates
    filtered_compiler_flags = list(
        sorted(
            set(
                filter_build_flags(
                    sources[0].build_flags,
                    ["-I", "-D", "-c", "-o", "-O", "-mcpu", "-Werror"],
                )
            )
        )
    )
    optimalization_level = find_build_flags(sources[0].build_flags, ["-O"])[0]
    defines = list(
        sorted(
            set(
                [
                    define[2:]
                    for define in find_build_flags(sources[0].build_flags, ["-D"])
                ]
            )
        )
    )

    confs = ET.SubElement(config_root, "confs")
    confs.append(
        generate_mplab_project_configuration(
            name,
            target_device,
            include_directories=["lely-core/include"],
            additional_compiler_flags=filtered_compiler_flags,
            optimalization_level=optimalization_level,
            defines=defines,
        )
    )

    return config_root


def prettify_xml(element):
    rough_string = ET.tostring(element, "utf-8")
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")


def generate_project_xml(project_name: str) -> str:
    return """<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://www.netbeans.org/ns/project/1">
    <type>com.microchip.mplab.nbide.embedded.makeproject</type>
    <configuration>
        <data xmlns="http://www.netbeans.org/ns/make-project/1">
            <name>{project_name}</name>
            <creation-uuid>{project_uuid}</creation-uuid>
            <make-project-type>0</make-project-type>
            <make-dep-projects/>
            <sourceRootList/>
            <confList>
                <confElem>
                    <name>default</name>
                    <type>3</type>
                </confElem>
            </confList>
            <sourceEncoding>UTF-8</sourceEncoding>
            <formatting>
                <project-formatting-style>false</project-formatting-style>
            </formatting>
        </data>
    </configuration>
</project>""".format(project_name=project_name, project_uuid=str(uuid.uuid4()))


def parse_args() -> argparse.Namespace:
    script_dir = Path(__file__).parent.resolve()
    # this assumes that current script resides in "resources/mplab" subdirectory
    default_lely_core_path = (script_dir / "../LibCANopen/lely-core").resolve()
    default_makefile_path = (script_dir / "Makefile").resolve()

    parser = argparse.ArgumentParser(description="Generate MPLAB project")
    parser.add_argument(
        "compile_commands_path",
        type=str,
        help="Path to compile_commands.json file",
    )
    parser.add_argument(
        "project_path",
        type=str,
        help="Directory of generated project",
    )
    parser.add_argument(
        "platform",
        type=str,
        choices=["atsamv71q21b", "atsamv71q21rt", "atsamrh71f20c", "atsamrh707f18a"],
        help="Target platform",
    )
    parser.add_argument(
        "--lely-core-dir",
        type=str,
        default=default_lely_core_path.absolute(),
        help="Path to lely-core sources",
    )
    parser.add_argument(
        "--makefile-path",
        type=str,
        default=default_makefile_path.absolute(),
        help="Path to custom Makefile. If not provided, Makefile should be placed next to the script",
    )
    parser.add_argument(
        "--lely-config-path",
        type=str,
        default=None,
        help="Path to config.h file. If not provided, it's assumed that it's in the same directory as compile_commands.json",
    )
    return parser.parse_args()


def main():
    args = parse_args()

    lely_core_dir = Path(args.lely_core_dir).resolve()
    compile_commands_path = Path(args.compile_commands_path).resolve()
    print(f"LibCANopen directory:\t\t{lely_core_dir.absolute()}")
    print(f"compile_commands.json path:\t{compile_commands_path.absolute()}")

    lely_core_sources_dir = lely_core_dir / "lib"
    lely_core_includes_dir = lely_core_dir / "include"
    lely_core_headers = (
        list(lely_core_includes_dir.rglob("*.h"))
        + list(lely_core_includes_dir.rglob("*.hpp"))
        + list(lely_core_includes_dir.rglob("*.def"))
    )

    compile_commands = load_compile_commands_json(compile_commands_path)
    mplab_sources = [
        MplabFile.from_compile_command(
            command,
            base_path="resources/LibCANopen/",
            base_xml_path="resources/LibCANopen/lely-core/lib/",
        )
        for command in compile_commands
    ]

    mplab_headers = []

    for header in lely_core_headers:
        relative_header_path = get_relative_path(header, "resources/LibCANopen/")
        relative_xml_header_path = get_relative_path(
            header, "resources/LibCANopen/lely-core/include/"
        )

        if relative_header_path is None or relative_xml_header_path is None:
            raise RuntimeError(f"Invalid header: {header}")

        mplab_headers.append(
            MplabFile(
                path=relative_header_path,
                xml_path=relative_xml_header_path.parent,
                build_flags=[],
            )
        )

    configurations_xml = generate_mplab_project_configurations_xml(
        sources=mplab_sources,
        headers=mplab_headers,
        name="LibCANOpen",
        target_device=args.platform,
    )

    makefile_source_path = Path(args.makefile_path).resolve()
    print(f"Used makefile:\t\t\t{makefile_source_path.absolute()}")

    lely_core_config_path = (
        (Path(args.compile_commands_path).parent / "config.h").resolve()
        if args.lely_config_path is None
        else Path(args.lely_config_path).resolve()
    )

    print(f"Used lely-core config:\t\t{lely_core_config_path.absolute()}")

    project_dir = Path(args.project_path)
    print(f"Generating project in {project_dir.absolute()}")
    project_sources_dir = project_dir / "lely-core/lib"
    project_includes_dir = project_dir / "lely-core/include"
    makefile_target_path = project_dir / "Makefile"

    project_metadata_dir = project_dir / "nbproject"
    project_metadata_dir.mkdir(parents=True, exist_ok=True)

    configurations_xml_path = project_metadata_dir / "configurations.xml"
    project_xml_path = project_metadata_dir / "project.xml"

    with open(configurations_xml_path, "w") as out:
        out.write(prettify_xml(configurations_xml))
    with open(project_xml_path, "w") as out:
        out.write(generate_project_xml("LibCANOpen"))
    shutil.copy(makefile_source_path, makefile_target_path)
    shutil.copytree(lely_core_sources_dir, project_sources_dir, dirs_exist_ok=True)
    shutil.copytree(lely_core_includes_dir, project_includes_dir, dirs_exist_ok=True)
    shutil.copy(lely_core_config_path, project_includes_dir / "config.h")

    print("Done!")


if __name__ == "__main__":
    main()
