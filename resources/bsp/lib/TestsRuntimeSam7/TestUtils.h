/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_BSP_TESTSRUNTIME_TESTUTILS_H
#define N7S_BSP_TESTSRUNTIME_TESTUTILS_H

/// \file TestUtils.h
/// \brief Utils for tests for SAMV71Q21 and SAMRH71Q21 device.
/// \details Provides config procedures and simple utilities, should be included only in C files.

#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(N7S_TARGET_SAMV71Q21)
// cppcheck-suppress misra-c2012-8.9
static const Pio_Port portLed0 = Pio_Port_A;
// cppcheck-suppress misra-c2012-8.9
static const Pio_Port portLed1 = Pio_Port_C;
// cppcheck-suppress misra-c2012-8.9
static const uint32_t pinMaskLed0 = PIO_PIN_23;
// cppcheck-suppress misra-c2012-8.9
static const uint32_t pinMaskLed1 = PIO_PIN_9;
// cppcheck-suppress misra-c2012-8.9
static const Pmc_PeripheralId periphLed0 = Pmc_PeripheralId_PioA;
// cppcheck-suppress misra-c2012-8.9
static const Pmc_PeripheralId periphLed1 = Pmc_PeripheralId_PioC;
#elif defined(N7S_TARGET_SAMRH71F20)
// cppcheck-suppress misra-c2012-8.9
static const Pio_Port portLed0 = Pio_Port_B;
// cppcheck-suppress misra-c2012-8.9
static const Pio_Port portLed1 = Pio_Port_B;
// cppcheck-suppress misra-c2012-8.9
static const uint32_t pinMaskLed0 = PIO_PIN_19;
// cppcheck-suppress misra-c2012-8.9
static const uint32_t pinMaskLed1 = PIO_PIN_23;
// cppcheck-suppress misra-c2012-8.9
static const Pmc_PeripheralId periphLed0 = Pmc_PeripheralId_PioA;
// cppcheck-suppress misra-c2012-8.9
static const Pmc_PeripheralId periphLed1 = Pmc_PeripheralId_PioA;
#elif defined(N7S_TARGET_SAMRH707F18)
// cppcheck-suppress misra-c2012-8.9
static const Pio_Port portLed0 = Pio_Port_B;
// cppcheck-suppress misra-c2012-8.9
static const Pio_Port portLed1 = Pio_Port_B;
// cppcheck-suppress misra-c2012-8.9
static const uint32_t pinMaskLed0 = PIO_PIN_11;
// cppcheck-suppress misra-c2012-8.9
static const uint32_t pinMaskLed1 = PIO_PIN_12;
// cppcheck-suppress misra-c2012-8.9
static const Pmc_PeripheralId periphLed0 = Pmc_PeripheralId_PioA;
// cppcheck-suppress misra-c2012-8.9
static const Pmc_PeripheralId periphLed1 = Pmc_PeripheralId_PioA;
#endif

/// \brief Enumeration listing possible LEDs selection.
typedef enum {
	Led_Select_0 = 0, ///< LED0
	Led_Select_1 = 1, ///< LED1
} Led_Select;

/// \brief Structure representing LED configuration.
typedef struct {
	Pmc_PeripheralId peripheralId; ///< Peripheral Id.
	Pio_Port port; ///< Pio port.
	uint32_t pinMask; ///< Pio Pin mask.
} Led_Config;

static inline Led_Config
getLedConfig(const Led_Select ledSelect)
{
	Led_Config config = { .peripheralId = periphLed0,
		.port = portLed0,
		.pinMask = pinMaskLed0 };

	switch (ledSelect) {
	case Led_Select_0:
		config.peripheralId = periphLed0;
		config.port = portLed0;
		config.pinMask = pinMaskLed0;
		break;

	case Led_Select_1:
		config.peripheralId = periphLed1;
		config.port = portLed1;
		config.pinMask = pinMaskLed1;
		break;
	}

	return config;
}

static inline void
setOnLed(const Led_Select ledSelect)
{
	const Led_Config ledConfig = getLedConfig(ledSelect);

	Pio pio;
	ErrorCode errCode = ErrorCode_NoError;
	(void)Pio_init(ledConfig.port, &pio, &errCode);

	(void)Pio_setPins(&pio, ledConfig.pinMask);
}

static inline void
setOffLed(const Led_Select ledSelect)
{
	const Led_Config ledConfig = getLedConfig(ledSelect);

	Pio pio;
	ErrorCode errCode = ErrorCode_NoError;
	(void)Pio_init(ledConfig.port, &pio, &errCode);

	Pio_resetPins(&pio, ledConfig.pinMask);
}

static inline void
initLed(const Led_Select ledSelect)
{
	const Led_Config ledConfig = getLedConfig(ledSelect);

	Pio pio;
	ErrorCode errCodeInit = ErrorCode_NoError;
	(void)Pio_init(ledConfig.port, &pio, &errCodeInit);

	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	Pmc_enablePeripheralClk(&pmc, ledConfig.peripheralId);

#ifdef N7S_TARGET_SAMV71Q21
	const Pio_Pin_Config pinConfig = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_None,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	const Pio_Pin_Config pinConfig = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_None,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};
#endif

	ErrorCode errCodeSetConfig = ErrorCode_NoError;
	(void)Pio_setPinsConfig(
			&pio, ledConfig.pinMask, &pinConfig, &errCodeSetConfig);

	setOffLed(ledSelect);
}

// Simple blinking of LED (actual delays between blinks may differ based on optimization option)
static inline void
blinkLed(const Led_Select ledSelect, const uint32_t delay)
{
	setOnLed(ledSelect);

	for (uint32_t i = 0; i < delay; i++) {
		asm volatile("nop");
	}

	setOffLed(ledSelect);

	for (uint32_t i = 0; i < delay; i++) {
		asm volatile("nop");
	}
}

static inline bool
performMatsMemorySelfTest(volatile uint32_t *const memory, const uint32_t words,
		volatile uint32_t **const failingAddress,
		uint32_t *const expectedValue, uint32_t *const readValue)
{
	for (uint32_t word = 0u; word < words; word++)
		memory[word] = 0u;

	for (uint32_t word = 0u; word < words; word++) {
		const uint32_t value = memory[word];
		if (value != 0u) {
			*expectedValue = 0u;
			*readValue = value;
			*failingAddress = &(memory[word]);
			return true;
		}
		memory[word] = 0xffffffffu;
	}

	for (uint32_t word = 0u; word < words; word++) {
		const uint32_t value = memory[word];
		if (value != 0xffffffffu) {
			*expectedValue = 0xffffffffu;
			*readValue = value;
			*failingAddress = &(memory[word]);
			return true;
		}
	}

	for (uint32_t word = 0u; word < words; word++)
		memory[word] = 0u;

	return false;
}

static inline bool
testMemoryAccess8Bit(volatile uint8_t *const memory, const uint32_t words)
{
	const uint32_t testPasses = 2u;
	const uint32_t valueLimit = (uint32_t)UINT8_MAX + 1u;

	for (uint32_t i = 0; i < words; i++)
		memory[i] = (uint8_t)(i % valueLimit);

	for (uint32_t test = 0; test < testPasses; test++)
		for (uint32_t i = 0; i < words; i++)
			if (memory[i] != (uint8_t)(i % valueLimit))
				return false;

	return true;
}

static inline bool
testMemoryAccess16Bit(volatile uint16_t *const memory, const uint32_t words)
{
	const uint32_t testPasses = 2u;
	const uint32_t valueLimit = (uint32_t)UINT16_MAX + 1u;

	for (uint32_t i = 0; i < words; i++)
		memory[i] = (uint16_t)(i % valueLimit);

	for (uint32_t test = 0; test < testPasses; test++)
		for (uint32_t i = 0; i < words; i++)
			if (memory[i] != (uint16_t)(i % valueLimit))
				return false;

	return true;
}

static inline bool
testMemoryAccess32Bit(volatile uint32_t *const memory, const uint32_t words)
{
	const uint32_t testPasses = 2u;
	const uint32_t smallPrime = 313u;

	for (uint32_t i = 0; i < words; i++)
		memory[i] = i * smallPrime;

	for (uint32_t test = 0; test < testPasses; test++)
		for (uint32_t i = 0; i < words; i++)
			if (memory[i] != (i * smallPrime))
				return false;

	return true;
}

static inline bool
testMemoryAccessMixed32to16Bit(volatile uint32_t *const memory32,
		const volatile uint16_t *const memory16, const uint32_t words32)
{
	const uint32_t testPasses = 2u;
	const uint32_t smallPrime = 313u;
	const uint32_t valueLimit16 = (uint32_t)UINT16_MAX + 1u;

	for (uint32_t i = 0; i < words32; i++)
		memory32[i] = (i % valueLimit16)
				+ (((i + smallPrime) % valueLimit16) << 16u);

	for (uint32_t test = 0; test < testPasses; test++) {
		for (uint32_t i = 0; i < words32; i++) {
			const uint16_t lower =
					memory16[i * 2u]; // Read lower 16 bits.
			const uint16_t higher = memory16[(i * 2u)
					+ 1u]; // Read higher 16 bits.

			if (lower != (uint16_t)(i % valueLimit16))
				return false;
			if (higher
					!= (uint16_t)((i + smallPrime)
							% valueLimit16))
				return false;
		}
	}

	return true;
}

static inline bool
testMemoryAccessMixed16to32Bit(volatile uint16_t *const memory16,
		const volatile uint32_t *const memory32, const uint32_t words32)
{
	const uint32_t testPasses = 2u;
	const uint32_t smallPrime = 313u;
	const uint32_t valueLimit16 = (uint32_t)UINT16_MAX + 1u;

	for (uint32_t i = 0; i < words32; i++) {
		memory16[i * 2u] = (uint16_t)((i + smallPrime)
				% valueLimit16); // Write lower 16 bits.
		memory16[(i * 2u) + 1u] = (uint16_t)(i
				% valueLimit16); // Write higher 16 bits.
	}

	for (uint32_t test = 0; test < testPasses; test++)
		for (uint32_t i = 0; i < words32; i++)
			if (memory32[i]
					!= (((i + smallPrime) % valueLimit16)
							+ ((i % valueLimit16)
									<< 16u)))
				return false;

	return true;
}

#ifdef __cplusplus
} // extern "C"
#endif

#endif // N7S_BSP_TESTSRUNTIME_TESTUTILS_H
