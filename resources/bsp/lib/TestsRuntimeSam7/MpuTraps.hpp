/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_BSP_TESTSRUNTIME_MPUTRAPS_HPP
#define N7S_BSP_TESTSRUNTIME_MPUTRAPS_HPP

/// \file MpuTraps.hpp
/// \brief Utils for setting up traps using MPU.
/// \details Provides ability to check accesses to selected addresses.

#include <functional>
#include <n7s/bsp/Mpu/Mpu.h>

enum class MpuTrapType {
	Write,
	Access,
};

using MpuTrapAction = std::function<void()>;

struct MpuTrapConfig {
#ifdef CLANG_TIDY
	// ifdef can be dropped (with contents) after upgrading clang to >=16

	MpuTrapConfig() = default;

	MpuTrapConfig(const volatile uint32_t *const address_,
			const MpuTrapType type_, MpuTrapAction action_)
	    : address(address_), type(type_), action(std::move(action_))
	{
	}
#endif

	const volatile uint32_t *address{};
	MpuTrapType type{};
	MpuTrapAction action{};
};

class MpuTraps
{
      public:
	constexpr static uint8_t REGION = 4u;

	explicit MpuTraps() noexcept;

	MpuTraps(const MpuTraps &) = delete;
	MpuTraps(MpuTraps &&) = delete;
	MpuTraps &operator=(const MpuTraps &) = delete;
	MpuTraps &operator=(MpuTraps &&) = delete;

	~MpuTraps() noexcept;

	void registerInstance() noexcept;
	static void interruptHandler() noexcept;

	void handleInterrupt() noexcept;

	void onWrite(const volatile uint32_t *const reg,
			const MpuTrapAction &callback) noexcept;
	void onAccess(const volatile uint32_t *const reg,
			const MpuTrapAction &callback) noexcept;

	void release() noexcept;

      private:
	static Mpu_RegionConfig makeConfig(
			const volatile uint32_t *const address,
			const Mpu_RegionAccess access);

	static inline MpuTraps *instance = nullptr;

	Mpu mpu{};
	MpuTrapAction currentCallback{};
};

#endif // N7S_BSP_TESTSRUNTIME_MPUTRAPS_HPP
