/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef MALLOC_ALIGNMENT
#define MALLOC_ALIGNMENT (sizeof(size_t))
#endif

extern void *_sbrk(const intptr_t incr); // NOLINT

void *
malloc(const size_t size)
{
	// first address of allocated block will contain it's size,
	// hence `+ sizeof(size_t)`.
	const size_t aligned_size = size + sizeof(size_t)
			+ (MALLOC_ALIGNMENT - (size % MALLOC_ALIGNMENT));

	size_t *const ptr = _sbrk(aligned_size);
	if (ptr == NULL)
		return NULL;

	// save the allocated block size for `realloc` and return next address
	*ptr = size;
	return (void *)(ptr + 1);
}

void *
calloc(const size_t num, const size_t size)
{
	const size_t block_size = num * size;
	void *memory = malloc(block_size);
	if (memory == NULL)
		return NULL;

	memset(memory, 0, block_size);
	return memory;
}

void *
realloc(void *const ptr, const size_t new_size)
{
	void *new_ptr = malloc(new_size);
	if (new_ptr == NULL)
		return NULL;

	// see `malloc` for details
	size_t const *block_size = ptr;
	--block_size;

	memcpy(new_ptr, (void *)ptr, *block_size);
	free(ptr);
	return new_ptr;
}

void
free(void *const ptr)
{
	// remove the metadata of allocated memory block
	// see `malloc` for details
	size_t *block_size = ptr;
	--block_size;
	*block_size = 0;
}
