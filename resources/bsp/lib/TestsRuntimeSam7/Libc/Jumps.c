/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <setjmp.h>

// The following assembly code is part of `pic32c` library from XC32 toolchain
// that was reimplemented here to build binaries without linking the whole library.

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

int __attribute__((naked))
setjmp(jmp_buf env)
{
	asm volatile("mov ip, sp\n"
		     "stmia.w r0!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr}\n"
		     "mov.w r0, #0\n"
		     "bx lr\n");
}

void __attribute__((naked))
longjmp(jmp_buf env, int status)
{
	asm volatile("ldmia.w r0!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr}\n"
		     "mov sp, ip\n"
		     "movs r0, r1\n"
		     "it eq\n"
		     "moveq r0, #1\n"
		     "bx lr\n"
		     "nop\n");
}

#pragma GCC diagnostic pop
