/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <SEGGER_RTT.h>
#include <assert.h>
#include <string.h>

#include <n7s/bsp/Fpu/Fpu.h>
#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Pmc/ChipConfig.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Wdt/Wdt.h>

#include <TestsRuntime/TestsRuntime.h>

#if defined(N7S_TARGET_SAMV71Q21)
#define EXPECTED_RAM_SIZE 0x60000u
#define MPU_REGION_1_OFFSET 0x40000u
#define MPU_REGION_0_SIZE 17
#define MPU_REGION_1_SIZE 16
#elif defined(N7S_TARGET_SAMRH71F20)
#define EXPECTED_RAM_SIZE 0xC0000u
#define MPU_REGION_1_OFFSET 0x80000u
#define MPU_REGION_0_SIZE 18
#define MPU_REGION_1_SIZE 17
#elif defined(N7S_TARGET_SAMRH707F18)
#define EXPECTED_RAM_SIZE 0x20000u
#define MPU_REGION_1_OFFSET 0x0u
#define MPU_REGION_0_SIZE 15
#define MPU_REGION_1_SIZE 4
#else

#error "Missing N7S_TARGET_* macro"
#endif

extern const uint32_t _ram_start; // NOLINT
extern const uint32_t _ram_end; // NOLINT
extern const uint32_t RTT_NOCACHE_SIZE;

#define EXPECTED_RTT_BUFFER_SIZE 1024u
#define RTT_MPU_REGION_SIZE 9u

static void
configureClocks(void)
{
	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	const Pmc_Config config = Pmc_ChipConfig_getConfig();
	const bool result =
			Pmc_setConfig(&pmc, &config, PMC_DEFAULT_TIMEOUT, NULL);
	assert(result);
	(void)result;
}

static void
configureMPU(void)
{
	Mpu mpu;
	Mpu_init(&mpu);

	const uint32_t ram_start =
			(uint32_t)(&_ram_start); // cppcheck-suppress [misra-c2012-11.4]
	const uint32_t ram_end =
			(uint32_t)(&_ram_end); // cppcheck-suppress [misra-c2012-11.4]
	const uint32_t rtt_size =
			(uint32_t)(&RTT_NOCACHE_SIZE); // cppcheck-suppress [misra-c2012-11.4]

	assert(((ram_end - ram_start) == EXPECTED_RAM_SIZE)
			&& "RAM memory size is not as expected, MPU config below is probably invalid");

	assert((rtt_size == EXPECTED_RTT_BUFFER_SIZE)
			&& "Size of non-cacheable block changed, number 10 below needs to be changed as well");

	Mpu_Config mpuConf = { .isEnabled = true,
		.isDefaultMemoryMapEnabled = true,
		.isMpuEnabledInHandlers = false };
	Mpu_setConfig(&mpu, &mpuConf);

	Mpu_RegionConfig mpuRegionConf = {
		.address = ram_start,
		.isEnabled = true,
		.size = MPU_REGION_0_SIZE,
		.subregionDisableMask = 0x00,
		.isShareable = true,
		.isExecutable = true,
		.memoryType = Mpu_RegionMemoryType_Normal,
		.innerCachePolicy =
				Mpu_RegionCachePolicy_WriteBackReadWriteAllocate,
		.outerCachePolicy =
				Mpu_RegionCachePolicy_WriteBackReadWriteAllocate,
		.privilegedAccess = Mpu_RegionAccess_ReadWrite,
		.unprivilegedAccess = Mpu_RegionAccess_ReadWrite,
	};
	Mpu_setRegionConfig(&mpu, 0, &mpuRegionConf);

	mpuRegionConf.address = ram_start + MPU_REGION_1_OFFSET;
	mpuRegionConf.size = MPU_REGION_1_SIZE;
	Mpu_setRegionConfig(&mpu, 1, &mpuRegionConf);

	// Mark RTT section (.rtt_nocache) as non-cacheable memory region with strong ordering
	mpuRegionConf.address = ram_end - rtt_size;
	mpuRegionConf.size = RTT_MPU_REGION_SIZE;
	mpuRegionConf.memoryType = Mpu_RegionMemoryType_StronglyOrdered;
	mpuRegionConf.innerCachePolicy = Mpu_RegionCachePolicy_NonCacheable;
	mpuRegionConf.outerCachePolicy = Mpu_RegionCachePolicy_NonCacheable;
	Mpu_setRegionConfig(&mpu, 2, &mpuRegionConf);

	(void)Scb_invalidateDCache();
}

static void
configureAndDisableWatchdog(void)
{
	const uint32_t wdtCounterValue = 0xFFF;
	const uint32_t wdtDeltaValue = 0xFFF;

	Wdt wdt;
	Wdt_init(&wdt);
	Wdt_Config config;
	config.isDisabled = true;
	config.isFaultInterruptEnabled = false;
	config.isResetEnabled = false;
	config.counterValue = wdtCounterValue;
	config.deltaValue = wdtDeltaValue;
	config.isHaltedOnDebug = false;
	config.isHaltedOnIdle = false;
#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	config.doesFaultActivateProcessorReset = true;
#endif
	Wdt_setConfig(&wdt, &config);
}

static void
configureFpu(void)
{
	Fpu fpu;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
}

// cppcheck-suppress [misra-c2012-8.4]
void
TestsRuntime_startup(const TestsRuntime_StartupArg *const arg)
{
	if (arg->disableWatchdog == true)
		configureAndDisableWatchdog();

	configureFpu();

	configureClocks();
	configureMPU();

	(void)Scb_enableICache();
	(void)Scb_enableDCache();

	SEGGER_RTT_Init();
}

// cppcheck-suppress [misra-c2012-8.4]
void
TestsRuntime_writeData(const uint8_t *data, const uint32_t count)
{
	const uint32_t written = SEGGER_RTT_Write(0, data, count);
	assert(written == count);
	(void)written;
}
