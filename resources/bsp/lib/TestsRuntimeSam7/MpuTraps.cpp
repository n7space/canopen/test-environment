/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MpuTraps.hpp"

#include <cassert>

#include <n7s/bsp/Scb/Scb.h>

MpuTraps::MpuTraps() noexcept
{
	Mpu_init(&mpu);

	const Mpu_Config mpuConf = {
		.isEnabled = true,
		.isDefaultMemoryMapEnabled = true,
		.isMpuEnabledInHandlers = true,
	};
	Mpu_setConfig(&mpu, &mpuConf);

	Scb_setMemoryManagementExceptionEnabled(true);
}

MpuTraps::~MpuTraps() noexcept
{
	release();

	if (instance == this)
		instance = nullptr;
}

void
MpuTraps::handleInterrupt() noexcept
{
	const auto callback = currentCallback;

	release();

	if (callback)
		callback();

	(void)Scb_invalidateDCache();
	MEMORY_SYNC_BARRIER();
}

namespace
{
uint32_t
align32(const uint32_t address)
{
	// cut off 5 LSB
	return address & 0xFFFFFFE0u;
}
} // namespace

Mpu_RegionConfig
MpuTraps::makeConfig(const volatile uint32_t *const address,
		const Mpu_RegionAccess access)
{
	return {
		.address = align32(reinterpret_cast<uintptr_t>(address)),
		.isEnabled = true,
		.size = 4u, // minimum region length is is 32 bytes: 2^(4 + 1)
		.subregionDisableMask = 0x00,
		.isShareable = true,
		.isExecutable = true,
		.memoryType = Mpu_RegionMemoryType_Normal,
		.innerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.outerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.privilegedAccess = access,
		.unprivilegedAccess = access,
	};
}

void
MpuTraps::onWrite(const volatile uint32_t *const reg,
		const MpuTrapAction &callback) noexcept
{
	currentCallback = callback;

	const auto conf = makeConfig(reg, Mpu_RegionAccess_ReadOnly);
	Mpu_setRegionConfig(&mpu, REGION, &conf);
}

void
MpuTraps::onAccess(const volatile uint32_t *const reg,
		const MpuTrapAction &callback) noexcept
{
	currentCallback = callback;

	const auto conf = makeConfig(reg, Mpu_RegionAccess_NoAccess);
	Mpu_setRegionConfig(&mpu, REGION, &conf);
}

void
MpuTraps::release() noexcept
{
	currentCallback = nullptr;

	const Mpu_RegionConfig disabledConf{};
	Mpu_setRegionConfig(&mpu, REGION, &disabledConf);
}

void
MpuTraps::registerInstance() noexcept
{
	assert(instance == nullptr);
	instance = this;
}

void
MpuTraps::interruptHandler() noexcept
{
	if (instance != nullptr)
		instance->handleInterrupt();
}
