/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// parts of the file are derived from the "GCC startup file for ATSAMV71Q21/ATSAMRH71F20B"
// distributed on Apache License, Version 2.0
//
// Copyright (c) 2017 Atmel Corporation, a wholly owned subsidiary of Microchip Technology Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TestsStartup.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Nvic/NvicVectorTable.h>

// Symbols form linker script
extern uint32_t _etext; // NOLINT
extern uint32_t _srelocate; // NOLINT
extern uint32_t _erelocate; // NOLINT
extern uint32_t _szero; // NOLINT
extern uint32_t _ezero; // NOLINT
#ifdef __XC32__
extern uint32_t _stack; // NOLINT
#else
extern uint32_t _estack; // NOLINT
#endif
extern uint32_t _rtt_start; // NOLINT
extern uint32_t _rtt_end; // NOLINT

extern const Nvic_VectorTable exception_table;

int main(void);

typedef void (*TestsStartup_Hook)(void);

extern TestsStartup_Hook __preinit_array_start[]
		__attribute__((weak)); // NOLINT
extern TestsStartup_Hook __preinit_array_end[] __attribute__((weak)); // NOLINT
extern TestsStartup_Hook __init_array_start[] __attribute__((weak)); // NOLINT
extern TestsStartup_Hook __init_array_end[] __attribute__((weak)); // NOLINT

#ifdef __XC32__
extern void __attribute__((long_call)) __libc_init_array(void);
extern void __attribute__((weak, long_call)) __xc32_on_reset(void);
extern void __attribute__((weak, long_call)) __xc32_on_bootstrap(void);

#else
void _init(void); // provided by libgcc //NOLINT

static void
execute_init_array(void)
{
	// cppcheck-suppress [misra-c2012-18.4]
	const ptrdiff_t preinit_count =
			__preinit_array_end - __preinit_array_start;
	for (ptrdiff_t i = 0; i < preinit_count; i++)
		__preinit_array_start[i]();

	_init();

	// cppcheck-suppress [misra-c2012-18.4]
	const ptrdiff_t init_count = __init_array_end - __init_array_start;
	for (ptrdiff_t i = 0; i < init_count; i++)
		__init_array_start[i]();
}
#endif

void __attribute__((naked, aligned(sizeof(uint64_t))))
Reset_Handler(void)
{
	// Disable interrupts, reset masks
	asm volatile("cpsid i \n"
		     "mov r0, 0 \n"
		     "msr basepri, r0 \n");

	// Integer unit registers initialization
	asm volatile("mov r0, 0 \n"
		     "mov r1, 0 \n"
		     "mov r2, 0 \n"
		     "mov r3, 0 \n"
		     "mov r4, 0 \n"
		     "mov r5, 0 \n"
		     "mov r0, 0 \n"
		     "mov r6, 0 \n"
		     "mov r7, 0 \n"
		     "mov r8, 0 \n"
		     "mov r9, 0 \n"
		     "mov r10, 0 \n"
		     "mov r11, 0 \n"
		     "mov r12, 0 \n");

	// Stack initialization
#ifdef __XC32__
	asm volatile("ldr sp, StackEnd \n"
		     "b %[Tail] \n"
		     "StackEnd: .word _stack\n"
			:
			: [Tail] "i"(&Reset_HandlerTail));
#else
	asm volatile("ldr sp, StackEnd \n"
		     "b %[Tail] \n"
		     "StackEnd: .word _estack\n"
			:
			: [Tail] "i"(&Reset_HandlerTail));
#endif
}

void
Reset_HandlerTail(void)
{
#ifdef __XC32__
	if (__xc32_on_reset)
		__xc32_on_reset();
#endif

	// Initialize the relocate segment.
	const uint32_t *src = &_etext;

	for (uint32_t *dest = &_srelocate; dest < &_erelocate; ++dest) {
		*dest = *src; // cppcheck-suppress [misra-c2012-14.2]
		++src;
	}

	// Clear the zero segment.
	for (uint32_t *dest = &_szero; dest < &_ezero; dest++)
		*dest = 0; // cppcheck-suppress [misra-c2012-14.2]

	// Clear the RTT segment
	for (uint32_t *dest = &_rtt_start; dest < &_rtt_end; dest++)
		*dest = 0; // cppcheck-suppress [misra-c2012-14.2]

	// Set the vector table base address.
	Nvic_relocateVectorTableUnsafe(&exception_table);
	Nvic_enableIrq();
	Nvic_enableFaultIrq();

	// Initialize the C library and global objects.
#ifdef __XC32__
	__libc_init_array();
#else
	execute_init_array();
#endif

#ifdef __XC32__
	if (__xc32_on_bootstrap)
		__xc32_on_bootstrap();
#endif

	// Branch to main function.
	const int result = main();

	// exit implemented by TestsRuntime
	exit(result); // cppcheck-suppress [misra-c2012-21.8]

	// Infinite loop - wait for test to be stopped
	while (true)
		;
}
