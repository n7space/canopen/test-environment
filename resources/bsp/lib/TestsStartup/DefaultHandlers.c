/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>

#include "TestsStartup.h"

// cppcheck-suppress [misra-c2012-20.7]
#define DEFAULT_HANDLER(name) \
	void name(void) __attribute__((weak, alias("Default_Handler")))

void
Default_Handler(void)
{
	asm("BKPT #255");
	while (true)
		;
}

DEFAULT_HANDLER(Reset_Handler);
DEFAULT_HANDLER(Reset_HandlerTail);
DEFAULT_HANDLER(NonMaskableInt_Handler);
DEFAULT_HANDLER(HardFault_Handler);
DEFAULT_HANDLER(MemManage_Handler);
DEFAULT_HANDLER(BusFault_Handler);
DEFAULT_HANDLER(UsageFault_Handler);
DEFAULT_HANDLER(SVCall_Handler);
DEFAULT_HANDLER(DebugMonitor_Handler);
DEFAULT_HANDLER(PendSV_Handler);
DEFAULT_HANDLER(SysTick_Handler);
DEFAULT_HANDLER(SUPC_Handler);
DEFAULT_HANDLER(RSTC_Handler);
DEFAULT_HANDLER(RTC_Handler);
DEFAULT_HANDLER(RTT_Handler);
DEFAULT_HANDLER(WDT_Handler);
DEFAULT_HANDLER(PMC_Handler);
DEFAULT_HANDLER(PIOA_Handler);
DEFAULT_HANDLER(PIOB_Handler);
DEFAULT_HANDLER(PIOC_Handler);
DEFAULT_HANDLER(PIOD_Handler);
DEFAULT_HANDLER(TC0CH0_Handler);
DEFAULT_HANDLER(TC0CH1_Handler);
DEFAULT_HANDLER(TC0CH2_Handler);
DEFAULT_HANDLER(TC1CH0_Handler);
DEFAULT_HANDLER(TC1CH1_Handler);
DEFAULT_HANDLER(TC1CH2_Handler);
DEFAULT_HANDLER(TC2CH0_Handler);
DEFAULT_HANDLER(TC2CH1_Handler);
DEFAULT_HANDLER(TC2CH2_Handler);
DEFAULT_HANDLER(PWM0_Handler);
DEFAULT_HANDLER(PWM1_Handler);
DEFAULT_HANDLER(MCAN0_INT0_Handler);
DEFAULT_HANDLER(MCAN0_INT1_Handler);
DEFAULT_HANDLER(MCAN1_INT0_Handler);
DEFAULT_HANDLER(MCAN1_INT1_Handler);
DEFAULT_HANDLER(ICM_Handler);
DEFAULT_HANDLER(TRNG_Handler);
DEFAULT_HANDLER(XDMAC_Handler);
DEFAULT_HANDLER(ARM_FPU_Handler);
DEFAULT_HANDLER(ARM_IXC_Handler);
DEFAULT_HANDLER(ARM_CCW_Handler);
DEFAULT_HANDLER(ARM_CCF_Handler);

#if defined(N7S_TARGET_SAMV71Q21)
DEFAULT_HANDLER(EFC_Handler);
DEFAULT_HANDLER(UART0_Handler);
DEFAULT_HANDLER(UART1_Handler);
#endif

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMV71Q21)
DEFAULT_HANDLER(PIOE_Handler);
DEFAULT_HANDLER(TC3CH0_Handler);
DEFAULT_HANDLER(TC3CH1_Handler);
DEFAULT_HANDLER(TC3CH2_Handler);
DEFAULT_HANDLER(GMAC_Handler);
DEFAULT_HANDLER(GMACQ1_Handler);
DEFAULT_HANDLER(GMACQ2_Handler);
DEFAULT_HANDLER(GMACQ3_Handler);
DEFAULT_HANDLER(GMACQ4_Handler);
DEFAULT_HANDLER(GMACQ5_Handler);
DEFAULT_HANDLER(QSPI_Handler);

#endif

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH707F18)
DEFAULT_HANDLER(DACC_Handler);
#endif

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
DEFAULT_HANDLER(MATRIX0_Handler);
DEFAULT_HANDLER(FLEXCOM0_Handler);
DEFAULT_HANDLER(FLEXCOM1_Handler);
DEFAULT_HANDLER(FLEXCOM2_Handler);
DEFAULT_HANDLER(FLEXCOM3_Handler);
DEFAULT_HANDLER(NMIC_Handler);
DEFAULT_HANDLER(TCMRAM_FIX_Handler);
DEFAULT_HANDLER(TCMRAM_NOFIX_Handler);
DEFAULT_HANDLER(FlexRAM_FIX_Handler);
DEFAULT_HANDLER(FlexRAM_NOFIX_Handler);
DEFAULT_HANDLER(SHA_Handler);
DEFAULT_HANDLER(WDT1_Handler);
DEFAULT_HANDLER(HEFC_Handler);
DEFAULT_HANDLER(HEFC_FIX_Handler);
DEFAULT_HANDLER(HEFC_NOFIX_Handler);
DEFAULT_HANDLER(HEMC_Handler);
DEFAULT_HANDLER(HEMC_FIX_Handler);
DEFAULT_HANDLER(HEMC_NOFIX_Handler);
DEFAULT_HANDLER(SFR_Handler);
DEFAULT_HANDLER(SPW_INT0_Handler);
DEFAULT_HANDLER(IP1553_Handler);
#endif

#if defined(N7S_TARGET_SAMV71Q21)
DEFAULT_HANDLER(UART2_Handler);
DEFAULT_HANDLER(UART3_Handler);
DEFAULT_HANDLER(UART4_Handler);
DEFAULT_HANDLER(USART0_Handler);
DEFAULT_HANDLER(USART1_Handler);
DEFAULT_HANDLER(USART2_Handler);
DEFAULT_HANDLER(HSMCI_Handler);
DEFAULT_HANDLER(TWIHS0_Handler);
DEFAULT_HANDLER(TWIHS1_Handler);
DEFAULT_HANDLER(TWIHS2_Handler);
DEFAULT_HANDLER(SPI0_Handler);
DEFAULT_HANDLER(SPI1_Handler);
DEFAULT_HANDLER(SSC_Handler);
DEFAULT_HANDLER(AFEC0_Handler);
DEFAULT_HANDLER(AFEC1_Handler);
DEFAULT_HANDLER(ACC_Handler);
DEFAULT_HANDLER(USBHS_Handler);
DEFAULT_HANDLER(MLB_INT0_Handler);
DEFAULT_HANDLER(MLB_INT1_Handler);
DEFAULT_HANDLER(AES_Handler);
DEFAULT_HANDLER(ISI_Handler);
DEFAULT_HANDLER(SDRAMC_Handler);
DEFAULT_HANDLER(RSWDT_Handler);
DEFAULT_HANDLER(I2SC0_Handler);
DEFAULT_HANDLER(I2SC1_Handler);

#endif

#if defined(N7S_TARGET_SAMRH71F20)
DEFAULT_HANDLER(FLEXCOM4_Handler);
DEFAULT_HANDLER(FLEXCOM5_Handler);
DEFAULT_HANDLER(FLEXCOM6_Handler);
DEFAULT_HANDLER(FLEXCOM7_Handler);
DEFAULT_HANDLER(FLEXCOM8_Handler);
DEFAULT_HANDLER(FLEXCOM9_Handler);
DEFAULT_HANDLER(PIOF_Handler);
DEFAULT_HANDLER(PIOG_Handler);
#endif

#if defined(N7S_TARGET_SAMRH707F18)
DEFAULT_HANDLER(CRCCU_Handler);
DEFAULT_HANDLER(ADC_Handler);
DEFAULT_HANDLER(PCC_Handler);
#endif
