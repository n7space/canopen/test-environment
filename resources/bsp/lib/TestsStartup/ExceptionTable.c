/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// parts of the file are derived from the "GCC startup file for ATSAMV71Q21"
// distributed on Apache License, Version 2.0
//
// Copyright (c) 2017 Atmel Corporation, a wholly owned subsidiary of Microchip Technology Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <n7s/bsp/Nvic/NvicVectorTable.h>

#include "TestsStartup.h"

// From linker script
#ifdef __XC32__
extern uint32_t _stack; // NOLINT
#else
extern uint32_t _estack; // NOLINT
#endif

// cppcheck-suppress [misra-c2012-8.4]
const Nvic_VectorTable exception_table __attribute__((section(".vectors"))) = {
// configure stack pointer using linker symbols
#ifdef __XC32__
  .initialStackPointer = (void*)(&_stack),
#else
  .initialStackPointer = (void*)(&_estack),
#endif

  // clang-format off
  .resetHandler        = Reset_Handler,
  .nmiHandler          = NonMaskableInt_Handler,
  .hardFaultHandler    = HardFault_Handler,
  .memManageHandler    = MemManage_Handler,
  .busFaultHandler     = BusFault_Handler,
  .usageFaultHandler   = UsageFault_Handler,
  .reserved0 = {
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
  },
  .svcHandler          = SVCall_Handler,
  .debugMonHandler     = DebugMonitor_Handler,
  .reserved1           = Default_Handler,
  .pendSvHandler       = PendSV_Handler,
  .sysTickHandler      = SysTick_Handler,
  // clang-format on

  .irqHandlers = {
    SUPC_Handler,                    // 0  Supply Controller
    RSTC_Handler,                    // 1  Reset Controller
    RTC_Handler,                     // 2  Real-time Clock
    RTT_Handler,                     // 3  Real-time Timer
    WDT_Handler,                     // 4  Watchdog Timer
    PMC_Handler,                     // 5  Power Management Controller
#if defined(N7S_TARGET_SAMV71Q21)
    EFC_Handler,                     // 6  Embedded Flash Controller
    UART0_Handler,                   // 7  Universal Asynchronous Receiver Transmitter
    UART1_Handler,                   // 8  Universal Asynchronous Receiver Transmitter
    Default_Handler,                 // 9  Reserved
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
    MATRIX0_Handler,                 // 6  Matrix 0
    FLEXCOM0_Handler,                // 7  FLEXCOM 0
    FLEXCOM1_Handler,                // 8  FLEXCOM 1
    NMIC_Handler,                    // 9  NMI Controller
#endif
    PIOA_Handler,                    // 10 Parallel Input/Output Controller
    PIOB_Handler,                    // 11 Parallel Input/Output Controller
    PIOC_Handler,                    // 12 Parallel Input/Output Controller
#if defined(N7S_TARGET_SAMV71Q21)
    USART0_Handler,                  // 13 Universal Synchronous Asynchronous Receiver Transmitter
    USART1_Handler,                  // 14 Universal Synchronous Asynchronous Receiver Transmitter
    USART2_Handler,                  // 15 Universal Synchronous Asynchronous Receiver Transmitter
    PIOD_Handler,                    // 16 Parallel Input/Output Controller
    PIOE_Handler,                    // 17 Parallel Input/Output Controller
    HSMCI_Handler,                   // 18 High Speed MultiMedia Card Interface
    TWIHS0_Handler,                  // 19 Two-wire Interface High Speed
    TWIHS1_Handler,                  // 20 Two-wire Interface High Speed
    SPI0_Handler,                    // 21 Serial Peripheral Interface
    SSC_Handler,                     // 22 Synchronous Serial Controller
    TC0CH0_Handler,                  // 23 Timer Counter 0 Channel 0
    TC0CH1_Handler,                  // 24 Timer Counter 0 Channel 1
    TC0CH2_Handler,                  // 25 Timer Counter 0 Channel 2
    TC1CH0_Handler,                  // 26 Timer Counter 1 Channel 0
    TC1CH1_Handler,                  // 27 Timer Counter 1 Channel 1
    TC1CH2_Handler,                  // 28 Timer Counter 1 Channel 2
    AFEC0_Handler,                   // 29 Analog Front-End Controller
    DACC_Handler,                    // 30 Digital-to-Analog Converter Controller
    PWM0_Handler,                    // 31 Pulse Width Modulation Controller
    ICM_Handler,                     // 32 Integrity Check Monitor
    ACC_Handler,                     // 33 Analog Comparator Controller
    USBHS_Handler,                   // 34 USB High-Speed Interface
    MCAN0_INT0_Handler,              // 35 MCAN 0 IRQ 0
    MCAN0_INT1_Handler,              // 36 MCAN 0 IRQ 1
    MCAN1_INT0_Handler,              // 37 MCAN 1 IRQ 0
    MCAN1_INT1_Handler,              // 38 MCAN 1 IRQ 1
    GMAC_Handler,                    // 39 Gigabit Ethernet MAC
    AFEC1_Handler,                   // 40 Analog Front-End Controller
    TWIHS2_Handler,                  // 41 Two-wire Interface High Speed
    SPI1_Handler,                    // 42 Serial Peripheral Interface
    QSPI_Handler,                    // 43 Quad Serial Peripheral Interface
    UART2_Handler,                   // 44 Universal Asynchronous Receiver Transmitter
    UART3_Handler,                   // 45 Universal Asynchronous Receiver Transmitter
    UART4_Handler,                   // 46 Universal Asynchronous Receiver Transmitter
    TC2CH0_Handler,                  // 47 Timer Counter 2 Channel 0
    TC2CH1_Handler,                  // 48 Timer Counter 2 Channel 1
    TC2CH2_Handler,                  // 49 Timer Counter 2 Channel 2
    TC3CH0_Handler,                  // 50 Timer Counter 3 Channel 0
    TC3CH1_Handler,                  // 51 Timer Counter 3 Channel 1
    TC3CH2_Handler,                  // 52 Timer Counter 3 Channel 2
    MLB_INT0_Handler,                // 53 MediaLB IRQ 0
    MLB_INT1_Handler,                // 54 MediaLB IRQ 1
    Default_Handler,                 // 55 Reserved
    AES_Handler,                     // 56 Advanced Encryption Standard
    TRNG_Handler,                    // 57 True Random Number Generator
    XDMAC_Handler,                   // 58 Extensible DMA Controller
    ISI_Handler,                     // 59 Image Sensor Interface
    PWM1_Handler,                    // 60 Pulse Width Modulation Controller
    ARM_FPU_Handler,                 // 61 ARM FPU
    SDRAMC_Handler,                  // 62 SDRAM Controller
    RSWDT_Handler,                   // 63 Reinforced Safety Watchdog Timer
    ARM_CCW_Handler,                 // 64 ARM Cache ECC Warning
    ARM_CCF_Handler,                 // 65 ARM Cache ECC Fault
    GMACQ1_Handler,                  // 66 GMAC Queue 1
    GMACQ2_Handler,                  // 67 GMAC Queue 2
    ARM_IXC_Handler,                 // 68 FPU Interrupt IXC associated with cumulative exception
    I2SC0_Handler,                   // 69 Inter-IC Sound Controller 0
    I2SC1_Handler,                   // 70 Inter-IC Sound Controller 1
    GMACQ3_Handler,                  // 71 GMAC Queue 3
    GMACQ4_Handler,                  // 72 GMAC Queue 4
    GMACQ5_Handler,                  // 73 GMAC Queue 5
#elif defined(N7S_TARGET_SAMRH71F20)
    FLEXCOM2_Handler,                // 13 FLEXCOM 2
    FLEXCOM3_Handler,                // 14 FLEXCOM 3
    FLEXCOM4_Handler,                // 15 FLEXCOM 4
    PIOD_Handler,                    // 16 Parallel Input/Output Controller
    PIOE_Handler,                    // 17 Parallel Input/Output Controller
    ARM_CCW_Handler,                 // 18 ARM Cache ECC Warning
    ARM_CCF_Handler,                 // 19 ARM Cache ECC Fault
    ARM_FPU_Handler,                 // 20 FPU except IXC
    ARM_IXC_Handler,                 // 21 FPU Interrupt IXC associated with cumulative exception
    FLEXCOM5_Handler,                // 22 FLEXCOM 5
    FLEXCOM6_Handler,                // 23 FLEXCOM 6
    FLEXCOM7_Handler,                // 24 FLEXCOM 7
    TC0CH0_Handler,                  // 25 Timer Counter Channel 0
    TC0CH1_Handler,                  // 26 Timer Counter Channel 1
    TC0CH2_Handler,                  // 27 Timer Counter Channel 2
    TC1CH0_Handler,                  // 28 Timer Counter Channel 3
    TC1CH1_Handler,                  // 29 Timer Counter Channel 4
    TC1CH2_Handler,                  // 30 Timer Counter Channel 5
    PWM0_Handler,                    // 31 Pulse Width Modulation 0
    PWM1_Handler,                    // 32 Pulse Width Modulation 1
    ICM_Handler,                     // 33 Integrity Check Monitor
    PIOF_Handler,                    // 34 Parallel I/O Controller F
    PIOG_Handler,                    // 35 Parallel I/O Controller G
    MCAN0_INT0_Handler,              // 36 MCAN 0 IRQ 0
    MCAN0_INT1_Handler,              // 37 MCAN 0 IRQ 1
    MCAN1_INT0_Handler,              // 38 MCAN 1 IRQ 0
    MCAN1_INT1_Handler,              // 39 MCAN 1 IRQ 1
    TCMRAM_FIX_Handler,              // 40 TCM RAM - HECC Fixable error detected
    TCMRAM_NOFIX_Handler,            // 41 TCM RAM - HECC Un-Fixable error detected
    FlexRAM_FIX_Handler,             // 42 FlexRAM - HECC Fixable error detected
    FlexRAM_NOFIX_Handler,           // 43 FlexRAM - HECC Un-Fixable error detected
    SHA_Handler,                     // 44 Secure Hash Algorithm
    FLEXCOM8_Handler,                // 45 FLEXCOM 8
    FLEXCOM9_Handler,                // 46 FLEXCOM 9
    WDT1_Handler,                    // 47 Watchdog Timer 1
    Default_Handler,                 // 48 Reserved
    QSPI_Handler,                    // 49 Quad I/O Serial Peripheral Interface
    HEFC_Handler,                    // 50 Hardened Embedded Flash Controller
    HEFC_FIX_Handler,                // 51 Hardened Embedded Flash Controller Fixable error
    HEFC_NOFIX_Handler,              // 52 Hardened Embedded Flash Controller Un-Fixable error
    TC2CH0_Handler,                  // 53 Timer Counter Channel 6
    TC2CH1_Handler,                  // 54 Timer Counter Channel 7
    TC2CH2_Handler,                  // 55 Timer Counter Channel 8
    TC3CH0_Handler,                  // 56 Timer Counter Channel 9
    TC3CH1_Handler,                  // 57 Timer Counter Channel 10
    TC3CH2_Handler,                  // 58 Timer Counter Channel 11
    HEMC_Handler,                    // 59 HEMC-SDRAM Controller
    HEMC_FIX_Handler,                // 60 HEMC-HECC Fixable Error detected
    HEMC_NOFIX_Handler,              // 61 HEMC-HECC Unfixable Error detected
    SFR_Handler,                     // 62 Special Function Register
    TRNG_Handler,                    // 63 True Random Generator
    XDMAC_Handler,                   // 64 eXtended DMA Controller
    SPW_INT0_Handler,                // 65 Space Wire
    Default_Handler,                 // 66 Reserved
    Default_Handler,                 // 67 Reserved
    IP1553_Handler,                  // 68 MIL 1553
    GMAC_Handler,                    // 69 Ethernet MAC
    GMACQ1_Handler,                  // 70 GMAC Priority Queue 1
    GMACQ2_Handler,                  // 71 GMAC Priority Queue 2
    GMACQ3_Handler,                  // 72 GMAC Priority Queue 3
    GMACQ4_Handler,                  // 73 GMAC Priority Queue 4
    GMACQ5_Handler,                  // 74 GMAC Priority Queue 5
    Default_Handler,                 // 75 Reserved
    Default_Handler,                 // 76 Reserved
    Default_Handler,                 // 77 Reserved
    Default_Handler,                 // 78 Reserved
    Default_Handler,                 // 79 Reserved
    Default_Handler,                 // 80 Reserved
    Default_Handler,                 // 81 Reserved
    Default_Handler,                 // 82 Reserved
    Default_Handler,                 // 83 Reserved
    Default_Handler,                 // 84 Reserved
    Default_Handler,                 // 85 Reserved
    Default_Handler,                 // 86 Reserved
    Default_Handler,                 // 87 Reserved
    Default_Handler,                 // 88 Reserved
    Default_Handler,                 // 89 Reserved
    Default_Handler,                 // 90 Reserved
#elif defined(N7S_TARGET_SAMRH707F18)
    FLEXCOM2_Handler,                 // 13 FLEXCOM 2
    FLEXCOM3_Handler,                 // 14 FLEXCOM 3
    Default_Handler,                  // 15 Reserved
    PIOD_Handler,                     // 16 Parallel I/O Controller D
    Default_Handler,                  // 17 Reserved
    ARM_CCW_Handler,                  // 18 ARM Cache ECC Warning
    ARM_CCF_Handler,                  // 19 ARM Cache ECC Fault
    ARM_FPU_Handler,                  // 20 Floating Point Unit except IXC
    ARM_IXC_Handler,                  // 21 Floating Point Unit Interrupt IXC
    CRCCU_Handler,                    // 22 CRCCU_HCBDMA
    ADC_Handler,                      // 23 ADC controller
    DACC_Handler,                     // 24 DAC Controller
    TC0CH0_Handler,                   // 25 Timer Counter Channel 0
    TC0CH1_Handler,                   // 26 Timer Counter Channel 1
    TC0CH2_Handler,                   // 27 Timer Counter Channel 2
    TC1CH0_Handler,                   // 28 Timer Counter Channel 3
    TC1CH1_Handler,                   // 29 Timer Counter Channel 4
    TC1CH2_Handler,                   // 30 Timer Counter Channel 5
    PWM0_Handler,                     // 31 Pulse Width Modulation 0
    PWM1_Handler,                     // 32 Pulse Width Modulation 1
    ICM_Handler,                      // 33 Integrity Check Monitor
    Default_Handler,                  // 34 Reserved
    Default_Handler,                  // 35 Reserved
    MCAN0_INT0_Handler,               // 36 MCAN Controller 0
    MCAN0_INT1_Handler,               // 37 MCAN 0 IRQ 1
    MCAN1_INT0_Handler,               // 38 MCAN Controller 1
    MCAN1_INT1_Handler,               // 39 MCAN 1 IRQ 1
    TCMRAM_FIX_Handler,               // 40 TCM RAM - HECC Fixable error detected
    TCMRAM_NOFIX_Handler,             // 41 TCM RAM - HECC Un-Fixable error detected
    FlexRAM_FIX_Handler,              // 42 FlexRAM - HECC Fixable error detected
    FlexRAM_NOFIX_Handler,            // 43 FlexRAM - HECC Un-Fixable error detected
    SHA_Handler,                      // 44 Secure Hash Algorithm
    PCC_Handler,                      // 45 PCC controller
    Default_Handler,                  // 46 Reserved
    WDT1_Handler,                     // 47 Watchdog Timer 1
    Default_Handler,                  // 48 Reserved
    Default_Handler,                  // 49 Reserved
    HEFC_Handler,                     // 50 HEFC INT0
    HEFC_FIX_Handler,                 // 51 HEFC Fixable Error
    HEFC_NOFIX_Handler,               // 52 HEFC Unfixable Error
    TC2CH0_Handler,                   // 53 Timer Counter Channel 6
    TC2CH1_Handler,                   // 54 Timer Counter Channel 7
    TC2CH2_Handler,                   // 55 Timer Counter Channel 8
    Default_Handler,                  // 56 Reserved
    Default_Handler,                  // 57 Reserved
    Default_Handler,                  // 58 Reserved
    HEMC_Handler,                     // 59 Hardened Embedded Flash Controller
    HEMC_FIX_Handler,                 // 60 Hardened Embedded Flash Controller Fixable error
    HEMC_NOFIX_Handler,               // 61 Hardened Embedded Flash Controller Un-Fixable error
    SFR_Handler,                      // 62 Special Function Register
    TRNG_Handler,                     // 63 True Random Generator
    XDMAC_Handler,                    // 64 eXtended DMA Controller
    SPW_INT0_Handler,                 // 65 Space Wire
    Default_Handler,                  // 66 Reserved
    Default_Handler,                  // 67 Reserved
    IP1553_Handler,                   // 68 MIL 1553
    Default_Handler,                  // 69 Reserved
    Default_Handler,                  // 70 Reserved
    Default_Handler,                  // 71 Reserved
    Default_Handler,                  // 72 Reserved
    Default_Handler,                  // 73 Reserved
    Default_Handler,                  // 74 Reserved
    Default_Handler,                  // 75 Reserved
    Default_Handler,                  // 76 Reserved
    Default_Handler,                  // 77 Reserved
    Default_Handler,                  // 78 Reserved
    Default_Handler,                  // 79 Reserved
#endif
  },
};
