/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file TestsStartup.h
/// \brief Base initialization and interrupts handlers.

#ifndef N7S_BSP_TESTSSTARTUP_TESTSSTARTUP_H
#define N7S_BSP_TESTSSTARTUP_TESTSSTARTUP_H

#ifdef __cplusplus
extern "C" {
#endif

/// @addtogroup Startup
/// @{

/// \brief Default interrupt handler for unused IRQs.
/// \details Generates breakpoint and enters infinite loop on interrupt occurrence.
///          Used when dedicated handler is not provided.
void Default_Handler(void);

/// @defgroup Startup-Core Cortex-M7 core interrupt handlers
/// @ingroup Startup
/// @{

/// \brief Reset Handler - the first code that gets called on processor reset.
/// \details Initializes the device and calls the `main` routine.
void Reset_Handler(void);

/// \brief Reset Handler Tail - part of the Reset_Handler() implemented in C, called from assembly.
void Reset_HandlerTail(void);

/// \brief Default non-maskable interrupt exception handler.
/// \details Implement this function to provide own handler.
void NonMaskableInt_Handler(void);

/// \brief Default hard fault exception handler.
/// \details Implement this function to provide own handler.
void HardFault_Handler(void);

/// \brief Default memory management fault handler.
/// \details Implement this function to provide own handler.
void MemManage_Handler(void);

/// \brief Default bus fault handler.
/// \details Implement this function to provide own handler.
void BusFault_Handler(void);

/// \brief Default usage fault handler.
/// \details Implement this function to provide own handler.
void UsageFault_Handler(void);

/// \brief Default supervisor call exception handler.
/// \details Implement this function to provide own handler.
void SVCall_Handler(void);

/// \brief Default debug monitor exception handler.
/// \details Implement this function to provide own handler.
void DebugMonitor_Handler(void);

/// \brief Default PendSV request handler.
/// \details Implement this function to provide own handler.
void PendSV_Handler(void);

/// \brief Default system timer exception handler.
/// \details Implement this function to provide own handler.
void SysTick_Handler(void);

/// @}

/// @defgroup Startup-Peripherals Device peripherals interrupt handlers
/// @ingroup Startup
/// @{

/// \brief Default Supply Controller (SUPC) interrupt handler.
/// \details Implement this function to provide own handler.
void SUPC_Handler(void);

/// \brief Default Reset Controller (RSTC) interrupt handler.
/// \details Implement this function to provide own handler.
void RSTC_Handler(void);

/// \brief Default Real-time Clock (RTC) interrupt handler.
/// \details Implement this function to provide own handler.
void RTC_Handler(void);

/// \brief Default Real-time Timer (RTT) interrupt handler.
/// \details Implement this function to provide own handler.
void RTT_Handler(void);

/// \brief Default Watchdog Timer (WDT) interrupt handler.
/// \details Implement this function to provide own handler.
void WDT_Handler(void);

/// \brief Default Power Management Controller (PMC) interrupt handler.
/// \details Implement this function to provide own handler.
void PMC_Handler(void);

/// \brief Default Parallel Input/Output Controller A (PIO A) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOA_Handler(void);

/// \brief Default Parallel Input/Output Controller B (PIO B) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOB_Handler(void);

/// \brief Default Parallel Input/Output Controller C (PIO C) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOC_Handler(void);

/// \brief Default Parallel Input/Output Controller D (PIO D) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOD_Handler(void);

/// \brief Default Timer Counter 0 Channel 0 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC0CH0_Handler(void);

/// \brief Default Timer Counter 0 Channel 1 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC0CH1_Handler(void);

/// \brief Default Timer Counter 0 Channel 2 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC0CH2_Handler(void);

/// \brief Default Timer Counter 1 Channel 0 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC1CH0_Handler(void);

/// \brief Default Timer Counter 1 Channel 1 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC1CH1_Handler(void);

/// \brief Default Timer Counter 1 Channel 2 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC1CH2_Handler(void);

/// \brief Default Timer Counter 2 Channel 0 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC2CH0_Handler(void);

/// \brief Default Timer Counter 2 Channel 1 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC2CH1_Handler(void);

/// \brief Default Timer Counter 2 Channel 2 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC2CH2_Handler(void);

/// \brief Default Pulse Width Modulation Controller 0 (PWM 0) interrupt handler.
/// \details Implement this function to provide own handler.
void PWM0_Handler(void);

/// \brief Default Pulse Width Modulation Controller 1 (PWM 1) interrupt handler.
/// \details Implement this function to provide own handler.
void PWM1_Handler(void);

/// \brief Default MCAN Controller 0 (MCAN 0) interrupt handler.
/// \details Implement this function to provide own handler.
void MCAN0_INT0_Handler(void);

/// \brief Default MCAN Controller 0 IRQ 1 (MCAN 0) interrupt handler.
/// \details Implement this function to provide own handler.
void MCAN0_INT1_Handler(void);

/// \brief Default MCAN Controller 1 (MCAN 1) interrupt handler.
/// \details Implement this function to provide own handler.
void MCAN1_INT0_Handler(void);

/// \brief Default MCAN Controller 1 IRQ 1 (MCAN 1) interrupt handler.
/// \details Implement this function to provide own handler.
void MCAN1_INT1_Handler(void);

/// \brief Default Integrity Check Monitor (ICM) interrupt handler.
/// \details Implement this function to provide own handler.
void ICM_Handler(void);

/// \brief Default True Random Number Generator (TRNG) interrupt handler.
/// \details Implement this function to provide own handler.
void TRNG_Handler(void);

/// \brief Default Extensible DMA Controller (XDMAC) interrupt handler.
/// \details Implement this function to provide own handler.
void XDMAC_Handler(void);

/// \brief Default Floating Point Unit except IXC (ARM FPU) interrupt handler.
/// \details Implement this function to provide own handler.
void ARM_FPU_Handler(void);

/// \brief Default Floating Point Unit Interrupt IXC associated with FPU cumulative exception bit
///        (ARM IXC) interrupt handler.
/// \details Implement this function to provide own handler.
void ARM_IXC_Handler(void);

/// \brief Default ARM Cache ECC Warning (ARM CCW) interrupt handler.
/// \details Implement this function to provide own handler.
void ARM_CCW_Handler(void);

/// \brief Default ARM Cache ECC Fault (ARM CCF) interrupt handler.
/// \details Implement this function to provide own handler.
void ARM_CCF_Handler(void);

#if defined(N7S_TARGET_SAMV71Q21)
/// \brief Default Enhanced Embedded Flash Controller (EEFC) interrupt handler.
/// \details Implement this function to provide own handler.
void EFC_Handler(void);

/// \brief Default Universal Asynchronous Receiver Transmitter 0 (UART 0) interrupt handler.
/// \details Implement this function to provide own handler.
void UART0_Handler(void);

/// \brief Default Universal Asynchronous Receiver Transmitter 1 (UART 1) interrupt handler.
/// \details Implement this function to provide own handler.
void UART1_Handler(void);
#endif

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMV71Q21)

/// \brief Default Parallel Input/Output Controller E (PIO E) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOE_Handler(void);

/// \brief Default Timer Counter 3 Channel 0 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC3CH0_Handler(void);

/// \brief Default Timer Counter 3 Channel 1 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC3CH1_Handler(void);

/// \brief Default Timer Counter 3 Channel 2 (TIC) interrupt handler.
/// \details Implement this function to provide own handler.
void TC3CH2_Handler(void);

/// \brief Default Gigabit Ethernet MAC (GMAC) interrupt handler.
/// \details Implement this function to provide own handler.
void GMAC_Handler(void);

/// \brief Default GMAC Queue 1 (GMACQ1) interrupt handler.
/// \details Implement this function to provide own handler.
void GMACQ1_Handler(void);

/// \brief Default GMAC Queue 2 (GMACQ2) interrupt handler.
/// \details Implement this function to provide own handler.
void GMACQ2_Handler(void);

/// \brief Default GMAC Queue 3 (GMACQ3) interrupt handler.
/// \details Implement this function to provide own handler.
void GMACQ3_Handler(void);

/// \brief Default GMAC Queue 4 (GMACQ4) interrupt handler.
/// \details Implement this function to provide own handler.
void GMACQ4_Handler(void);

/// \brief Default GMAC Queue 5 (GMACQ5) interrupt handler.
/// \details Implement this function to provide own handler.
void GMACQ5_Handler(void);

/// \brief Default Quad Serial Peripheral Interface (QSPI) interrupt handler.
/// \details Implement this function to provide own handler.
void QSPI_Handler(void);

#endif

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH707F18)
/// \brief Default Digital-to-Analog Converter Controller (DACC) interrupt handler.
/// \details Implement this function to provide own handler.
void DACC_Handler(void);

#endif

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)

/// \brief Default MATRIX interrupt handler.
/// \details Implement this function to provide own handler.
void MATRIX0_Handler(void);

/// \brief Default FLEXCOM 0 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM0_Handler(void);

/// \brief Default FLEXCOM 1 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM1_Handler(void);

/// \brief Default FLEXCOM 2 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM2_Handler(void);

/// \brief Default FLEXCOM 3 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM3_Handler(void);

/// \brief Default NMI Controller (NMIC) interrupt handler.
/// \details Implement this function to provide own handler.
void NMIC_Handler(void);

/// \brief Default TCM RAM - HECC Fixable error detected (TCMRAM FIX) interrupt handler.
/// \details Implement this function to provide own handler.
void TCMRAM_FIX_Handler(void);

/// \brief Default TCM RAM - HECC Un-Fixable error detected (TCMRAM NOFIX) interrupt handler.
/// \details Implement this function to provide own handler.
void TCMRAM_NOFIX_Handler(void);

/// \brief Default FlexRAM - HECC Fixable error detected (FlexRAM FIX) interrupt handler.
/// \details Implement this function to provide own handler.
void FlexRAM_FIX_Handler(void);

/// \brief Default FlexRAM - HECC Un-Fixable error detected (FlexRAM NOFIX) interrupt handler.
/// \details Implement this function to provide own handler.
void FlexRAM_NOFIX_Handler(void);

/// \brief Default Secure Hash Algorithm (SHA) interrupt handler.
/// \details Implement this function to provide own handler.
void SHA_Handler(void);

/// \brief Default Watchdog Timer 1 (WDT 1) interrupt handler.
/// \details Implement this function to provide own handler.
void WDT1_Handler(void);

/// \brief Default Hardened Embedded Flash Controller (HEFC) interrupt handler.
/// \details Implement this function to provide own handler.
void HEFC_Handler(void);

/// \brief Default Hardened Embedded Flash Controller Fixable error detected (HEFC FIX) interrupt
///        handler.
/// \details Implement this function to provide own handler.
void HEFC_FIX_Handler(void);

/// \brief Default Hardened Embedded Flash Controller Un-Fixable error detected (HEFC NO FIX)
///        interrupt handler.
/// \details Implement this function to provide own handler.
void HEFC_NOFIX_Handler(void);

/// \brief Default HEMC-SDRAM Controller (HEMC)
///        interrupt handler.
/// \details Implement this function to provide own handler.
void HEMC_Handler(void);

/// \brief Default HEMC-HECC Fixable error detected (HEMC FIX) interrupt handler.
/// \details Implement this function to provide own handler.
void HEMC_FIX_Handler(void);

/// \brief Default HEMC-HECC Un-Fixable error detected (HEMC NOFIX) interrupt handler.
/// \details Implement this function to provide own handler.
void HEMC_NOFIX_Handler(void);

/// \brief Default Special Function Register (SFR) interrupt handler.
/// \details Implement this function to provide own handler.
void SFR_Handler(void);

/// \brief Default SpaceWire (SPW) interrupt handler.
/// \details Implement this function to provide own handler.
void SPW_INT0_Handler(void);

/// \brief Default MIL 1553 (IP1553) interrupt handler.
/// \details Implement this function to provide own handler.
void IP1553_Handler(void);

#endif

#if defined(N7S_TARGET_SAMV71Q21)

/// \brief Default Universal Asynchronous Receiver Transmitter 2 (UART 2) interrupt handler.
/// \details Implement this function to provide own handler.
void UART2_Handler(void);

/// \brief Default Universal Asynchronous Receiver Transmitter 3 (UART 3) interrupt handler.
/// \details Implement this function to provide own handler.
void UART3_Handler(void);

/// \brief Default Universal Asynchronous Receiver Transmitter 4 (UART 4) interrupt handler.
/// \details Implement this function to provide own handler.
void UART4_Handler(void);

/// \brief Default Universal Synchronous Asynchronous Receiver Transmitter 0 (USART 0) interrupt
///        handler.
/// \details Implement this function to provide own handler.
void USART0_Handler(void);

/// \brief Default Universal Synchronous Asynchronous Receiver Transmitter 1 (USART 1) interrupt
///        handler.
/// \details Implement this function to provide own handler.
void USART1_Handler(void);

/// \brief Default Universal Synchronous Asynchronous Receiver Transmitter 1 (USART 1) interrupt
///        handler.
/// \details Implement this function to provide own handler.
void USART2_Handler(void);

/// \brief Default High Speed MultiMedia Card Interface (HSMCI) interrupt handler.
/// \details Implement this function to provide own handler.
void HSMCI_Handler(void);

/// \brief Default Two-wire Interface High Speed 0 (TWIHS 0) interrupt handler.
/// \details Implement this function to provide own handler.
void TWIHS0_Handler(void);

/// \brief Default Two-wire Interface High Speed 1 (TWIHS 1) interrupt handler.
/// \details Implement this function to provide own handler.
void TWIHS1_Handler(void);

/// \brief Default Two-wire Interface High Speed 2 (TWIHS 2) interrupt handler.
/// \details Implement this function to provide own handler.
void TWIHS2_Handler(void);

/// \brief Default Serial Peripheral Interface 0 (SPI 0) interrupt handler.
/// \details Implement this function to provide own handler.
void SPI0_Handler(void);

/// \brief Default Serial Peripheral Interface 1 (SPI 1) interrupt handler.
/// \details Implement this function to provide own handler.
void SPI1_Handler(void);

/// \brief Default Synchronous Serial Controller (SSC) interrupt handler.
/// \details Implement this function to provide own handler.
void SSC_Handler(void);

/// \brief Default Analog Front-End Controller 0 (AFEC 0) interrupt handler.
/// \details Implement this function to provide own handler.
void AFEC0_Handler(void);

/// \brief Default Analog Front-End Controller 1 (AFEC 1) interrupt handler.
/// \details Implement this function to provide own handler.
void AFEC1_Handler(void);

/// \brief Default Analog Comparator Controller (ACC) interrupt handler.
/// \details Implement this function to provide own handler.
void ACC_Handler(void);

/// \brief Default USB High-Speed Interface (USBHS) interrupt handler.
/// \details Implement this function to provide own handler.
void USBHS_Handler(void);

/// \brief Default MediaLB (MLB) IRQ 0 interrupt handler.
/// \details Implement this function to provide own handler.
void MLB_INT0_Handler(void);

/// \brief Default MediaLB (MLB) IRQ 1 interrupt handler.
/// \details Implement this function to provide own handler.
void MLB_INT1_Handler(void);

/// \brief Default Advanced Encryption Standard (AES) interrupt handler.
/// \details Implement this function to provide own handler.
void AES_Handler(void);

/// \brief Default Image Sensor Interface (ISI) interrupt handler.
/// \details Implement this function to provide own handler.
void ISI_Handler(void);

/// \brief Default SDRAM Controller (SDRAMC) interrupt handler.
/// \details Implement this function to provide own handler.
void SDRAMC_Handler(void);

/// \brief Default Reinforced Safety Watchdog Timer (RSWDT) interrupt handler.
/// \details Implement this function to provide own handler.
void RSWDT_Handler(void);

/// \brief Default Inter-IC Sound Controller 0 (I2SC 0) interrupt handler.
/// \details Implement this function to provide own handler.
void I2SC0_Handler(void);

/// \brief Default Inter-IC Sound Controller 1 (I2SC 1) interrupt handler.
/// \details Implement this function to provide own handler.
void I2SC1_Handler(void);

#endif
#if defined(N7S_TARGET_SAMRH71F20)

/// \brief Default FLEXCOM 4 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM4_Handler(void);

/// \brief Default FLEXCOM 5 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM5_Handler(void);

/// \brief Default FLEXCOM 6 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM6_Handler(void);

/// \brief Default FLEXCOM 7 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM7_Handler(void);

/// \brief Default FLEXCOM 8 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM8_Handler(void);

/// \brief Default FLEXCOM 9 interrupt handler.
/// \details Implement this function to provide own handler.
void FLEXCOM9_Handler(void);

/// \brief Default Parallel Input/Output Controller F (PIO F) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOF_Handler(void);

/// \brief Default Parallel Input/Output Controller G (PIO G) interrupt handler.
/// \details Implement this function to provide own handler.
void PIOG_Handler(void);

#endif

#if defined(N7S_TARGET_SAMRH707F18)

/// \brief Default CRC Calculation Unit interrupt handler.
/// \details Implement this function to provide own handler.
void CRCCU_Handler(void);

/// \brief Default Analog to Digital Controller interrupt handler.
/// \details Implement this function to provide own handler.
void ADC_Handler(void);

/// \brief Default Parallel Calture Controller interrupt handler.
/// \details Implement this function to provide own handler.
void PCC_Handler(void);

#endif

/// @}

/// @}

#ifdef __cplusplus
} // extern "C"
#endif

#endif // N7S_BSP_TESTSSTARTUP_TESTSSTARTUP_H
