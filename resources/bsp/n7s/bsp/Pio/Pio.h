/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_BSP_PIO_PIO_H
#define N7S_BSP_PIO_PIO_H

/// \file Pio.h
/// \addtogroup Bsp
/// \brief Header containing interface for Pio module.

#if defined(N7S_TARGET_SAMV71Q21)

#include <n7s/bsp/Pio/samv71q21/Pio.h>

#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)

#include <n7s/bsp/Pio/samrh7/Pio.h>

#else

#error "No target platform specified (missing N7S_TARGET_* macro)"

#endif

#endif // N7S_BSP_PIO_PIO_H
