#!/bin/bash

# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TOPDIR="$(pwd)"

# Detect if coverage is enabled
COV_ENABLED=false
for i in "$@"; do
    if [[ $i == "--gen-coverage" ]]; then
        COV_ENABLED=true
        break
    fi
done

# Select venv dir depending on coverage settings
if [[ $COV_ENABLED == true ]]; then
    export DCF2DEV_VENV="${TOPDIR}/${BUILD_DIR_ROOT}/dcf2dev-cov-venv"
else
    export DCF2DEV_VENV="${TOPDIR}/${BUILD_DIR_ROOT}/dcf2dev-venv"
fi

# Setup common variables
DCF2DEV_COVERAGE_RCFILE="${DIR}/coverage.rc"
LOCKFILE="${DCF2DEV_VENV}/creation-lock"
EGG_CACHE="${DCF2DEV_VENV}/egg-cache"
PIP_CACHE="${DCF2DEV_VENV}/pip-cache"
SETUPTOOLS_CACHE="${DCF2DEV_VENV}/setuptools-cache"
PYTHON=python3

mkdir -p "${DCF2DEV_VENV}"

# Lock venv directory and setup it if necessary
(
    flock 9 || exit 1
    if ! [ -f "${DCF2DEV_VENV}/bin/activate" ]; then
        ${PYTHON} -m venv "${DCF2DEV_VENV}"
        source "${DCF2DEV_VENV}/bin/activate"
        mkdir -p "${PIP_CACHE}"
        ${PYTHON} -m pip --cache-dir "${PIP_CACHE}" install setuptools==74.1.2
        mkdir -p "${SETUPTOOLS_CACHE}"
        (cd "${DIR}/lely-core/python/dcf-tools" ;
         ${PYTHON} setup.py \
                   build -b "${SETUPTOOLS_CACHE}/build" \
                   egg_info -e "${SETUPTOOLS_CACHE}" \
                   bdist_wheel -b "${SETUPTOOLS_CACHE}/build" -d "${SETUPTOOLS_CACHE}/dist")
        WHL_FILE=$(ls "${SETUPTOOLS_CACHE}"/dist/*.whl)
        ${PYTHON} -m pip --cache-dir "${PIP_CACHE}" install "${WHL_FILE}"
        mkdir -p "${EGG_CACHE}"
        if [[ $COV_ENABLED == true ]]; then
            ${PYTHON} -m pip install coverage==7.4.0
        fi
    fi
) 9>"${LOCKFILE}"

# Activate venv
source "${DCF2DEV_VENV}/bin/activate"

if [[ $COV_ENABLED == true ]]; then
    # copy @ to args and remove --gen-coverage from it
    args=("$@")
    for ((i=0; i<"${#args[@]}"; ++i)); do
        if [[ ${args[i]} == "--gen-coverage" ]]; then
            unset args[i];
        fi
    done

    echo "Measuring code coverage..."
    PYTHON_EGG_CACHE="${EGG_CACHE}" \
                    coverage run --rcfile="${DCF2DEV_COVERAGE_RCFILE}" \
                    --append `which dcf2dev` "${args[@]}"
    EXIT_CODE=$?
    PYTHON_EGG_CACHE="${EGG_CACHE}" \
                    coverage html --rcfile="${DCF2DEV_COVERAGE_RCFILE}"

    exit "$EXIT_CODE"
else
    PYTHON_EGG_CACHE="${EGG_CACHE}" dcf2dev "$@"
fi
