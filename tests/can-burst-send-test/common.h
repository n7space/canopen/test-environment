/**@file
 * This file is part of the Test Environment internal tests.
 *
 * @copyright 2021-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/can/msg.h>

#include <TestFramework/HAL/CanInterface.h>
#include <TestFramework/TestHarness.h>

static const uint32_t MSG_COUNT = 128u;

static inline void
validateMessage(uint32_t counter, const struct can_msg *const msg)
{
	for (uint8_t i = 0; i < CAN_MSG_MAX_LEN; ++i) {
		if (msg->data[i] != counter) {
			LOG_UINT32("Message data", msg->data[i]);
			LOG_UINT32("Counter", counter);
			FAIL_TEST("Invalid data in CAN frame");
			break;
		}
	}
}

static inline void
sendMessage(Can_Bus bus, uint32_t counter)
{
	struct can_msg msg = CAN_MSG_INIT;
	msg.len = CAN_MSG_MAX_LEN;

	for (uint8_t j = 0; j < CAN_MSG_MAX_LEN; ++j)
		msg.data[j] = (uint_least8_t)counter;

	LOG_UINT32("Sending message", counter);

	if (CanSend(bus, &msg))
		LOG_UINT32("Message successfully sent", counter);
	else
		FAIL_TEST("Failed to send message.");
}
