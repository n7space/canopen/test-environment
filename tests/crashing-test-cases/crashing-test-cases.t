# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Various crashing test cases

Given Test Cases prepared to crash on each execution
When executing those Test Cases
Then CTESW will detect crashes on both Host and HWTB.

SRS: SRS-CTESW-FUN-040
SRS: SRS-CTESW-FUN-045
SRS: SRS-CTESW-FUN-100
SRS: SRS-CTESW-FUN-105
SRS: SRS-CTESW-FUN-260
SRS: SRS-CTESW-FUN-265
SRS: SRS-CTESW-IF-050

Note: The test validates CTESW capabilities to observe results from
execution of applications on Host [SRS-CTESW-FUN-040] and HWTB [SRS-CTESW-FUN-045].

Note: The test executes applications that are designed to "crash" (stop execution in non-nominal fashion)
on HWTB [SRS-CTESW-FUN-105] and Host [SRS-CTESW-FUN-100].

Note: The test will fail if any crash is not detected by CTESW.

Note: This test relies on CTESW capability of selecting CANSW test case for execution [SRS-CTESW-FUN-260].
Only a single test case is executed at once to allow separatre checks for all types of crashes.
Selection is performed via CTESW CLI interface [SRS-CTESW-IF-050].

Note: The test validates that the whole execution is marked as failed even when a single test fails [SRS-CTESW-FUN-265].

  $ source "${TESTDIR}/../common.sh"

Skip on coverage run (no way to gather coverage)
  $ if [ "$CTESW_BUILD_TYPE" = "coverage" ] ; then exit 80 ; fi ;

Crash on HOST
  $ scons_wrapper test-case-crash-host | grep "scons: \*\*\* \\["
  scons: *** [*/test-case-crash-host.log] Error -1 (glob)

  $ cat "$TESTLOGPATH/test-case-crash-host.host.log"
  ============ TEST MAIN START ============
  Crashing on HOST

Crash on HWTB
  $ scons_wrapper test-case-crash-hwtb | grep "scons: \*\*\* \\["
  scons: *** [*/test-case-crash-hwtb.log] Error -1 (glob)

  $ cat "$TESTLOGPATH/test-case-crash-hwtb.hwtb.log"
  ============ TEST MAIN START ============
  Crashing on HWTB

Crash on HWTB because of assert
  $ scons_wrapper test-case-crash-hwtb-assert | grep "scons: \*\*\*"
  scons: *** [*/test-case-crash-hwtb-assert.log] Error -2 (glob)

  $ cat "$TESTLOGPATH/test-case-crash-hwtb-assert.hwtb.log"
  ============ TEST MAIN START ============
  Crashing on HWTB because of assertion
  [0x0000000000000000 s 0x00000000 ns] - ASSERTION FAILED!
  [0x0000000000000000 s 0x00000000 ns] - File: "tests/crashing-test-cases/hwtb-main-assert.c"
  [0x0000000000000000 s 0x00000000 ns] - Line: 0x00000022
  [0x0000000000000000 s 0x00000000 ns] - Function: "EntryPoint_main"
  [0x0000000000000000 s 0x00000000 ns] - Failed assertion: "2 + 2 == 5"
  
  >> EXIT STATUS: 00000001

