# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Test results report generation test.

Given Executed Test Case 
When generating test results report
Then JSON file containing execution status is created.

  $ source "${TESTDIR}/../common.sh"

Execute and dump results
  $ scons_wrapper test-case-for-result-logging | grep "scons: done building targets."
  scons: done building targets.

Check created JSON
  $ cat "$TESTLOGPATH/path.json"
  [
    {
      "specificationId": "test-case-for-result-logging-hwtb-to-host",
      "logPath": "tests/results/test-case-for-result-logging-hwtb-to-host.log",
      "status": "Successful",
      "date": "*" (glob)
    },
    {
      "specificationId": "test-case-for-result-logging-host-to-hwtb",
      "logPath": "tests/results/test-case-for-result-logging-host-to-hwtb.log",
      "status": "Successful",
      "date": "*" (glob)
    },
    {
      "specificationId": "test-case-for-result-logging",
      "logPath": "tests/results/test-case-for-result-logging.log",
      "status": "Successful",
      "date": "*" (glob)
    }
  ] (no-eol)
