# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Basic empty test case using test harness

Given 'empty' Test Case defined using Test Harness provided by Test Framework
When building and executing such Test Case
Then test harness methods are executed in proper order and test output log is stored.

SRS: SRS-CTESW-FUN-150
SRS: SRS-CTESW-FUN-160
SRS: SRS-CTESW-FUN-185
SRS: SRS-CTESW-FUN-190
SRS: SRS-CTESW-FUN-194
SRS: SRS-CTESW-FUN-196
SRS: SRS-CTESW-FUN-260
SRS: SRS-CTESW-FUN-265
SRS: SRS-CTESW-POR-010
SRS: SRS-CTESW-IF-050
SRS: SRS-CTESW-IF-060
SRS: SRS-CTESW-IF-070
SRS: SRS-CTESW-OP-010

Note: The test contains an "empty" CANSW Test Case - it's empty because
it does not validate any CANSW features, but uses Test Framework
features to validate them [SRS-CTESW-FUN-150].

Note: The same code is executed compiled and executed for Host and HWTB
to check that Test Framework provides proper layer of separation from
hardware, as required by [SRS-CTESW-FUN-160] [SRS-CTESW-POR-010].

Note: Test Case based on a Test Framework finishes nominally only if proper
command is executed by the application - the test validates it [SRS-CTESW-FUN-185].
Otherwise the application would indefinitely loop [SRS-CTESW-FUN-190]
(until CTESW would kill it).

Note: The test validates, via code example, that Test Framework provides
"step" [SRS-CTESW-FUN-190], "setup" [SRS-CTESW-FUN-194]
and "teardown" [SRS-CTESW-FUN-196] procedures to be filled by Test Case author.

Note: The test uses CTESW feature [SRS-CTESW-FUN-260] to execute only the
selected "empty" Test Case. As it is the only executed Test Case, the whole
execution will be marked PASSED [SRS-CTESW-FUN-265].

Note: The Test Case selection for the execution is performed via
CTESW CLI interface [SRS-CTESW-IF-050].

Note: The Test Case used by the test is defined as specified in [SRS-CTESW-IF-060].

Note: The test validates the contents of the provided execution logs and check if
they match requirement [SRS-CTESW-IF-070].

Note: The execution of the basic Test Case used in this test does not require any
human interaction [SRS-CTESW-OP-010].

Note: Test is to be executed on platforms specified in SValP and validates Test Case
applicability for those: Host [SRS-CTESW-PER-010] and HWTB [SRS-CTESW-PER-020].

  $ source "${TESTDIR}/../common.sh"

Build and execute
  $ scons_wrapper empty-test-harness | grep "scons: done building targets."
  scons: done building targets.

HOST log
  $ cat "$TESTLOGPATH/empty-test-harness.host.log"
  ============ TEST MAIN START ============
  \[.*\] - HOST - test specific setup (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - HOST - test specific teardown (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB log
  $ cat "$TESTLOGPATH/empty-test-harness.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - HWTB - test specific setup (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - HWTB - test specific teardown (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Complete case status
  $ cat "$TESTLOGPATH/empty-test-harness.log"
  empty-test-harness - PASSED
