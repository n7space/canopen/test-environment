# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CAN handshake test case

Given two applications prepared to perform 'CAN handshake'
When executing those applications using HWTB
Then CAN message exchange logs contain expected sequences needed to synchronize execution of applications on HWTB and Host.

SRS: SRS-CTESW-FUN-170
SRS: SRS-CTESW-FUN-175
SRS: SRS-CTESW-PER-030
SRS: SRS-CTESW-IF-010
SRS: SRS-CTESW-IF-020

Note: The test performs very basic CAN bus exchange, validating that bus is properly set up
between HWTB and Host [SRS-CTESW-IF-010] [SRS-CTESW-FUN-170] [SRS-CTESW-FUN-175].

Note: Handshake procedure validated by the test is used by CTESW to synchronize
application execution [SRS-CTESW-PER-030].

Note: The hardware setup specified in SValP and used in validation relies on
Ethernet proxy for CAN bus [SRS-CTESW-IF-020].

  $ source "${TESTDIR}/../common.sh"

Building and executing
  $ scons_wrapper can-handshake-test | grep "scons: done building targets."
  scons: done building targets.

HOST log
  $ cat "$TESTLOGPATH/can-handshake-test.host.log"
  ============ TEST MAIN START ============
  Starting 'handshake receiver' routine
  Handshake 1 complete
  Starting 'handshake sender' routine
  Handshake 2 complete
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB log
  $ cat "$TESTLOGPATH/can-handshake-test.hwtb.log"
  ============ TEST MAIN START ============
  Starting 'handshake sender' routine
  Handshake 1 complete
  Starting 'handshake receiver' routine
  Handshake 2 complete
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

CAN log
('>' is cram special char, replacing, and removing CR)
  $ cat "$TESTLOGPATH/can-handshake-test.can-a.log" | tr -d '\015' | tr '>' '@'
  @ \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}\.\d{6}  length=16 from=0 to=15 (re)
   22 00 00 00 04 00 00 00 49 4e 49 54 00 00 00 00
  < \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}\.\d{6}  length=16 from=0 to=15 (re)
   11 00 00 00 03 00 00 00 41 43 4b 00 00 00 00 00
  @ \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}\.\d{6}  length=16 from=16 to=31 (re)
   22 00 00 00 03 00 00 00 41 43 4b 00 00 00 00 00
  < \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}\.\d{6}  length=16 from=16 to=31 (re)
   22 00 00 00 04 00 00 00 49 4e 49 54 00 00 00 00
  @ \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}\.\d{6}  length=16 from=32 to=47 (re)
   11 00 00 00 03 00 00 00 41 43 4b 00 00 00 00 00
  < \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}\.\d{6}  length=16 from=32 to=47 (re)
   22 00 00 00 03 00 00 00 41 43 4b 00 00 00 00 00

Whole case status
  $ cat "$TESTLOGPATH/can-handshake-test.log"
  can-handshake-test - PASSED
