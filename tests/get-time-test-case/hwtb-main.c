/**@file
 * This file is part of the Test Environment internal tests.
 *
 * @copyright 2020-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <TestFramework/TestHarness.h>
#include <TestFramework/TestTime.h>
#include <lely/util/time.h>

static const uint32_t DURATION_MS = 100;
static struct timespec lastTime = { 0, 0 };
static struct timespec startTime = { 0, 0 };
static uint32_t testStepCount = 0;

void
TestSetup(can_net_t *const net)
{
	(void)net;
	LOG("HWTB - test specific setup");
}

void
TestTeardown(void)
{
	LOG("HWTB - test specific teardown");
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;
}

void
TestStep(void)
{
	struct timespec tp;
	GetCurrentTestTime(&tp);

	if (testStepCount == 0) {
		startTime = tp;
	} else {
		const int64_t delta = timespec_diff_nsec(&tp, &lastTime);
		if (delta < 0) {
			LOG_TIME("tp", &tp);
			LOG_TIME("lastTime", &lastTime);
			LOG_INT64("delta", delta);
			FAIL_TEST("Invalid time diff between current and last time value.");
		}
	}

	const int64_t sinceStart = timespec_diff_msec(&tp, &startTime);
	// sanity check of CheckTimeInterval
	if (CheckTimeInterval((int32_t)sinceStart, DURATION_MS))
		FINISH_TEST();

	lastTime = tp;
	++testStepCount;
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
}
