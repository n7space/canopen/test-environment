# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Failing test cases

Given Test Cases prepared to fail on each execution
When execution those Test Cases
Then CTESW will detect failure, mark Test Case as FAILED and report failure reason.

SRS: SRS-CTESW-FUN-060
SRS: SRS-CTESW-FUN-070
SRS: SRS-CTESW-FUN-080
SRS: SRS-CTESW-FUN-090
SRS: SRS-CTESW-FUN-095
SRS: SRS-CTESW-FUN-110
SRS: SRS-CTESW-FUN-130
SRS: SRS-CTESW-FUN-180
SRS: SRS-CTESW-FUN-260
SRS: SRS-CTESW-FUN-265
SRS: SRS-CTESW-IF-050

Note: The test contains a "failing" CANSW Test Cases - they are designed to
fail at predefined moments.

Note: Test Cases are defined as required by [SRS-CTESW-FUN-060].

Note: The test validates that CTESW properly observes and reports the failure
of the Test Case [SRS-CTESW-FUN-070].

Note: All Test Cases' executions must be marked FAILED by the CTESW [SRS-CTESW-FUN-080].

Note: The failure of the application used by the Test Cases is reported by them
as non-zero exit code [SRS-CTESW-FUN-090] [SRS-CTESW-FUN-095].
The test executes failing application on both HWTB and Host.
The failure is designed to occur at moment chosen by Test Case author [SRS-CTESW-FUN-180].

Note: Predefined Test Case uses "always failing" Python script to enforce
failure of the Test Case, as specified in [SRS-CTESW-FUN-110]. Script uses
interface as described in [SRS-CTESW-FUN-130].

Note: The test uses CTESW feature [SRS-CTESW-FUN-260] to execute only the
selected "failing" Test Cases, one by one. As failure occures, the whole
execution must be marked as failed [SRS-CTESW-FUN-265]. The Test Case selection
for the execution is performed via CTESW CLI interface [SRS-CTESW-IF-050].


  $ source "${TESTDIR}/../common.sh"

Failures on HOST
  $ scons_wrapper test-case-fail-host | grep "scons: \*\*\* \\["
  scons: *** [*/test-case-fail-host.log] Error 42 (glob)

  $ cat "$TESTLOGPATH/test-case-fail-host.host.log"
  ============ TEST MAIN START ============
  Failed on HOST
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 0000002A

Failures on HWTB
  $ scons_wrapper test-case-fail-hwtb | grep "scons: \*\*\* \\["
  scons: *** [*/test-case-fail-hwtb.log] Error 13 (glob)

  $ cat "$TESTLOGPATH/test-case-fail-hwtb.hwtb.log"
  ============ TEST MAIN START ============
  Failed on HWTB
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 0000000D

Failures in additional asserts
  $ scons_wrapper test-case-fail-asserts | grep -A1 "additonal assertions failed"
  \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3} ctesw_test_case \[ERROR   \]\(MainThread\): FAIL: test-case-fail-asserts - additonal assertions failed (re)
  scons: *** [*/test-case-fail-asserts.log] Error -1 (glob)

