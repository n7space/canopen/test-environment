# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Traceability report generation test.

Given Test Case with defined traces
When generating traceability report
Then JSON file containing required information is created.
SRS: SRS-CTESW-FUN-220
SRS: SRS-CTESW-IF-080

  $ source "${TESTDIR}/../common.sh"

Dump traces
  $ scons_wrapper traces | grep "scons: done building targets."
  scons: done building targets.

Check created JSON
  $ cat "$TESTLOGPATH/path.json"
  [
    {
      "traces": [
        "SRS3"
      ],
      "doc": {
        "given": "single test case",
        "when": "describing it",
        "then": "it ends up in JSON"
      },
      "id": "test-case-for-tracing",
      "title": "test-case-for-tracing Test Case",
      "path": "tests/traces"
    },
    {
      "traces": [
        "SRS1",
        "SRS2"
      ],
      "doc": {
        "given": "something",
        "when": "doing",
        "then": "expecting"
      },
      "title": "symmetrical-test-case-for-tracing Test Case (Symmetrical)",
      "id": "symmetrical-test-case-for-tracing",
      "path": "tests/traces"
    }
  ] (no-eol)
