# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Unexpected messages detection

Given Test Case configured to fail on unexpected messages
When message is sent or received
Then Test Case fails with proper message

SRS: SRS-CTESW-FUN-060
SRS: SRS-CTESW-FUN-150
SRS: SRS-CTESW-FUN-160
SRS: SRS-CTESW-FUN-198

Note: The test contains Test Cases defined according to [SRS-CTESW-FUN-060].
Each Test Case is designed to fail upon sending or receiving CAN bus message
at different stage of Test Case execution or different link.

Note: The test validates Test Framework [SRS-CTESW-FUN-150] capabilities for
inspecting CAN bus exchange [SRS-CTESW-FUN-198].

Note: The same Test Cases applications are executed on both Host and HWTB, validating
Hardware Abstraction Layer provided by Test Framework [SRS-CTESW-FUN-160].

Note: The test will fail if any expected failure does not occur or log contents differ
from expected ones.


  $ source "${TESTDIR}/../common.sh"

Unexpected send on HOST
  $ scons_wrapper unexpected-sent-can-msg-host-to-hwtb | grep "scons: \*\*\* \\["
  scons: *** [*/unexpected-sent-can-msg-host-to-hwtb.log] Error 4294967295 (glob)

  $ cat "$TESTLOGPATH/unexpected-sent-can-msg-host-to-hwtb.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - message: \[ \] (re)
  \[.*\] - Unexpected message sent (re)
  \[.*\] - TEST FAILED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: FFFFFFFF

  $ cat "$TESTLOGPATH/unexpected-sent-can-msg-host-to-hwtb.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - Message received (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Unexpected send on HWTB
  $ scons_wrapper unexpected-sent-can-msg-hwtb-to-host | grep "scons: \*\*\* \\["
  scons: *** [*/unexpected-can-msg/unexpected-sent-can-msg-hwtb-to-host.log] Error 4294967295 (glob)

  $ cat "$TESTLOGPATH/unexpected-sent-can-msg-hwtb-to-host.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - Message received (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

  $ cat "$TESTLOGPATH/unexpected-sent-can-msg-hwtb-to-host.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - message: \[ \] (re)
  \[.*\] - Unexpected message sent (re)
  \[.*\] - TEST FAILED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: FFFFFFFF

Unexpected receive on HWTB
  $ scons_wrapper unexpected-recv-can-msg-host-to-hwtb | grep "scons: \*\*\* \\["
  scons: *** [*/unexpected-can-msg/unexpected-recv-can-msg-host-to-hwtb.log] Error 4294967295 (glob)

  $ cat "$TESTLOGPATH/unexpected-recv-can-msg-host-to-hwtb.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - Message sent (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

  $ cat "$TESTLOGPATH/unexpected-recv-can-msg-host-to-hwtb.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - message: \[ \] (re)
  \[.*\] - Unexpected message received (re)
  \[.*\] - TEST FAILED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: FFFFFFFF

Unexpected receive on HOST
  $ scons_wrapper unexpected-recv-can-msg-hwtb-to-host | grep "scons: \*\*\* \\["
  scons: *** [*/unexpected-can-msg/unexpected-recv-can-msg-hwtb-to-host.log] Error 4294967295 (glob)

  $ cat "$TESTLOGPATH/unexpected-recv-can-msg-hwtb-to-host.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - message: \[ \] (re)
  \[.*\] - Unexpected message received (re)
  \[.*\] - TEST FAILED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: FFFFFFFF

  $ cat "$TESTLOGPATH/unexpected-recv-can-msg-hwtb-to-host.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - Message sent (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Unexpected receive on second bus on HOST
  $ scons_wrapper unexpected-ignored-can-msg-hwtb-to-host | grep "scons: \*\*\* \\["
  scons: *** [*/unexpected-can-msg/unexpected-ignored-can-msg-hwtb-to-host.log] Error 4294967295 (glob)

  $ cat "$TESTLOGPATH/unexpected-ignored-can-msg-hwtb-to-host.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - message: \[ \] (re)
  \[.*\] - Message ignored (re)
  \[.*\] - TEST FAILED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: FFFFFFFF

  $ cat "$TESTLOGPATH/unexpected-ignored-can-msg-hwtb-to-host.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - Message sent (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Unexpected receive on second bus on HWTB 
  $ scons_wrapper unexpected-ignored-can-msg-host-to-hwtb | grep "scons: \*\*\* \\["
  scons: *** [*/unexpected-can-msg/unexpected-ignored-can-msg-host-to-hwtb.log] Error 4294967295 (glob)

  $ cat "$TESTLOGPATH/unexpected-ignored-can-msg-host-to-hwtb.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - Message sent (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

  $ cat "$TESTLOGPATH/unexpected-ignored-can-msg-host-to-hwtb.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - message: \[ \] (re)
  \[.*\] - Message ignored (re)
  \[.*\] - TEST FAILED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: FFFFFFFF
