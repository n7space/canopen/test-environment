/**@file
 * This file is part of the Test Environment internal tests.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <TestFramework/TestHarness.h>

void
TestSetup(can_net_t *const net)
{
	(void)net;
}

void
TestTeardown(void)
{
	// NOOP
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;
	LOG("Message received");
	FINISH_TEST();
}

void
TestStep(void)
{
	// NOOP
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
}
