/**@file
 * This file is part of the Test Environment internal tests.
 *
 * @copyright 2021-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>

#include <lely/util/error.h>

#include <TestFramework/HAL/CanInterface.h>
#include <TestFramework/TestHarness.h>

static can_net_t *net_;

void
TestSetup(can_net_t *const net)
{
	(void)net;
	SetCanCallbacksBusA(&FailOnMessageSent, &FailOnMessageReceived);

	net_ = net;
}

void
TestTeardown(void)
{
	// NOOP
}

void
TestMessageReceived(const struct can_msg *const msg)
{
	(void)msg;
	FAIL_TEST("Should not be called");
}

void
TestStep(void)
{
	const struct can_msg msg = CAN_MSG_INIT;
	const int result = can_net_send(net_, &msg);
	if (result != 0) {
		LOG_INT32("result", result);
		LOG_INT32("errc", get_errc());
		FAIL_TEST("Send failed");
	}
}

void
TestMessageSent(const struct can_msg *const msg)
{
	(void)msg;
	FAIL_TEST("Should not be called");
}
