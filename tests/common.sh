# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# File to be shared accross CRAM test

TOPDIR=$( cd "${TESTDIR}/../../" >/dev/null 2>&1 && pwd)

CTESW_BUILD_TYPE=${CTESW_BUILD_TYPE:=release}
SCONS_BUILD_DIR=${SCONS_BUILD_DIR:=build}

export BUILDPATH="${TOPDIR}"
export SCONSBUILDPATH="${BUILDPATH}/${SCONS_BUILD_DIR}/${CTESW_BUILD_TYPE}"
export TESTNAME="${TESTFILE%.t}"
export TESTLOGPATH="${SCONSBUILDPATH}/tests/${TESTNAME}"

COVERAGE_RCFILE="${BUILDPATH}/coverage.rc"

[[ "$CTESW_BUILD_TYPE" = "coverage" ]] \
    && SCONS_CALL="coverage run --rcfile=${COVERAGE_RCFILE} --append $(which scons)" \
    || SCONS_CALL="scons"

function scons_wrapper() {
    ${SCONS_CALL} --debug=explain ${SCONS_OPTS} buildDirName=${SCONS_BUILD_DIR} build=${CTESW_BUILD_TYPE} $1 2>&1 | tee "${TESTLOGPATH}/$1.scons.log"
}

mkdir -p "${TESTLOGPATH}"
cd "${BUILDPATH}"
