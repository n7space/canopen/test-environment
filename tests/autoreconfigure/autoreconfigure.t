# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Autotools support check

Given example autotools project
When reconfiguring it
Then CTESW build succeeds and configure script appears

SRS: SRS-CTESW-FUN-010
SRS: SRS-CTESW-FUN-015

Note: this test validates that internal CTESW features can be used to
configure any Autotools based project, so will work with CANSW and any platform.

Note: test fails if either autotools process will fail or configure script
will not appear.

  $ source "${TESTDIR}/../common.sh"

  $ scons_wrapper test-autoreconfigure | grep "scons: done building targets."
  scons: done building targets.

  $ ls "${SCONSBUILDPATH}/tests/autoreconfigure/src/configure"
  */build*/tests/autoreconfigure/src/configure (glob)
