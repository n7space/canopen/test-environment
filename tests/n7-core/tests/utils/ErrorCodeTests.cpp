/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  ErrorCodeTests.cpp
/// \brief ErrorCode tests suite implementation.

#include <n7s/utils/ErrorCode.h>

#include <CppUTest/TestHarness.h>

TEST_GROUP(ErrorCodeTests){};

/// @name returnError()
/// @{

/// \Given any error code
///
/// \When calling returnError with it any error code pointer and the error code
///
/// \Then false is always returned
///
TEST(ErrorCodeTests, returnError_alwaysReturnsFalse)
{
	CHECK_FALSE(returnError(nullptr, 0));
}

/// \Given not null pointer to error code
///
/// \When passing it to returnError
///
/// \Then memory contents referenced by pointer are updated with error code
///
TEST(ErrorCodeTests, returnError_setsErrCodeWhenNotNull)
{
	ErrorCode errCode = ErrorCode_NoError;

	const auto result = returnError(
			&errCode, ERROR_CODE_DEFINE(0x01, 0x02, 0x03, 0x04));

	LONGS_EQUAL(0x01020304, errCode);
	CHECK_FALSE(result);
}

/// @}
