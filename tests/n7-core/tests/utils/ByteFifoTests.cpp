/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  ByteFifoTests.cpp
/// \brief ByteFifo test suite implementation.

#include <n7s/utils/ByteFifo.h>

#include <array>

#include <CppUTest/TestHarness.h>

TEST_GROUP(ByteFifoTests){};

/// @name ByteFifo_init()
/// @{

/// \Given Memory array
///
/// \When using it to initialize ByteFifo
///
/// \Then queue fields point to memory array
///
TEST(ByteFifoTests, ByteFifo_init_setsFieldsToPointToGivenMemoryArray)
{
	std::array<uint8_t, 20> mem{};

	ByteFifo fifo;
	ByteFifo_init(&fifo, mem.data(), mem.size());

	POINTERS_EQUAL(mem.data(), fifo.begin);
	POINTERS_EQUAL(mem.data() + mem.size(), fifo.end);
	POINTERS_EQUAL(nullptr, fifo.first);
	POINTERS_EQUAL(mem.data(), fifo.last);
	CHECK_TRUE(ByteFifo_isEmpty(&fifo));
	CHECK_FALSE(ByteFifo_isFull(&fifo));
}

/// @}

/// @name ByteFifo_initFromBytes()
/// @{

/// \Given Memory array with data
///
/// \When using it to initialize ByteFifo
///
/// \Then queue fields point to memory array and ByteFifo is full
///
TEST(ByteFifoTests, ByteFifo_initFromBytes_setsFieldsToPointToGivenMemoryArray)
{
	std::array<uint8_t, 4> mem = { 13, 17, 23, 29 };

	ByteFifo fifo;
	ByteFifo_initFromBytes(&fifo, mem.data(), mem.size());

	uint8_t data = 0;
	POINTERS_EQUAL(mem.data(), fifo.begin);
	POINTERS_EQUAL(mem.data() + mem.size(), fifo.end);
	POINTERS_EQUAL(fifo.first, fifo.last);
	POINTERS_EQUAL(mem.data(), fifo.last);
	CHECK_FALSE(ByteFifo_isEmpty(&fifo));
	CHECK_TRUE(ByteFifo_isFull(&fifo));
	CHECK_TRUE(ByteFifo_pull(&fifo, &data));
	UNSIGNED_LONGS_EQUAL(13, data);
}

/// @}

/// @name BYTE_FIFO_CREATE()
/// @{

/// \Given no queue
///
/// \When constructing queue with create macro
///
/// \Then queue of desired size is created
///
TEST(ByteFifoTests, ByteFifo_createMacro_createsQueueOfRequestedSize)
{
	BYTE_FIFO_CREATE(fifo, 30);

	CHECK(fifo.begin != nullptr);
	POINTERS_EQUAL(fifo.last, fifo.begin);
	POINTERS_EQUAL(fifo.begin + 30, fifo.end);
	POINTERS_EQUAL(nullptr, fifo.first);
	CHECK_TRUE(ByteFifo_isEmpty(&fifo));
	CHECK_FALSE(ByteFifo_isFull(&fifo));
}

/// @}

/// @name BYTE_FIFO_CREATE_FILLED()
/// @{

/// \Given no queue
///
/// \When constructing queue with create macro
///
/// \Then queue with desired contents is created
///
TEST(ByteFifoTests, ByteFifo_createMacro_createsFilledQueue)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 12, 22, 32 });

	uint8_t data = 0;
	CHECK_TRUE(ByteFifo_isFull(&fifo));
	CHECK_TRUE(ByteFifo_pull(&fifo, &data));
	UNSIGNED_LONGS_EQUAL(12, data);
}

/// @}

/// @name ByteFifo_push()
/// @{

/// \Given empty ByteFifo
///
/// \When pushing item to it
///
/// \Then queue is no longer empty
///
TEST(ByteFifoTests, ByteFifo_push_makesQueueNotEmpty)
{
	BYTE_FIFO_CREATE(fifo, 30);

	const bool retCode = ByteFifo_push(&fifo, 0);

	CHECK_TRUE(retCode);
	CHECK_FALSE(ByteFifo_isEmpty(&fifo));
}

/// \Given empty ByteFifo
///
/// \When pushing items up to its capacity
///
/// \Then queue becomes full
///
TEST(ByteFifoTests, ByteFifo_push_makesQueueFull)
{
	BYTE_FIFO_CREATE(fifo, 2);

	CHECK_TRUE(ByteFifo_push(&fifo, 0));
	CHECK_TRUE(ByteFifo_push(&fifo, 0));

	CHECK_TRUE(ByteFifo_isFull(&fifo));
}

/// @}

/// @name ByteFifo_pull()
/// @{

/// \Given full ByteFifo
///
/// \When pulling all items
///
/// \Then queue becomes empty
///
TEST(ByteFifoTests, ByteFifo_pull_makesQueueEmpty)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 12, 22 });

	std::array<uint8_t, 2> data = { 0 };
	ByteFifo_pull(&fifo, &data[0]);
	ByteFifo_pull(&fifo, &data[1]);

	CHECK_FALSE(ByteFifo_isFull(&fifo));
	CHECK_TRUE(ByteFifo_isEmpty(&fifo));
	BYTES_EQUAL(12, data[0]);
	BYTES_EQUAL(22, data[1]);
}

/// @}

/// @name ByteFifo_push()
/// @{

/// \Given almost full ByteFifo
///
/// \When pushing to it
///
/// \Then ByteFifo becomes full
///
TEST(ByteFifoTests, ByteFifo_push_afterPullMakesQueueFullAgain)
{
	uint8_t data = 42;
	BYTE_FIFO_CREATE_FILLED(fifo, { 12, 22 });
	CHECK_TRUE(ByteFifo_pull(&fifo, &data));

	const bool retCode = ByteFifo_push(&fifo, 0);

	CHECK_TRUE(retCode);
	CHECK_TRUE(ByteFifo_isFull(&fifo));
	CHECK_FALSE(ByteFifo_isEmpty(&fifo));
}

/// @}

/// @name ByteFifo_pull()
/// @{

/// \Given ByteFifo filled after being pulled
///
/// \When pulling all items from it
///
/// \Then items are retrieved in proper order
///
TEST(ByteFifoTests, ByteFifo_pull_afterPushMakesItemAppearInProperOrder)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 12, 22 });
	std::array<uint8_t, 2> data = { 0 };
	ByteFifo_pull(&fifo, &data[0]);
	CHECK_TRUE(ByteFifo_push(&fifo, 42));

	ByteFifo_pull(&fifo, &data[0]);
	ByteFifo_pull(&fifo, &data[1]);

	CHECK_FALSE(ByteFifo_isFull(&fifo));
	CHECK_TRUE(ByteFifo_isEmpty(&fifo));
	BYTES_EQUAL(22u, data[0]); // cppcheck-suppress [misra-c2012-10.4]
	BYTES_EQUAL(42u, data[1]); // cppcheck-suppress [misra-c2012-10.4]
}

/// @}

/// @name ByteFifo_push()
/// @{

/// \Given full ByteFifo
///
/// \When trying to push to it
///
/// \Then false is returned
///
TEST(ByteFifoTests, ByteFifo_push_failWhenQueueIsFull)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 12, 22 });

	const bool retCode = ByteFifo_push(&fifo, 0);

	CHECK_FALSE(retCode);
}

/// @}

/// @name ByteFifo_pull()
/// @{

/// \Given empty ByteFifo
///
/// \When trying to pull from it
///
/// \Then false is returned
///
TEST(ByteFifoTests, ByteFifo_pull_failWhenQueueIsEmpty)
{
	BYTE_FIFO_CREATE(fifo, 10);

	uint8_t data = 42;
	const bool retCode = ByteFifo_pull(&fifo, &data);

	CHECK_FALSE(retCode);
}

/// @}

/// @name ByteFifo_clear()
/// @{

/// \Given full ByteFifo
///
/// \When clearing it
///
/// \Then queue becomes empty
///
TEST(ByteFifoTests, ByteFifo_clear_makesQueueEmpty)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 12, 22 });

	ByteFifo_clear(&fifo);

	POINTERS_EQUAL(nullptr, fifo.first);
	POINTERS_EQUAL(fifo.begin, fifo.last);
	CHECK_TRUE(ByteFifo_isEmpty(&fifo));
}

/// @}

/// @name ByteFifo_getCount()
/// @{

/// \Given not empty ByteFifo
///
/// \When getting its item count
///
/// \Then proper value is returned
///
TEST(ByteFifoTests, ByteFifo_getCount_returnsNumberOfElementsInFifo)
{
	BYTE_FIFO_CREATE(fifo, 2);
	CHECK_TRUE(ByteFifo_push(&fifo, 0x34));

	const size_t fifoCount = ByteFifo_getCount(&fifo);

	UNSIGNED_LONGS_EQUAL(1, fifoCount);
}

/// \Given empty ByteFifo
///
/// \When getting its item count
///
/// \Then zero is returned
///
TEST(ByteFifoTests, ByteFifo_getCount_returnsZeroIfFifoIsEmpty)
{
	BYTE_FIFO_CREATE(fifo, 2);

	const size_t fifoCount = ByteFifo_getCount(&fifo);

	UNSIGNED_LONGS_EQUAL(0, fifoCount);
}

/// \Given full ByteFifo
///
/// \When getting its item count
///
/// \Then its capacity is returned
///
TEST(ByteFifoTests, ByteFifo_getCount_returnsFifoCapacityWhenQueueIsFull)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 0x01, 0x02, 0x03 });

	const size_t fifoCount = ByteFifo_getCount(&fifo);

	UNSIGNED_LONGS_EQUAL(3, fifoCount);
}

/// \Given ByteFifo after cycling through its internal buffer
///
/// \When getting its item count
///
/// \Then proper value is returned
///
TEST(ByteFifoTests,
		ByteFifo_getCount_returnsCorrentNumberOfElementsAfterPointerOverflow)
{
	BYTE_FIFO_CREATE_FILLED(fifo, { 0x01, 0x02, 0x03 });
	uint8_t elem = 0x0;
	CHECK_TRUE(ByteFifo_pull(&fifo, &elem));
	CHECK_TRUE(ByteFifo_pull(&fifo, &elem));
	CHECK_TRUE(ByteFifo_push(&fifo, elem));

	const size_t fifoCount = ByteFifo_getCount(&fifo);

	UNSIGNED_LONGS_EQUAL(2, fifoCount);
}

/// @}
