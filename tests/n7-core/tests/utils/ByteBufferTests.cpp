/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  ByteBufferTests.cpp
/// \brief ByteBuffer test suite implementation.

#include <n7s/utils/ByteBuffer.h>

#include <array>

#include <CppUTest/TestHarness.h>

TEST_GROUP(ByteBufferTests){};

/// @name ByteBuffer_init()
/// @{

/// \Given new ByteBuffer
///
/// \When initializing it with memory array
///
/// \Then buffer fields point to array
///
TEST(ByteBufferTests, ByteBuffer_init_setsFieldsToPointToGivenMemoryArray)
{
	std::array<uint8_t, 20> mem{};

	ByteBuffer buf;
	ByteBuffer_init(&buf, mem.data(), mem.size());

	POINTERS_EQUAL(mem.data(), buf.begin);
	POINTERS_EQUAL(buf.end, buf.begin);
	POINTERS_EQUAL(mem.data() + mem.size(), buf.capacityEnd);
}

/// @}

/// @name ByteBuffer_getCapacity()
/// @{

/// \Given ByteBuffer initialized with some memory array
///
/// \When getting buffer's capacity
///
/// \Then memory array size is returned
///
TEST(ByteBufferTests, ByteBuffer_getCapacity_returnsBufferCapacity)
{
	std::array<uint8_t, 20> mem{};
	ByteBuffer buf;
	ByteBuffer_init(&buf, mem.data(), mem.size());

	const size_t capacity = ByteBuffer_getCapacity(&buf);

	UNSIGNED_LONGS_EQUAL(mem.size(), capacity);
}

/// @}

/// @name BYTE_BUFFER_CREATE()
/// @{

/// \Given no buffer
///
/// \When using ByteBuffer construction macro
///
/// \Then buffer with required capacity is created
///
TEST(ByteBufferTests, ByteBuffer_createMacro_createsInitializedBuffer)
{
	BYTE_BUFFER_CREATE(buffer, 30);

	CHECK(buffer.begin != nullptr);
	UNSIGNED_LONGS_EQUAL(30, ByteBuffer_getCapacity(&buffer));
}

/// @}

/// @name ByteBuffer_getCount()
/// @{

/// \Given newly created ByteBuffer
///
/// \When getting buffer's element count
///
/// \Then zero is returned
///
TEST(ByteBufferTests, ByteBuffer_getCount_returnsZeroForNewBuffer)
{
	BYTE_BUFFER_CREATE(buffer, 20);

	const size_t count = ByteBuffer_getCount(&buffer);

	UNSIGNED_LONGS_EQUAL(0, count);
}

/// \Given empty ByteBuffer
///
/// \When appending item to buffer
///
/// \Then buffer item count is updated
///
TEST(ByteBufferTests, ByteBuffer_getCount_returnsCountAfterAppend)
{
	BYTE_BUFFER_CREATE(buffer, 20);

	CHECK_TRUE(ByteBuffer_append(&buffer, 0xAAu));

	UNSIGNED_LONGS_EQUAL(1, ByteBuffer_getCount(&buffer));
}

/// @}

/// @name ByteBuffer_append()
/// @{

/// \Given empty ByteBuffer
///
/// \When appending items
///
/// \Then those items are present in buffer
///
TEST(ByteBufferTests, ByteBuffer_append_addsValuesToBuffer)
{
	BYTE_BUFFER_CREATE(buffer, 5);

	CHECK_TRUE(ByteBuffer_append(&buffer, 0xAAu));
	CHECK_TRUE(ByteBuffer_append(&buffer, 0xBBu));

	const uint8_t expected[] = { 0xAA, 0xBB };
	MEMCMP_EQUAL(expected, buffer.begin, sizeof(expected));
}

/// \Given not full ByteBuffer
///
/// \When appending item
///
/// \Then true is returned
///
TEST(ByteBufferTests, ByteBuffer_append_returnsTrueAfterSuccesfulAppend)
{
	BYTE_BUFFER_CREATE(buffer, 5);

	const bool r = ByteBuffer_append(&buffer, 0xAAu);

	CHECK_TRUE(r);
}

/// \Given full ByteBuffer
///
/// \When trying to append item
///
/// \Then false is returned
///
TEST(ByteBufferTests, ByteBuffer_append_returnsFalseWhenBufferIsFull)
{
	BYTE_BUFFER_CREATE(buffer, 2);
	CHECK_TRUE(ByteBuffer_append(&buffer, 0xAAu));
	CHECK_TRUE(ByteBuffer_append(&buffer, 0xAAu));

	const bool r = ByteBuffer_append(&buffer, 0xAAu);

	CHECK_FALSE(r);
}

/// @}

/// @name ByteBuffer_clear()
/// @{

/// \Given not empty ByteBuffer
///
/// \When clearing it
///
/// \Then buffer item count is set to zero
///
TEST(ByteBufferTests, ByteBuffer_clear_resetsSize)
{
	BYTE_BUFFER_CREATE(buffer, 5);
	CHECK_TRUE(ByteBuffer_append(&buffer, 0xAAu));
	CHECK_TRUE(ByteBuffer_append(&buffer, 0xBBu));

	ByteBuffer_clear(&buffer);

	UNSIGNED_LONGS_EQUAL(0, ByteBuffer_getCount(&buffer));
}

/// @}

/// @name ByteBuffer_memset()
/// @{

/// \Given ByteBuffer
///
/// \When memsetting it with given value
///
/// \Then buffer contents are set to that value
///
TEST(ByteBufferTests, ByteBuffer_memset_setsBufferMemoryContents)
{
	BYTE_BUFFER_CREATE(buf, 5);

	ByteBuffer_memset(&buf, 0xFEu);

	const uint8_t expected[] = { 0xFE, 0xFE, 0xFE, 0xFE, 0xFE };
	MEMCMP_EQUAL(expected, buf.begin, ByteBuffer_getCapacity(&buf));
}

/// @}

/// @name BYTE_BUFFER_CREATE_FILLED()
/// @{

/// \Given no buffer
///
/// \When constructing ByteBuffer with create macro
///
/// \Then buffer is created with desired contents
///
TEST(ByteBufferTests, ByteBuffer_createMacro_createsFilledBuffer)
{
	BYTE_BUFFER_CREATE_FILLED(buffer, { 0xFE, 0xFA, 0x80 });

	const auto expected = std::array<uint8_t, 3>({ 0xFE, 0xFA, 0x80 });
	UNSIGNED_LONGS_EQUAL(sizeof(expected), ByteBuffer_getCount(&buffer));
	UNSIGNED_LONGS_EQUAL(ByteBuffer_getCapacity(&buffer),
			ByteBuffer_getCount(&buffer));
	MEMCMP_EQUAL(expected.data(), buffer.begin, sizeof(expected));
}

/// @}

/// @name ByteBuffer_isFull()
/// @{

/// \Given empty ByteBuffer
///
/// \When checking if it's full
///
/// \Then false is returned
///
TEST(ByteBufferTests, ByteBuffer_isFull_returnsFalseForNotFullBuffer)
{
	BYTE_BUFFER_CREATE(buffer, 5);

	const bool full = ByteBuffer_isFull(&buffer);

	CHECK_FALSE(full);
}

/// \Given full ByteBuffer
///
/// \When checking if it's full
///
/// \Then true is returned
///
TEST(ByteBufferTests, ByteBuffer_isFull_returnsTrueForFullBuffer)
{
	BYTE_BUFFER_CREATE_FILLED(buffer, { 0x00 });

	const bool full = ByteBuffer_isFull(&buffer);

	CHECK_TRUE(full);
}

/// @}
