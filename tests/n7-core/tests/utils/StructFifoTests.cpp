/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  StructFifoTests.cpp
/// \brief StructFifo unit test suite implementation.

#include <n7s/utils/StructFifo.h>

#include <array>

#include <CppUTest/TestHarness.h>

TEST_GROUP(StructFifoTests){};

struct DummyStruct {
	uint32_t contents;
};

/// @name StructFifo_init()
/// @{

/// \Given Memory array
///
/// \When using it to initialize StructFifo
///
/// \Then queue fields point to memory array
///
TEST(StructFifoTests, StructFifo_init_setsFieldsToPointToGivenMemoryArray)
{
	std::array<DummyStruct, 20> mem{};

	StructFifo fifo;
	StructFifo_init(&fifo, mem.data(), sizeof(DummyStruct), mem.size());

	POINTERS_EQUAL(mem.data(), fifo.begin);
	POINTERS_EQUAL(mem.data() + mem.size(), fifo.end);
	POINTERS_EQUAL(nullptr, fifo.first);
	POINTERS_EQUAL(mem.data(), fifo.last);
	UNSIGNED_LONGS_EQUAL(sizeof(DummyStruct), fifo.elementSize);
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	CHECK_FALSE(StructFifo_isFull(&fifo));
}

/// @}

/// @name STRUCT_FIFO_CREATE()
/// @{

/// \Given no queue
///
/// \When constructing queue with create macro
///
/// \Then queue of desired size is created
///
TEST(StructFifoTests, StructFifo_createMacro_createsQueueOfRequestedSize)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 30); // NOLINT

	CHECK(fifo.begin != nullptr);
	POINTERS_EQUAL(fifo.last, fifo.begin);
	POINTERS_EQUAL(fifo.begin + (30u * sizeof(DummyStruct)), fifo.end);
	POINTERS_EQUAL(nullptr, fifo.first);
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	CHECK_FALSE(StructFifo_isFull(&fifo));
}

/// @}

/// @name STRUCT_FIFO_CREATE_FILLED()
/// @{

/// \Given no queue
///
/// \When constructing queue with create macro
///
/// \Then queue with desired contents is created
///
TEST(StructFifoTests, StructFifo_createMacro_createsFilledQueue)
{
	STRUCT_FIFO_CREATE_FILLED(fifo, DummyStruct,
			{ { 12 }, { 21 }, { 32 } }); // NOLINT

	DummyStruct data{};
	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_pull(&fifo, &data));
	LONGS_EQUAL(12, data.contents);
	CHECK_TRUE(StructFifo_pull(&fifo, &data));
	LONGS_EQUAL(21, data.contents);
	CHECK_TRUE(StructFifo_pull(&fifo, &data));
	LONGS_EQUAL(32, data.contents);
}

/// @}

/// @name StructFifo_push()
/// @{

/// \Given empty StructFifo
///
/// \When pushing item to it
///
/// \Then queue is no longer empty
///
TEST(StructFifoTests, StructFifo_push_makesQueueNotEmpty)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 30); // NOLINT

	const DummyStruct data = { 12 };
	const bool retCode = StructFifo_push(&fifo, &data);

	CHECK_TRUE(retCode);
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// \Given empty StructFifo
///
/// \When pushing items up to its capacity
///
/// \Then queue becomes full
///
TEST(StructFifoTests, StructFifo_push_makesQueueFull)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 2); // NOLINT

	const DummyStruct data = { 12 };
	CHECK_TRUE(StructFifo_push(&fifo, &data));
	CHECK_TRUE(StructFifo_push(&fifo, &data));

	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// @}

/// @name StructFifo_pushUnaligned()
/// @{

/// \Given empty StructFifo
///
/// \When pushing item to it
///
/// \Then queue is no longer empty
///
TEST(StructFifoTests, StructFifo_pushUnaligned_makesQueueNotEmpty)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 30); // NOLINT

	const DummyStruct data = { 12 };
	const bool retCode = StructFifo_pushUnaligned(&fifo, &data);

	CHECK_TRUE(retCode);
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// \Given empty StructFifo
///
/// \When pushing items up to its capacity
///
/// \Then queue becomes full
///
TEST(StructFifoTests, StructFifo_pushUnaligned_makesQueueFull)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 2); // NOLINT

	const DummyStruct data = { 12 };
	CHECK_TRUE(StructFifo_pushUnaligned(&fifo, &data));
	CHECK_TRUE(StructFifo_pushUnaligned(&fifo, &data));

	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// @}

/// @name StructFifo_pull()
/// @{

/// \Given full StructFifo
///
/// \When pulling all items
///
/// \Then queue becomes empty
///
TEST(StructFifoTests, StructFifo_pull_makesQueueEmpty)
{
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT

	std::array<DummyStruct, 2> data{};
	CHECK_TRUE(StructFifo_pull(&fifo, &data[0]));
	CHECK_TRUE(StructFifo_pull(&fifo, &data[1]));

	CHECK_FALSE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	LONGS_EQUAL(12, data[0].contents);
	LONGS_EQUAL(21, data[1].contents);
}

/// @}

/// @name StructFifo_pullUnaligned()
/// @{

/// \Given full StructFifo
///
/// \When pulling all items
///
/// \Then queue becomes empty
///
TEST(StructFifoTests, StructFifo_pullUnaligned_makesQueueEmpty)
{
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT

	std::array<DummyStruct, 2> data{};
	CHECK_TRUE(StructFifo_pullUnaligned(&fifo, &data[0]));
	CHECK_TRUE(StructFifo_pullUnaligned(&fifo, &data[1]));

	CHECK_FALSE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	LONGS_EQUAL(12, data[0].contents);
	LONGS_EQUAL(21, data[1].contents);
}

/// @}

/// @name StructFifo_peek()
/// @{

/// \Given not empty StructFifo
///
/// \When peeking an item
///
/// \Then queue stays not empty
///
TEST(StructFifoTests, StructFifo_peek_doesNotMakeQueueEmpty)
{
	STRUCT_FIFO_CREATE_FILLED(fifo, DummyStruct, { { 12 } }); // NOLINT

	std::array<DummyStruct, 1> data{};
	CHECK_TRUE(StructFifo_peek(&fifo, &data[0]));

	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
	LONGS_EQUAL(12, data[0].contents);
}

/// @}

/// @name StructFifo_drop()
/// @{

/// \Given full StructFifo
///
/// \When dropping all items
///
/// \Then queue becomes empty
///
TEST(StructFifoTests, StructFifo_drop_makesQueueEmpty)
{
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT

	CHECK_TRUE(StructFifo_drop(&fifo));
	CHECK_TRUE(StructFifo_drop(&fifo));

	CHECK_FALSE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
}

/// \Given empty StructFifo
///
/// \When dropping an item
///
/// \Then queue remains valid and can be filled
///
TEST(StructFifoTests, StructFifo_drop_onQueueEmpty)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 2); // NOLINT

	const bool retCode = StructFifo_drop(&fifo);

	CHECK_FALSE(retCode);
	CHECK_TRUE(StructFifo_isEmpty(&fifo));

	const DummyStruct data = { 12 };
	CHECK_TRUE(StructFifo_pushUnaligned(&fifo, &data));
	CHECK_TRUE(StructFifo_pushUnaligned(&fifo, &data));

	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// @}

/// @name StructFifo_push()
/// @{

/// \Given almost full StructFifo
///
/// \When pushing to it
///
/// \Then StructFifo becomes full
///
TEST(StructFifoTests, StructFifo_push_afterPullMakesQueueFullAgain)
{
	DummyStruct data{};
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT
	CHECK_TRUE(StructFifo_pull(&fifo, &data));

	const bool retCode = StructFifo_push(&fifo, &data);

	CHECK_TRUE(retCode);
	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// @}

/// @name StructFifo_pull()
/// @{

/// \Given StructFifo filled after being pulled
///
/// \When pulling all items from it
///
/// \Then items are retrieved in proper order
///
TEST(StructFifoTests, StructFifo_pull_afterPushMakesItemAppearInProperOrder)
{
	std::array<DummyStruct, 2> data; // NOLINT
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT
	CHECK_TRUE(StructFifo_pull(&fifo, &data[0]));
	const DummyStruct input = { .contents = 42 };
	CHECK_TRUE(StructFifo_push(&fifo, &input));

	CHECK_TRUE(StructFifo_pull(&fifo, &data[0]));
	CHECK_TRUE(StructFifo_pull(&fifo, &data[1]));

	CHECK_FALSE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	LONGS_EQUAL(21, data[0].contents);
	LONGS_EQUAL(42, data[1].contents);
}

/// @}

/// @name StructFifo_pushUnaligned()
/// @{

/// \Given almost full StructFifo
///
/// \When pushing to it
///
/// \Then StructFifo becomes full
///
TEST(StructFifoTests, StructFifo_pushUnaligned_afterPullMakesQueueFullAgain)
{
	DummyStruct data{};
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT
	CHECK_TRUE(StructFifo_pullUnaligned(&fifo, &data));

	const bool retCode = StructFifo_pushUnaligned(&fifo, &data);

	CHECK_TRUE(retCode);
	CHECK_TRUE(StructFifo_isFull(&fifo));
	CHECK_FALSE(StructFifo_isEmpty(&fifo));
}

/// @}

/// @name StructFifo_pullUnaligned()
/// @{

/// \Given StructFifo filled after being pulled
///
/// \When pulling all items from it
///
/// \Then items are retrieved in proper order
///
TEST(StructFifoTests,
		StructFifo_pullUnaligned_afterPushMakesItemAppearInProperOrder)
{
	std::array<DummyStruct, 2> data{};
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT
	CHECK_TRUE(StructFifo_pullUnaligned(&fifo, &data[0]));
	const DummyStruct input = { .contents = 42 };
	CHECK_TRUE(StructFifo_pushUnaligned(&fifo, &input));

	CHECK_TRUE(StructFifo_pullUnaligned(&fifo, &data[0]));
	CHECK_TRUE(StructFifo_pullUnaligned(&fifo, &data[1]));

	CHECK_FALSE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	LONGS_EQUAL(21, data[0].contents);
	LONGS_EQUAL(42, data[1].contents);
}

/// @}

/// @name StructFifo_peek()
/// @{

/// \Given StructFifo filled after being pulled
///
/// \When peeking and dropping all items from it
///
/// \Then items are retrieved in proper order
///
TEST(StructFifoTests,
		StructFifo_peekAndDrop_afterPushMakesItemAppearInProperOrder)
{
	std::array<DummyStruct, 2> data{};
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT
	CHECK_TRUE(StructFifo_pull(&fifo, &data[0]));
	const DummyStruct input = { .contents = 42 };
	CHECK_TRUE(StructFifo_push(&fifo, &input));

	CHECK_TRUE(StructFifo_peek(&fifo, &data[0]));
	CHECK_TRUE(StructFifo_drop(&fifo));
	CHECK_TRUE(StructFifo_peek(&fifo, &data[1]));
	CHECK_TRUE(StructFifo_drop(&fifo));

	CHECK_FALSE(StructFifo_isFull(&fifo));
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
	LONGS_EQUAL(21, data[0].contents);
	LONGS_EQUAL(42, data[1].contents);
}

/// @}

/// @name StructFifo_push()
/// @{

/// \Given full StructFifo
///
/// \When trying to push to it
///
/// \Then false is returned
///
TEST(StructFifoTests, StructFifo_push_failWhenQueueIsFull)
{
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT

	const DummyStruct data = { .contents = 13 };
	const bool retCode = StructFifo_push(&fifo, &data);

	CHECK_FALSE(retCode);
}

/// @}

/// @name StructFifo_pull()
/// @{

/// \Given empty StructFifo
///
/// \When trying to pull from it
///
/// \Then false is returned
///
TEST(StructFifoTests, StructFifo_pull_failWhenQueueIsEmpty)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 10); // NOLINT

	DummyStruct data{};
	const bool retCode = StructFifo_pull(&fifo, &data);

	CHECK_FALSE(retCode);
}

/// @}

/// @name StructFifo_pushUnaligned()
/// @{

/// \Given full StructFifo
///
/// \When trying to push to it
///
/// \Then false is returned
///
TEST(StructFifoTests, StructFifo_pushUnaligned_failWhenQueueIsFull)
{
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT

	const DummyStruct data = { .contents = 13 };
	const bool retCode = StructFifo_pushUnaligned(&fifo, &data);

	CHECK_FALSE(retCode);
}

/// @}

/// @name StructFifo_pullUnaligned()
/// @{

/// \Given empty StructFifo
///
/// \When trying to pull from it
///
/// \Then false is returned
///
TEST(StructFifoTests, StructFifo_pullUnaligned_failWhenQueueIsEmpty)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 10); // NOLINT

	DummyStruct data{};
	const bool retCode = StructFifo_pullUnaligned(&fifo, &data);

	CHECK_FALSE(retCode);
}

/// @}

/// @name StructFifo_peek()
/// @{

/// \Given empty StructFifo
///
/// \When trying to peek from it
///
/// \Then false is returned
///
TEST(StructFifoTests, StructFifo_peek_failWhenQueueIsEmpty)
{
	STRUCT_FIFO_CREATE(fifo, DummyStruct, 10); // NOLINT

	DummyStruct data{};
	const bool retCode = StructFifo_peek(&fifo, &data);

	CHECK_FALSE(retCode);
}

/// @}

/// @name StructFifo_clear()
/// @{

/// \Given full ByteFifo
///
/// \When clearing it
///
/// \Then queue becomes empty
///
TEST(StructFifoTests, StructFifo_clear_makesQueueEmpty)
{
	STRUCT_FIFO_CREATE_FILLED(
			fifo, DummyStruct, { { 12 }, { 21 } }); // NOLINT

	StructFifo_clear(&fifo);

	POINTERS_EQUAL(nullptr, fifo.first);
	POINTERS_EQUAL(fifo.begin, fifo.last);
	CHECK_TRUE(StructFifo_isEmpty(&fifo));
}

/// @}
