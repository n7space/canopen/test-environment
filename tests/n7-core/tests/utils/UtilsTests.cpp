/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  UtilsTests.cpp
/// \brief Utility functions unit test suite implementation.

#include <n7s/utils/Utils.h>

#include <CppUTest/TestHarness.h>

TEST_GROUP(UtilsTests){};

/// @name minUInt32()
/// @{

/// \Given N/A,
///
/// \When minUInt32() is called with two uint32_t values, with first one being smaller,
///
/// \Then the first value is returned.
///
TEST(UtilsTests, minUInt32_firstValueIsSmallerThanTheSecondOne)
{
	LONGS_EQUAL(1, minUInt32(1, 2));
}

/// \Given N/A,
///
/// \When minUInt32() is called with two uint32_t values, with second one being smaller,
///
/// \Then the second value is returned.
///
TEST(UtilsTests, minUInt32_secondValueIsSmallerThanTheFirstOne)
{
	LONGS_EQUAL(1, minUInt32(2, 1));
}

/// \Given N/A,
///
/// \When minUInt32() is called with two uint32_t values, with both being equal,
///
/// \Then the value is returned.
///
TEST(UtilsTests, minUInt32_valuesAreEqual) { LONGS_EQUAL(1, minUInt32(1, 1)); }

/// @}

/// @name maxUInt32()
/// @{

/// \Given N/A,
///
/// \When maxUInt32() is called with two uint32_t values, with first one being bigger,
///
/// \Then the first value is returned.
///
TEST(UtilsTests, maxUInt32_firstValueIsBiggerThanTheSecondOne)
{
	LONGS_EQUAL(2, maxUInt32(2, 1));
}

/// \Given N/A,
///
/// \When maxUInt32() is called with two uint32_t values, with second one being bigger,
///
/// \Then the second value is returned.
///
TEST(UtilsTests, maxUInt32_secondValueIsBiggerThanTheFirstOne)
{
	LONGS_EQUAL(2, maxUInt32(1, 2));
}

/// \Given N/A,
///
/// \When maxUInt32() is called with two uint32_t values, with both being equal,
///
/// \Then the value is returned.
///
TEST(UtilsTests, maxUInt32_valuesAreEqual) { LONGS_EQUAL(1, maxUInt32(1, 1)); }

/// @}

/// @name uint8ToBcd()
/// @{

/// \Given a set of numbers
///
/// \When converting them to BCD
///
/// \Then the converted values are correct.
///
TEST(UtilsTests, uint8ToBcd_correctlyConvertsValues)
{
	LONGS_EQUAL(0x99, uint8ToBcd(99));
	LONGS_EQUAL(0x00,
			uint8ToBcd(00)); // cppcheck-suppress [misra-c2012-7.1]
	LONGS_EQUAL(0x19, uint8ToBcd(19));
	LONGS_EQUAL(0x28, uint8ToBcd(28));
	LONGS_EQUAL(0x37, uint8ToBcd(37));
	LONGS_EQUAL(0x46, uint8ToBcd(46));
	LONGS_EQUAL(0x55, uint8ToBcd(55));
	LONGS_EQUAL(0x64, uint8ToBcd(64));
	LONGS_EQUAL(0x73, uint8ToBcd(73));
	LONGS_EQUAL(0x82, uint8ToBcd(82));
	LONGS_EQUAL(0x91, uint8ToBcd(91));
}

/// @}

/// @name bcdToUint8()
/// @{

/// \Given a set of numbers
///
/// \When converting them from BCD
///
/// \Then the converted values are correct.
///
TEST(UtilsTests, bcdToUint8_correctlyConvertsValues)
{
	LONGS_EQUAL(99, bcdToUint8(0x99u));
	LONGS_EQUAL(00, bcdToUint8(0x00u)); // cppcheck-suppress [misra-c2012-7.1]
	LONGS_EQUAL(19, bcdToUint8(0x19u));
	LONGS_EQUAL(28, bcdToUint8(0x28u));
	LONGS_EQUAL(37, bcdToUint8(0x37u));
	LONGS_EQUAL(46, bcdToUint8(0x46u));
	LONGS_EQUAL(55, bcdToUint8(0x55u));
	LONGS_EQUAL(64, bcdToUint8(0x64u));
	LONGS_EQUAL(73, bcdToUint8(0x73u));
	LONGS_EQUAL(82, bcdToUint8(0x82u));
	LONGS_EQUAL(91, bcdToUint8(0x91u));
}

/// @}

static bool
trueLambda()
{
	return true;
}

static bool
falseLambda()
{
	return false;
}

static bool
trueArgLambda(void *const arg)
{
	(void)arg;
	return true;
}

static bool
falseArgLambda(void *const arg)
{
	(void)arg;
	return false;
}

/// @name evaluateLambdaWithTimeout()
/// @{

/// \Given two boolean lambdas, one always false, other always true,
///
/// \When they are evaluated with a timeout,
///
/// \Then the true lambda yields success while the false one yields failure.
///
TEST(UtilsTests, evaluateLambdaWithTimeout_evaluatesCorrectly)
{
	CHECK_TRUE(evaluateLambdaWithTimeout(trueLambda, 10));
	CHECK_FALSE(evaluateLambdaWithTimeout(falseLambda, 10));
}

/// @}

/// @name waitForRegisterWithTimeout()
/// @{

/// \Given data register, with some bits set
///
/// \When is waited with a timeout and mask on set bit,
///
/// \Then return the true.
///
TEST(UtilsTests, waitForRegisterWithTimeout_maskSetBit)
{
	const uint32_t data = 0x01;

	CHECK_TRUE(waitForRegisterWithTimeout(&data, 0x01, 10));
}

/// \Given data register, with some bits set
///
/// \When is waited with a timeout and mask on not set bit,
///
/// \Then return the false.
///
TEST(UtilsTests, waitForRegisterWithTimeout_maskNotSetBit)
{
	const uint32_t data = 0x01;

	CHECK_FALSE(waitForRegisterWithTimeout(&data, 0x02, 10));
}

/// @}

/// @name waitForRegisterClearWithTimeout()
/// @{

/// \Given data register, with some bits set
///
/// \When is waited for clear with a timeout and mask on set bit,
///
/// \Then return the false.
///
TEST(UtilsTests, waitForRegisterClearWithTimeout_maskSetBit)
{
	const uint32_t data = 0x01;

	CHECK_FALSE(waitForRegisterClearWithTimeout(&data, 0x01, 10));
}

/// \Given data register, with some bits set
///
/// \When is waited for clear with a timeout and mask on not set bit,
///
/// \Then return the true.
///
TEST(UtilsTests, waitForRegisterClearWithTimeout_maskNotSetBit)
{
	const uint32_t data = 0x01;

	CHECK_TRUE(waitForRegisterClearWithTimeout(&data, 0x02, 10));
}

/// @}

/// @name evaluateArgLambdaWithTimeout()
/// @{

/// \Given two boolean lambdas with argument, one always false, other always true,
///
/// \When they are evaluated with a timeout,
///
/// \Then the true lambda yields success while the false one yields failure.
///
TEST(UtilsTests, evaluateArgLambdaWithTimeout_evaluatesCorrectly)
{
	CHECK_TRUE(evaluateArgLambdaWithTimeout(trueArgLambda, nullptr, 10));
	CHECK_FALSE(evaluateArgLambdaWithTimeout(falseArgLambda, nullptr, 10));
}

/// @}

/// @name isBetweenUint32()
/// @{

/// \Given a range and a set of numbers,
///
/// \When isBetweenUint32() is evaluated for the range and each of the numbers,
///
/// \Then the evaluation yields correct answer.
///
TEST(UtilsTests, isBetweenUint32_evaluatesCorrectly)
{
	const uint32_t upperBound = 28;
	const uint32_t lowerBound = 17;
	CHECK_TRUE(isBetweenUint32((lowerBound + upperBound) / 2u, lowerBound,
			upperBound));
	CHECK_FALSE(isBetweenUint32(lowerBound, lowerBound, upperBound));
	CHECK_FALSE(isBetweenUint32(upperBound, lowerBound, upperBound));
	CHECK_FALSE(isBetweenUint32(
			(uint32_t)(upperBound + 1u), lowerBound, upperBound));
	CHECK_FALSE(isBetweenUint32(
			(uint32_t)(lowerBound - 1u), lowerBound, upperBound));
}

/// @}

/// @name busyWaitLoop()
/// @{

/// \Given number of iterations,
///
/// \When busyWaitLoop() is called,
///
/// \Then function finishes normally after performing the given amount of iterations.
///
/// \Note as observing the impact of the function is diffucult from inside the processor,
///       the method was verified by inspection of the generated assembly.
TEST(UtilsTests, busyWaitLoop_executesNumberOfIterations)
{
	const uint32_t delay = 100u;

	busyWaitLoop(delay);

	// nothing to verify here. Inspect assembly to check if the loop is not reordered.
}

/// @}
