/**@file
 * This file is part of the N7-Core library used in the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  BitsTests.cpp
/// \brief Bit operations test suite implementation.

#include <n7s/utils/Bits.h>

#include <CppUTest/TestHarness.h>

TEST_GROUP(BitsTests){};

/// @name changeBitAtOffset()
/// @{

/// \Given uint32_t value with chosen bit set
///
/// \When calling changeBitAtOffset() with pointer to that value, the offset of chosen bit and false
///
/// \Then bit in the field is reset
///
TEST(BitsTests, changeBitAtOffset_resetsSelectedBit)
{
	uint32_t value = 0xFF0300FFu;
	const uint32_t offset = 17u;

	changeBitAtOffset(&value, offset, false);

	UNSIGNED_LONGS_EQUAL(0xFF0100FFu, value);
}

/// \Given uint32_t value with chosen bit reset
///
/// \When calling changeBitAtOffset() with pointer to that value, the offset of chosen bit and false
///
/// \Then the value is not changed
///
TEST(BitsTests, changeBitAtOffset_doNotChangesAlreadyResetBit)
{
	uint32_t value = 0xFF0100FFu;
	const uint32_t offset = 17u;

	changeBitAtOffset(&value, offset, false);

	UNSIGNED_LONGS_EQUAL(0xFF0100FFu, value);
}

/// \Given uint32_t value with chosen bit reseted
///
/// \When calling changeBitAtOffset() with pointer to that value, the offset of chosen bit and true
///
/// \Then bit in the field is reset
///
TEST(BitsTests, changeBitAtOffset_setsSelectedBit)
{
	uint32_t value = 0xFF0010FFu;
	const uint32_t offset = 13u;

	changeBitAtOffset(&value, offset, true);

	UNSIGNED_LONGS_EQUAL(0xFF0030FFu, value);
}

/// \Given uint32_t value with chosen bit set
///
/// \When calling changeBitAtOffset() with pointer to that value, the offset of chosen bit and true
///
/// \Then the value is not changed
///
TEST(BitsTests, changeBitAtOffset_doNotChangesAlreadySetBit)
{
	uint32_t value = 0xFF0030FFu;
	const uint32_t offset = 13u;

	changeBitAtOffset(&value, offset, true);

	UNSIGNED_LONGS_EQUAL(0xFF0030FFu, value);
}

/// @}

/// @name setValueAtOffset()
/// @{

/// \Given uint32_t value with some value
///
/// \When calling setValueAtOffset() with pointer to that value, desired value of chosen bits,
///       and the offset and mask of those bits
///
/// \Then the value at selected bits is changed
///
TEST(BitsTests, setValueAtOffset_changesBitValues)
{
	uint32_t value = 0xFF00FF00u;
	const uint32_t offset = 12u;

	setValueAtOffset(&value, 0x0DEAD033u, offset, 0x000FF000u);

	UNSIGNED_LONGS_EQUAL(0xFF033F00u, value);
}

/// @}

/// @name getValueAtOffset()
/// @{

/// \Given an uint32_t value with some value
///
/// \When calling getValueAtOffset() with offset and mask of desired value
///
/// \Then the returned value is a right-aligned value of masked bits
///
TEST(BitsTests, getValueAtOffset_returnsCorrectValue)
{
	const uint32_t exampleValue = 0xDEADBEEFu;
	const uint32_t expectedValue = 0x0000DEADu;
	const uint32_t offset = 16u;
	const uint32_t mask = 0xFFFF0000u;

	const uint32_t readValue = getValueAtOffset(exampleValue, offset, mask);
	UNSIGNED_LONGS_EQUAL(readValue, expectedValue);
}

/// @}

/// @name shiftValueLeft()
/// @{

/// \Given N/A
///
/// \When calling shiftValue() with selected value and the desired offset and mask of this value
///
/// \Then the shifted and masked value is returned
///
TEST(BitsTests, shiftValueLeft_returnsTransformedValue)
{
	const uint32_t value = 0x0DEAD033u;
	const uint32_t offset = 12u;
	const uint32_t mask = UINT32_C(0xFF) << offset;

	const auto result = shiftValueLeft(value, offset, mask);

	UNSIGNED_LONGS_EQUAL(0x00033000u, result);
}

/// @}

/// @name shiftBitLeft()
/// @{

/// \Given N/A
///
/// \When calling shiftValue() with true value and the desired offset of this value
///
/// \Then the shifted and value is returned
///
TEST(BitsTests, shiftBitLeft_returnsTransformedValue)
{
	const uint32_t offset = 12u;

	const auto result = shiftBitLeft(true, offset);

	UNSIGNED_LONGS_EQUAL(0x00001000u, result);
}

/// \Given N/A
///
/// \When calling shiftValue() with false value and the any offset of this value
///
/// \Then the zero is returned
///
TEST(BitsTests, shiftBitLeft_returnsZeroForValueFalse)
{
	const uint32_t offset = 12u;

	const auto result = shiftBitLeft(false, offset);

	UNSIGNED_LONGS_EQUAL(0u, result);
}

/// @}

/// @name BIT_FIELD_VALUE
/// @{

/// \Given N/A
///
/// \When using BIT_FIELD_VALUE macro with field name and value
///
/// \Then the value is shifted and masked according to offset and mask found by naming convention
///
TEST(BitsTests, bitFieldValue_properlyShiftsValue)
{
	const uint32_t value = 0x0DEAD033u;
	const uint32_t example_OFFSET = 12u;
	const uint32_t example_MASK = UINT32_C(0xFF) << example_OFFSET;

	const auto result = BIT_FIELD_VALUE(example, value);

	UNSIGNED_LONGS_EQUAL(0x00033000u, result);
}

/// @}

/// @name BIT_VALUE
/// @{

/// \Given N/A
///
/// \When using BIT_VALUE macro with bit name and value
///
/// \Then the value is shifted according to offset found by naming convention
///
TEST(BitsTests, bitValue_properlyShiftsValue)
{
	const uint32_t example_OFFSET = 12u;

	const auto result = BIT_VALUE(example, true);

	UNSIGNED_LONGS_EQUAL(0x00001000u, result);
}

/// @}

/// @name GET_FIELD_VALUE
/// @{

/// \Given uint32_t value with some field
///
/// \When using GET_FIELD_VALUE macro with field name and value
///
/// \Then field value is returned and it's right-aligned
///
TEST(BitsTests, getFieldValue_returnsCorrectValue)
{
	const uint32_t exampleValue = 0xDEADBEEFu;
	const uint32_t expectedValue = 0x000000EAu;
	const uint32_t field_OFFSET = 20u;
	const uint32_t field_MASK = 0x0FF00000u;

	const uint32_t readValue = GET_FIELD_VALUE(field, exampleValue);
	UNSIGNED_LONGS_EQUAL(expectedValue, readValue);
}

/// @}

/// @name SET_FIELD_VALUE
/// @{

/// \Given uint32_t value with some value
///
/// \When using SET_FIELD_VALUE macro with field name and value
///
/// \Then the value at selected bits is changed
///
TEST(BitsTests, setFieldValue_setsCorrectBits)
{
	uint32_t exampleValue = 0xDEADBEEFu;
	const uint32_t expectedValue = 0xDE55BEEFu;
	const uint32_t field_OFFSET = 16u;
	const uint32_t field_MASK = 0x00FF0000u;
	const uint8_t fieldValue = 0x55u;

	SET_FIELD_VALUE(field, &exampleValue, fieldValue);
	UNSIGNED_LONGS_EQUAL(expectedValue, exampleValue);
}

/// @}

/// @name isFieldSet()
/// @{

/// \Given 32-bit value with set field at provided mask,
///
/// \When isFieldSet() is called to check if the field is non-zero,
///
/// \Then it returns true.
///
TEST(BitsTests, isFieldSet_checksIfFieldIsSet)
{
	const uint32_t exampleValue = 0x00A00000u;
	const uint32_t exampleMask = 0x00F00000u;
	CHECK_TRUE(isFieldSet(exampleValue, exampleMask));
}

/// \Given 32-bit value with unset field at provided mask,
///
/// \When isFieldSet() is called to check if the field is non-zero,
///
/// \Then it returns false.
///
TEST(BitsTests, isFieldSet_checksIfFieldIsNotSet)
{
	const uint32_t exampleValue = 0x00A00000u;
	const uint32_t exampleMask = 0x00000F00u;
	CHECK_FALSE(isFieldSet(exampleValue, exampleMask));
}

/// @}

/// @name isBitSet()
/// @{

/// \Given 32-bit value with set bit at provided offset,
///
/// \When isBitSet() is called to check if the bit is set,
///
/// \Then it returns true.
///
TEST(BitsTests, isBitSet_checksIfBitIsSet)
{
	const uint32_t exampleValue = 0x01000000u;
	const uint32_t offset = 24u;
	CHECK_TRUE(isBitSet(exampleValue, offset));
}

/// \Given 32-bit value with unset bit at provided offset,
///
/// \When isBitSet() is called to check if the bit is set,
///
/// \Then it returns false.
///
TEST(BitsTests, isBitSet_checksIfBitIsNotSet)
{
	const uint32_t exampleValue = 0x01000000u;
	const uint32_t offset = 8u;
	CHECK_FALSE(isBitSet(exampleValue, offset));
}

/// @}

/// @name IS_FIELD_SET()
/// @{

/// \Given 32-bit value with set field at provided mask defined with _MASK suffix,
///
/// \When IS_FIELD_SET() is used to check if the field is non-zero,
///
/// \Then it returns true.
///
TEST(BitsTests, isFieldSetMacro_checksIfFieldIsSet)
{
	const uint32_t exampleValue = 0x00A00000u;
	const uint32_t field_MASK = 0x00F00000u;
	CHECK_TRUE(IS_FIELD_SET(field, exampleValue));
}

/// \Given 32-bit value with unset field at provided mask defined with _MASK suffix,
///
/// \When IS_FIELD_SET() is used to check if the field is non-zero,
///
/// \Then it returns true.
///
TEST(BitsTests, isFieldSetMacro_checksIfFieldIsNotSet)
{
	const uint32_t exampleValue = 0x00A00000u;
	const uint32_t field_MASK = 0x00000F00u;
	CHECK_FALSE(IS_FIELD_SET(field, exampleValue));
}

/// @}

/// @name IS_BIT_SET()
/// @{

/// \Given 32-bit value with set bit at provided offset defined with _OFFSET suffix,
///
/// \When IS_BIT_SET() is used to check if the bit is set,
///
/// \Then it returns true.
///
TEST(BitsTests, isBitSetMacro_checksIfBitIsSet)
{
	const uint32_t exampleValue = 0x01000000u;
	const uint32_t field_OFFSET = 24u;
	CHECK_TRUE(IS_BIT_SET(field, exampleValue));
}

/// \Given 32-bit value with unset bit at provided offset defined with _OFFSET suffix,
///
/// \When IS_BIT_SET() is used to check if the bit is set,
///
/// \Then it returns false.
///
TEST(BitsTests, isBitSetMacro_checksIfBitIsNotSet)
{
	const uint32_t exampleValue = 0x01000000u;
	const uint32_t field_OFFSET = 8u;
	CHECK_FALSE(IS_BIT_SET(field, exampleValue));
}

/// @}
