/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  WdtTests.cpp
/// \brief Wdt driver test suite implementation.

#include <CppUTest/TestHarness.h>

#include <n7s/bsp/Rstc/Rstc.h>
#include <n7s/bsp/Wdt/Wdt.h>
#include <n7s/bsp/Wdt/WdtRegisters.h>

#include <TestsRuntime/SuiteFixture.h>

TEST_GROUP(WdtTests) { static inline Wdt_Config reset_config = {}; };

using WdtTests = TEST_GROUP_CppUTestGroupWdtTests;

void
SuiteFixture_setup(void)
{
	// Retrieve after-reset WDT config before tests are run (which will modify it)
	// it will be checked later in one of the tests
	Wdt wdt;
	Wdt_init(&wdt);
	Wdt_getConfig(&wdt, &WdtTests::reset_config);
}

/// @name Wdt_init()
/// @{

/// \Given an initialized Wdt driver,
///
/// \When the default configuration is read from HW,
///
/// \Then the configuration matches the one in the HW reference manual.
///
TEST(WdtTests, Wdt_correctlyReadsDefaults)
{
	// Use WdtTests::reset_config read in suite setup before tests are executed
	Wdt wdt;
	Wdt_init(&wdt);
	POINTERS_EQUAL(reinterpret_cast<void *>(WDT0_ADDRESS_BASE),
			wdt.registers);

#if defined(N7S_TARGET_SAMV71Q21)
	// Wdt infinity loop protection check
	const uint8_t desiredResetLength = 0x1u;
	UNSIGNED_LONGS_EQUAL(desiredResetLength, Rstc_getExternalResetLength());
#endif // N7S_TARGET_SAMV71Q21

	// Default MR register value:
	// 0x3FFF_2FFF
	// 0011 1111 1111 1111 0010 1111 1111 1111
	CHECK_FALSE(WdtTests::reset_config.isDisabled);
	CHECK_FALSE(WdtTests::reset_config.isFaultInterruptEnabled);
	CHECK_TRUE(WdtTests::reset_config.isResetEnabled);
	CHECK_TRUE(WdtTests::reset_config.isHaltedOnIdle);
	CHECK_TRUE(WdtTests::reset_config.isHaltedOnIdle);
	UNSIGNED_LONGS_EQUAL(0x0FFF, WdtTests::reset_config.counterValue);
	UNSIGNED_LONGS_EQUAL(0x0FFF, WdtTests::reset_config.deltaValue);
}

/// @}

/// @name Wdt_setConfig()
/// @{

/// \Given an initialized Wdt driver,
///
/// \When Wdt_setConfig() function is called for this Wdt with a configuration descriptor set to a
/// nominal value and the counter is reset,
///
/// \Then Wdt configuration descriptor retrieved using Wdt_getConfig() function is the same as the
///       one used to set the Wdt configuration and there is no underflow.
///
TEST(WdtTests, Wdt_correctlySetsConfigurationAndResetsCounter)
{
	Wdt wdt;
	Wdt_Config writtenConfig{};
	Wdt_Config readConfig{};
	Wdt_init(&wdt);
	writtenConfig.counterValue = 0xF;
	writtenConfig.deltaValue = 0xF;
	writtenConfig.isDisabled = false;
	writtenConfig.isFaultInterruptEnabled = false;
	writtenConfig.isResetEnabled = false;
	writtenConfig.isHaltedOnDebug = false;
	writtenConfig.isHaltedOnIdle = false;

	busyWaitLoop(15000);
	Wdt_setConfig(&wdt, &writtenConfig);

	CHECK_FALSE(Wdt_isUnderflow(&wdt));
	CHECK_FALSE(Wdt_isError(&wdt));

	Wdt_getConfig(&wdt, &readConfig);
	CHECK_FALSE(readConfig.isDisabled);
	CHECK_FALSE(readConfig.isFaultInterruptEnabled);
	CHECK_FALSE(readConfig.isResetEnabled);
	CHECK_FALSE(readConfig.isHaltedOnIdle);
	CHECK_FALSE(readConfig.isHaltedOnIdle);
	UNSIGNED_LONGS_EQUAL(0xF, readConfig.counterValue);
	UNSIGNED_LONGS_EQUAL(0xF, readConfig.deltaValue);

	for (uint32_t i = 0u; i < (1024u * 1024u * 8u); i++)
		asm volatile("nop");

	CHECK_TRUE(Wdt_isUnderflow(&wdt));

	for (uint32_t i = 0u; i < (1024u * 8u); i++) {
		Wdt_reset(&wdt);
		for (uint32_t j = 0u; j < 1024u; j++)
			asm volatile("nop");
	}

	CHECK_FALSE(Wdt_isUnderflow(&wdt));
}

/// @}

/// @name Wdt_getConfig()
/// @{

/// \Given an initialized Wdt driver with dummy registers,
///
/// \When Wdt_setConfig() function is called for this Wdt with a configuration descriptor set to a
/// nominal value,
///
/// \Then Wdt configuration descriptor retrieved using Wdt_getConfig() function is the same as the
/// one used to set the Wdt configuration.
///
/// \details This test supplements Wdt_correctlySetsConfigurationAndResetsCounter,
///          as the watchdog cannot be freely reconfigured.
///
TEST(WdtTests, Wdt_correctlySetsAndGetsConfiguration)
{
	Wdt wdt;
	Wdt_Registers registers{};
	Wdt_Config writtenConfig{};
	Wdt_Config readConfig{};
	Wdt_init(&wdt);
	wdt.registers = &registers;
	writtenConfig.counterValue = 0xE;
	writtenConfig.deltaValue = 0xC;
	writtenConfig.isDisabled = true;
	writtenConfig.isFaultInterruptEnabled = true;
	writtenConfig.isResetEnabled = true;
	writtenConfig.isHaltedOnDebug = true;
	writtenConfig.isHaltedOnIdle = true;
#if defined(N7S_TARGET_SAMRH71F20)
	writtenConfig.doesFaultActivateProcessorReset = true;
#endif
	Wdt_setConfig(&wdt, &writtenConfig);
	Wdt_getConfig(&wdt, &readConfig);
	CHECK_TRUE(readConfig.isDisabled);
	CHECK_TRUE(readConfig.isFaultInterruptEnabled);
	CHECK_TRUE(readConfig.isResetEnabled);
	CHECK_TRUE(readConfig.isHaltedOnIdle);
	CHECK_TRUE(readConfig.isHaltedOnIdle);
#if defined(N7S_TARGET_SAMRH71F20)
	CHECK_TRUE(readConfig.doesFaultActivateProcessorReset);
#endif
	UNSIGNED_LONGS_EQUAL(0xE, readConfig.counterValue);
	UNSIGNED_LONGS_EQUAL(0xC, readConfig.deltaValue);
}

/// @}
