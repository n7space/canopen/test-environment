/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Matrix1Tests.cpp
/// \brief Matrix driver test suite implementation.

#include "TestUtils/TestUtils.hpp"

TEST_GROUP_BASE(MatrixTests, TestUtils){};

using MatrixTests = TEST_GROUP_CppUTestGroupMatrixTests;

/// @name Matrix_init()
/// @{

/// \Given uninitialized Matrix device descriptor,
///
/// \When Matrix_init() function is called for this device descriptor, with an arbitrary Matrix
///       device base address,
///
/// \Then the function initializes Matrix device descriptor with correct registers' addresses
///       based on provided Matrix device base address.
///
TEST(MatrixTests,
		init_initializesDeviceDescriptorBasedOnArbitraryDeviceBaseAddress)
{
	Matrix_init(&matrix, reinterpret_cast<Matrix_Registers *>(0x12345678u));

	// Checking if device's registers addresses are set based on arbitrary base address.
	const auto *const arbitraryBaseAddress =
			reinterpret_cast<const uint8_t *>(0x12345678u);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0000u), matrix.regs);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0000u), &matrix.regs->mcfg[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x003Cu),
			&matrix.regs->mcfg[15]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0040u), &matrix.regs->scfg[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x007Cu),
			&matrix.regs->scfg[15]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0080u),
			&matrix.regs->prs[0].pras);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0084u),
			&matrix.regs->prs[0].prbs);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x00F8u),
			&matrix.regs->prs[15].pras);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x00FCu),
			&matrix.regs->prs[15].prbs);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0100u), &matrix.regs->mrcr);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0150u), &matrix.regs->meier);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0154u), &matrix.regs->meidr);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0158u), &matrix.regs->meimr);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x015Cu), &matrix.regs->mesr);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0160u), &matrix.regs->mear[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x019Cu),
			&matrix.regs->mear[15]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x01E4u), &matrix.regs->wpmr);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x01E8u), &matrix.regs->wpsr);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0200u), &matrix.regs->psr[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x023Cu), &matrix.regs->psr[15]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0240u),
			&matrix.regs->passr[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x027Cu),
			&matrix.regs->passr[15]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x0280u),
			&matrix.regs->prtsr[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x02BCu),
			&matrix.regs->prtsr[15]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x02C0u),
			&matrix.regs->ppselr[0]);
	POINTERS_EQUAL((arbitraryBaseAddress + 0x02C8u),
			&matrix.regs->ppselr[2]);
}

/// \Given uninitialized Matrix device descriptor,
///
/// \When Matrix_init() function is called for this device descriptor, with a real Matrix
///       device base address provided by Matrix_getDeviceBaseAddress() function,
///
/// \Then the function initializes Matrix device descriptor with correct registers' addresses of a
///       real Matrix device.
///
TEST(MatrixTests, init_initializesDeviceDescriptorBasedOnRealDeviceBaseAddress)
{
	Matrix_init(&matrix, Matrix_getDeviceBaseAddress());

	// Checking if device's registers addresses are set according to SAMRH71 Data Sheet document.
	const auto *const expectedBaseAddress =
			reinterpret_cast<const uint8_t *>(0x40000000u);
	POINTERS_EQUAL((expectedBaseAddress + 0x0000u), matrix.regs);
	POINTERS_EQUAL((expectedBaseAddress + 0x0000u), &matrix.regs->mcfg[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x003Cu), &matrix.regs->mcfg[15]);
	POINTERS_EQUAL((expectedBaseAddress + 0x0040u), &matrix.regs->scfg[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x007Cu), &matrix.regs->scfg[15]);
	POINTERS_EQUAL((expectedBaseAddress + 0x0080u),
			&matrix.regs->prs[0].pras);
	POINTERS_EQUAL((expectedBaseAddress + 0x0084u),
			&matrix.regs->prs[0].prbs);
	POINTERS_EQUAL((expectedBaseAddress + 0x00F8u),
			&matrix.regs->prs[15].pras);
	POINTERS_EQUAL((expectedBaseAddress + 0x00FCu),
			&matrix.regs->prs[15].prbs);
	POINTERS_EQUAL((expectedBaseAddress + 0x0100u), &matrix.regs->mrcr);
	POINTERS_EQUAL((expectedBaseAddress + 0x0150u), &matrix.regs->meier);
	POINTERS_EQUAL((expectedBaseAddress + 0x0154u), &matrix.regs->meidr);
	POINTERS_EQUAL((expectedBaseAddress + 0x0158u), &matrix.regs->meimr);
	POINTERS_EQUAL((expectedBaseAddress + 0x015Cu), &matrix.regs->mesr);
	POINTERS_EQUAL((expectedBaseAddress + 0x0160u), &matrix.regs->mear[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x019Cu), &matrix.regs->mear[15]);
	POINTERS_EQUAL((expectedBaseAddress + 0x01E4u), &matrix.regs->wpmr);
	POINTERS_EQUAL((expectedBaseAddress + 0x01E8u), &matrix.regs->wpsr);
	POINTERS_EQUAL((expectedBaseAddress + 0x0200u), &matrix.regs->psr[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x023Cu), &matrix.regs->psr[15]);
	POINTERS_EQUAL((expectedBaseAddress + 0x0240u), &matrix.regs->passr[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x027Cu),
			&matrix.regs->passr[15]);
	POINTERS_EQUAL((expectedBaseAddress + 0x0280u), &matrix.regs->prtsr[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x02BCu),
			&matrix.regs->prtsr[15]);
	POINTERS_EQUAL((expectedBaseAddress + 0x02C0u),
			&matrix.regs->ppselr[0]);
	POINTERS_EQUAL((expectedBaseAddress + 0x02C8u),
			&matrix.regs->ppselr[2]);
}

/// @}

/// @name Matrix_setMasterConfig()
/// @{

/// \Given Matrix device descriptor initialized with mock registers having all bits set to zero,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having only Burst Type option set to maximum value,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests,
		setMasterConfig_setsMasterConfigWithOnlyBurstTypeOptionSetToMaximumValue)
{
	const Matrix_Master master = Matrix_Master_CortexM7Port0;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = false,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	expectedMatrixRegs.mcfg[0] = 0x00000007u;
	expectedMatrixRegs.mrcr = 0u;
	expectedMatrixRegs.meier = 0u;
	expectedMatrixRegs.meidr = 0x00000001u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all bits set to zero,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having only Remapped Address Decoding option enabled,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withOnlyAddressRemappingDecodingOptionEnabled)
{
	const Matrix_Master master = Matrix_Master_Can1Dma;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = false,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	expectedMatrixRegs.mcfg[5] = 0u;
	expectedMatrixRegs.mrcr = 0x00000020u;
	expectedMatrixRegs.meier = 0u;
	expectedMatrixRegs.meidr = 0x00000020u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all bits set to zero,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having only Error Interrupt option enabled,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withOnlyErrorInterruptOptionEnabled)
{
	const Matrix_Master master = Matrix_Master_SpwTx;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = true,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	expectedMatrixRegs.mcfg[7] = 0u;
	expectedMatrixRegs.mrcr = 0u;
	expectedMatrixRegs.meier = 0x00000080u;
	expectedMatrixRegs.meidr = 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all significant bits
///        set to one,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having Burst Type option set to minimum value
///         and other options being enabled,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withOnlyBurstTypeOptionSetToMinimumValue)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.mcfg[0] = 0x00000007u;
	mockMatrixRegs.mrcr = 0x0000FFFFu;
	mockMatrixRegs.meier = 0u;
	mockMatrixRegs.meidr = 0u;

	const Matrix_Master master = Matrix_Master_CortexM7Port0;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = true,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	(void)memset(static_cast<void *>(&expectedMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	expectedMatrixRegs.mcfg[0] = 0u;
	expectedMatrixRegs.mrcr = 0x0000FFFFu;
	expectedMatrixRegs.meier = 0x00000001u;
	expectedMatrixRegs.meidr = 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all significant bits
///        set to one,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having Remapped Address Decoding option disabled,
///         Burst Type option set to maximum value and Error Interrupt option enabled,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests,
		setMasterConfig_withOnlyAddressRemappingDecodingOptionDisabled)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.mcfg[5] = 0x00000007u;
	mockMatrixRegs.mrcr = 0x0000FFFFu;
	mockMatrixRegs.meier = 0u;
	mockMatrixRegs.meidr = 0u;

	const Matrix_Master master = Matrix_Master_Can1Dma;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = true,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	(void)memset(static_cast<void *>(&expectedMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	expectedMatrixRegs.mcfg[5] = 0x00000007u;
	expectedMatrixRegs.mrcr = 0x0000FFDFu;
	expectedMatrixRegs.meier = 0x00000020u;
	expectedMatrixRegs.meidr = 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all significant bits
///        set to one,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having Error Interrupt option disabled,
///         Burst Type option set to maximum value and Remapped Address Decoding option enabled,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withOnlyErrorInterruptOptionDisabled)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.mcfg[3] = 0x00000007u;
	mockMatrixRegs.mrcr = 0x0000FFFFu;
	mockMatrixRegs.meier = 0u;
	mockMatrixRegs.meidr = 0u;

	const Matrix_Master master = Matrix_Master_XdmacPort0;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = false,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	(void)memset(static_cast<void *>(&expectedMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	expectedMatrixRegs.mcfg[3] = 0x00000007u;
	expectedMatrixRegs.mrcr = 0x0000FFFFu;
	expectedMatrixRegs.meier = 0u;
	expectedMatrixRegs.meidr = 0x00000008u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all bits set to zero,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having all options set to nominal values,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withAllOptionsSetToNominalValues)
{
	const Matrix_Master master = Matrix_Master_Can0Dma;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_16Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = true,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	expectedMatrixRegs.mcfg[4] = 0x00000004u;
	expectedMatrixRegs.mrcr = 0x00000010u;
	expectedMatrixRegs.meier = 0x00000010u;
	expectedMatrixRegs.meidr = 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all bits set to zero,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having all options set to their maximum values,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withAllOptionsSetToMaximumValues)
{
	const Matrix_Master master = Matrix_Master_XdmacPort0;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = true,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	expectedMatrixRegs.mcfg[3] = 0x0000007u;
	expectedMatrixRegs.mrcr = 0x00000008u;
	expectedMatrixRegs.meier = 0x00000008u;
	expectedMatrixRegs.meidr = 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all significant bits
///        set to one,
///
/// \When Matrix_setMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor having all options set to their minimum values,
///       .
///
/// \Then the function correctly sets Matrix MCFG, MRCR, MEIER and MEIDR registers
///       for the selected master.
///
TEST(MatrixTests, setMasterConfig_withAllOptionsSetToMinimumValues)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.mcfg[0] = 0x00000007u;
	mockMatrixRegs.mrcr = 0x0000FFFFu;
	mockMatrixRegs.meier = 0u;
	mockMatrixRegs.meidr = 0u;

	const Matrix_Master master = Matrix_Master_CortexM7Port0;
	const Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = false,
	};
	Matrix_setMasterConfig(&mockMatrix, master, &config);

	(void)memset(static_cast<void *>(&expectedMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	expectedMatrixRegs.mcfg[0] = 0u;
	expectedMatrixRegs.mrcr = 0x0000FFFEu;
	expectedMatrixRegs.meier = 0u;
	expectedMatrixRegs.meidr = 0x00000001u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// @}

/// @name Matrix_getMasterConfig()
/// @{

/// \Given Matrix device descriptor initialized with mock registers having only ULBT field in
///        Matrix MCFG register of the selected master set to it's maximum value,
///
/// \When Matrix_getMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor,
///       .
///
/// \Then the function correctly gets configuration of the selected master based on the contents of
///       Matrix MCFG, MRCR and MEIMR registers.
///
TEST(MatrixTests, getMasterConfig_withOnlyBurstTypeOptionSetToMaximumValue)
{
	mockMatrixRegs.mcfg[0] = 0x00000007u;
	mockMatrixRegs.mrcr = 0u;
	mockMatrixRegs.meimr = 0u;

	const Matrix_Master master = Matrix_Master_CortexM7Port0;
	Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = true,
	};
	Matrix_getMasterConfig(&mockMatrix, master, &config);

	const Matrix_MasterConfig expectedConfig = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = false,
	};
	ENUMS_EQUAL_INT(expectedConfig.burstType, config.burstType);
	CHECK_EQUAL(expectedConfig.isRemappedAddressDecodingEnabled,
			config.isRemappedAddressDecodingEnabled);
	CHECK_EQUAL(expectedConfig.isErrorIrqEnabled, config.isErrorIrqEnabled);
}

/// \Given Matrix device descriptor initialized with mock registers having only RCB field
///        of the selected master in Matrix MRCR register set to it's maximum value,
///
/// \When Matrix_getMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor,
///       .
///
/// \Then the function correctly gets configuration of the selected master based on the contents of
///       Matrix MCFG, MRCR and MEIMR registers.
///
TEST(MatrixTests, getMasterConfig_withOnlyAddressRemappingDecodingOptionEnabled)
{
	mockMatrixRegs.mcfg[5] = 0u;
	mockMatrixRegs.mrcr = 1u << 5u;
	mockMatrixRegs.meimr = 0u;

	const Matrix_Master master = Matrix_Master_Can1Dma;
	Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = true,
	};
	Matrix_getMasterConfig(&mockMatrix, master, &config);

	const Matrix_MasterConfig expectedConfig = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = false,
	};
	ENUMS_EQUAL_INT(expectedConfig.burstType, config.burstType);
	CHECK_EQUAL(expectedConfig.isRemappedAddressDecodingEnabled,
			config.isRemappedAddressDecodingEnabled);
	CHECK_EQUAL(expectedConfig.isErrorIrqEnabled, config.isErrorIrqEnabled);
}

/// \Given Matrix device descriptor initialized with mock registers having only MERR field
///        of the selected master in Matrix MEIMR register set to it's maximum value,
///
/// \When Matrix_getMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor,
///       .
///
/// \Then the function correctly gets configuration of the selected master based on the contents of
///       Matrix MCFG, MRCR and MEIMR registers.
///
TEST(MatrixTests, getMasterConfig_withOnlyErrorInterruptOptionEnabled)
{
	mockMatrixRegs.mcfg[9] = 0u;
	mockMatrixRegs.mrcr = 0u;
	mockMatrixRegs.meimr = UINT32_C(1) << 9u;

	const Matrix_Master master = Matrix_Master_SpwRmap;
	Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = false,
	};
	Matrix_getMasterConfig(&mockMatrix, master, &config);

	const Matrix_MasterConfig expectedConfig = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = true,
	};
	ENUMS_EQUAL_INT(expectedConfig.burstType, config.burstType);
	CHECK_EQUAL(expectedConfig.isRemappedAddressDecodingEnabled,
			config.isRemappedAddressDecodingEnabled);
	CHECK_EQUAL(expectedConfig.isErrorIrqEnabled, config.isErrorIrqEnabled);
}

/// \Given Matrix device descriptor initialized with mock registers having following Matrix
///        registers' fields set to nominal values:
///        - ULBT field in MCFG register of the selected master,
///        - RCB field of the selected master in MRCR register,
///        - MERR field of the selected master in MEIMR register,
///        .
///
/// \When Matrix_getMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor,
///       .
///
/// \Then the function correctly gets configuration of the selected master based on the contents of
///       Matrix MCFG, MRCR and MEIMR registers.
///
TEST(MatrixTests, getMasterConfig_withAllOptionsSetToNominalValues)
{
	mockMatrixRegs.mcfg[3] = 0x00000003u;
	mockMatrixRegs.mrcr = (UINT32_C(1) << 3u) | (UINT32_C(1) << 8u);
	mockMatrixRegs.meimr = 0x0000FFFFu & ~(UINT32_C(1) << 3u);

	const Matrix_Master master = Matrix_Master_XdmacPort0;
	Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = true,
	};
	Matrix_getMasterConfig(&mockMatrix, master, &config);

	const Matrix_MasterConfig expectedConfig = {
		.burstType = Matrix_BurstType_8Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = false,
	};
	ENUMS_EQUAL_INT(expectedConfig.burstType, config.burstType);
	CHECK_EQUAL(expectedConfig.isRemappedAddressDecodingEnabled,
			config.isRemappedAddressDecodingEnabled);
	CHECK_EQUAL(expectedConfig.isErrorIrqEnabled, config.isErrorIrqEnabled);
}

/// \Given Matrix device descriptor initialized with mock registers having following Matrix
///        registers' fields set to maximum values:
///        - ULBT field in MCFG register of the selected master,
///        - RCB field of the selected master in MRCR register,
///        - MERR field of the selected master in MEIMR register,
///        .
///
/// \When Matrix_getMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor,
///       .
///
/// \Then the function correctly gets configuration of the selected master based on the contents of
///       Matrix MCFG, MRCR and MEIMR registers.
///
TEST(MatrixTests, getMasterConfig_withAllOptionsSetToMaximumValues)
{
	mockMatrixRegs.mcfg[9] = 0x00000007u;
	mockMatrixRegs.mrcr = UINT32_C(1) << 9u;
	mockMatrixRegs.meimr = UINT32_C(1) << 9u;

	const Matrix_Master master = Matrix_Master_SpwRmap;
	Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = false,
	};
	Matrix_getMasterConfig(&mockMatrix, master, &config);

	const Matrix_MasterConfig expectedConfig = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = true,
	};
	ENUMS_EQUAL_INT(expectedConfig.burstType, config.burstType);
	CHECK_EQUAL(expectedConfig.isRemappedAddressDecodingEnabled,
			config.isRemappedAddressDecodingEnabled);
	CHECK_EQUAL(expectedConfig.isErrorIrqEnabled, config.isErrorIrqEnabled);
}

/// \Given Matrix device descriptor initialized with mock registers having following Matrix
///        registers' fields set to minimum values:
///        - ULBT field in MCFG register of the selected master,
///        - RCB field of the selected master in MRCR register,
///        - MERR field of the selected master in MEIMR register,
///        .
///
/// \When Matrix_getMasterConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected master,
///       - master configuration descriptor,
///
/// \Then the function correctly gets configuration of the selected master based on the contents of
///       Matrix MCFG, MRCR and MEIMR registers.
///
TEST(MatrixTests, getMasterConfig_withAllOptionsSetToMinimumValues)
{
	mockMatrixRegs.mcfg[0] = 0u;
	mockMatrixRegs.mrcr = 0x0000FFFFu & ~(1u << 0u);
	mockMatrixRegs.meimr = 0x0000FFFFu & ~(1u << 0u);

	const Matrix_Master master = Matrix_Master_CortexM7Port0;
	Matrix_MasterConfig config = {
		.burstType = Matrix_BurstType_128Beat,
		.isRemappedAddressDecodingEnabled = true,
		.isErrorIrqEnabled = true,
	};
	Matrix_getMasterConfig(&mockMatrix, master, &config);

	const Matrix_MasterConfig expectedConfig = {
		.burstType = Matrix_BurstType_Unlimited,
		.isRemappedAddressDecodingEnabled = false,
		.isErrorIrqEnabled = false,
	};
	ENUMS_EQUAL_INT(expectedConfig.burstType, config.burstType);
	CHECK_EQUAL(expectedConfig.isRemappedAddressDecodingEnabled,
			config.isRemappedAddressDecodingEnabled);
	CHECK_EQUAL(expectedConfig.isErrorIrqEnabled, config.isErrorIrqEnabled);
}

/// @}
