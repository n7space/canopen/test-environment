/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Matrix3Tests.cpp
/// \brief Matrix driver test suite implementation.

#include "TestUtils/TestUtils.hpp"

TEST_GROUP_BASE(MatrixTests, TestUtils){};

using MatrixTests = TEST_GROUP_CppUTestGroupMatrixTests;

/// @name Matrix_getMastersStatuses()
/// @{

/// \Given Matrix device descriptor initialized with mock registers having:
///        - only one bit corresponding to a specific master set to one in Matrix MESR register,
///        - Matrix MEAR register of a specific master set to a nominal value,
///        .
///
/// \When Matrix_getMastersStatuses() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - pointer to the Matrix Masters' statuses structure,
///       .
///
/// \Then correct statuses of Matrix masters are read based on contents of Matrix MESR register
///       and Matrix MEAR registers.
///
TEST(MatrixTests, getMastersStatuses_withNominalValues)
{
	mockMatrixRegs.mesr = 1u << 5u;
	mockMatrixRegs.mear[5u] = 0xABCDEF01u;

	Matrix_MastersStatuses statuses = {};
	Matrix_getMastersStatuses(&mockMatrix, &statuses);

	Matrix_MastersStatuses expectedStatuses = {};
	expectedStatuses.isMasterIrqTriggered[5] = true;
	expectedStatuses.masterErrorAddress[5] = 0xABCDEF01u;
	checkMastersStatuses(expectedStatuses, statuses, __LINE__);
}

/// \Given Matrix device descriptor initialized with mock registers having:
///        - all bits corresponding to matrix masters set to one in Matrix MESR register,
///        - all Matrix MEAR registers set to a maximum value,
///        .
///
/// \When Matrix_getMastersStatuses() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - pointer to the Matrix Masters' statuses structure,
///       .
///
/// \Then correct statuses of Matrix masters are read based on contents of Matrix MESR register
///       and Matrix MEAR registers.
///
TEST(MatrixTests, getMastersStatuses_withMaximumValues)
{
	mockMatrixRegs.mesr = 0x00001FFFu;
	for (uint32_t i = 0u; i < static_cast<uint32_t>(Matrix_Master_Count);
			i++)
		mockMatrixRegs.mear[i] = 0xFFFFFFFFu;

	Matrix_MastersStatuses statuses = {};
	Matrix_getMastersStatuses(&mockMatrix, &statuses);

	Matrix_MastersStatuses expectedStatuses = {};
	for (uint32_t i = 0u; i < static_cast<uint32_t>(Matrix_Master_Count);
			i++) {
		expectedStatuses.isMasterIrqTriggered[i] = true;
		expectedStatuses.masterErrorAddress[i] = 0xFFFFFFFFu;
	}
	checkMastersStatuses(expectedStatuses, statuses, __LINE__);
}

/// \Given Matrix device descriptor initialized with mock registers having:
///        - all bits corresponding to matrix masters set to zero in Matrix MESR register,
///        - all Matrix MEAR registers set to a zero,
///        .
///
/// \When Matrix_getMastersStatuses() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - pointer to the Matrix Masters' statuses structure,
///       .
///
/// \Then correct statuses of Matrix masters are read based on contents of Matrix MESR register
///       and Matrix MEAR registers.
///
TEST(MatrixTests, getMastersStatuses_withMinimumValues)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.mesr &= ~0x00001FFFu;
	for (uint32_t i = 0u; i < static_cast<uint32_t>(Matrix_Master_Count);
			i++)
		mockMatrixRegs.mear[i] = 0u;

	Matrix_MastersStatuses statuses = {};
	for (uint32_t i = 0u; i < static_cast<uint32_t>(Matrix_Master_Count);
			i++) {
		statuses.isMasterIrqTriggered[i] = true;
		statuses.masterErrorAddress[i] = 0xFFFFFFFFu;
	}
	Matrix_getMastersStatuses(&mockMatrix, &statuses);

	const Matrix_MastersStatuses expectedStatuses = {};
	checkMastersStatuses(expectedStatuses, statuses, __LINE__);
}

/// @}

/// @name Matrix_setWriteProtectionEnabled(), Matrix_isWriteProtectionEnabled(),
///       Matrix_isWriteProtectionViolated()
/// @{

/// \Given Matrix device descriptor initialized with real registers having write protection
///        disabled,
///
/// \When write protection is enabled or disabled using Matrix_setWriteProtectionEnabled() function,
///       write protection enablement is checked using Matrix_isWriteProtectionEnabled() function,
///       write protection violation is checked using Matrix_isWriteProtectionViolated() function,
///
/// \Then write protection feature works correctly.
///
TEST(MatrixTests, writeProtectionWorks)
{
	Matrix_init(&matrix, Matrix_getDeviceBaseAddress());

	// Ensure that write protection is disabled and no write protection violation is asserted.
	Matrix_setWriteProtectionEnabled(&matrix, false);
	UNSIGNED_LONGS_EQUAL(0u, matrix.regs->wpmr);
	UNSIGNED_LONGS_EQUAL(0u, matrix.regs->wpsr);

	// Check if enabling of write protection works.
	Matrix_setWriteProtectionEnabled(&matrix, true);
	CHECK_TRUE((matrix.regs->wpmr & (1u << 0u)) != 0u);

	// Check if reading enabled write protection works.
	CHECK_TRUE(Matrix_isWriteProtectionEnabled(&matrix));

	// Attempt to write Master configuration while write protection is enabled.
	const Matrix_Master master = Matrix_Master_Can0Dma;
	Matrix_MasterConfig config = {};
	Matrix_getMasterConfig(&matrix, master, &config);
	config.isErrorIrqEnabled = false;
	Matrix_setMasterConfig(&matrix, master, &config);
	UNSIGNED_LONGS_EQUAL(0x00015401u, matrix.regs->wpsr);

	// Check if violation of write protection occurred.
	uint16_t offset = 0xFFFFu;
	CHECK_TRUE(Matrix_isWriteProtectionViolated(&matrix, &offset));
	UNSIGNED_LONGS_EQUAL(0x0154u, offset);

	// Check if disabling of write protection works.
	Matrix_setWriteProtectionEnabled(&matrix, false);
	UNSIGNED_LONGS_EQUAL(0u, matrix.regs->wpmr);
	UNSIGNED_LONGS_EQUAL(0u, matrix.regs->wpsr);

	// Check if reading disabled write protection works.
	CHECK_FALSE(Matrix_isWriteProtectionEnabled(&matrix));

	// Attempt to write Master configuration while write protection is disabled.
	Matrix_setMasterConfig(&matrix, master, &config);
	UNSIGNED_LONGS_EQUAL(0u, matrix.regs->wpsr);

	// Check if violation of write protection did not occurred.
	CHECK_FALSE(Matrix_isWriteProtectionViolated(&matrix, nullptr));
}

/// @}

/// @name Matrix_setSlaveRegionProtectionConfig(), Matrix_getSlaveRegionProtectionConfig()
/// @{

/// \Given Matrix device descriptor initialized with mock registers,
///
/// \When Matrix AHB Slave Protected Region configuration is set using
///       Matrix_setSlaveRegionProtectionConfig() function and Matrix AHB Slave Protected Region
///       configuration is read using Matrix_getSlaveRegionProtectionConfig() function,
///
/// \Then Matrix_setSlaveRegionProtectionConfig() function correctly sets Matrix PSR, PASSR and
///       PRTSR registers for the selected slave, based on provided slave Region ID and
///       Protected Region configuration descriptor.
///       Matrix_getSlaveRegionProtectionConfig() function correctly gets slave Protected Region
///       configuration for the selected slave and selected slave Region ID, based on contents of
///       Matrix PSR, PASSR and PRTSR registers.
///
TEST(MatrixTests, setsAndGetsSlaveRegionProtectionConfig)
{
	// Set config using nominal values and then read config.
	const Matrix_Slave slaveOne = Matrix_Slave_Hemc;
	const Matrix_ProtectedRegionId regionOne = Matrix_ProtectedRegionId_5;

	const Matrix_SlaveRegionProtectionConfig writtenConfigOne = {
		.isPrivilegedRegionUserWriteAllowed = false,
		.isPrivilegedRegionUserReadAllowed = true,
		.regionSplitOffsetDirection =
				Matrix_RegionSplitDirection_FromBottomUpwards,
		.protectedRegionSize = Matrix_Size_2MB,
		.regionSplitOffset = Matrix_Size_256KB,
		.regionOrder = Matrix_RegionSplitOrder_UpperPrivilegedLowerUser,
	};
	Matrix_setSlaveRegionProtectionConfig(
			&mockMatrix, slaveOne, regionOne, &writtenConfigOne);

	expectedMatrixRegs.psr[3] |= (UINT32_C(1) << 13u) | (UINT32_C(1) << 5u);
	expectedMatrixRegs.passr[3] |= (UINT32_C(6) << 20u);
	expectedMatrixRegs.prtsr[3] |= (UINT32_C(9) << 20u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_SlaveRegionProtectionConfig readConfigOne = {
		.isPrivilegedRegionUserWriteAllowed = true,
		.isPrivilegedRegionUserReadAllowed = false,
		.regionSplitOffsetDirection =
				Matrix_RegionSplitDirection_FromTopDownwards,
		.protectedRegionSize = Matrix_Size_4KB,
		.regionSplitOffset = Matrix_Size_4KB,
		.regionOrder = Matrix_RegionSplitOrder_UpperUserLowerPrivileged,
	};
	Matrix_getSlaveRegionProtectionConfig(
			&mockMatrix, slaveOne, regionOne, &readConfigOne);

	checkSlaveRegionProtectionConfig(
			writtenConfigOne, readConfigOne, __LINE__);

	// Set config using maximum values and then read config.
	const Matrix_Slave slaveTwo = Matrix_Slave_Flexram2;
	const Matrix_ProtectedRegionId regionTwo = Matrix_ProtectedRegionId_7;

	const Matrix_SlaveRegionProtectionConfig writtenConfigTwo = {
		.isPrivilegedRegionUserWriteAllowed = true,
		.isPrivilegedRegionUserReadAllowed = true,
		.regionSplitOffsetDirection =
				Matrix_RegionSplitDirection_FromTopDownwards,
		.protectedRegionSize = Matrix_Size_128MB,
		.regionSplitOffset = Matrix_Size_128MB,
		.regionOrder = Matrix_RegionSplitOrder_UpperPrivilegedLowerUser,
	};
	Matrix_setSlaveRegionProtectionConfig(
			&mockMatrix, slaveTwo, regionTwo, &writtenConfigTwo);

	expectedMatrixRegs.psr[7] |= (UINT32_C(1) << 31u) | (UINT32_C(1) << 23u)
			| (UINT32_C(1) << 15u) | (UINT32_C(1) << 7u);
	expectedMatrixRegs.passr[7] |= (UINT32_C(0xF) << 28u);
	expectedMatrixRegs.prtsr[7] |= (UINT32_C(0xF) << 28u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_SlaveRegionProtectionConfig readConfigTwo = {
		.isPrivilegedRegionUserWriteAllowed = false,
		.isPrivilegedRegionUserReadAllowed = false,
		.regionSplitOffsetDirection =
				Matrix_RegionSplitDirection_FromBottomUpwards,
		.protectedRegionSize = Matrix_Size_4KB,
		.regionSplitOffset = Matrix_Size_4KB,
		.regionOrder = Matrix_RegionSplitOrder_UpperUserLowerPrivileged,
	};
	Matrix_getSlaveRegionProtectionConfig(
			&mockMatrix, slaveTwo, regionTwo, &readConfigTwo);

	checkSlaveRegionProtectionConfig(
			writtenConfigTwo, readConfigTwo, __LINE__);

	// Set config using minimum values and then read config.
	const Matrix_Slave slaveThree = slaveTwo;
	const Matrix_ProtectedRegionId regionThree = regionTwo;

	const Matrix_SlaveRegionProtectionConfig writtenConfigThree = {
		.isPrivilegedRegionUserWriteAllowed = false,
		.isPrivilegedRegionUserReadAllowed = false,
		.regionSplitOffsetDirection =
				Matrix_RegionSplitDirection_FromBottomUpwards,
		.protectedRegionSize = Matrix_Size_4KB,
		.regionSplitOffset = Matrix_Size_4KB,
		.regionOrder = Matrix_RegionSplitOrder_UpperUserLowerPrivileged,
	};
	Matrix_setSlaveRegionProtectionConfig(&mockMatrix, slaveThree,
			regionThree, &writtenConfigThree);

	expectedMatrixRegs.psr[7] &= ~(UINT32_C(1) << 31u)
			& ~(UINT32_C(1) << 23u) & ~(UINT32_C(1) << 15u)
			& ~(UINT32_C(1) << 7u);
	expectedMatrixRegs.passr[7] &= ~(UINT32_C(0xF) << 28u);
	expectedMatrixRegs.prtsr[7] &= ~(UINT32_C(0xF) << 28u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_SlaveRegionProtectionConfig readConfigThree = {
		.isPrivilegedRegionUserWriteAllowed = true,
		.isPrivilegedRegionUserReadAllowed = true,
		.regionSplitOffsetDirection =
				Matrix_RegionSplitDirection_FromTopDownwards,
		.protectedRegionSize = Matrix_Size_128MB,
		.regionSplitOffset = Matrix_Size_128MB,
		.regionOrder = Matrix_RegionSplitOrder_UpperPrivilegedLowerUser,
	};
	Matrix_getSlaveRegionProtectionConfig(
			&mockMatrix, slaveThree, regionThree, &readConfigThree);

	checkSlaveRegionProtectionConfig(
			writtenConfigThree, readConfigThree, __LINE__);
}

/// @}

/// @name Matrix_setPeripheralProtectionConfig()
/// @{

/// \Given Matrix device descriptor initialized with mock registers having all bits set to zero,
///
/// \When Matrix_setPeripheralProtectionConfig() function is called for
///       minimal, nominal and maximal value of peripheral ID, with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected peripheral ID,
///       - peripheral protection config descriptor with Access type option set to User Access,
///       .
///
/// \Then the function correctly modifies proper Matrix PPSELR register.
///
TEST(MatrixTests,
		setPeripheralProtectionConfig_withAccessTypeOptionSetToUserAccess)
{
	const Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_User,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral minimalPeripheralId = Matrix_Peripheral_Supc;
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Mcan0;
	const Matrix_Peripheral maximalPeripheralId =
			Matrix_Peripheral_Gmac_Queue5;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral minimalPeripheralId =
			Matrix_Peripheral_ReportChipId;
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Adc;
	const Matrix_Peripheral maximalPeripheralId = Matrix_Peripheral_Nmic;
#endif

	Matrix_setPeripheralProtectionConfig(
			&mockMatrix, minimalPeripheralId, &config);
	expectedMatrixRegs.ppselr[0] |= (UINT32_C(1) << 0u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_setPeripheralProtectionConfig(
			&mockMatrix, nominalPeripheralId, &config);
	expectedMatrixRegs.ppselr[1] |= (UINT32_C(1) << 4u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_setPeripheralProtectionConfig(
			&mockMatrix, maximalPeripheralId, &config);
	expectedMatrixRegs.ppselr[2] |= (UINT32_C(1) << 10u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// \Given Matrix device descriptor initialized with mock registers having all bits set to one,
///
/// \When Matrix_setPeripheralProtectionConfig() function is called for
///       minimal, nominal and maximal value of peripheral ID, with following parameters:
///       - pointer to the Matrix device descriptor,
///       - selected peripheral ID,
///       - peripheral protection config descriptor with Access type option set to Privileged
///         Access,
///       .
///
/// \Then the function correctly modifies proper Matrix PPSELR register.
///
TEST(MatrixTests,
		setPeripheralProtectionConfig_withAccessTypeOptionSetToPrivilegedAccess)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	(void)memset(static_cast<void *>(&expectedMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));

	const Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_Privileged,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral minimalPeripheralId = Matrix_Peripheral_Supc;
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Mcan0;
	const Matrix_Peripheral maximalPeripheralId =
			Matrix_Peripheral_Gmac_Queue5;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral minimalPeripheralId =
			Matrix_Peripheral_ReportChipId;
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Adc;
	const Matrix_Peripheral maximalPeripheralId = Matrix_Peripheral_Nmic;
#endif

	Matrix_setPeripheralProtectionConfig(
			&mockMatrix, minimalPeripheralId, &config);
	expectedMatrixRegs.ppselr[0] &= ~(UINT32_C(1) << 0u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_setPeripheralProtectionConfig(
			&mockMatrix, nominalPeripheralId, &config);
	expectedMatrixRegs.ppselr[1] &= ~(UINT32_C(1) << 4u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_setPeripheralProtectionConfig(
			&mockMatrix, maximalPeripheralId, &config);
	expectedMatrixRegs.ppselr[2] &= ~(UINT32_C(1) << 10u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);
}

/// @}
