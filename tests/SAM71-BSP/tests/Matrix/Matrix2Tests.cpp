/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Matrix2Tests.cpp
/// \brief Matrix driver test suite implementation.

#include "TestUtils/TestUtils.hpp"

TEST_GROUP_BASE(MatrixTests, TestUtils){};

using MatrixTests = TEST_GROUP_CppUTestGroupMatrixTests;

/// @name Matrix_setSlaveConfig(), Matrix_getSlaveConfig()
/// @{

/// \Given Matrix device descriptor initialized with mock registers,
///
/// \When Matrix slave configuration is set using Matrix_setSlaveConfig() function and
///       Matrix slave configuration is read using Matrix_getSlaveConfig() function,
///
/// \Then Matrix_setSlaveConfig() function correctly sets Matrix SCFG, PRAS and PRBS registers for
///       the selected slave based on provided slave configuration descriptor and
///       Matrix_getSlaveConfig() function correctly gets slave configuration based on contents of
///       Matrix SCFG, PRAS and PRBS registers.
///
TEST(MatrixTests, setsAndGetsSlaveConfig)
{
	// Set slave config using nominal values and then read config.
	const Matrix_Slave slaveOne = Matrix_Slave_Hemc;
	const Matrix_Master masterOne = Matrix_Master_Can1Dma;
	Matrix_SlaveConfig writtenConfigOne = {};
	writtenConfigOne.slaveDefaultMasterType =
			Matrix_SlaveDefaultMasterType_Last;
	writtenConfigOne.fixedMasterForSlaveSelection = masterOne;
	writtenConfigOne.maximumBusGrantDurationForMasters = 0x00AB;
	writtenConfigOne.accessPriority[masterOne].isLatencyQosEnabled = true;
	writtenConfigOne.accessPriority[masterOne].masterPriority =
			Matrix_MasterPriority_2;
	Matrix_setSlaveConfig(&mockMatrix, slaveOne, &writtenConfigOne);

	expectedMatrixRegs.scfg[3] |= (UINT32_C(5) << 18u)
			| (UINT32_C(1) << 16u) | (UINT32_C(0x00AB) << 0u);
	expectedMatrixRegs.prs[3].pras |=
			(UINT32_C(1) << 22u) | (UINT32_C(2) << 20u);
	expectedMatrixRegs.prs[3].prbs |= 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_SlaveConfig readConfigOne = {};
	Matrix_getSlaveConfig(&mockMatrix, slaveOne, &readConfigOne);

	checkSlaveConfig(writtenConfigOne, readConfigOne, __LINE__);

	// Set slave config using maximum values and then read config.
#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Slave slaveTwo = Matrix_Slave_Flexram2;
	const Matrix_Master masterTwo = Matrix_Master_XdmacPort1;

	const size_t slaveIndex = 7;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Slave slaveTwo = Matrix_Slave_Adc;
	const Matrix_Master masterTwo = Matrix_Master_Crccu;

	const size_t slaveIndex = 8;
#endif

	Matrix_SlaveConfig writtenConfigTwo = {};
	writtenConfigTwo.slaveDefaultMasterType =
			Matrix_SlaveDefaultMasterType_Fixed;
	writtenConfigTwo.fixedMasterForSlaveSelection = masterTwo;
	writtenConfigTwo.maximumBusGrantDurationForMasters = 511;
	writtenConfigTwo.accessPriority[masterTwo].isLatencyQosEnabled = true;
	writtenConfigTwo.accessPriority[masterTwo].masterPriority =
			Matrix_MasterPriority_3;
	Matrix_setSlaveConfig(&mockMatrix, slaveTwo, &writtenConfigTwo);

	expectedMatrixRegs.scfg[slaveIndex] |= (UINT32_C(12) << 18u)
			| (UINT32_C(2) << 16u) | (UINT32_C(511) << 0u);
	expectedMatrixRegs.prs[slaveIndex].pras |= 0u;
	expectedMatrixRegs.prs[slaveIndex].prbs |=
			(UINT32_C(1) << 18u) | (UINT32_C(3) << 16u);
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_SlaveConfig readConfigTwo = {};
	Matrix_getSlaveConfig(&mockMatrix, slaveTwo, &readConfigTwo);

	checkSlaveConfig(writtenConfigTwo, readConfigTwo, __LINE__);

	// Set slave config using minimum values and then read config.
	const Matrix_Slave slaveThree = slaveTwo;
	const Matrix_Master masterAccessPriority = masterTwo;
	const Matrix_Master masterThree = Matrix_Master_CortexM7Port0;

	Matrix_SlaveConfig writtenConfigThree = {};
	writtenConfigThree.slaveDefaultMasterType =
			Matrix_SlaveDefaultMasterType_None;
	writtenConfigThree.fixedMasterForSlaveSelection = masterThree;
	writtenConfigThree.maximumBusGrantDurationForMasters = 0;
	writtenConfigThree.accessPriority[masterAccessPriority]
			.isLatencyQosEnabled = false;
	writtenConfigThree.accessPriority[masterAccessPriority].masterPriority =
			Matrix_MasterPriority_0;
	Matrix_setSlaveConfig(&mockMatrix, slaveThree, &writtenConfigThree);

	expectedMatrixRegs.scfg[slaveIndex] = 0u;
	expectedMatrixRegs.prs[slaveIndex].prbs = 0u;
	expectedMatrixRegs.prs[slaveIndex].pras = 0u;
	STRUCTS_EQUAL(expectedMatrixRegs, mockMatrixRegs);

	Matrix_SlaveConfig readConfigThree = writtenConfigTwo;
	Matrix_getSlaveConfig(&mockMatrix, slaveThree, &readConfigThree);

	checkSlaveConfig(writtenConfigThree, readConfigThree, __LINE__);
}

/// @}
