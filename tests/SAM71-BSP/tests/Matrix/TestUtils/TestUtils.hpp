/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_MATRIX_TESTS_TESTUTILS_HPP
#define N7S_MATRIX_TESTS_TESTUTILS_HPP

/// \file  TestUtils.hpp
/// \brief MATRIX driver tests helper header.

#include <n7s/bsp/Matrix/Matrix.h>

#include <cstring>
#include <string>

#include <CppUTest/TestHarness.h>

// NOLINTBEGIN(clang-diagnostic-unused-function)
static inline bool
operator==(const Matrix_Registers &first, const Matrix_Registers &second)
{
	// Allow this way of comparing, because register structs shall be contiguous memory blocks
	// without padding. "Reserved" members of register structs can also be compared against each other
	// as long as "==" operator is used to compare mock register structs.
	// NOLINTNEXTLINE(bugprone-suspicious-memory-comparison,cert-exp42-c,cert-flp37-c)
	return memcmp(&first, &second, sizeof(Matrix_Registers)) == 0;
}

static inline size_t
diffOffset(const Matrix_Registers &first, const Matrix_Registers &second)
{
	const auto *const firstPtr = reinterpret_cast<const uint8_t *>(&first);
	const auto *const secondPtr =
			reinterpret_cast<const uint8_t *>(&second);
	size_t i = 0;
	while ((i < sizeof(Matrix_Registers)) && (firstPtr[i] == secondPtr[i]))
		i++;
	return i >= sizeof(Matrix_Registers) ? SIZE_MAX : i;
}

// NOLINTEND(clang-diagnostic-unused-function)

// Allow this macro for the purpose of showing actual line where fail happened.
// NOLINTBEGIN(cppcoreguidelines-macro-usage)
#define STRUCTS_EQUAL(expected, actual) \
	CHECK_TEXT((expected) == (actual), \
			(std::string("Structs not equal, difference starts at offset ") \
					+ std::to_string(diffOffset( \
							(expected), \
							(actual)))) \
					.c_str())

// NOLINTEND(cppcoreguidelines-macro-usage)

TEST_BASE(TestUtils)
{
	using LineNumber = int;

	static inline Matrix matrix{};
	static inline Matrix mockMatrix{};
	static inline Matrix_Registers mockMatrixRegs{};
	static inline Matrix_Registers expectedMatrixRegs{};

	void setup() override
	{
		matrix = {};
		mockMatrix = {};
		mockMatrixRegs = {};
		expectedMatrixRegs = {};

		Matrix_init(&mockMatrix, &mockMatrixRegs);
	}

#if defined(N7S_TARGET_SAMRH71F20)
	static void checkRh71SlaveConfig(const Matrix_SlaveConfig &expected,
			const Matrix_SlaveConfig &actual,
			const LineNumber lineNumber)
	{
		// cppcheck-suppress [misra-c2012-18.4]
		const auto text = std::string(__FUNCTION__)
				+ " called at line: "
				+ std::to_string(lineNumber);
		constexpr Matrix_Master master = Matrix_Master_GmacDma;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
	}
#endif

	static void checkSlaveConfig(const Matrix_SlaveConfig &expected,
			const Matrix_SlaveConfig &actual,
			const LineNumber lineNumber)
	{
		// cppcheck-suppress [misra-c2012-18.4]
		const auto text = std::string(__FUNCTION__)
				+ " called at line: "
				+ std::to_string(lineNumber);
		CHECK_EQUAL_TEXT(expected.slaveDefaultMasterType,
				actual.slaveDefaultMasterType, text.c_str());
		CHECK_EQUAL_TEXT(expected.fixedMasterForSlaveSelection,
				actual.fixedMasterForSlaveSelection,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.maximumBusGrantDurationForMasters,
				actual.maximumBusGrantDurationForMasters,
				text.c_str());
		Matrix_Master master = Matrix_Master_CortexM7Port0;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_CortexM7Port1;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_CortexM7PeripheralPort;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_XdmacPort0;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_Can0Dma;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_Can1Dma;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_SpwTx;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_SpwRx;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_SpwRmap;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_Ip1553;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_Icm;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
		master = Matrix_Master_XdmacPort1;
		CHECK_EQUAL_TEXT(expected.accessPriority[master]
						 .isLatencyQosEnabled,
				actual.accessPriority[master]
						.isLatencyQosEnabled,
				text.c_str());
		CHECK_EQUAL_TEXT(expected.accessPriority[master].masterPriority,
				actual.accessPriority[master].masterPriority,
				text.c_str());
#if defined(N7S_TARGET_SAMRH71F20)
		checkRh71SlaveConfig(expected, actual, lineNumber);
#endif
	}

	static void checkMastersStatuses(const Matrix_MastersStatuses &expected,
			const Matrix_MastersStatuses &actual,
			const LineNumber lineNumber)
	{
		const std::string text = std::string(__FUNCTION__)
				+ std::string(" called at line: ")
				+ std::to_string(lineNumber);
		const char *const charText = text.c_str();

		Matrix_Master master = Matrix_Master_CortexM7Port0;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_CortexM7Port1;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_CortexM7PeripheralPort;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_XdmacPort0;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_Can0Dma;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_Can1Dma;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
#if defined(N7S_TARGET_SAMRH71F20)
		master = Matrix_Master_GmacDma;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
#endif
		master = Matrix_Master_SpwTx;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_SpwRx;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_SpwRmap;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_Ip1553;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_Icm;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
		master = Matrix_Master_XdmacPort1;
		CHECK_EQUAL_TEXT(expected.isMasterIrqTriggered[master],
				actual.isMasterIrqTriggered[master], charText);
		CHECK_EQUAL_TEXT(expected.masterErrorAddress[master],
				actual.masterErrorAddress[master], charText);
	}

	static void checkSlaveRegionProtectionConfig(
			const Matrix_SlaveRegionProtectionConfig &expected,
			const Matrix_SlaveRegionProtectionConfig &actual,
			const LineNumber lineNumber)
	{
		const std::string text = std::string(__FUNCTION__)
				+ std::string(" called at line: ")
				+ std::to_string(lineNumber);
		const char *const charText = text.c_str();

		CHECK_EQUAL_TEXT(expected.isPrivilegedRegionUserWriteAllowed,
				actual.isPrivilegedRegionUserWriteAllowed,
				charText);
		CHECK_EQUAL_TEXT(expected.isPrivilegedRegionUserReadAllowed,
				actual.isPrivilegedRegionUserReadAllowed,
				charText);
		CHECK_EQUAL_TEXT(expected.regionSplitOffsetDirection,
				actual.regionSplitOffsetDirection, charText);
		CHECK_EQUAL_TEXT(expected.protectedRegionSize,
				actual.protectedRegionSize, charText);
		CHECK_EQUAL_TEXT(expected.regionSplitOffset,
				actual.regionSplitOffset, charText);
		CHECK_EQUAL_TEXT(expected.regionOrder, actual.regionOrder,
				charText);
	}
};

#endif // N7S_MATRIX_TESTS_TESTUTILS_HPP
