/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Matrix4Tests.cpp
/// \brief Matrix driver test suite implementation.

#include "TestUtils/TestUtils.hpp"

TEST_GROUP_BASE(MatrixTests, TestUtils){};

using MatrixTests = TEST_GROUP_CppUTestGroupMatrixTests;

/// @name Matrix_getPeripheralProtectionConfig()
/// @{

/// \Given Matrix device descriptor initialized with mock registers having only the bit that
///        corresponds to peripheral with minimal value of ID in Matrix PPSELR registers set to one,
///
/// \When Matrix_getPeripheralProtectionConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - peripheral ID of minimal value,
///       - peripheral protection config descriptor,
///       .
///
/// \Then the function gets correct peripheral protection configuration of the selected peripheral
///       based on the contents of Matrix PPSELR registers.
///
TEST(MatrixTests,
		getPeripheralProtectionConfig_withAccessTypeOptionSetToUserAccessOfPeripheralWithMinimalId)
{
	mockMatrixRegs.ppselr[0] = 1u << 0u;

	Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_Privileged,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_Supc;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_ReportChipId;
#endif

	Matrix_getPeripheralProtectionConfig(
			&mockMatrix, peripheralId, &config);

	ENUMS_EQUAL_INT(Matrix_AccessType_User, config.accessType);
}

/// \Given Matrix device descriptor initialized with mock registers having only the bit that
///        corresponds to peripheral with nominal value of ID in Matrix PPSELR registers set to one,
///
/// \When Matrix_getPeripheralProtectionConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - peripheral ID of nominal value,
///       - peripheral protection config descriptor,
///       .
///
/// \Then the function gets correct peripheral protection configuration of the selected peripheral
///       based on the contents of Matrix PPSELR registers.
///
TEST(MatrixTests,
		getPeripheralProtectionConfig_withAccessTypeOptionSetToUserAccessOfPeripheralWithNominalId)
{
	mockMatrixRegs.ppselr[1] = UINT32_C(1) << 4u;

	Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_Privileged,
	};
#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Mcan0;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Adc;
#endif

	Matrix_getPeripheralProtectionConfig(
			&mockMatrix, nominalPeripheralId, &config);

	ENUMS_EQUAL_INT(Matrix_AccessType_User, config.accessType);
}

/// \Given Matrix device descriptor initialized with mock registers having only the bit that
///        corresponds to peripheral with maximal value of ID in Matrix PPSELR registers set to one,
///
/// \When Matrix_getPeripheralProtectionConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - peripheral ID of maximal value,
///       - peripheral protection config descriptor,
///       .
///
/// \Then the function gets correct peripheral protection configuration of the selected peripheral
///       based on the contents of Matrix PPSELR registers.
///
TEST(MatrixTests,
		getPeripheralProtectionConfig_withAccessTypeOptionSetToUserAccessOfPeripheralWithMaximalId)
{
	mockMatrixRegs.ppselr[2] = UINT32_C(1) << 10u;

	Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_Privileged,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_Gmac_Queue5;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_Nmic;
#endif

	Matrix_getPeripheralProtectionConfig(
			&mockMatrix, peripheralId, &config);

	ENUMS_EQUAL_INT(Matrix_AccessType_User, config.accessType);
}

/// \Given Matrix device descriptor initialized with mock registers having only the bit that
///        corresponds to peripheral with minimal value of ID in Matrix PPSELR registers set to
///        zero,
///
/// \When Matrix_getPeripheralProtectionConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - peripheral ID of minimal value,
///       - peripheral protection config descriptor,
///       .
///
/// \Then the function gets correct peripheral protection configuration of the selected peripheral
///       based on the contents of Matrix PPSELR registers.
///
TEST(MatrixTests,
		getPeripheralProtectionConfig_wWithAccessTypeOptionSetToPrivilegedAccessOfPeripheralWithMinimalId)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.ppselr[0] &= ~(UINT32_C(1) << 0u);

	Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_User,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_Supc;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_ReportChipId;
#endif
	Matrix_getPeripheralProtectionConfig(
			&mockMatrix, peripheralId, &config);

	ENUMS_EQUAL_INT(Matrix_AccessType_Privileged, config.accessType);
}

/// \Given Matrix device descriptor initialized with mock registers having only the bit that
///        corresponds to peripheral with nominal value of ID in Matrix PPSELR registers set to
///        zero,
///
/// \When Matrix_getPeripheralProtectionConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - peripheral ID of nominal value,
///       - peripheral protection config descriptor,
///       .
///
/// \Then the function gets correct peripheral protection configuration of the selected peripheral
///       based on the contents of Matrix PPSELR registers.
///
TEST(MatrixTests,
		getPeripheralProtectionConfig_withAccessTypeOptionSetToPrivilegedAccessOfPeripheralWithNominalId)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.ppselr[1] &= ~(UINT32_C(1) << 4u);

	Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_User,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Mcan0;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral nominalPeripheralId = Matrix_Peripheral_Adc;
#endif

	Matrix_getPeripheralProtectionConfig(
			&mockMatrix, nominalPeripheralId, &config);

	ENUMS_EQUAL_INT(Matrix_AccessType_Privileged, config.accessType);
}

/// \Given Matrix device descriptor initialized with mock registers having only the bit that
///        corresponds to peripheral with maximal value of ID in Matrix PPSELR registers set to
///        zero,
///
/// \When Matrix_getPeripheralProtectionConfig() function is called with following parameters:
///       - pointer to the Matrix device descriptor,
///       - peripheral ID of maximal value,
///       - peripheral protection config descriptor,
///       .
///
/// \Then the function gets correct peripheral protection configuration of the selected peripheral
///       based on the contents of Matrix PPSELR registers.
///
TEST(MatrixTests,
		getPeripheralProtectionConfig_withAccessTypeOptionSetToPrivilegedAccessOfPeripheralWithMaximalId)
{
	(void)memset(static_cast<void *>(&mockMatrixRegs), 0xFFu,
			sizeof(Matrix_Registers));
	mockMatrixRegs.ppselr[2] &= ~(UINT32_C(1) << 10u);

	Matrix_PeripheralProtectionConfig config = {
		.accessType = Matrix_AccessType_User,
	};

#if defined(N7S_TARGET_SAMRH71F20)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_Gmac_Queue5;
#elif defined(N7S_TARGET_SAMRH707F18)
	const Matrix_Peripheral peripheralId = Matrix_Peripheral_Nmic;
#endif

	Matrix_getPeripheralProtectionConfig(
			&mockMatrix, peripheralId, &config);

	ENUMS_EQUAL_INT(Matrix_AccessType_Privileged, config.accessType);
}

/// @}
