/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PmcFailures2Tests.cpp
/// \brief Pmc driver test suite implementation.

#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Pmc/PmcRegisters.h>
#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Systick/Systick.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsRuntimeSam7/MpuTraps.hpp>

#include "TestUtils.hpp"

TEST_GROUP(PmcTests)
{
	void setup() override
	{
#if defined(__XC32__) && defined(N7S_TARGET_SAMRH707F18)
		// FIXME: PMC failure tests built with XC32 for SAMRH707 are crashing in MPU traps.
		// This behavior is cache-related, disabling data cache allows the tests to pass.
		(void)Scb_disableDCache();
#endif
		TestUtils::setup();
		mpuTraps.registerInstance();
	}

	void teardown() override { TestUtils::teardown(); }

	MpuTraps mpuTraps;
};

using PmcTests = TEST_GROUP_CppUTestGroupPmcTests;

/// @name Pmc_measureMainck()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When source clock oscillator frequency is measured and hardware does not raise ready flag,
///
/// \Then measured frequency is zero and timeout error is returned.
///
TEST(PmcTests, Pmc_measureMainck_returnsErrorOnFrequencyMeasurementTimeout)
{
	Pmc_MainckMeasurement measurement = {
		.measuredClk = Pmc_MeasuredClk_RcOsc,
		.refFreq = PMC_SLOW_CLOCK_FREQ,
		.measuredFreq = 0,
	};

	ErrorCode errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_measureMainck(&TestUtils::mockPmc, &measurement,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_FrequencyMeasurementTimeout, errCode);
}

/// @}

/// @name Pmc_setConfig()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with invalid main clock source,
///
/// \Then invalid clock source error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorForInvalidMainClockSource)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	ErrorCode errCode = ErrorCode_NoError;
	config.mainck.src = arbitraryEnumValue<Pmc_MainckSrc>(0xDEAD);

	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCSELS_MASK;

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_InvalidClockSource, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware does not raise any
///       ready flags,
///
/// \Then RC enable timeout error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorRCFailsToStabilize)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	const std::array<Pmc_MainckSrc, 3> MAINCK_SOURCES_TO_TEST = {
		Pmc_MainckSrc_RcOsc, Pmc_MainckSrc_XOsc,
		Pmc_MainckSrc_XOscBypassed
	};
#elif defined(N7S_TARGET_SAMRH707F18)
	const std::array<Pmc_MainckSrc, 2> MAINCK_SOURCES_TO_TEST = {
		Pmc_MainckSrc_RcOsc, Pmc_MainckSrc_XOsc
	};
#endif
	// Result should be the same for each of the allowed main clock sources
	for (const Pmc_MainckSrc mainckSrc : MAINCK_SOURCES_TO_TEST) {
		config.mainck.src = mainckSrc;
		ErrorCode errCode = ErrorCode_NoError;
		CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
				PMC_DEFAULT_TIMEOUT, &errCode));
		ENUMS_EQUAL_INT(Pmc_ErrorCode_RCStabilizationTimeout, errCode);
	}
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware raises only MOSCRCS flag,
///
/// \Then main clock selection timeout error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorIfOnlyRCReadyFlagIsSet)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	const std::array<Pmc_MainckSrc, 3> MAINCK_SOURCES_TO_TEST = {
		Pmc_MainckSrc_RcOsc, Pmc_MainckSrc_XOsc,
		Pmc_MainckSrc_XOscBypassed
	};
#elif defined(N7S_TARGET_SAMRH707F18)
	const std::array<Pmc_MainckSrc, 2> MAINCK_SOURCES_TO_TEST = {
		Pmc_MainckSrc_RcOsc, Pmc_MainckSrc_XOsc
	};
#endif
	// Result should be the same for each of the allowed main clock sources
	for (const Pmc_MainckSrc mainckSrc : MAINCK_SOURCES_TO_TEST) {
		config.mainck.src = mainckSrc;
		ErrorCode errCode = ErrorCode_NoError;
		TestUtils::mockPmcRegsAligned.regs = {};
		TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK;

		CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
				PMC_DEFAULT_TIMEOUT, &errCode));
		ENUMS_EQUAL_INT(Pmc_ErrorCode_MainClkSelectTimeout, errCode);
	}
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware raises all ready flags
///       but MCKRDY fails after disabling PLL,
///
/// \Then master clock ready timeout error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorIfClockSetupFailsAfterPllConfig)
{
	ErrorCode errCode = ErrorCode_NoError;

	// mock flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MCKRDY_MASK | PMC_SR_MOSCSELS_MASK;

	// setup trap on PLLAR write
	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrPllar, []() {
		// Change SR flags (MCKRDY is not set)
		TestUtils::mockPmcRegsAligned.regs.sr =
				PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK;
	});

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc,
			&TestUtils::defaultPmcConfig, PMC_DEFAULT_TIMEOUT,
			&errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware fails to reset
/// Master clock,
///
/// \Then error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorOnMasterClockResetFailure)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK;

	// setup trap on MCKR write
	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.mckr, []() {
		// drop all ready flags
		TestUtils::mockPmcRegsAligned.regs.sr = 0u;
	});

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware fails to reset
///       Main clock generator,
///
/// \Then error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorOnMainClockGeneratorResetFailure)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK;

	// setup trap on MCRK(w) -> CKGR_MOR(r+w)
	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.mckr, [this]() {
		mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.ckgrMor,
				[]() {
					TestUtils::mockPmcRegsAligned.regs.sr =
							0u;
				});
	});

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_RCStabilizationTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware fails to reset
///       Master clock prescaler and divider,
///
/// \Then error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorOnMasterClockPrescalerResetFailure)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK;

	// setup trap on MCKR(w) -> CKGR_MOR(rw) -> MCKR(w)
	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.mckr, [this]() {
		mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
			mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs
							 .mckr,
					[]() {
						TestUtils::mockPmcRegsAligned
								.regs.sr = 0u;
					});
		});
	});

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration and hardware fails to setup
/// master clock,
///
/// \Then error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsErrorOnMasterClockSetupFailure)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = arbitraryEnumValue<Pmc_MasterckSrc>(0xAD);
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
#if defined(N7S_TARGET_SAMV71Q21)
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK;
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK
			| PMC_SR_LOCKB_MASK;
#endif
	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_InvalidClockSource, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration with PLL,
///       PLLA used as master clock source and hardware raises
///       MCKRDY, MOSCRCS, MOSCSELS
///
/// \Then PLL not locked error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsPLLANotLockedOnPllError)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK;

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_PllaNotLocked, errCode);

	// Also try to call this with errCode set to nullptr
	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, nullptr));
}

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration for PLLB,
///       hardware raises MCKRDY, MOSCRCS, MOSCSELS, LOCKA, but not LOCKB
///
/// \Then PLLB not locked error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsPLLBNotLockedOnPllError)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	config.pll.pllbDiv = 1u;
	config.pll.pllbMul = 1u;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_LOCKA_MASK;

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_PllbNotLocked, errCode);

	// Also try to call this with errCode set to nullptr
	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, nullptr));
}
#endif

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration with PCK enabled  and hardware
///       fails to raise PCKRDYx flag,
///
/// \Then PLL not locked error is returned.
///
TEST(PmcTests, Pmc_setConfig_returnsPckReadyTimeoutErrorWithoutPckrdyFlag)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	config.pck[0].isEnabled = true;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK;

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	TestUtils::mockPmcRegsAligned.regs.sr =
			TestUtils::mockPmcRegsAligned.regs.sr
			| PMC_SR_LOCKB_MASK;
#endif

	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_PckReadyTimeout, errCode);

	// Also try to call this with errCode set to nullptr
	CHECK_FALSE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, nullptr));
}

/// @}
