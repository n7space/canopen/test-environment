/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2022-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_PMC_TESTS_TESTUTILS_HPP
#define N7S_PMC_TESTS_TESTUTILS_HPP

/// \file  TestUtils.hpp
/// \brief PMC driver tests helper header.

#include <array>
#include <cstring>
#include <functional>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pmc/ChipConfig.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Pmc/PmcRegisters.h>
#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Systick/Systick.h>
#include <n7s/bsp/Tic/Tic.h>

class TestUtils
{
      public:
	static inline Tic tic;

	static inline const uint32_t delayedWriteTicks = 20000u;

	static inline std::function<void()> timerCallback = nullptr;

	static inline Pmc pmc;
	static inline Pmc mockPmc;

	// Structure with registers needs to be aligned to 16 bytes, but NOT to 32 bytes
	// (in the middle of 32 byte region). Smallest MPU region is 32 bytes and
	// boundary between consecutive regions needs to be between PLLAR and MCKR registers
	// Align outer structure to 32 bytes and insert 16 byte padding to ensure proper alignment.
	__attribute__((__aligned__(32))) static inline struct {
		std::array<uint32_t, 4> padding;
		Pmc_Registers regs;
	} mockPmcRegsAligned;

	static void
	setup()
	{
		// Back up PMC config before running test
		Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

		mockPmcRegsAligned.regs = {};
		(void)memset(mockPmcRegsAligned.padding.data(), 0, 16);
		Pmc_init(&mockPmc, &mockPmcRegsAligned.regs);

		Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc0Ch0);

		Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel0);

		Scb_setMemoryManagementExceptionEnabled(true);

		Tic_init(&tic, Tic_Id_0);
	}

	static void
	teardown()
	{
		ErrorCode errCode = ErrorCode_NoError;
		const Pmc_Config config = Pmc_ChipConfig_getConfig();
		(void)Pmc_setConfig(
				&pmc, &config, PMC_DEFAULT_TIMEOUT, &errCode);

		Tic_disableChannel(&tic, Tic_Channel_0);
		Nvic_clearInterruptPending(Nvic_Irq_Timer0_Channel0);
	}

	static void
	resetTimer()
	{
		const Tic_ChannelConfig config{};
		Tic_ChannelStatus status;
		Tic_getChannelStatus(&tic, Tic_Channel_0, &status);
		Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
		Nvic_clearInterruptPending(Nvic_Irq_Timer0_Channel0);
	}

	static inline const Pmc_Config defaultPmcConfig = {
    .mainck = {
      .src = Pmc_MainckSrc_XOsc,
      .rcOscFreq = Pmc_RcOscFreq_8M,
      .xoscStartupTime = 72,
#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
      .detectClockFail = false,
      .xt32kMonitorEnabled = false,
      .badCpuResetEnabled = false,
      .badCpuNmicIrqEnabled = false,
#endif
    },
    .pll = {
      .pllaMul = 18,
      .pllaDiv = 2,
      .pllaStartupTime = 49,

#if defined(N7S_TARGET_SAMRH71F20)
      .pllaVco = 2,
      .pllaFilterCapacitor = Pmc_FilterCapacitor_60p,
      .pllaFilterResistor = Pmc_FilterResistor_24K,
      .pllaCurrent = Pmc_PllCurrent_1250u,

      .pllbSrc = Pmc_PllbSrc_Mainck,
      .pllbMul = 20 - 1,
      .pllbDiv = 1,
      .pllbStartupTime = 60,
      .pllbVco = 2,
      .pllbFilterCapacitor = Pmc_FilterCapacitor_40p,
      .pllbFilterResistor = Pmc_FilterResistor_24K,
      .pllbCurrent = Pmc_PllCurrent_1250u,
#elif defined(N7S_TARGET_SAMRH707F18)
      .pllaVco = 1,
      .pllaFilterCapacitor = Pmc_FilterCapacitor_60p,
      .pllaFilterResistor = Pmc_FilterResistor_24K,
      .pllaCurrent = Pmc_PllCurrent_1250u,

      .pllbSrc = Pmc_PllbSrc_Mainck,
      .pllbMul = 10 - 1,
      .pllbDiv = 1,
      .pllbStartupTime = 60,
      .pllbVco = 1,
      .pllbFilterCapacitor = Pmc_FilterCapacitor_40p,
      .pllbFilterResistor = Pmc_FilterResistor_24K,
      .pllbCurrent = Pmc_PllCurrent_1250u,
#endif
    },
    .masterck = {
      .src = Pmc_MasterckSrc_Mainck,
      .presc = Pmc_MasterckPresc_4,
      .divider = Pmc_MasterckDiv_2,
    },
    .pck = {
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
#if defined(N7S_TARGET_SAMV71Q21)
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
      { .isEnabled = false, .src = Pmc_PckSrc_Slck, .presc = 0u },
#endif
    },

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
    .rc2Osc = {
      .isEnabled = true,
      .frequency = Pmc_RcOscFreq_10M,
    },
#endif
  };

	static void
	setupDelayedWrite(volatile uint32_t *const address,
			const uint32_t value, const uint32_t delayTicks)
	{
		Tic_ChannelConfig ticConfig{};
		ticConfig.isEnabled = true;
		ticConfig.channelMode = Tic_Mode_Waveform;
		ticConfig.clockSource = Tic_ClockSelection_MckBy8;
		ticConfig.irqConfig.isRcCompareIrqEnabled = true;
		ticConfig.modeConfig.waveformModeConfig.isDisabledOnRcCompare =
				true;
		ticConfig.rc = delayTicks;

		Tic_setChannelConfig(&tic, Tic_Channel_0, &ticConfig);

		timerCallback = [address, value]() { *address = value; };
	}

	static void
	triggerDelayedWrite()
	{
		Tic_triggerChannel(&tic, Tic_Channel_0);
	}
};

#endif // N7S_PMC_TESTS_TESTUTILS_HPP
