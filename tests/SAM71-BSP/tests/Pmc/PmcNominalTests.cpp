/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PmcNominalTests.cpp
/// \brief Pmc driver test suite implementation.

#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Pmc/PmcRegisters.h>
#include <n7s/bsp/Systick/Systick.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

TEST_GROUP(PmcTests){ void setup() override{ TestUtils::setup();
}

void
teardown() override
{
	TestUtils::teardown();
}
}
;

using PmcTests = TEST_GROUP_CppUTestGroupPmcTests;

/// @name Pmc_setConfig()
/// @{

/// \Given Pmc driver,
///
/// \When Pmc_setConfig() function is called for this Pmc with a configuration descriptor
/// set to a nominal value,
///
/// \Then registers are set to correct values.
///
TEST(PmcTests, Pmc_setConfig_configuresPmc)
{
	Pmc_Config config{};

	config.mainck.xoscStartupTime = 72;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.pll.pllaMul = 18;
	config.pll.pllaDiv = 2;
	config.pll.pllaStartupTime = 49;
	config.masterck.src = Pmc_MasterckSrc_Mainck;
	config.masterck.presc = Pmc_MasterckPresc_4;

	config.masterck.divider = Pmc_MasterckDiv_2;
	for (auto &pck : config.pck)
		pck.isEnabled = false;

	config.pck[2].isEnabled = true;
	config.pck[2].src = Pmc_PckSrc_Pllack;
	config.pck[2].presc = 0x4;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK
			| PMC_SR_PCKRDY2_MASK;

	ErrorCode errCode = ErrorCode_NoError;
	const auto result = Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode);
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

#if defined(N7S_TARGET_SAMV71Q21)
	Pmc_enablePeripheralClk(&TestUtils::mockPmc, Pmc_PeripheralId_Usart1);
	Pmc_enablePeripheralClk(&TestUtils::mockPmc, Pmc_PeripheralId_Uart1);
	Pmc_enablePeripheralClk(&TestUtils::mockPmc, Pmc_PeripheralId_PioB);
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	Pmc_enablePeripheralClk(&TestUtils::mockPmc, Pmc_PeripheralId_Flexcom1);
	Pmc_enablePeripheralClk(&TestUtils::mockPmc, Pmc_PeripheralId_PioA);
#else
#error "No target platform specified (missing N7S_TARGET_* macro)"
#endif

	UNSIGNED_LONGS_EQUAL(CKGR_MOR_MOSCRCEN_MASK
					| (CKGR_MOR_KEY_VALUE
							<< CKGR_MOR_KEY_OFFSET),
			TestUtils::mockPmc.registers->ckgrMor);
	UNSIGNED_LONGS_EQUAL(((UINT32_C(0x2) << CKGR_PLLAR_DIVA_OFFSET)
					     & CKGR_PLLAR_DIVA_MASK)
					| ((UINT32_C(0x12) << CKGR_PLLAR_MULA_OFFSET)
							& CKGR_PLLAR_MULA_MASK)
					| ((UINT32_C(0x31) << CKGR_PLLAR_PLLACOUNT_OFFSET)
							& CKGR_PLLAR_PLLACOUNT_MASK)
					| ((UINT32_C(0x1) << CKGR_PLLAR_ONE_OFFSET)
							& CKGR_PLLAR_ONE_MASK),
			TestUtils::mockPmc.registers->ckgrPllar);
	UNSIGNED_LONGS_EQUAL(((UINT32_C(0x1) << PMC_MCKR_CSS_OFFSET)
					     & PMC_MCKR_CSS_MASK)
					| ((UINT32_C(0x02) << PMC_MCKR_PRES_OFFSET)
							& PMC_MCKR_PRES_MASK)
					| ((UINT32_C(0x1) << PMC_MCKR_MDIV_OFFSET)
							& PMC_MCKR_MDIV_MASK),
			TestUtils::mockPmc.registers->mckr);

	UNSIGNED_LONGS_EQUAL(((UINT32_C(0x2) << PMC_PCK_CSS_OFFSET)
					     & PMC_PCK_CSS_MASK)
					| ((UINT32_C(0x4) << PMC_PCK_PRES_OFFSET)
							& PMC_PCK_PRES_MASK),
			TestUtils::mockPmc.registers->pck[2]);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setConfig() is called with valid configuration with PLL and hardware
///       raises all ready flags,
///
/// \Then configuration succeeds.
///
TEST(PmcTests, Pmc_setConfig_succeedsIfHardwareIsReady)
{
	Pmc_Config config = TestUtils::defaultPmcConfig;
	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.masterck.src = Pmc_MasterckSrc_Pllack;
	ErrorCode errCode = ErrorCode_NoError;

	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK;

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	TestUtils::mockPmcRegsAligned.regs.sr =
			TestUtils::mockPmcRegsAligned.regs.sr
			| PMC_SR_LOCKB_MASK;
#endif

	CHECK_TRUE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// Also try to call this with errCode set to nullptr
	CHECK_TRUE(Pmc_setConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, nullptr));
}

/// @}

/// @name Pmc_getConfig()
/// @{

/// \Given Pmc driver,
///
/// \When Pmc_setConfig() function is called for this Pmc with a configuration descriptor
/// set to a nominal value,
///
/// \Then Pmc configuration descriptor retrieved using Pmc_getConfig() function
/// is the same as the one used to set the Pmc configuration.
///
TEST(PmcTests, Pmc_getConfig_retrievesPmcConfiguration)
{
	Pmc_Config originalConfig{};
	Pmc_Config config{};
	Pmc_Config readConfig{};
	Pmc_getConfig(&TestUtils::pmc, &originalConfig);

	config.mainck.src = Pmc_MainckSrc_RcOsc;
	config.mainck.rcOscFreq = Pmc_RcOscFreq_8M;
	config.pll.pllaMul = 17;
	config.pll.pllaDiv = 9;
	config.pll.pllaStartupTime = 61;
	config.masterck.src = Pmc_MasterckSrc_Mainck;
	config.masterck.presc = Pmc_MasterckPresc_16;
	config.masterck.divider = Pmc_MasterckDiv_2;

	for (auto &pck : config.pck)
		pck.isEnabled = false;

	// RH71 has only 4 PCKs, SAMV71 has 8 PCKs
#if defined(N7S_TARGET_SAMV71Q21)
	config.pck[5].isEnabled = true;
	config.pck[5].src = Pmc_PckSrc_Masterck;
	config.pck[5].presc = 0x8;
#elif defined(N7S_TARGET_SAMRH71F20)
	config.rc2Osc.isEnabled = true;
	config.rc2Osc.frequency = Pmc_RcOscFreq_10M;

	config.pll.pllaVco = 2;
	config.pll.pllaFilterCapacitor = Pmc_FilterCapacitor_60p;
	config.pll.pllaFilterResistor = Pmc_FilterResistor_24K;
	config.pll.pllaCurrent = Pmc_PllCurrent_1250u;

	config.pll.pllbMul = 20 - 1;
	config.pll.pllbDiv = 1;
	config.pll.pllbStartupTime = 60;
	config.pll.pllbVco = 2;
	config.pll.pllbSrc = Pmc_PllbSrc_Mainck;
	config.pll.pllbFilterCapacitor = Pmc_FilterCapacitor_40p;
	config.pll.pllbFilterResistor = Pmc_FilterResistor_24K;
	config.pll.pllbCurrent = Pmc_PllCurrent_1250u;
#elif defined(N7S_TARGET_SAMRH707F18)
	config.rc2Osc.isEnabled = true;
	config.rc2Osc.frequency = Pmc_RcOscFreq_10M;

	config.pll.pllaVco = 1;
	config.pll.pllaFilterCapacitor = Pmc_FilterCapacitor_60p;
	config.pll.pllaFilterResistor = Pmc_FilterResistor_24K;
	config.pll.pllaCurrent = Pmc_PllCurrent_1250u;

	config.pll.pllbMul = 10 - 1;
	config.pll.pllbDiv = 1;
	config.pll.pllbStartupTime = 60;
	config.pll.pllbVco = 1;
	config.pll.pllbSrc = Pmc_PllbSrc_Mainck;
	config.pll.pllbFilterCapacitor = Pmc_FilterCapacitor_40p;
	config.pll.pllbFilterResistor = Pmc_FilterResistor_24K;
	config.pll.pllbCurrent = Pmc_PllCurrent_1250u;
#else
#error "No target platform specified (missing N7S_TARGET_* macro)"
#endif
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pmc_setConfig(&TestUtils::pmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode);
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pmc_getConfig(&TestUtils::pmc, &readConfig);

	const bool result2 = Pmc_setConfig(&TestUtils::pmc, &originalConfig,
			PMC_DEFAULT_TIMEOUT, &errCode);
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);

	// Pmc_setConfig always returns Pllb not locked error on RH71 due to hardware flaw
	// (LOCKB bit not set in PMC SR register)

	MEMCMP_EQUAL(&config, &readConfig, sizeof(Pmc_Config));
}

/// @}

/// @name Pmc_enablePeripheralClk()
/// @{

/// \Given Pmc driver,
///
/// \When Pmc_enablePeripheralClk() is called,
///
/// \Then the corresponding peripheral Clocks are enabled.
///
TEST(PmcTests, Pmc_enablePeripheralClk_enablesPeripheralClocks)
{
	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc1Ch1);
	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Xdmac);

	CHECK_TRUE(Pmc_isPeripheralClkEnabled(
			&TestUtils::pmc, Pmc_PeripheralId_Tc1Ch1));
	CHECK_TRUE(Pmc_isPeripheralClkEnabled(
			&TestUtils::pmc, Pmc_PeripheralId_Xdmac));
}

/// @}

/// @name Pmc_disablePeripheralClk()
/// @{

/// \Given Pmc driver,
///
/// \When Pmc_disablePeripheralClk() is called,
///
/// \Then the corresponding peripheral clocks are disabled.
///
TEST(PmcTests, Pmc_disablePeripheralClk_disablesPeripheralClocks)
{
	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc1Ch1);
	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Xdmac);
	Pmc_disablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc1Ch1);
	Pmc_disablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Xdmac);

	CHECK_FALSE(Pmc_isPeripheralClkEnabled(
			&TestUtils::pmc, Pmc_PeripheralId_Tc1Ch1));
	CHECK_FALSE(Pmc_isPeripheralClkEnabled(
			&TestUtils::pmc, Pmc_PeripheralId_Xdmac));
}

/// @}

/// @name Pmc_measureMainck()
/// @{

/// \Given Pmc with source clock set to RC oscillator,
///
/// \When Pmc_measureMainck() is called and source clock oscillator frequency is measured,
///
/// \Then the measured value is within the expected range.
///
TEST(PmcTests, Pmc_measureMainck_measuresFrequency)
{
	Pmc_Config pmcConfig = Pmc_ChipConfig_getConfig();
	pmcConfig.mainck.src = Pmc_MainckSrc_RcOsc;

#if defined(N7S_TARGET_SAMV71Q21)
	const uint32_t RC_OSC_FREQUENCY = (12 * 1000 * 1000);
	pmcConfig.mainck.rcOscFreq = Pmc_RcOscFreq_12M;
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	const uint32_t RC_OSC_FREQUENCY = (10 * 1000 * 1000);
	pmcConfig.mainck.rcOscFreq = Pmc_RcOscFreq_10M;
#else
#error "No target platform specified (missing N7S_TARGET_* macro)"
#endif

	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pmc_setConfig(&TestUtils::pmc, &pmcConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));

	// Frequency measurement has accuracy +/- 10% (or +/- 40% if Main RC is @ 4MHz).
	const double measurementAccuracy = 0.15;

	Pmc_MainckMeasurement measurement = {
		.measuredClk = Pmc_MeasuredClk_RcOsc,
		.refFreq = PMC_SLOW_CLOCK_FREQ,
		.measuredFreq = 0,
	};

	CHECK_TRUE(Pmc_measureMainck(&TestUtils::pmc, &measurement,
			PMC_DEFAULT_TIMEOUT, &errCode));

	const auto freqMax = static_cast<uint32_t>(
			RC_OSC_FREQUENCY * (1.0 + measurementAccuracy));
	const auto freqMin = static_cast<uint32_t>(
			RC_OSC_FREQUENCY * (1.0 - measurementAccuracy));

	CHECK_COMPARE(measurement.measuredFreq, >=, freqMin);
	CHECK_COMPARE(measurement.measuredFreq, <=, freqMax);
}

/// @}

/// @name Pmc_setConfig()
/// @{

/// \Given Pmc driver and initialized Tic driver with Slow Clock as source clock,
///
/// \When Processor frequency is changed and processor executes
///       known number of instructions,
///
/// \Then Tic counter shall count to higher value in case of slower core clock.
///
TEST(PmcTests, Pmc_processorFrequencyChanges)
{
	Pmc_Config config;

	Pmc_getConfig(&TestUtils::pmc, &config);

	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc0Ch0);
	const Tic_Channel channel = Tic_Channel_0;
	const uint32_t testDuration = 100000u;

	Tic tic;
	Tic_ChannelConfig ticConfig{};
	ticConfig.isEnabled = true;
	ticConfig.clockSource = Tic_ClockSelection_Slck;

	Tic_init(&tic, Tic_Id_0);
	Tic_setChannelConfig(&tic, channel, &ticConfig);
	Tic_enableChannel(&tic, channel);

	Tic_triggerChannel(&tic, channel);
	busyWaitLoop(testDuration);

	Tic_disableChannel(&tic, channel);
	const uint32_t counterValueBaseFreq =
			Tic_getCounterValue(&tic, channel);

	// slowest possible frequency
	config.masterck.presc = Pmc_MasterckPresc_64;
	config.masterck.divider = Pmc_MasterckDiv_2;
	Pmc_disablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc0Ch0);
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pmc_setConfig(&TestUtils::pmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode);
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc0Ch0);

	Tic_setChannelConfig(&tic, channel, &ticConfig);
	Tic_enableChannel(&tic, channel);

	Tic_triggerChannel(&tic, channel);
	busyWaitLoop(testDuration);

	Tic_disableChannel(&tic, channel);
	const uint32_t counterValueSlowFreq =
			Tic_getCounterValue(&tic, channel);
	CHECK_COMPARE(counterValueBaseFreq, <, counterValueSlowFreq);
}

/// @}

/// @name Pmc_setPllConfig()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setPllConfig() is called with config with zeroed divider or multiplier,
///
/// \Then Affected PLL is disabled, all configuration fields related to that PLL are zeroed.
///
TEST(PmcTests, Pmc_zeroMultiplierOrDividerDisablesPll)
{
	Pmc_PllConfig pllConfig = TestUtils::defaultPmcConfig.pll;
	pllConfig.pllaMul = 0;

	// mock PLL lock flags
#if defined(N7S_TARGET_SAMV71Q21)
	TestUtils::mockPmcRegsAligned.regs.sr = BIT_VALUE(PMC_SR_LOCKA, true);
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	TestUtils::mockPmcRegsAligned.regs.sr = BIT_VALUE(PMC_SR_LOCKA, true)
			| BIT_VALUE(PMC_SR_LOCKB, true);
#endif

	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pmc_setPllConfig(&TestUtils::mockPmc, &pllConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// verify that pllar register has only ONE bit set (bit 29)
	UNSIGNED_LONGS_EQUAL(UINT32_C(1) << 29u,
			TestUtils::mockPmcRegsAligned.regs.ckgrPllar);

	// for pllaDiv the same should be true
	pllConfig = TestUtils::defaultPmcConfig.pll;
	pllConfig.pllaDiv = 0;

	CHECK_TRUE(Pmc_setPllConfig(&TestUtils::mockPmc, &pllConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(UINT32_C(1) << 29u,
			TestUtils::mockPmcRegsAligned.regs.ckgrPllar);

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	pllConfig = TestUtils::defaultPmcConfig.pll;
	pllConfig.pllbMul = 0;
	CHECK_TRUE(Pmc_setPllConfig(&TestUtils::mockPmc, &pllConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// verify that pllbr register is zeroed
	UNSIGNED_LONGS_EQUAL(0u, TestUtils::mockPmcRegsAligned.regs.ckgrPllbr);

	// for pllbDiv the same should be true
	pllConfig = TestUtils::defaultPmcConfig.pll;
	pllConfig.pllbDiv = 0;

	CHECK_TRUE(Pmc_setPllConfig(&TestUtils::mockPmc, &pllConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(0u, TestUtils::mockPmcRegsAligned.regs.ckgrPllbr);
#endif
}

/// @}

/// @name Pmc_setMainckConfig()
/// @{

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
/// \Given Pmc initialized with mock registers,
///
/// \When Bypassed external crystal is selected as main clock source,
///
/// \Then Fields in CKGR_MOR regsiter is set to correct value.
///
TEST(PmcTests, Pmc_setMainckConfig_canSetupMainClockBypass)
{
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;

	mainckConfig.src = Pmc_MainckSrc_XOscBypassed;
	mainckConfig.rcOscFreq = Pmc_RcOscFreq_8M;
	mainckConfig.xoscStartupTime = 72;

	// mock ready flags in registers
	TestUtils::mockPmcRegsAligned.regs.sr = BIT_VALUE(PMC_SR_MOSCRCS, true)
			| BIT_VALUE(PMC_SR_MOSCSELS, true)
			| BIT_VALUE(PMC_SR_MCKRDY, true);

	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// verify that RC (MOSCRCEN - bit 3) and crystal (MOSCXTEN - bit 0) oscillators are disabled
	// and bypass bit (MOSCXTBY - bit 1) is set, also xtal (MOSCSEL - bit 24) is selected
	// PMC CKGR_MOR register also needs key (bits 16:23) set to 0x37
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 1u) | (UINT32_C(1) << 24u)
					| (UINT32_C(0x37) << 16u),
			TestUtils::mockPmcRegsAligned.regs.ckgrMor);
}
#endif

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with xtal as clock source,
///       all ready flags are set and frequency measurement passes
///
/// \Then Master clock is switched to external crystal, RC is disabled and no error is returned.
///
TEST(PmcTests, Pmc_setMainckConfig_Xtal)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOsc;
	mainckConfig.xoscStartupTime = 72;

	// mock RC enabled
	TestUtils::mockPmcRegsAligned.regs.ckgrMor |= CKGR_MOR_MOSCRCEN_MASK;

	// mock all ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MOSCXTS_MASK
			| PMC_SR_MCKRDY_MASK;
	const uint32_t freq =
			(PMC_MAIN_CRYSTAL_FREQ << 4u) / PMC_SLOW_CLOCK_FREQ;
	TestUtils::setupDelayedWrite(
			&(TestUtils::mockPmcRegsAligned.regs.ckgrMcfr),
			CKGR_MCFR_MAINFRDY_MASK
					| BIT_FIELD_VALUE(
							CKGR_MCFR_MAINF, freq),
			TestUtils::delayedWriteTicks);

	TestUtils::triggerDelayedWrite();

	// use longer timeout to make window for delayed register write wider
	CHECK_TRUE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT * 10u, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// verify that RC (MOSCRCEN - bit 3) is cleared, crystal (MOSCXTEN - bit 0) is set
	// and bypass bit (MOSCXTBY - bit 1) is clear, also xtal (MOSCSEL - bit 24) is selected
	// PMC CKGR_MOR register also needs key (bits 23:16) set to 0x37
	// XTAL oscillator startup time should also be present (MOSCXTST - bits 15:8)
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 0u) | (UINT32_C(1) << 24u)
					| (UINT32_C(0x37) << 16u)
					| (static_cast<uint32_t>(
							   mainckConfig.xoscStartupTime
							   / 8u)
							<< 8u),
			TestUtils::mockPmcRegsAligned.regs.ckgrMor);
}

/// @}

/// @name Pmc_getMainckConfig()
/// @{

/// \Given Pmc initialized with mock registers containing valid configuration,
///
/// \When Main clock configuration is read,
///
/// \Then Retrieved configuration matches values stored in registers.
///
TEST(PmcTests, Pmc_getMainckConfig_returnsConfigThatMatchesRegisters)
{
#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	const Pmc_MainckConfig mainckConfig1 = {
		.src = Pmc_MainckSrc_XOscBypassed,
		.rcOscFreq = Pmc_RcOscFreq_8M,
		.xoscStartupTime = 72,
	// additional flags are checked in separate test for RH71
#if defined(N7S_TARGET_SAMRH71F20)
		.detectClockFail = false,
		.xt32kMonitorEnabled = false,
		.badCpuResetEnabled = false,
		.badCpuNmicIrqEnabled = false,
#endif
	};
#endif

	const Pmc_MainckConfig mainckConfig2 = {
		.src = Pmc_MainckSrc_XOsc,
		.rcOscFreq = Pmc_RcOscFreq_4M,
		.xoscStartupTime = 64,
#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
		.detectClockFail = false,
		.xt32kMonitorEnabled = false,
		.badCpuResetEnabled = false,
		.badCpuNmicIrqEnabled = false,
#endif
	};

	const Pmc_MainckConfig mainckConfig3 = {
#if defined(N7S_TARGET_SAMV71Q21)
		.src = Pmc_MainckSrc_RcOsc,
		.rcOscFreq = Pmc_RcOscFreq_12M,
		.xoscStartupTime = 40,
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
		.src = Pmc_MainckSrc_RcOsc,
		.rcOscFreq = Pmc_RcOscFreq_10M,
		.xoscStartupTime = 40,
		.detectClockFail = false,
		.xt32kMonitorEnabled = false,
		.badCpuResetEnabled = false,
		.badCpuNmicIrqEnabled = false,
#endif
	};

	Pmc_MainckConfig readConfig{};

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	// crystal selected, bypass enabled (MOSCSEL - bit 24, and MOSCXTBY - bit 1 set)
	// frequency 8M (MOSCRCF=1, bits 4:6)
	// startup time 72 SCLK cycles (MOSCXTST=72/8, bits 8:15)
	TestUtils::mockPmcRegsAligned.regs.ckgrMor = (UINT32_C(1) << 24u)
			| (UINT32_C(1) << 1u) | (UINT32_C(0x01) << 4u)
			| ((UINT32_C(72) >> 3u) << 8u);
	Pmc_getMainckConfig(&TestUtils::mockPmc, &readConfig);
	MEMCMP_EQUAL(&mainckConfig1, &readConfig, sizeof(Pmc_MainckConfig));
#endif

	// crystal selected, (MOSCSEL - bit 24 set)
	// frequency 4M (MOSCRCF=0, bits 4:6)
	// startup time 64 SCLK cycles (MOSCXTST=64/8, bits 8:15)
	TestUtils::mockPmcRegsAligned.regs.ckgrMor =
			(UINT32_C(1) << 24u) | ((UINT32_C(64) >> 3u) << 8u);
	Pmc_getMainckConfig(&TestUtils::mockPmc, &readConfig);
	MEMCMP_EQUAL(&mainckConfig2, &readConfig, sizeof(Pmc_MainckConfig));

	// RC osc selected, all select bits clear
	// frequency 12M (MOSCRCF=2, bits 4:6)
	// startup time 40 SCLK cycles (MOSCXTST=40/8, bits 8:15)
	TestUtils::mockPmcRegsAligned.regs.ckgrMor =
			(UINT32_C(0x02) << 4u) | ((UINT32_C(40) >> 3u) << 8u);
	Pmc_getMainckConfig(&TestUtils::mockPmc, &readConfig);
	MEMCMP_EQUAL(&mainckConfig3, &readConfig, sizeof(Pmc_MainckConfig));
}

/// @}

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)

/// @name Pmc_setMainckConfig()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with valid configurations for failure detection,
///
/// \Then correct values are set in CKGR_MOR register.
///
TEST(PmcTests, Pmc_setMainckConfig_setsFailureDetectionBitsInCkgrMor)
{
	// mock flags in SR register
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK;

	const Pmc_MainckConfig mainckConfig1 = {
		.src = Pmc_MainckSrc_RcOsc,
		.rcOscFreq = Pmc_RcOscFreq_4M,
		.xoscStartupTime = 96,
		.detectClockFail = true,
		.xt32kMonitorEnabled = true,
		.badCpuResetEnabled = true,
		.badCpuNmicIrqEnabled = true,
	};

	const Pmc_MainckConfig mainckConfig2 = {
		.src = Pmc_MainckSrc_RcOsc,
		.rcOscFreq = Pmc_RcOscFreq_10M,
		.xoscStartupTime = 64,
		.detectClockFail = false,
		.xt32kMonitorEnabled = true,
		.badCpuResetEnabled = true,
		.badCpuNmicIrqEnabled = false,
	};

	const Pmc_MainckConfig mainckConfig3 = {
		.src = Pmc_MainckSrc_RcOsc,
		.rcOscFreq = Pmc_RcOscFreq_4M,
		.xoscStartupTime = 24,
		.detectClockFail = true,
		.xt32kMonitorEnabled = false,
		.badCpuResetEnabled = false,
		.badCpuNmicIrqEnabled = false,
	};

	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig1,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 3u)
					| (static_cast<uint32_t>(
							   Pmc_RcOscFreq_4M)
							<< 4u)
					| (UINT32_C(0x37) << 16u)
					| (UINT32_C(1) << 25u)
					| (UINT32_C(1) << 26u)
					| (UINT32_C(1) << 27u)
					| (UINT32_C(1) << 28u),
			TestUtils::mockPmcRegsAligned.regs.ckgrMor);

	CHECK_TRUE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig2,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 3u)
					| (static_cast<uint32_t>(
							   Pmc_RcOscFreq_10M)
							<< 4u)
					| (UINT32_C(0x37) << 16u)
					| (UINT32_C(1) << 26u)
					| (UINT32_C(1) << 27u),
			TestUtils::mockPmcRegsAligned.regs.ckgrMor);

	CHECK_TRUE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig3,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 3u)
					| (static_cast<uint32_t>(
							   Pmc_RcOscFreq_4M)
							<< 4u)
					| (UINT32_C(0x37) << 16u)
					| (UINT32_C(1) << 25u),
			TestUtils::mockPmcRegsAligned.regs.ckgrMor);
}

/// @}

/// @name Pmc_isGenericClockEnabled()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_isGenericClockEnabled() is called,
///
/// \Then returned value matches values stored in registers.
///
TEST(PmcTests, Pmc_isgenericClockEnabled_readsStateFromRegisters)
{
	// GCSR registers is zero, all peripheralIds should return false, check corner cases
	// (register boundary) and few values in the middle of each register
	const std::array<uint32_t, 8> testPeripheralIds1 = { 1u, 16u, 31u, 48u,
		64u, 95u, 100u, 127u };
	const std::array<uint32_t, 8> testPeripheralIds2 = { 0u, 8u, 23u, 32u,
		63u, 79u, 96u, 107u };

	for (const auto id : testPeripheralIds1)
		CHECK_FALSE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	for (const auto id : testPeripheralIds2)
		CHECK_FALSE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	// Enable all, fill 4 GCSR registers with binary ones
	TestUtils::mockPmcRegsAligned.regs.gcsr[0] = 0xffffffffu;
	TestUtils::mockPmcRegsAligned.regs.gcsr[1] = 0xffffffffu;
	TestUtils::mockPmcRegsAligned.regs.gcsr[2] = 0xffffffffu;
	TestUtils::mockPmcRegsAligned.regs.gcsr[3] = 0xffffffffu;

	for (const auto id : testPeripheralIds1)
		CHECK_TRUE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	for (const auto id : testPeripheralIds2)
		CHECK_TRUE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	// enable bits from group1 - corresponding clock should read as enabled, the rest is disabled
	TestUtils::mockPmcRegsAligned.regs.gcsr[0] = 0x80010002u;
	TestUtils::mockPmcRegsAligned.regs.gcsr[1] = 0x00010000u;
	TestUtils::mockPmcRegsAligned.regs.gcsr[2] = 0x80000001u;
	TestUtils::mockPmcRegsAligned.regs.gcsr[3] = 0x80000010u;

	for (const auto id : testPeripheralIds1)
		CHECK_TRUE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	for (const auto id : testPeripheralIds2)
		CHECK_FALSE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	// enable bits from group2
	TestUtils::mockPmcRegsAligned.regs.gcsr[0] = 0x00800101u;
	TestUtils::mockPmcRegsAligned.regs.gcsr[1] = 0x80000001u;
	TestUtils::mockPmcRegsAligned.regs.gcsr[2] = 0x00008000u;
	TestUtils::mockPmcRegsAligned.regs.gcsr[3] = 0x00000801u;

	for (const auto id : testPeripheralIds1)
		CHECK_FALSE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));

	for (const auto id : testPeripheralIds2)
		CHECK_TRUE(Pmc_isGenericClockEnabled(&TestUtils::mockPmc,
				static_cast<Pmc_PeripheralId>(id)));
}

/// @}

/// @name Pmc_enableGenericClock()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_enableGenericClock() is called,
///
/// \Then correct values are written to PCR register.
///
TEST(PmcTests, Pmc_enableGenericClock_setsRegistersCorrectly)
{
	TestUtils::mockPmcRegsAligned.regs.pcr = 0u;
	Pmc_enableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Tc0Ch0,
			Pmc_PckSrc_Mainck, 2u);

	// set bits GCLKEN, CMD. peripheral id ORed with clock selection << 8 and prescaler << 20
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 29u) | (UINT32_C(2) << 20u)
					| (UINT32_C(1) << 12u)
					| (static_cast<uint32_t>(
							   Pmc_PckSrc_Mainck)
							<< 8u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Tc0Ch0),
			TestUtils::mockPmcRegsAligned.regs.pcr);

	TestUtils::mockPmcRegsAligned.regs.pcr = 0xffffffffu;
	Pmc_enableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Xdmac,
			Pmc_PckSrc_Pllack, 46u);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 29u) | (UINT32_C(46) << 20u)
					| (UINT32_C(1) << 12u)
					| (static_cast<uint32_t>(
							   Pmc_PckSrc_Pllack)
							<< 8u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Xdmac),
			TestUtils::mockPmcRegsAligned.regs.pcr);

	TestUtils::mockPmcRegsAligned.regs.pcr = 0u;
	Pmc_enableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_PioA,
			Pmc_PckSrc_Masterck, 64u);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 29u) | (UINT32_C(64) << 20u)
					| (UINT32_C(1) << 12u)
					| (static_cast<uint32_t>(
							   Pmc_PckSrc_Masterck)
							<< 8u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_PioA),
			TestUtils::mockPmcRegsAligned.regs.pcr);

#if defined(N7S_TARGET_SAMRH71F20)
	TestUtils::mockPmcRegsAligned.regs.pcr = 0xffffffffu;
	Pmc_enableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Gmac,
			Pmc_PckSrc_Pllbck, 24u);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 29u) | (UINT32_C(24) << 20u)
					| (UINT32_C(1) << 12u)
					| (static_cast<uint32_t>(
							   Pmc_PckSrc_Pllbck)
							<< 8u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Gmac),
			TestUtils::mockPmcRegsAligned.regs.pcr);

	TestUtils::mockPmcRegsAligned.regs.pcr = 0u;
	Pmc_enableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Qspi,
			Pmc_PckSrc_Slck, 36u);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 29u) | (UINT32_C(36) << 20u)
					| (UINT32_C(1) << 12u)
					| (static_cast<uint32_t>(
							   Pmc_PckSrc_Slck)
							<< 8u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Qspi),
			TestUtils::mockPmcRegsAligned.regs.pcr);
#endif
}

/// @}

/// @name Pmc_disableGenericClock()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_disableGenericClock() is called,
///
/// \Then correct values are written to PCR register.
///
TEST(PmcTests, Pmc_disableGenericClock_setsRegistersCorrectly)
{
	TestUtils::mockPmcRegsAligned.regs.pcr = 0xffffffffu;
	Pmc_disableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Tc0Ch0);

	// set bits CMD. peripheral id ORed with clock selection << 8 and prescaler << 20
	// GCLKEN is cleared
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 12u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Tc0Ch0),
			TestUtils::mockPmcRegsAligned.regs.pcr);

	TestUtils::mockPmcRegsAligned.regs.pcr = 0u;
	Pmc_disableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Xdmac);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 12u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Xdmac),
			TestUtils::mockPmcRegsAligned.regs.pcr);

	TestUtils::mockPmcRegsAligned.regs.pcr = 0xffffffffu;
	Pmc_disableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_PioA);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 12u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_PioA),
			TestUtils::mockPmcRegsAligned.regs.pcr);

#if defined(N7S_TARGET_SAMRH71F20)
	TestUtils::mockPmcRegsAligned.regs.pcr = 0u;
	Pmc_disableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Gmac);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 12u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Gmac),
			TestUtils::mockPmcRegsAligned.regs.pcr);

	TestUtils::mockPmcRegsAligned.regs.pcr = 0xffffffffu;
	Pmc_disableGenericClk(&TestUtils::mockPmc, Pmc_PeripheralId_Qspi);
	UNSIGNED_LONGS_EQUAL((UINT32_C(1) << 12u)
					| static_cast<uint32_t>(
							Pmc_PeripheralId_Qspi),
			TestUtils::mockPmcRegsAligned.regs.pcr);
#endif
}

/// @}

/// @name Pmc_getPeripheralClockConfig()
/// @{

/// \Given initialized and configured Pmc driver ,
///
/// \When Pmc_getPeripheralClockConfig() is called,
///
/// \Then correct values are read from PCR register.
///
/// \Note This needs to be tested with working hardawre,
///       PCR is written and then immediately read back with values set by hardware.
///
TEST(PmcTests, Pmc_Pmc_getPeripheralClockConfig_readsPCRRegisterCorrectly)
{
	// Setup known configuration for several non-critical peripherals and try to read it back
	const Pmc_PeripheralClkConfig flexcom3ClockConfig = {
		.isPeripheralClkEnabled = false,
		.isGclkEnabled = true,
		.gclkSrc = Pmc_GclkSrc_Mainck,
		.gclkPresc = 11,
	};

	const Pmc_PeripheralClkConfig mcan0ClockConfig = {
		.isPeripheralClkEnabled = true,
		.isGclkEnabled = true,
		.gclkSrc = Pmc_GclkSrc_Pllack,
		.gclkPresc = 56,
	};

	const Pmc_PeripheralClkConfig tc2ClockConfig = {
		.isPeripheralClkEnabled = false,
		.isGclkEnabled = false,
		.gclkSrc = Pmc_GclkSrc_Slck,
		.gclkPresc = 42,
	};

	Pmc_setPeripheralClkConfig(&TestUtils::pmc, Pmc_PeripheralId_Flexcom3,
			&flexcom3ClockConfig);
	Pmc_setPeripheralClkConfig(&TestUtils::pmc, Pmc_PeripheralId_Mcan0,
			&mcan0ClockConfig);
	Pmc_setPeripheralClkConfig(&TestUtils::pmc, Pmc_PeripheralId_Tc2Ch0,
			&tc2ClockConfig);

	Pmc_PeripheralClkConfig readConfig{};

	Pmc_getPeripheralClkConfig(&TestUtils::pmc, Pmc_PeripheralId_Flexcom3,
			&readConfig);
	MEMCMP_EQUAL(&flexcom3ClockConfig, &readConfig,
			sizeof(Pmc_PeripheralClkConfig));

	Pmc_getPeripheralClkConfig(
			&TestUtils::pmc, Pmc_PeripheralId_Mcan0, &readConfig);
	MEMCMP_EQUAL(&mcan0ClockConfig, &readConfig,
			sizeof(Pmc_PeripheralClkConfig));

	Pmc_getPeripheralClkConfig(
			&TestUtils::pmc, Pmc_PeripheralId_Tc2Ch0, &readConfig);
	MEMCMP_EQUAL(&tc2ClockConfig, &readConfig,
			sizeof(Pmc_PeripheralClkConfig));
}

/// @}

/// @name Pmc_setPeripheralClkConfig()
/// @{

/// \Given Initialized Pmc and Tic drivers,
///
/// \When Pmc_setPeripheralClkConfig() is called with different configurations,
///
/// \Then Clock configuration for affected peripheral (TIC) changes accordingly.
///
TEST(PmcTests, Pmc_setPeripheralClkConfigChangesClockConfiguration)
{
	const Pmc_PeripheralClkConfig peripheral_clk_config1 = {
		.isPeripheralClkEnabled = true,
		.isGclkEnabled = true,
		.gclkSrc = Pmc_GclkSrc_Masterck,
		.gclkPresc = 11,
	};

	const Pmc_PeripheralClkConfig peripheral_clk_config2 = {
		.isPeripheralClkEnabled = true,
		.isGclkEnabled = true,
		.gclkSrc = Pmc_GclkSrc_Masterck,
		.gclkPresc = 47,
	};

	// Set up TIC channel a a means of comparing different clocks
	Pmc_enablePeripheralClk(&TestUtils::pmc, Pmc_PeripheralId_Tc0Ch0);
	const Tic_Channel channel = Tic_Channel_0;

	Tic tic;
	Tic_ChannelConfig ticConfig{};
	ticConfig.isEnabled = true;
	ticConfig.clockSource = Tic_ClockSelection_MckBy8;

	Tic_init(&tic, Tic_Id_0);
	Tic_setChannelConfig(&tic, channel, &ticConfig);
	Tic_enableChannel(&tic, channel);

	const uint32_t measurement_time = 10000u;

	// Establish baseline, MCK/8
	Tic_triggerChannel(&tic, channel);
	busyWaitLoop(measurement_time);
	const uint32_t baseline_mck8 = Tic_getCounterValue(&tic, channel);

	// Measured value should be positive
	CHECK_COMPARE(baseline_mck8, >, 0u);

	// Switch TIC to GCLK derived from MCK divided by 12
	ticConfig.clockSource = Tic_ClockSelection_Gclk;
	Pmc_setPeripheralClkConfig(&TestUtils::pmc, Pmc_PeripheralId_Tc0Ch0,
			&peripheral_clk_config1);
	Tic_setChannelConfig(&tic, channel, &ticConfig);

	Tic_triggerChannel(&tic, channel);
	busyWaitLoop(measurement_time);
	const uint32_t ticks_gclk_mck12 = Tic_getCounterValue(&tic, channel);
	CHECK_COMPARE(ticks_gclk_mck12, >, 0u);

	// Switch TIC to GCLK derived from MCK divided by 48
	ticConfig.clockSource = Tic_ClockSelection_Gclk;
	Pmc_setPeripheralClkConfig(&TestUtils::pmc, Pmc_PeripheralId_Tc0Ch0,
			&peripheral_clk_config2);
	Tic_setChannelConfig(&tic, channel, &ticConfig);

	Tic_triggerChannel(&tic, channel);
	busyWaitLoop(measurement_time);
	const uint32_t ticks_gclk_mck48 = Tic_getCounterValue(&tic, channel);
	CHECK_COMPARE(ticks_gclk_mck48, >, 0u);

	// kill timer and check if measured values look correct
	// they should satisfy following conditions:
	// baseline_mck8 > ticks_gclk_mck12 > ticks_gclk_mck48
	// baseline_mck8 * 8 =~ ticks_gclk_mck12 * 12 =~ ticks_gclk_mck48 * 48
	Tic_disableChannel(&tic, channel);
	CHECK_COMPARE(baseline_mck8, >, ticks_gclk_mck12);
	CHECK_COMPARE(ticks_gclk_mck12, >, ticks_gclk_mck48);

	const uint32_t baseline_95percent = (baseline_mck8 * 8u * 95u) / 100u;
	const uint32_t baseline_105percent = (baseline_mck8 * 8u * 105u) / 100u;

	CHECK_TRUE(isBetweenUint32(ticks_gclk_mck12 * 12u, baseline_95percent,
			baseline_105percent));
	CHECK_TRUE(isBetweenUint32(ticks_gclk_mck48 * 48u, baseline_95percent,
			baseline_105percent));
}

/// @}

/// @name Pmc_setCalibrationConfigurationForMainRc()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setCalibrationConfigurationForMainRc() is called with valid config,
///
/// \Then correct values are written to registers.
///
TEST(PmcTests, Pmc_setCalibrationConfigurationForMainRc_setsCalibrationRegisters)
{
	// The trim fields are relatively small, we can test all allowed values
	const std::array<uint32_t, 8> freqTrimTestPatterns = { 0x00, 0x01, 0x02,
		0x03, 0x04, 0x05, 0x06, 0x07 };
	const std::array<uint32_t, 4> tempTrimTestPatterns = { 0x00, 0x01, 0x02,
		0x03 };

	for (const auto freqTrim : freqTrimTestPatterns) {
		for (const auto tempTrim : tempTrimTestPatterns) {
			TestUtils::mockPmcRegsAligned.regs.ocr1 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForMainRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_4M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr1,
					0x80u | tempTrim | (freqTrim << 2u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);

			TestUtils::mockPmcRegsAligned.regs.ocr1 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForMainRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_8M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr1,
					0x8000u | (tempTrim << 8u)
							| (freqTrim << 10u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);

			TestUtils::mockPmcRegsAligned.regs.ocr1 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForMainRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_12M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr1,
					0x80000000u | (tempTrim << 16u)
							| (freqTrim << 18u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);

			TestUtils::mockPmcRegsAligned.regs.ocr1 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForMainRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_10M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr1,
					0x800000u | (tempTrim << 24u)
							| (freqTrim << 26u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);
		}
	}
}
/// @}

/// @name Pmc_setCalibrationConfigurationForSecondRc()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setCalibrationConfigurationForSecondRc() is called with valid config,
///
/// \Then correct values are written to registers.
///
TEST(PmcTests, Pmc_setCalibrationConfigurationForSecondRc_setsCalibrationRegisters)
{
	// The trim fields are relatively small, we can test all allowed values
	const std::array<uint32_t, 8> freqTrimTestPatterns = { 0x00, 0x01, 0x02,
		0x03, 0x04, 0x05, 0x06, 0x07 };
	const std::array<uint32_t, 4> tempTrimTestPatterns = { 0x00, 0x01, 0x02,
		0x03 };

	for (const auto freqTrim : freqTrimTestPatterns) {
		for (const auto tempTrim : tempTrimTestPatterns) {
			TestUtils::mockPmcRegsAligned.regs.ocr2 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForSecondRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_4M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr2,
					0x80u | tempTrim | (freqTrim << 2u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);

			TestUtils::mockPmcRegsAligned.regs.ocr2 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForSecondRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_8M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr2,
					0x8000u | (tempTrim << 8u)
							| (freqTrim << 10u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);

			TestUtils::mockPmcRegsAligned.regs.ocr2 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForSecondRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_12M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr2,
					0x800000u | (tempTrim << 16u)
							| (freqTrim << 18u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);

			TestUtils::mockPmcRegsAligned.regs.ocr2 = 0u;
			TestUtils::mockPmcRegsAligned.regs.osc2 = 0u;
			Pmc_setCalibrationConfigurationForSecondRc(
					&TestUtils::mockPmc, Pmc_RcOscFreq_10M,
					tempTrim, freqTrim);
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.ocr2,
					0x80000000u | (tempTrim << 24u)
							| (freqTrim << 26u));
			UNSIGNED_LONGS_EQUAL(
					TestUtils::mockPmcRegsAligned.regs.osc2,
					UINT32_C(0x37) << 16u);
		}
	}
}
/// @}

#endif
