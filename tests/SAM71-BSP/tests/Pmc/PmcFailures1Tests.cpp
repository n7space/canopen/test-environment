/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PmcFailures1Tests.cpp
/// \brief Pmc driver test suite implementation.

#include <array>
#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Pmc/PmcRegisters.h>
#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Systick/Systick.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsRuntimeSam7/MpuTraps.hpp>

#include "TestUtils.hpp"

TEST_GROUP(PmcTests)
{
	void setup() override
	{
#if defined(__XC32__) && defined(N7S_TARGET_SAMRH707F18)
		// FIXME: PMC failure tests built with XC32 for SAMRH707 are crashing in MPU traps.
		// This behavior is cache-related, disabling data cache allows the tests to pass.
		(void)Scb_disableDCache();
#endif
		TestUtils::setup();
		mpuTraps.registerInstance();
	}

	void teardown() override { TestUtils::teardown(); }

	MpuTraps mpuTraps;
};

using PmcTests = TEST_GROUP_CppUTestGroupPmcTests;

/// @name Pmc_setMainckConfig()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with valid configuration and hardware does not raise
///       any ready flags,
///
/// \Then timeout error is returned.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnTimeoutSimple)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;

	// check that result is an error each of the allowed clocks. First step of the config will fail
	// for Internal RC this will be enable RC
	mainckConfig.src = Pmc_MainckSrc_RcOsc;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_RCStabilizationTimeout, errCode);

	// for xtal, Stabilization will fail first
	mainckConfig.src = Pmc_MainckSrc_XOsc;
	errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_XtalStabilizationTimeout, errCode);

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	// for bypassed xtal, MCKRDY will fail first
	mainckConfig.src = Pmc_MainckSrc_XOscBypassed;
	errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
#endif
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with valid configuration and hardware raises only MCKRDY
///       flag,
///
/// \Then timeout error is returned for all clock sources.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnTimeoutOnlyMckrdy)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;

	// mock MCKRDY flag
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK;

	// check that result is an error for all clock sources.
	// for Internal RC this will be enable RC
	mainckConfig.src = Pmc_MainckSrc_RcOsc;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_RCStabilizationTimeout, errCode);

	// for xtal, Stabilization will fail first
	mainckConfig.src = Pmc_MainckSrc_XOsc;
	errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_XtalStabilizationTimeout, errCode);

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	// for bypassed xtal, main clock selection will fail first
	mainckConfig.src = Pmc_MainckSrc_XOscBypassed;
	errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MainClkSelectTimeout, errCode);
#endif
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with RC as clock source, MOSCRCS flag is raised,
///
/// \Then Main clock selection timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_RCStabilizationTimeout)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_RcOsc;

	// mock MOSCRCS
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK;

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MainClkSelectTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with xtal as clock source, MCKRDY flag
///       fails after disabling RC,
///
/// \Then Master clock ready timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_XtalTimeoutAfterRCDisable)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOsc;

	// mock all ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MOSCXTS_MASK
			| PMC_SR_MCKRDY_MASK;
	const uint32_t freq =
			(PMC_MAIN_CRYSTAL_FREQ << 4u) / PMC_SLOW_CLOCK_FREQ;
	TestUtils::setupDelayedWrite(
			&(TestUtils::mockPmcRegsAligned.regs.ckgrMcfr),
			CKGR_MCFR_MAINFRDY_MASK
					| BIT_FIELD_VALUE(
							CKGR_MCFR_MAINF, freq),
			TestUtils::delayedWriteTicks);

	TestUtils::triggerDelayedWrite();
	// setup trap on CKGR_MOR(w) -> SR(r) -> CKGR_MOR(w) -> SR(r) -> MCFR(w)
	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
		mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [this]() {
			mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
				mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned
								  .regs.sr,
						[this]() {
							mpuTraps.onWrite(
									&TestUtils::mockPmcRegsAligned
											.regs
											.ckgrMcfr,
									[]() {
										// drop MCKRDY flag
										// to mock correct frequency measurement
										TestUtils::mockPmcRegsAligned
												.regs
												.sr =
												TestUtils::mockPmcRegsAligned
														.regs
														.sr
												& ~PMC_SR_MCKRDY_MASK;
									});
						});
			});
		});
	});

	// use longer timeout to make window for delayed register write wider
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT * 10u, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
/// \Given Pmc initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with bypassed xtal as clock source,
///       MCKRDY flag fails after switching main clock
///
/// \Then Master clock ready timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_BypassedXtalTimeoutAfterMainckSwitch)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOscBypassed;

	// mock all ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MOSCXTS_MASK
			| PMC_SR_MCKRDY_MASK;

	// setup trap on SR(r) -> CKGR_MOR(w)
	mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [this]() {
		mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor,
				[]() {
					// drop MCKRDY flag
					TestUtils::mockPmcRegsAligned.regs.sr =
							TestUtils::mockPmcRegsAligned
									.regs.sr
							& ~PMC_SR_MCKRDY_MASK;
				});
	});

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with bypassed xtal as clock source,
///       MCKRDY flag fails after disabling RC oscillator,
///
/// \Then Master clock ready timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_BypassedXtalTimeoutAfterRCDisable)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOscBypassed;

	// mock all ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MOSCXTS_MASK
			| PMC_SR_MCKRDY_MASK;

	mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [this]() {
		mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
			mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [this]() {
				mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned
								  .regs.ckgrMor,
						[]() {
							// drop MCKRDY flag
							TestUtils::mockPmcRegsAligned
									.regs
									.sr =
									TestUtils::mockPmcRegsAligned
											.regs
											.sr
									& ~PMC_SR_MCKRDY_MASK;
						});
			});
		});
	});

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}
#endif

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with RC as clock source, MOSCRCS, MOSCSELS flags
/// are raised,
///
/// \Then Master clock ready timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_RCSelectionTimeout)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_RcOsc;

	// mock MOSCRCS and MOSCSELS flags
	TestUtils::mockPmcRegsAligned.regs.sr =
			PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK;

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with Xtal as clock source, MOSCRCS flag is raised,
///
/// \Then clock switch timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnTimeoutMckrdyXtalStable)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOsc;

	// mock MCKRDY and MOSCXTS flags
	TestUtils::mockPmcRegsAligned.regs.sr =
			PMC_SR_MCKRDY_MASK | PMC_SR_MOSCXTS_MASK;

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MainClkSelectTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with invalid Main clock source,
///
/// \Then registers are left unmodified and error is returned.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnInvalidClockSource)
{
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = arbitraryEnumValue<Pmc_MainckSrc>(0xDEAD);
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_InvalidClockSource, errCode);

	const Pmc_Registers zeroRegs{};
	MEMCMP_EQUAL(&zeroRegs, &TestUtils::mockPmcRegsAligned.regs,
			sizeof(Pmc_Registers));
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with Xtal as clock source, MOSCRCS flag is raised,
///
/// \Then Clock switch timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnTimeoutMckrdyXtalStableNoMckrdy)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOsc;

	// mock MOSCXTS flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCXTS_MASK;

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMainckConfig() is called with Xtal as clock source,
///       master clock ready is not raised after xtal stabilization,
///
/// \Then Clock switch timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnMckrdyFailAfterXtalStabilized)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOsc;

	// mock flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCXTS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MCKRDY_MASK;

	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
		mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [this]() {
			mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
				mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned
								  .regs.sr,
						[]() {
							TestUtils::mockPmcRegsAligned
									.regs
									.sr =
									PMC_SR_MOSCSELS_MASK;
						});
			});
		});
	});

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
/// \Given Pmc driver initialized with mock registers, internal RC is enabled and running
///
/// \When Pmc_setMainckConfig() is called with bypassed Xtal as clock source,
///       RC osc ready is not raised after clock was switched to Xtal and RC
///       reset to default frequency before disabling it,
///
/// \Then RC stabilization timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnMoscrcsNotRaisedIfRCIsEnabledXtalBypassed)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOscBypassed;

	// mock flags (all except MOSCRCS)
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCXTS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MCKRDY_MASK;
	TestUtils::mockPmcRegsAligned.regs.ckgrMor = CKGR_MOR_MOSCRCEN_MASK;

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_RCStabilizationTimeout, errCode);
}
#endif

/// \Given Pmc driver initialized with mock registers, internal RC is enabled and running
///
/// \When Pmc_setMainckConfig() is called with Xtal as clock source,
///       RC osc ready is not raised after clock was switched to Xtal and RC
///       reset to default frequency before disabling it,
///
/// \Then RC stabilization timeout error is raised.
///
TEST(PmcTests, Pmc_setMainckConfig_returnsErrorOnMoscrcsNotRaisedIfRCIsEnabledXtal)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc_MainckConfig mainckConfig = TestUtils::defaultPmcConfig.mainck;
	mainckConfig.src = Pmc_MainckSrc_XOsc;

	// mock flags (all except MOSCRCS)
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCXTS_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_MCKRDY_MASK;
	TestUtils::mockPmcRegsAligned.regs.ckgrMor = CKGR_MOR_MOSCRCEN_MASK;

	const uint32_t freq =
			(PMC_MAIN_CRYSTAL_FREQ << 4u) / PMC_SLOW_CLOCK_FREQ;
	TestUtils::setupDelayedWrite(
			&(TestUtils::mockPmcRegsAligned.regs.ckgrMcfr),
			CKGR_MCFR_MAINFRDY_MASK
					| BIT_FIELD_VALUE(
							CKGR_MCFR_MAINF, freq),
			TestUtils::delayedWriteTicks);

	TestUtils::triggerDelayedWrite();

	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &mainckConfig,
			PMC_DEFAULT_TIMEOUT * 10u, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_RCStabilizationTimeout, errCode);
}

/// @}

/// @name Pmc_setMainckConfig()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When External crystal is selected as main clock source and Pmc failed to measure Xtal
/// frequency,
///
/// \Then frequency measurement timeout error is reported.
///
/// \Note There is no automatic fallback to internal RC is this case, but it stays ON
///
TEST(PmcTests, Pmc_setMainckConfig_frequencyMeasurementTimeout)
{
	Pmc_MainckConfig config = TestUtils::defaultPmcConfig.mainck;
	config.src = Pmc_MainckSrc_XOsc;

	// mock ready flags in registers
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MCKRDY_MASK;

	// verify that function fails with Incorrect Frequency error
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT, &errCode));

	ENUMS_EQUAL_INT(Pmc_ErrorCode_FrequencyMeasurementTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers
///
/// \When external crystal is selected as main clock source and PMC measured incorrect frequency,
///
/// \Then Configuration falls back to internal RC and error is reported.
///
TEST(PmcTests, Pmc_setMainckConfig_frequencyMeasurementFailFallsBackToInternalRc)
{
	Pmc_MainckConfig config = TestUtils::defaultPmcConfig.mainck;
	config.src = Pmc_MainckSrc_XOsc;

	// mock ready flags in registers
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MCKRDY_MASK;

	const uint32_t wrongFreq = ((PMC_MAIN_CRYSTAL_FREQ / 3u) << 4u)
			/ PMC_SLOW_CLOCK_FREQ;
	TestUtils::setupDelayedWrite(
			&(TestUtils::mockPmcRegsAligned.regs.ckgrMcfr),
			CKGR_MCFR_MAINFRDY_MASK
					| BIT_FIELD_VALUE(CKGR_MCFR_MAINF,
							wrongFreq),
			TestUtils::delayedWriteTicks);

	TestUtils::triggerDelayedWrite();

	// verify that function fails with Incorrect Frequency error
	// use longer timeout to make window for delayed register write wider
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT * 10u, &errCode));

	ENUMS_EQUAL_INT(Pmc_ErrorCode_MeasuredFrequencyIsIncorrect, errCode);

	// verify that ckgrMor register has MOSCSEL bit disabled (bit 24), (MAINCK set to internal RC)
	CHECK_TRUE((TestUtils::mockPmcRegsAligned.regs.ckgrMor
				   & (UINT32_C(1) << 24u))
			== 0u);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When external crystal is selected as main clock source and Pmc measured incorrect frequency
///       and switch to internal RC fails,
///
/// \Then error is reported.
///
TEST(PmcTests, Pmc_setMainckConfig_frequencyMeasurementFailFallsBackToInternalRcFailure)
{
	Pmc_MainckConfig config = TestUtils::defaultPmcConfig.mainck;
	config.src = Pmc_MainckSrc_XOsc;

	// Mock ready flags in registers
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MCKRDY_MASK;

	const uint32_t wrongFreq = ((PMC_MAIN_CRYSTAL_FREQ / 3u) << 4u)
			/ PMC_SLOW_CLOCK_FREQ;
	TestUtils::setupDelayedWrite(
			&(TestUtils::mockPmcRegsAligned.regs.ckgrMcfr),
			CKGR_MCFR_MAINFRDY_MASK
					| BIT_FIELD_VALUE(CKGR_MCFR_MAINF,
							wrongFreq),
			TestUtils::delayedWriteTicks);

	TestUtils::triggerDelayedWrite();

	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
		mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [this]() {
			mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.ckgrMor, [this]() {
				mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned
								  .regs.sr,
						[this]() {
							mpuTraps.onWrite(
									&TestUtils::mockPmcRegsAligned
											.regs
											.ckgrMcfr,
									[]() {
										// DROP MOSCSELS flag
										TestUtils::mockPmcRegsAligned
												.regs
												.sr =
												TestUtils::mockPmcRegsAligned
														.regs
														.sr
												& ~PMC_SR_MOSCSELS_MASK;
									});
						});
			});
		});
	});

	// verify that function fails with Main clock select Timeout error
	// use longer timeout to make window for delayed register write wider
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_FALSE(Pmc_setMainckConfig(&TestUtils::mockPmc, &config,
			PMC_DEFAULT_TIMEOUT * 10u, &errCode));

	ENUMS_EQUAL_INT(Pmc_ErrorCode_MainClkSelectTimeout, errCode);

	// verify that ckgrMor register has MOSCSEL bit disabled (bit 24), (MAINCK set to internal RC)
	CHECK_TRUE((TestUtils::mockPmcRegsAligned.regs.ckgrMor
				   & (UINT32_C(1) << 24u))
			== 0u);
}

/// @}

#if defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)

/// @name Pmc_setCalibrationConfigurationForSecondRc()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setCalibrationConfigurationForSecondRc() is called with invalid frequency selection,
///
/// \Then registers are not modified.
///
TEST(PmcTests, Pmc_setCalibrationConfigurationForSecondRc_doesNotModifyRegistersOnInvalidFreq)
{
	// Test against zero and synthetic patterns
	const std::array<uint32_t, 4> patterns = { 0x00000000u, 0xAAAAAAAAu,
		0x55555555u, 0xFFFFFFFFu };

	for (const auto pattern : patterns) {
		TestUtils::mockPmcRegsAligned.regs.ocr2 = pattern;
		TestUtils::mockPmcRegsAligned.regs.osc2 = pattern;

		Pmc_setCalibrationConfigurationForSecondRc(&TestUtils::mockPmc,
				arbitraryEnumValue<Pmc_RcOscFreq>(0xDEADu),
				0x03, 0x03);

		UNSIGNED_LONGS_EQUAL(pattern,
				TestUtils::mockPmcRegsAligned.regs.osc2);
		UNSIGNED_LONGS_EQUAL(pattern,
				TestUtils::mockPmcRegsAligned.regs.ocr2);
	}
}

/// @}

/// @name Pmc_setCalibrationConfigurationForMainRc()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setCalibrationConfigurationForMainRc() is called with invalid frequency selection,
///
/// \Then registers are not modified.
///
TEST(PmcTests, Pmc_setCalibrationConfigurationForMainRc_doesNotModifyRegistersOnInvalidFreq)
{
	// test against zero and synthetic patterns
	const std::array<uint32_t, 4> patterns = { 0x00000000u, 0xAAAAAAAAu,
		0x55555555u, 0xFFFFFFFFu };

	for (const auto pattern : patterns) {
		TestUtils::mockPmcRegsAligned.regs.ocr1 = pattern;
		TestUtils::mockPmcRegsAligned.regs.osc2 = pattern;

		Pmc_setCalibrationConfigurationForMainRc(&TestUtils::mockPmc,
				arbitraryEnumValue<Pmc_RcOscFreq>(0xDEADu),
				0x03, 0x03);

		UNSIGNED_LONGS_EQUAL(pattern,
				TestUtils::mockPmcRegsAligned.regs.osc2);
		UNSIGNED_LONGS_EQUAL(pattern,
				TestUtils::mockPmcRegsAligned.regs.ocr1);
	}
}

/// @}

#endif
