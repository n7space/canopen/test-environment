/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024-2025 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PmcFailures3Tests.cpp
/// \brief Pmc driver test suite implementation.

#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Pmc/PmcRegisters.h>
#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Systick/Systick.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsRuntimeSam7/MpuTraps.hpp>

#include "TestUtils.hpp"

TEST_GROUP(PmcTests)
{
	void setup() override
	{
#if defined(__XC32__) && defined(N7S_TARGET_SAMRH707F18)
		// FIXME: PMC failure tests built with XC32 for SAMRH707 are crashing in MPU traps.
		// This behavior is cache-related, disabling data cache allows the tests to pass.
		(void)Scb_disableDCache();
#endif
		TestUtils::setup();
		mpuTraps.registerInstance();
	}

	void teardown() override { TestUtils::teardown(); }

	MpuTraps mpuTraps;
};

using PmcTests = TEST_GROUP_CppUTestGroupPmcTests;

/// @name Pmc_setMasterckConfig()
/// @{

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMasterckConfig() is called with various Master clock dividers,
///
/// \Then only division by 1 and 2 is supported, other dividers return error.
///
TEST(PmcTests, Pmc_setMasterckConfig_supportsOnly1and2masterckDivFactors)
{
	// mock flags in SR register (all set)
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCRCS_MASK | PMC_SR_MOSCSELS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_LOCKA_MASK;
	Pmc_MasterckConfig masterckConfig =
			TestUtils::defaultPmcConfig.masterck;

	ErrorCode errCode = ErrorCode_NoError;

	// try valid dividers first
	masterckConfig.divider = Pmc_MasterckDiv_1;
	CHECK_TRUE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	masterckConfig.divider = Pmc_MasterckDiv_2;
	CHECK_TRUE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// try invalid dividers
	masterckConfig.divider = arbitraryEnumValue<Pmc_MasterckDiv>(3u);
	CHECK_FALSE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_InvalidMasterckDiv, errCode);

	errCode = ErrorCode_NoError;
	masterckConfig.divider =
			arbitraryEnumValue<Pmc_MasterckDiv>(0xFFFFFFFFu);
	CHECK_FALSE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_InvalidMasterckDiv, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMasterckConfig() is called with valid configuration and hardware does not
///       raise any ready flags,
///
/// \Then timeout error is returned.
///
TEST(PmcTests, Pmc_setMasterckConfig_returnsErrorOnMasterClockTimeout)
{
	Pmc_MasterckConfig masterckConfig =
			TestUtils::defaultPmcConfig.masterck;

	// result should be the same for each of the allowed master clock sources
	for (const Pmc_MasterckSrc masterckSrc : { Pmc_MasterckSrc_Mainck,
			     Pmc_MasterckSrc_Pllack, Pmc_MasterckSrc_Slck }) {
		masterckConfig.src = masterckSrc;
		ErrorCode errCode = ErrorCode_NoError;
		CHECK_FALSE(Pmc_setMasterckConfig(&TestUtils::mockPmc,
				&masterckConfig, PMC_DEFAULT_TIMEOUT,
				&errCode));
		ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
	}
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMasterckConfig() is called with invalid clock source,
///
/// \Then invalid clock source error is returned.
///
TEST(PmcTests, Pmc_setMasterckConfig_returnsErrorOnInvalidClockSource)
{
	Pmc_MasterckConfig masterckConfig =
			TestUtils::defaultPmcConfig.masterck;
	masterckConfig.src = arbitraryEnumValue<Pmc_MasterckSrc>(0xDEAD);
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_FALSE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_InvalidClockSource, errCode);
}

namespace
{

void
setupMasterckConfigTimeout(MpuTraps &mpuTraps)
{
	// mock ready flags
	TestUtils::mockPmcRegsAligned.regs.sr = PMC_SR_MOSCRCS_MASK
			| PMC_SR_MOSCXTS_MASK | PMC_SR_MCKRDY_MASK
			| PMC_SR_MOSCSELS_MASK | PMC_SR_LOCKA_MASK;

	// setup trap on register
	// Expected order of operations is:
	// WRITE MCKR
	// READ SR
	// WRITE MCKR
	// READ SR
	// WRITE MCKR
	//  --- clear ready flags here
	mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.mckr, [&mpuTraps]() {
		mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned.regs.sr, [&mpuTraps]() {
			mpuTraps.onWrite(&TestUtils::mockPmcRegsAligned.regs.mckr, [&mpuTraps]() {
				mpuTraps.onAccess(&TestUtils::mockPmcRegsAligned
								  .regs.sr,
						[&mpuTraps]() {
							mpuTraps.onWrite(
									&TestUtils::mockPmcRegsAligned
											.regs
											.mckr,
									[]() {
										TestUtils::mockPmcRegsAligned
												.regs
												.sr =
												0u;
									});
						});
			});
		});
	});
}

} // namespace

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMasterckConfig() is called with valid configuration and hardware fails to set
///       ready flag after setting master clock source,
///
/// \Then Master clock ready timeout error is returned.
///
TEST(PmcTests, Pmc_setMasterckConfig_returnsErrorIfSettingMasterClockSourceFails)
{
	Pmc_MasterckConfig masterckConfig =
			TestUtils::defaultPmcConfig.masterck;
	masterckConfig.src = Pmc_MasterckSrc_Pllack;
	ErrorCode errCode = ErrorCode_NoError;

	setupMasterckConfigTimeout(mpuTraps);

	CHECK_FALSE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// \Given Pmc driver initialized with mock registers,
///
/// \When Pmc_setMasterckConfig() is called with valid configuration and hardware fails to set
///       ready flag after configuring clock divider,
///
/// \Then Master clock ready timeout error is returned.
///
TEST(PmcTests, Pmc_setMasterckConfig_returnsErrorIfSettingMasterClockDividerFails)
{
	Pmc_MasterckConfig masterckConfig =
			TestUtils::defaultPmcConfig.masterck;
	masterckConfig.src = Pmc_MasterckSrc_Mainck;
	ErrorCode errCode = ErrorCode_NoError;

	setupMasterckConfigTimeout(mpuTraps);

	CHECK_FALSE(Pmc_setMasterckConfig(&TestUtils::mockPmc, &masterckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Pmc_ErrorCode_MasterClkReadyTimeout, errCode);
}

/// @}
