/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  ScbTests.cpp
/// \brief Scb driver test suite implementation.

#include <array>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>

#include <n7s/bsp/Scb/Scb.h>
#include <n7s/bsp/Scb/ScbRegisters.h>

TEST_GROUP(ScbTests)
{
	void setup() override
	{
		// Make sure that cache is disabled
		(void)Scb_disableDCache();
		(void)Scb_disableICache();
	}

	static inline const volatile Scb_Registers *const scbRegs =
			reinterpret_cast<volatile Scb_Registers *>(
					SCB_BASE_ADDRESS);
};

/// @name Scb_setMemoryManagementExceptionEnabled()
/// @{

/// \Given Scb,
///
/// \When memory management exception is requested to be enabled and disabled,
///
/// \Then the memory management exception is being enabled and disabled.
///       (MEMFAULTENA bit in SHCSR register is set or cleared)
///
TEST(ScbTests, Scb_enablesAndDisablesMemoryManagementException)
{
	Scb_setMemoryManagementExceptionEnabled(true);
	CHECK_TRUE(Scb_isMemoryManagementExceptionEnabled());
	CHECK_TRUE(IS_BIT_SET(SCB_SHCSR_MEMFAULTENA, scbRegs->shcsr));

	Scb_setMemoryManagementExceptionEnabled(false);
	CHECK_FALSE(Scb_isMemoryManagementExceptionEnabled());
	CHECK_FALSE(IS_BIT_SET(SCB_SHCSR_MEMFAULTENA, scbRegs->shcsr));
}

/// @}

/// @name Scb_enableDCache()
/// @{

/// \Given Scb,
///
/// \When data cache is requested to be enabled and disabled,
///
/// \Then the data cache is being enabled and disabled.
///       (DC bit in SCB CCR register is set or cleared respectively)
///
TEST(ScbTests, Scb_enablesAndDisablesDataCache)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());
	CHECK_TRUE((scbRegs->ccr & SCB_CCR_DC_MASK) != 0u);
	CHECK_TRUE(IS_BIT_SET(SCB_CCR_DC, scbRegs->ccr));

	CHECK_TRUE(Scb_disableDCache());
	CHECK_FALSE(Scb_isDCacheEnabled());
	CHECK_FALSE(IS_BIT_SET(SCB_CCR_DC, scbRegs->ccr));
}

/// \Given Scb,
///
/// \When data cache is requested to be enabled twice,
///
/// \Then the data cache is being enabled only once during first call.
///
/// \Note Exact behavior of Scb_enableDCache() in this case is verified by analysis,
///       This test checks that enabling cache twice does not break execution
///       and leaves cache in enabled state.
///
TEST(ScbTests, Scb_tryEnableDataCacheTwice)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());
	CHECK_TRUE(IS_BIT_SET(SCB_CCR_DC, scbRegs->ccr));

	// Try enable data cache second time
	CHECK_FALSE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());
	CHECK_TRUE(IS_BIT_SET(SCB_CCR_DC, scbRegs->ccr));
}

/// @}

/// @name Scb_invalidateDCache()
/// @{

/// \Given Scb,
///
/// \When data cache is requested to invalidate data,
///
/// \Then the data is invalidated.
///
/// \Note Function Scb_invalidateDCache() allows to invalidate data cache but function's results
///       are not visible outside processor core. Due to that function Scb_invalidateDCache()
///       has been verified by analysis.
///
TEST(ScbTests, Scb_invalidateDCacheOperation)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_invalidateDCache());

	CHECK_TRUE(Scb_disableDCache());
	CHECK_FALSE(Scb_invalidateDCache());
}

/// @}

/// @name Scb_cleanDCache()
/// @{

/// \Given Scb,
///
/// \When Scb_cleanDCache() is called,
///
/// \Then the data cache is cleaned.
///
/// \Note Scb_cleanDCache() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_cleanDCache_CleansDataCache)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());

	CHECK_TRUE(Scb_cleanDCache());

	CHECK_TRUE(Scb_disableDCache());
	CHECK_FALSE(Scb_isDCacheEnabled());
}

/// \Given Scb,
///
/// \When data cache is requested to clean data but data cache have not been initialized,
///
/// \Then the data cache is not being cleaned before data cache initialization.
///
TEST(ScbTests, Scb_cleanDCache_TryCleansDataCacheBeforeDataCacheInitialization)
{
	// Try to clean data cache without initialized data cache
	CHECK_FALSE(Scb_cleanDCache());

	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());

	CHECK_TRUE(Scb_cleanDCache());

	CHECK_TRUE(Scb_disableDCache());
	CHECK_FALSE(Scb_isDCacheEnabled());
}

/// @}

/// @name Scb_cleanDCacheByAddr()
/// @{

/// \Given Scb,
///
/// \When Scb_cleanDCacheByAddr() is called with address and size of memory buffer,
///
/// \Then the data cache lines corresponding to given address are cleaned.
///
/// \Note Scb_cleanDCacheByAddr() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_cleanDCacheByAddr_CleansDataCache)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};
	CHECK_TRUE(Scb_cleanDCacheByAddr(buffer.data(), buffer.size()));
}

/// \Given Scb,
///
/// \When data cache is requested to clean data but data cache have not been initialized,
///
/// \Then the data cache is not being cleaned, false is returned.
///
/// \Note Scb_cleanDCacheByAddr() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_cleanDCacheByAddr_FailsWhenCacheIsDisabled)
{
	CHECK_FALSE(Scb_isDCacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, 32u>
			buffer{};

	// Try to clean data cache without initialized data cache
	CHECK_FALSE(Scb_cleanDCacheByAddr(buffer.data(), buffer.size()));
}

/// @}

/// @name Scb_cleanInvalidateDCacheByAddr()
/// @{

/// \Given Scb,
///
/// \When Scb_cleanInvalidateDCacheByAddr() is called with address and size of memory buffer,
///
/// \Then the data cache lines corresponding to given address are cleaned and invalidated.
///
/// \Note Scb_cleanInvalidateDCacheByAddr()
///       does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_cleanInvalidateDCacheByAddr_CleansDataCache)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};
	CHECK_TRUE(Scb_cleanInvalidateDCacheByAddr(
			buffer.data(), buffer.size()));

	CHECK_TRUE(Scb_disableDCache());
	CHECK_FALSE(Scb_isDCacheEnabled());
}

/// \Given Scb,
///
/// \When data cache is requested to clean data but data cache have not been initialized,
///
/// \Then the data cache is not being cleaned, false is returned.
///
/// \Note Scb_cleanInvalidateDCacheByAddr()
///       does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_cleanInvalidateDCacheByAddr_FailsWhenCacheIsDisabled)
{
	CHECK_FALSE(Scb_isDCacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};

	// Try to clean and invalidate data cache without initialized data cache
	CHECK_FALSE(Scb_cleanInvalidateDCacheByAddr(
			buffer.data(), buffer.size()));
}

/// @}

/// @name Scb_invalidateDCacheByAddr()
/// @{

/// \Given Scb,
///
/// \When Scb_invalidateDCacheByAddr() is called with address and size of memory buffer,
///
/// \Then the data cache lines corresponding to given address are invalidated.
///
/// \Note Scb_invalidateDCacheByAddr() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_invalidateDCacheByAddr_invalidatesDataCache)
{
	CHECK_TRUE(Scb_enableDCache());
	CHECK_TRUE(Scb_isDCacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};
	CHECK_TRUE(Scb_invalidateDCacheByAddr(buffer.data(), buffer.size()));
}

/// \Given Scb,
///
/// \When data cache is requested to invalidate data but data cache have not been initialized,
///
/// \Then the data cache is not being  invalidated, false is returned.
///
/// \Note Scb_invalidateDCacheByAddr() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_invalidateDCacheByAddr_FailsWhenCacheIsDisabled)
{
	CHECK_FALSE(Scb_isDCacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};

	// Try to invalidate data cache without initialized data cache
	CHECK_FALSE(Scb_invalidateDCacheByAddr(buffer.data(), buffer.size()));
}

/// @}

/// @name Scb_invalidateICacheByAddr()
/// @{

/// \Given Scb,
///
/// \When Scb_invalidateICacheByAddr() is called with address and size of memory buffer,
///
/// \Then the instruction cache lines corresponding to given address are invalidated.
///
/// \Note Scb_invalidateICacheByAddr() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, invalidateICacheByAddr_invalidatesDataCache)
{
	CHECK_TRUE(Scb_enableICache());
	CHECK_TRUE(Scb_isICacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};
	CHECK_TRUE(Scb_invalidateICacheByAddr(buffer.data(), buffer.size()));
}

/// \Given Scb,
///
/// \When data cache is requested to invalidate instruction cache
///       but instruction cache have not been initialized,
///
/// \Then the instruction cache is not being invalidated, false is returned.
///
/// \Note Scb_invalidateICacheByAddr() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, invalidateICacheByAddr_FailsWhenCacheIsDisabled)
{
	CHECK_FALSE(Scb_isICacheEnabled());

	__attribute__((aligned(SCB_CACHE_LINE_SIZE)))
	const std::array<uint8_t, SCB_CACHE_LINE_SIZE>
			buffer{};

	// Try to invalidate data cache without initialized data cache
	CHECK_FALSE(Scb_invalidateICacheByAddr(buffer.data(), buffer.size()));
}

/// @}

/// @name Scb_enableICache()
/// @{

/// \Given Scb,
///
/// \When instruction cache is requested to be enabled and disabled,
///
/// \Then the instruction cache is being enabled and disabled.
///       (IC bit in SCB CCR register is set or cleared respectively)
///
TEST(ScbTests, Scb_enablesAndDisablesInstructionCache)
{
	CHECK_TRUE(Scb_enableICache());
	CHECK_TRUE(Scb_isICacheEnabled());
	CHECK_TRUE(IS_BIT_SET(SCB_CCR_IC, scbRegs->ccr));

	CHECK_TRUE(Scb_disableICache());
	CHECK_FALSE(Scb_isICacheEnabled());
	CHECK_FALSE(IS_BIT_SET(SCB_CCR_IC, scbRegs->ccr));
}

/// @}

/// @name Scb_disableICache()
/// @{

/// \Given Scb and disabled instruction cache,
///
/// \When the instruction cache is disabled again,
///
/// \Then the cache disable procedure is not invoked.
///
/// \Note Scb_disableICache() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_doesNotInvalidateInstructionCacheWhenAlreadyDisabled)
{
	// Test starts with disabled cache
	CHECK_FALSE(Scb_disableICache());
	CHECK_FALSE(Scb_disableICache());
}

/// @}

/// @name Scb_disableDCache()
/// @{

/// \Given Scb and disabled data cache,
///
/// \When the data cache is disabled again,
///
/// \Then the cache disable procedure is not invoked.
///
/// \Note Scb_disableDCache() does not have effect observable outside of processor core.
///       Exact behavior was verified by analysis.
///
TEST(ScbTests, Scb_doesNotInvalidateDataCacheWhenAlreadyDisabled)
{
	// Test starts with disabled cache
	CHECK_FALSE(Scb_disableDCache());
	CHECK_FALSE(Scb_disableDCache());
}

/// @}
