/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  MpuTests.cpp
/// \brief Header containing interface for MPU (Memory Protection Unit) driver.

#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/bsp/Mpu/Mpu.h>
#include <n7s/bsp/Scb/Scb.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

TEST_GROUP(MpuTests)
{
	static inline volatile bool memoryExceptionHandlerCalled = false;

	void setup() override
	{
		// Make sure that cache is disabled
		(void)Scb_disableDCache();
		(void)Scb_disableICache();
	}
};

enum class AccessProtection : uint32_t {
	/// \brief Access is denied.
	Denied = 0x00u,
	/// \brief Read/Write access for priviledged code only.
	Priviledged = 0x01u,
	/// \brief Read/Write access for priviledged code, Read only access for unprivileged code.
	UnpriviledgedRo = 0x02u,
	/// \brief Read/Write access.
	Rw = 0x03u,
	/// \brief Read only access for priviledged code only.
	RoUnpriviledgedDenied = 0x05u,
	/// \brief Read only access.
	Ro = 0x06u,

};

enum class CacheAttribute : uint32_t {
	/// \brief Not shared, device.
	NotSharedDevice = 0x10u,
	/// \brief Not shared, outer and inner write-through.
	NotSharedWriteThrough = 0x02u,
	/// \brief Shared, outer and inner write-through.
	SharedWriteThrough = 0x6u,
	/// \brief Shared, outer and inner write-back, no write allocate.
	SharedWriteBackNoAllocate = 0x7u,
	/// \brief Shared, outer and inner non-cacheable.
	SharedNoCache = 0xcu,
	/// \brief Shared, outer and inner write-back, write and read allocate.
	SharedWriteBackAllocate = 0xfu,
	/// \brief Shared, outer write-back and inner write-through, write and read allocate.
	SharedInnerWriteThroughOutterWriteBack = 0x3E,
};

using MpuTests = TEST_GROUP_CppUTestGroupMpuTests;

#ifndef DOXYGEN
void
MemManage_Handler(void)
{
	MpuTests::memoryExceptionHandlerCalled = true;

	// Disable the MPU to allow for exception caused instruction to execute normally.
	Mpu mpu;
	Mpu_init(&mpu);
	const Mpu_Config mpuConf = {
		.isEnabled = false,
		.isDefaultMemoryMapEnabled = true,
		.isMpuEnabledInHandlers = false,
	};
	Mpu_setConfig(&mpu, &mpuConf);
}
#endif

/// @name Mpu_setConfig()
/// @{

/// \Given an Mpu device descriptor with dummy register,
///
/// \When the configured is set,
///
/// \Then the memory control register is correctly set.
///
TEST(MpuTests, Mpu_setConfiguration)
{
	Mpu mpu{};
	Mpu_Registers reg{};

	mpu.reg = &reg;

	Mpu_Config mpuConf{};
	Mpu_setConfig(&mpu, &mpuConf);
	UNSIGNED_LONGS_EQUAL(0x00000000, mpu.reg->ctrl);

	mpuConf.isEnabled = true;
	Mpu_setConfig(&mpu, &mpuConf);
	UNSIGNED_LONGS_EQUAL(MPU_CTRL_ENABLE_MASK, mpu.reg->ctrl);

	mpuConf.isDefaultMemoryMapEnabled = true;
	Mpu_setConfig(&mpu, &mpuConf);
	UNSIGNED_LONGS_EQUAL(
			MPU_CTRL_ENABLE_MASK | MPU_CTRL_PRIVDEFENA_ENABLE_MASK,
			mpu.reg->ctrl);

	mpuConf.isMpuEnabledInHandlers = true;
	Mpu_setConfig(&mpu, &mpuConf);
	UNSIGNED_LONGS_EQUAL(MPU_CTRL_ENABLE_MASK
					| MPU_CTRL_PRIVDEFENA_ENABLE_MASK
					| MPU_CTRL_HFNMIENA_ENABLE_MASK,
			mpu.reg->ctrl);

	mpuConf.isEnabled = false;
	Mpu_setConfig(&mpu, &mpuConf);
	UNSIGNED_LONGS_EQUAL(0x00000000, mpu.reg->ctrl);

	mpuConf.isEnabled = true;
	mpuConf.isDefaultMemoryMapEnabled = false;
	Mpu_setConfig(&mpu, &mpuConf);
	UNSIGNED_LONGS_EQUAL(
			MPU_CTRL_ENABLE_MASK | MPU_CTRL_HFNMIENA_ENABLE_MASK,
			mpu.reg->ctrl);
}

/// @}

/// @name Mpu_getConfig()
/// @{

/// \Given an Mpu device descriptor with dummy register,
///
/// \When the dummy ctrl register is set,
///
/// \Then the config is read correctly.
///
TEST(MpuTests, Mpu_getConfig_readsConfiguration)
{
	Mpu mpu{};
	Mpu_Registers reg{};

	mpu.reg = &reg;

	Mpu_Config mpuConf{};
	Mpu_getConfig(&mpu, &mpuConf);

	Mpu_getConfig(&mpu, &mpuConf);
	CHECK_FALSE(mpuConf.isEnabled);
	CHECK_FALSE(mpuConf.isDefaultMemoryMapEnabled);
	CHECK_FALSE(mpuConf.isMpuEnabledInHandlers);

	(void)memset(&mpuConf, 0, sizeof(Mpu_Config));
	reg.ctrl = MPU_CTRL_ENABLE_MASK;
	Mpu_getConfig(&mpu, &mpuConf);
	CHECK_TRUE(mpuConf.isEnabled);
	CHECK_FALSE(mpuConf.isDefaultMemoryMapEnabled);
	CHECK_FALSE(mpuConf.isMpuEnabledInHandlers);

	(void)memset(&mpuConf, 0, sizeof(Mpu_Config));
	reg.ctrl = MPU_CTRL_ENABLE_MASK | MPU_CTRL_PRIVDEFENA_ENABLE_MASK;
	Mpu_getConfig(&mpu, &mpuConf);
	CHECK_TRUE(mpuConf.isEnabled);
	CHECK_TRUE(mpuConf.isDefaultMemoryMapEnabled);
	CHECK_FALSE(mpuConf.isMpuEnabledInHandlers);

	(void)memset(&mpuConf, 0, sizeof(Mpu_Config));
	reg.ctrl = MPU_CTRL_ENABLE_MASK | MPU_CTRL_HFNMIENA_ENABLE_MASK;
	Mpu_getConfig(&mpu, &mpuConf);
	CHECK_TRUE(mpuConf.isEnabled);
	CHECK_FALSE(mpuConf.isDefaultMemoryMapEnabled);
	CHECK_TRUE(mpuConf.isMpuEnabledInHandlers);

	(void)memset(&mpuConf, 0, sizeof(Mpu_Config));
	reg.ctrl = MPU_CTRL_ENABLE_MASK | MPU_CTRL_PRIVDEFENA_ENABLE_MASK
			| MPU_CTRL_HFNMIENA_ENABLE_MASK;
	Mpu_getConfig(&mpu, &mpuConf);
	CHECK_TRUE(mpuConf.isEnabled);
	CHECK_TRUE(mpuConf.isDefaultMemoryMapEnabled);
	CHECK_TRUE(mpuConf.isMpuEnabledInHandlers);
}

/// @}

/// @name Mpu_setRegionConfig()
/// @{

/// \Given an initialized Mpu device descriptor,
///
/// \When the memory region in configured,
///
/// \Then the memory region configuration is correctly set.
///
TEST(MpuTests, Mpu_configuresMemoryRegion)
{
	Mpu mpu;
	Mpu_init(&mpu);

	const Mpu_Config mpuConf = {
		.isEnabled = true,
		.isDefaultMemoryMapEnabled = true,
		.isMpuEnabledInHandlers = false,
	};
	Mpu_setConfig(&mpu, &mpuConf);

	Mpu_RegionConfig mpuRegionConf = {
		.address = 0x80000000u,
		.isEnabled = true,
		.size = 11, // Tested region is 4096 bytes long: 2^(11 + 1)
		.subregionDisableMask = 0x00,
		.isShareable = true,
		.isExecutable = false,
		.memoryType = Mpu_RegionMemoryType_Device,
		.innerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.outerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.privilegedAccess = Mpu_RegionAccess_ReadWrite,
		.unprivilegedAccess = Mpu_RegionAccess_ReadWrite,
	};

	Mpu_setRegionConfig(&mpu, 3, &mpuRegionConf);
	// Set the region number to read.
	mpu.reg->rnr = 3;
	UNSIGNED_LONGS_EQUAL(0x00000005, mpu.reg->ctrl);
	UNSIGNED_LONGS_EQUAL(0x80000003, mpu.reg->rbar);
	UNSIGNED_LONGS_EQUAL(0x13050017, mpu.reg->rasr);

	mpuRegionConf.isEnabled = false;
	Mpu_setRegionConfig(&mpu, 3, &mpuRegionConf);
	// Set the region number to read.
	mpu.reg->rnr = 3;
	UNSIGNED_LONGS_EQUAL(0x00000005, mpu.reg->ctrl);
	UNSIGNED_LONGS_EQUAL(0x00000003, mpu.reg->rbar);
	UNSIGNED_LONGS_EQUAL(0x00000000, mpu.reg->rasr);

	mpuRegionConf.isEnabled = true;
	Mpu_setRegionConfig(&mpu, 3, &mpuRegionConf);
	// Set the region number to read.
	mpu.reg->rnr = 3;
	UNSIGNED_LONGS_EQUAL(0x00000005, mpu.reg->ctrl);
	UNSIGNED_LONGS_EQUAL(0x80000003, mpu.reg->rbar);
	UNSIGNED_LONGS_EQUAL(0x13050017, mpu.reg->rasr);

	mpuRegionConf.isExecutable = true;
	Mpu_setRegionConfig(&mpu, 3, &mpuRegionConf);
	// Set the region number to read.
	mpu.reg->rnr = 3;
	UNSIGNED_LONGS_EQUAL(0x00000005, mpu.reg->ctrl);
	UNSIGNED_LONGS_EQUAL(0x80000003, mpu.reg->rbar);
	UNSIGNED_LONGS_EQUAL(0x03050017, mpu.reg->rasr);
}

/// \Given an initialized Mpu device descriptor and memory region configured as read-only,
///
/// \When the memory read-only region is written,
///
/// \Then the MemoryManagement exception is called.
///
TEST(MpuTests, Mpu_causesMemoryManagementExceptionIfRegionIsReadOnly)
{
	Mpu mpu;
	Mpu_init(&mpu);

	const Mpu_Config mpuConf = {
		.isEnabled = true,
		.isDefaultMemoryMapEnabled = true,
		.isMpuEnabledInHandlers = false,
	};
	Mpu_setConfig(&mpu, &mpuConf);

#if defined(N7S_TARGET_SAMV71Q21)
	const uint32_t ramEnd = 0x20460000u;
#elif defined(N7S_TARGET_SAMRH71F20)
	const uint32_t ramEnd = 0x210C0000u;
#elif defined(N7S_TARGET_SAMRH707F18)
	const uint32_t ramEnd = 0x21020000u;
#else
#error "No target platform specified (missing N7S_TARGET_* macro)"
#endif

	const Mpu_RegionConfig mpuRegionConf = {
		.address = ramEnd - 0x200u, // The last 512B of SRAM
		.isEnabled = true,
		.size = 8, // Tested region is 512 bytes long: 2^(8 + 1)
		.subregionDisableMask = 0x00,
		.isShareable = false,
		.isExecutable = false,
		.memoryType = Mpu_RegionMemoryType_StronglyOrdered,
		.innerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.outerCachePolicy = Mpu_RegionCachePolicy_NonCacheable,
		.privilegedAccess = Mpu_RegionAccess_ReadOnly,
		.unprivilegedAccess = Mpu_RegionAccess_ReadOnly,
	};

	Mpu_setRegionConfig(&mpu, 3, &mpuRegionConf);

	Scb_setMemoryManagementExceptionEnabled(true);

	memoryExceptionHandlerCalled = false;

	volatile uint32_t *const readAddress = reinterpret_cast<uint32_t *>(
			mpuRegionConf.address); // NOLINT

	*readAddress = 15;
	CHECK_TRUE(memoryExceptionHandlerCalled);
}

/// @}

static uint32_t
generateAttributes(const AccessProtection ap, const CacheAttribute ca)
{
	constexpr uint32_t AP_OFFSET = 24;
	constexpr uint32_t CA_OFFSET = 16;

	const uint32_t a = static_cast<uint32_t>(ap) << AP_OFFSET;
	const uint32_t c = static_cast<uint32_t>(ca) << CA_OFFSET;

	return a | c;
}

/// @name Mpu_generateAttributes()
/// @{

/// \Given a set of memory region attributes,
///
/// \When Mpu_generateAttributes() is called for the given set of attributes,
///
/// \Then correct attribute mask is generated.
///
TEST(MpuTests, Mpu_generateAttributes_generatesCorrectMasks)
{
	// Bitmasks for Access Permissions and Attributes are taken directly from
	// ARMv7-M Architecture Reference Manual
	const uint32_t sharedNonCacheableRw = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_NonCacheable,
			Mpu_RegionCachePolicy_NonCacheable,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_ReadWrite);
	UNSIGNED_LONGS_EQUAL(generateAttributes(AccessProtection::Rw,
					     CacheAttribute::SharedNoCache),
			sharedNonCacheableRw);

	const uint32_t notSharedDeviceRw = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Device, false,
			Mpu_RegionCachePolicy_NonCacheable,
			Mpu_RegionCachePolicy_NonCacheable,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_ReadWrite);
	UNSIGNED_LONGS_EQUAL(generateAttributes(AccessProtection::Rw,
					     CacheAttribute::NotSharedDevice),
			notSharedDeviceRw);

	const uint32_t sharedWriteBackAllocateRo = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_WriteBackReadWriteAllocate,
			Mpu_RegionCachePolicy_WriteBackReadWriteAllocate,
			Mpu_RegionAccess_ReadOnly, Mpu_RegionAccess_ReadOnly);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(AccessProtection::Ro,
					CacheAttribute::SharedWriteBackAllocate),
			sharedWriteBackAllocateRo);

	const uint32_t sharedWriteBackNoAllocateDenied = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_WriteBackNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteBackNoWriteAllocate,
			Mpu_RegionAccess_NoAccess, Mpu_RegionAccess_NoAccess);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(AccessProtection::Denied,
					CacheAttribute::SharedWriteBackNoAllocate),
			sharedWriteBackNoAllocateDenied);

	const uint32_t sharedWriteThroughPriviledged = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_NoAccess);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(AccessProtection::Priviledged,
					CacheAttribute::SharedWriteThrough),
			sharedWriteThroughPriviledged);

	const uint32_t sharedWriteThroughRoUnpriviledgedDenied = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionAccess_ReadOnly, Mpu_RegionAccess_NoAccess);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(
					AccessProtection::RoUnpriviledgedDenied,
					CacheAttribute::SharedWriteThrough),
			sharedWriteThroughRoUnpriviledgedDenied);

	const uint32_t sharedWriteThroughUnpriviledgedRo = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_ReadOnly);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(AccessProtection::UnpriviledgedRo,
					CacheAttribute::SharedWriteThrough),
			sharedWriteThroughUnpriviledgedRo);

	const uint32_t notsharedWriteThroughUnpriviledgedRo = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, false,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_ReadOnly);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(AccessProtection::UnpriviledgedRo,
					CacheAttribute::NotSharedWriteThrough),
			notsharedWriteThroughUnpriviledgedRo);

	const uint32_t sharedInnerOuterDiffUnpriviledgedRo = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Normal, true,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteBackNoWriteAllocate,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_ReadOnly);
	UNSIGNED_LONGS_EQUAL(
			generateAttributes(AccessProtection::UnpriviledgedRo,
					CacheAttribute::SharedInnerWriteThroughOutterWriteBack),
			sharedInnerOuterDiffUnpriviledgedRo);
}

/// \Given a set of corrupted memory region attributes,
///
/// \When Mpu_generateAttributes() is called for the given set of corrupted attributes,
///
/// \Then corrupted parameters are ignored and default values are used instead.
///
TEST(MpuTests, Mpu_generateAttributes_generatesMasksCorruptedData)
{
	const auto corruptedCachePolicy =
			arbitraryEnumValue<Mpu_RegionCachePolicy>(0x09);

	const uint32_t wrongAnswerDueToCorruptedRegionCachePolicy =
			Mpu_generateAttributes(Mpu_RegionMemoryType_Normal,
					false, corruptedCachePolicy,
					corruptedCachePolicy,
					Mpu_RegionAccess_ReadWrite,
					Mpu_RegionAccess_ReadOnly);

	UNSIGNED_LONGS_EQUAL(
			0x2000000, wrongAnswerDueToCorruptedRegionCachePolicy);

#if defined(NDEBUG)
	static Mpu_RegionMemoryType Mpu_RegionMemoryType_Corrupted =
			Mpu_RegionMemoryType_Normal;

	// Inserting incorrect value
	(void)memset(&Mpu_RegionMemoryType_Corrupted, 0x09, sizeof(uint8_t));

	const uint32_t wrongAnswerDueToCorruptedRegionMemoryType = Mpu_generateAttributes(
			Mpu_RegionMemoryType_Corrupted, true,
			Mpu_RegionCachePolicy_WriteThroughNoWriteAllocate,
			Mpu_RegionCachePolicy_WriteBackNoWriteAllocate,
			Mpu_RegionAccess_ReadWrite, Mpu_RegionAccess_ReadOnly);

	UNSIGNED_LONGS_EQUAL(0, wrongAnswerDueToCorruptedRegionMemoryType);
#endif // NDEBUG
}

/// @}
