/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file   NvicTests.cpp
/// \brief  Implementation of unit tests for the Nvic driver.

#include <atomic>
#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Nvic/NvicRegisters.h>
#include <n7s/bsp/Nvic/NvicVectorTable.h>
#include <n7s/bsp/Scb/ScbRegisters.h>

#include <TestsStartup/TestsStartup.h>

TEST_GROUP(NvicTests)
{
	void setup() override
	{
		originalVectorTable = static_cast<Nvic_VectorTable *>(
				Nvic_getVectorTableAddress());
	}

	void teardown() override
	{
		Nvic_disableInterrupt(Nvic_Irq_Timer0_Channel1);
		Nvic_disableInterrupt(Nvic_Irq_Timer1_Channel1);
		Nvic_disableInterrupt(Nvic_Irq_Timer2_Channel1);
		Nvic_disableInterrupt(Nvic_Irq_Timer2_Channel2);
		Nvic_disableInterrupt(Nvic_Irq_Timer0_Channel0);
		Nvic_disableInterrupt(Nvic_Irq_Timer1_Channel0);
		Nvic_disableInterrupt(Nvic_Irq_Timer2_Channel0);

		Nvic_relocateVectorTable(originalVectorTable);
	}

	Nvic_VectorTable *originalVectorTable{};

	static inline Nvic_VectorTable vectorTable
			__attribute__((aligned(0x200)));
	static inline volatile Nvic_Registers *const nvic =
			reinterpret_cast<Nvic_Registers *>(NVIC_BASE_ADDRESS);
	static inline Scb_Registers *const scb =
			reinterpret_cast<Scb_Registers *>(SCB_BASE_ADDRESS);

	static inline std::atomic<bool> tc0ch0IrqActiveStatus = false;

	static inline std::atomic<bool> tc0ch0HandlerCalled = false;
	static inline std::atomic<bool> tc0ch1HandlerCalled = false;
	static inline std::atomic<bool> tc1ch1HandlerCalled = false;
	static inline std::atomic<bool> tc1ch1HandlerSkipped = false;
	static inline std::atomic<bool> tc2ch1HandlerCalled = false;

	static inline std::atomic<uint32_t> irqCounter = 0;

	static inline std::atomic<uint32_t> tc1ch0HandlerCounter = 0;
	static inline std::atomic<uint32_t> tc2ch0HandlerCounter = 0;

	static inline std::atomic<bool> customIrqHandlerCalled = false;
};

using NvicTests = TEST_GROUP_CppUTestGroupNvicTests;

constexpr static uint32_t NVIC_TEST_TIMEOUT = 100000u;

static void
customIrqHandler()
{
	NvicTests::irqCounter = NvicTests::irqCounter + 1;
	NvicTests::customIrqHandlerCalled = true;
}

#ifndef DOXYGEN
void
TC0CH0_Handler(void)
{
	NvicTests::tc0ch0IrqActiveStatus =
			Nvic_isInterruptActive(Nvic_Irq_Timer0_Channel0);

	NvicTests::tc0ch0HandlerCalled = true;
	busyWaitLoop(NVIC_TEST_TIMEOUT);
}

void
TC1CH0_Handler(void)
{
	NvicTests::irqCounter = NvicTests::irqCounter + 1;
	NvicTests::tc1ch0HandlerCounter = NvicTests::irqCounter.load();
}

void
TC2CH0_Handler(void)
{
	NvicTests::irqCounter = NvicTests::irqCounter + 1;
	NvicTests::tc2ch0HandlerCounter = NvicTests::irqCounter.load();
}

void
TC0CH1_Handler(void)
{
	NvicTests::tc0ch1HandlerCalled = true;
	busyWaitLoop(NVIC_TEST_TIMEOUT);

	if (Nvic_isInterruptPending(Nvic_Irq_Timer1_Channel1)) {
		Nvic_clearInterruptPending(Nvic_Irq_Timer1_Channel1);
		NvicTests::tc1ch1HandlerSkipped = true;
	}
	Nvic_setInterruptPending(Nvic_Irq_Timer2_Channel1);
}

void
TC1CH1_Handler(void)
{
	NvicTests::tc1ch1HandlerCalled = true;
}

void
TC2CH1_Handler(void)
{
	NvicTests::tc2ch1HandlerCalled = true;
}
#endif

/// @name Nvic_relocateVectorTable()
/// @{

/// \Given the scp,
///
/// \When relocating the Vector Table
///
/// \Then the getVectorTable returns correct address.
TEST(NvicTests, Nvic_relocateVectorTable_update)
{
	(void)memcpy(&vectorTable, Nvic_getVectorTableAddress(),
			sizeof(Nvic_VectorTable));
	CHECK_EQUAL(memcmp(Nvic_getVectorTableAddress(), &vectorTable,
				    sizeof(Nvic_VectorTable)),
			0);
	scb->vtor = 0;
	POINTERS_EQUAL(nullptr, Nvic_getVectorTableAddress());
	Nvic_relocateVectorTable(&vectorTable);
	POINTERS_EQUAL(&vectorTable, Nvic_getVectorTableAddress());
}

/// @}

/// @name Nvic_setInterruptHandlerAddress()
/// @{

/// \Given the Nvic driver,
///
/// \When the interrupt handler address for UART1 device is changed through the driver
///       and the interrupt is triggered,
///
/// \Then the custom handler will be called.
TEST(NvicTests, Nvic_allowsRegistrationOfCustomHandler)
{
	(void)memcpy(&vectorTable, Nvic_getVectorTableAddress(),
			sizeof(Nvic_VectorTable));
	CHECK_EQUAL(memcmp(Nvic_getVectorTableAddress(), &vectorTable,
				    sizeof(Nvic_VectorTable)),
			0);
	Nvic_relocateVectorTable(&vectorTable);

	const auto customIrq = Nvic_Irq_Timer0_Channel0;
	Nvic_setInterruptHandlerAddress(customIrq, customIrqHandler);
	FUNCTIONPOINTERS_EQUAL(customIrqHandler,
			Nvic_getInterruptHandlerAddress(customIrq));
	Nvic_enableInterrupt(customIrq);
	Nvic_setInterruptPriority(customIrq, 0);

	Nvic_disableIrq();
	customIrqHandlerCalled = false;
	Nvic_triggerInterrupt(customIrq);
	Nvic_enableIrq();

	CHECK_TRUE(customIrqHandlerCalled.load());

	CHECK_COMPARE(irqCounter, !=, 0);
}

/// @}

/// @name Nvic_enableInterrupt()
/// @{

/// \Given the Nvic driver,
///
/// \When an interrupt is enabled through an appropriate driver procedure,
///
/// \Then the bit representing the interrupt is unmasked in the Interrupt Set-enable Register.
///
TEST(NvicTests, Nvic_enableInterrupt_enablesAnInterrupt)
{
	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel2);
	CHECK_TRUE(Nvic_isInterruptEnabled(Nvic_Irq_Timer2_Channel2));
	CHECK_COMPARE((nvic->iser[Nvic_Irq_Timer2_Channel2 / UINT32_C(32)]
				      & (UINT32_C(1) << (Nvic_Irq_Timer2_Channel2
							 % UINT32_C(32)))),
			!=, 0u);
}

/// @}

/// @name Nvic_isInterruptActive()
/// @{

/// \Given the Nvic driver,
///
/// \When Nvic_isInterruptActive() is called outside and inside interrupt context
///
/// \Then Nvic_isInterruptActive() return true only during handler execution.
TEST(NvicTests, Nvic_isInterruptActive_returnsTrueInActiveIRQandFalseOtherwise)
{
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel0);

	tc0ch0HandlerCalled = false;
	tc0ch0IrqActiveStatus = false;
	irqCounter = 0;

	CHECK_FALSE(Nvic_isInterruptActive(Nvic_Irq_Timer0_Channel0));

	Nvic_triggerInterrupt(Nvic_Irq_Timer0_Channel0);

	while (!tc0ch0HandlerCalled)
		asm volatile("nop");

	CHECK_FALSE(Nvic_isInterruptActive(Nvic_Irq_Timer0_Channel0));
	CHECK_TRUE(tc0ch0IrqActiveStatus);
}

/// @}

/// @name Nvic_disableInterrupt()
/// @{

/// \Given the Nvic driver,
///
/// \When an interrupt is disabled through an appropriate driver procedure,
///
/// \Then the bit representing the interrupt is masked in the Interrupt Set-enable Register.
///
TEST(NvicTests, Nvic_disableInterrupt_disablesAnInterrupt)
{
	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel2);
	Nvic_disableInterrupt(Nvic_Irq_Timer2_Channel2);
	CHECK_FALSE(Nvic_isInterruptEnabled(Nvic_Irq_Timer2_Channel2));
	UNSIGNED_LONGS_EQUAL(0u,
			nvic->iser[Nvic_Irq_Timer2_Channel2 / UINT32_C(32)]
					& (UINT32_C(1) << (Nvic_Irq_Timer2_Channel2
							   % UINT32_C(32))));
}

/// @}

/// @name Nvic_setInterruptPriority()
/// @{

/// \Given the Nvic driver,
///
/// \When interrupt priorities are changed through the driver for a particular set of interrupts,
///
/// \Then those interrupts will be executed in the specified order.
///
TEST(NvicTests, Nvic_setInterruptPriority_modifiesInterruptPriority)
{
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel0);
	Nvic_enableInterrupt(Nvic_Irq_Timer1_Channel0);
	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel0);

	Nvic_setInterruptPriority(Nvic_Irq_Timer0_Channel0, 0);
	UNSIGNED_LONGS_EQUAL(0u,
			Nvic_getInterruptPriority(Nvic_Irq_Timer0_Channel0));

	tc0ch0HandlerCalled = false;
	irqCounter = 0;
	tc1ch0HandlerCounter = 0;
	tc2ch0HandlerCounter = 0;
	Nvic_setInterruptPriority(Nvic_Irq_Timer1_Channel0, 1);
	UNSIGNED_LONGS_EQUAL(1u,
			Nvic_getInterruptPriority(Nvic_Irq_Timer1_Channel0));
	Nvic_setInterruptPriority(Nvic_Irq_Timer2_Channel0, 2);
	UNSIGNED_LONGS_EQUAL(2u,
			Nvic_getInterruptPriority(Nvic_Irq_Timer2_Channel0));

	Nvic_disableIrq();
	Nvic_triggerInterrupt(Nvic_Irq_Timer0_Channel0);
	Nvic_triggerInterrupt(Nvic_Irq_Timer1_Channel0);
	Nvic_triggerInterrupt(Nvic_Irq_Timer2_Channel0);
	Nvic_enableIrq();

	while (!tc0ch0HandlerCalled)
		asm volatile("nop");

	UNSIGNED_LONGS_EQUAL(1u, tc1ch0HandlerCounter);
	UNSIGNED_LONGS_EQUAL(2u, tc2ch0HandlerCounter);

	tc0ch0HandlerCalled = false;
	irqCounter = 0;
	tc1ch0HandlerCounter = 0;
	tc2ch0HandlerCounter = 0;
	Nvic_setInterruptPriority(Nvic_Irq_Timer1_Channel0, 2);
	Nvic_setInterruptPriority(Nvic_Irq_Timer2_Channel0, 1);

	Nvic_disableIrq();
	Nvic_triggerInterrupt(Nvic_Irq_Timer0_Channel0);
	Nvic_triggerInterrupt(Nvic_Irq_Timer1_Channel0);
	Nvic_triggerInterrupt(Nvic_Irq_Timer2_Channel0);
	Nvic_enableIrq();

	while (!tc0ch0HandlerCalled)
		asm volatile("nop");

	UNSIGNED_LONGS_EQUAL(2u, tc1ch0HandlerCounter);
	UNSIGNED_LONGS_EQUAL(1u, tc2ch0HandlerCounter);
}

/// @}

/// @name Nvic_triggerInterrupt()
/// @{

/// \Given the Nvic driver,
///
/// \When a software trigger request is issued for an interrupt through a driver,
///
/// \Then that interrupt's handler will be called.
TEST(NvicTests, Nvic_triggerInterrupt_triggersAnInterrupt)
{
	tc0ch0HandlerCalled = false;
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel0);
	Nvic_triggerInterrupt(Nvic_Irq_Timer0_Channel0);
	CHECK_TRUE(tc0ch0HandlerCalled);
}

/// @}

/// @name Tic_setChannelConfig()
/// @{

/// \Given the Nvic driver,
///
/// \When the interrupt pending bits are manipulated through the driver,
///
/// \Then the handlers for those interrupts will be executed depending on the state of the bits.
///
TEST(NvicTests, Nvic_canManipulatePendingBitsForInterrupts)
{
	Nvic_setInterruptPriority(Nvic_Irq_Timer0_Channel1, 0);
	Nvic_setInterruptPriority(Nvic_Irq_Timer1_Channel1, 1);
	Nvic_setInterruptPriority(Nvic_Irq_Timer2_Channel1, 2);
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel1);
	Nvic_enableInterrupt(Nvic_Irq_Timer1_Channel1);
	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel1);

	Nvic_disableIrq();
	Nvic_triggerInterrupt(Nvic_Irq_Timer2_Channel1);
	Nvic_triggerInterrupt(Nvic_Irq_Timer1_Channel1);
	Nvic_triggerInterrupt(Nvic_Irq_Timer0_Channel1);
	Nvic_enableIrq();

	while (!tc0ch1HandlerCalled)
		asm volatile("nop");

	CHECK_TRUE(tc0ch1HandlerCalled);
	CHECK_FALSE(tc1ch1HandlerCalled);
	CHECK_TRUE(tc1ch1HandlerSkipped);
	CHECK_TRUE(tc2ch1HandlerCalled);
}

/// @}

/// @name Nvic_setPriorityGrouping()
/// @{

/// \Given the Nvic driver,
///
/// \When priority grouping is set and read back through the driver,
///
/// \Then the read back priority grouping value is correct.
///
TEST(NvicTests, Nvic_canManipulatePriorityGrouping)
{
	const uint8_t desiredPriorityGrouping = 0x7u;
	// Save the old grouping for restoration.
	const uint8_t oldPriorityGrouping = Nvic_getPriorityGrouping();
	Nvic_setPriorityGrouping(desiredPriorityGrouping);
	const uint8_t readPriorityGrouping = Nvic_getPriorityGrouping();
	// Restore the old grouping.
	Nvic_setPriorityGrouping(oldPriorityGrouping);
	UNSIGNED_LONGS_EQUAL(desiredPriorityGrouping, readPriorityGrouping);
}

/// @}

/// \Given Nvic registers structure,
///
/// \When register group address is queried,
///
/// \Then the returned address is correct.
TEST(NvicTests, Nvic_registersHaveCorrectOffsets)
{
	UNSIGNED_LONGS_EQUAL(
			0xe000e100u, reinterpret_cast<uint32_t>(&nvic->iser));
	UNSIGNED_LONGS_EQUAL(
			0xe000e180u, reinterpret_cast<uint32_t>(&nvic->icer));
	UNSIGNED_LONGS_EQUAL(
			0xe000e200u, reinterpret_cast<uint32_t>(&nvic->ispr));
	UNSIGNED_LONGS_EQUAL(
			0xe000e280u, reinterpret_cast<uint32_t>(&nvic->icpr));
	UNSIGNED_LONGS_EQUAL(
			0xe000e300u, reinterpret_cast<uint32_t>(&nvic->iabr));
	UNSIGNED_LONGS_EQUAL(
			0xe000e400u, reinterpret_cast<uint32_t>(&nvic->ipr));
	UNSIGNED_LONGS_EQUAL(
			0xe000ef00u, reinterpret_cast<uint32_t>(&nvic->stir));
}
