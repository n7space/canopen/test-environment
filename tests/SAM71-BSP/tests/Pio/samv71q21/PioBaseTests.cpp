/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PioBaseTests.cpp
/// \brief Pio driver test suite implementation.

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>

TEST_GROUP(PioTests){ public: void setup() override{ Pmc pmc;
Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioA);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioB);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioC);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioD);
}
}
;

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with Schmitt trigger enabled
///
/// \Then proper Schmitt trigger configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadSchmittTriggerConfiguration)
{
	const uint32_t pinMask = PIO_PIN_6;
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_PeripheralB;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	pinConf.isSchmittTriggerDisabled = true;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.isSchmittTriggerDisabled,
			readConf.isSchmittTriggerDisabled);

	pinConf.isSchmittTriggerDisabled = false;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.isSchmittTriggerDisabled,
			readConf.isSchmittTriggerDisabled);
}

/// \Given initialized Pio
///
/// \When multidrive pin configuration is set
///
/// \Then proper multidrive pin configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadMultiDriveConfiguration)
{
	const uint32_t pinMask = PIO_PIN_21 | PIO_PIN_22;
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	Pio_Pin_Config readConf;
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.filter = Pio_Filter_None;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	pinConf.isMultiDriveEnabled = true;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioD, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.isMultiDriveEnabled, readConf.isMultiDriveEnabled);

	pinConf.isMultiDriveEnabled = false;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioD, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.isMultiDriveEnabled, readConf.isMultiDriveEnabled);
}

/// \Given initialized Pio
///
/// \When pins drive strength is configured
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_getPinsConfig_canConfigureDriveStrength)
{
	const uint32_t pinMask = PIO_PIN_21 | PIO_PIN_22;
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	Pio_Pin_Config readConf;
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.filter = Pio_Filter_None;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.irq = Pio_Irq_None;

	// Rest of the register bits, not touched by configuration changes
	const uint32_t reg_driver = pioD.reg->driver & ~pinMask;

	pinConf.driveStrength = Pio_Drive_Low;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioD, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(0, pioD.reg->driver & pinMask);
	UNSIGNED_LONGS_EQUAL(reg_driver, pioD.reg->driver & ~pinMask);

	pinConf.driveStrength = Pio_Drive_High;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, PIO_PIN_21, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_21, pioD.reg->driver & pinMask);
	UNSIGNED_LONGS_EQUAL(reg_driver, pioD.reg->driver & ~pinMask);

	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioD, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(pinMask, pioD.reg->driver & pinMask);
	UNSIGNED_LONGS_EQUAL(reg_driver, pioD.reg->driver & ~pinMask);
}

/// \Given initialized Pio
///
/// \When pins are configured with interrupts enabled
///
/// \Then proper pin interrupt configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadInterruptConfiguration)
{
	const uint32_t pinMask = PIO_PIN_6;
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Input;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = true;
	pinConf.driveStrength = Pio_Drive_Low;

	pinConf.irq = Pio_Irq_None;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	pinConf.irq = Pio_Irq_EdgeFalling;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	pinConf.irq = Pio_Irq_EdgeRising;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	pinConf.irq = Pio_Irq_LevelLow;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	pinConf.irq = Pio_Irq_LevelHigh;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	pinConf.irq = Pio_Irq_EdgeBoth;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
}

/// \Given initialized Pio
///
/// \When pin pull configuration is set
///
/// \Then proper pin pull configuration can be read
///
TEST(PioTests, Pio_getPinsConfig_canReadPullConfiguration)
{
	const uint32_t pinMask = PIO_PIN_28;
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	Pio_Pin_Config readConf;
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	pinConf.pull = Pio_Pull_Up;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.pull, readConf.pull);

	pinConf.pull = Pio_Pull_Down;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.pull, readConf.pull);

	pinConf.pull = Pio_Pull_None;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.pull, readConf.pull);
}

/// \Given initialized Pio
///
/// \When pins are configured as synchronous outputs,
///
/// \Then the corresponding output configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadSynchronousOutputConfiguration)
{
	const uint32_t pinMask = PIO_PIN_13;
	Pio pioB;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_B, &pioB, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {
		.control = Pio_Control_PeripheralA,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.filter = Pio_Filter_Glitch,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	Pio_Pin_Config readConf = {};

	pinConf.direction = Pio_Direction_SynchronousOutput;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);

	pinConf.direction = Pio_Direction_Output;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);

	pinConf.direction = Pio_Direction_SynchronousOutput;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);
}

/// \Given initialized Pio
///
/// \When pins are configured with input glitch filter
///
/// \Then the corresponding filter configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadFilterConfiguration)
{
	const uint32_t pinMask = PIO_PIN_13;
	Pio pioB;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_B, &pioB, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	Pio_Pin_Config readConf;
	pinConf.control = Pio_Control_PeripheralA;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	pinConf.filter = Pio_Filter_None;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.filter, readConf.filter);

	pinConf.filter = Pio_Filter_Debounce;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.filter, readConf.filter);

	pinConf.filter = Pio_Filter_Glitch;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.filter, readConf.filter);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with Schmitt trigger enabled
///
/// \Then the corresponding registers are set to the correct values.
///
TEST(PioTests, Pio_setPinsConfig_canEnableSchmittTrigger)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_PeripheralB;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	pinConf.isSchmittTriggerDisabled = false;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(0, pioC.reg->schmitt & PIO_PIN_6);

	pinConf.isSchmittTriggerDisabled = true;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_6, pioC.reg->schmitt & PIO_PIN_6);
}

/// \Given initialized Pio
///
/// \When pins are configured in multidrive mode
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canConfigureMultiDrive)
{
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = true;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	CHECK_TRUE(Pio_setPinsConfig(
			&pioD, PIO_PIN_21 | PIO_PIN_22, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_21 | PIO_PIN_22,
			pioD.reg->mdsr & (PIO_PIN_21 | PIO_PIN_22));

	pinConf.isMultiDriveEnabled = false;

	CHECK_TRUE(Pio_setPinsConfig(&pioD, PIO_PIN_21, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_22, pioD.reg->mdsr & PIO_PIN_22);
}

/// \Given initialized Pio
///
/// \When pins are configured with interrupts enabled
///
/// \Then the corresponding registers are set to the correct values.
///
TEST(PioTests, Pio_setPinsConfig_canConfigureInterrupts)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Input;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;

	pinConf.irq = Pio_Irq_None;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(0u, (pioC.reg->imr & PIO_PIN_6));

	pinConf.irq = Pio_Irq_EdgeFalling;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_COMPARE((pioC.reg->imr & PIO_PIN_6), !=, 0u);
	UNSIGNED_LONGS_EQUAL(0u, (pioC.reg->frlhsr & PIO_PIN_6));
	UNSIGNED_LONGS_EQUAL(0u, (pioC.reg->elsr & PIO_PIN_6));
	CHECK_COMPARE((pioC.reg->aimmr & PIO_PIN_6), !=, 0u);

	pinConf.irq = Pio_Irq_EdgeRising;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_COMPARE((pioC.reg->imr & PIO_PIN_6), !=, 0u);
	CHECK_COMPARE((pioC.reg->frlhsr & PIO_PIN_6), !=, 0u);
	UNSIGNED_LONGS_EQUAL(0u, (pioC.reg->elsr & PIO_PIN_6));
	CHECK_COMPARE((pioC.reg->aimmr & PIO_PIN_6), !=, 0u);

	pinConf.irq = Pio_Irq_LevelLow;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_COMPARE((pioC.reg->imr & PIO_PIN_6), !=, 0u);
	UNSIGNED_LONGS_EQUAL(0u, (pioC.reg->frlhsr & PIO_PIN_6));
	CHECK_COMPARE((pioC.reg->elsr & PIO_PIN_6), !=, 0u);
	CHECK_COMPARE((pioC.reg->aimmr & PIO_PIN_6), !=, 0u);

	pinConf.irq = Pio_Irq_LevelHigh;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_COMPARE((pioC.reg->imr & PIO_PIN_6), !=, 0u);
	CHECK_COMPARE((pioC.reg->frlhsr & PIO_PIN_6), !=, 0u);
	CHECK_COMPARE((pioC.reg->elsr & PIO_PIN_6), !=, 0u);
	CHECK_COMPARE((pioC.reg->aimmr & PIO_PIN_6), !=, 0u);

	pinConf.irq = Pio_Irq_EdgeBoth;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_COMPARE((pioC.reg->imr & PIO_PIN_6), !=, 0u);
	UNSIGNED_LONGS_EQUAL(0u, (pioC.reg->aimmr & PIO_PIN_6));
}

/// \Given initialized Pio, with pins in output mode
///
/// \When pin pull configuration is set
///
/// \Then pin value changes accordingly.
///
TEST(PioTests, Pio_setPinsConfig_canForceResistorsIntoPullUpOrPullDownConfiguration)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const uint32_t PIO_CONFIGURATION_DELAY = 1000u;

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	pinConf.pull = Pio_Pull_Up;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_28, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	busyWaitLoop(PIO_CONFIGURATION_DELAY);

	UNSIGNED_LONGS_EQUAL(PIO_PIN_28, Pio_getPins(&pioC) & PIO_PIN_28);

	pinConf.pull = Pio_Pull_Down;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_28, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	busyWaitLoop(PIO_CONFIGURATION_DELAY);

	UNSIGNED_LONGS_EQUAL(0, Pio_getPins(&pioC) & PIO_PIN_28);
}

/// \Given initialized Pio
///
/// \When pins are configured as synchronous outputs
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canSetSynchronousOutputConfiguration)
{
	Pio pioD = {};
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_PeripheralA;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	const uint32_t pinMask = PIO_PIN_21 | PIO_PIN_22;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(pinMask, pioD.reg->osr & pinMask);
	UNSIGNED_LONGS_EQUAL(0, pioD.reg->owsr & pinMask);

	pinConf.direction = Pio_Direction_SynchronousOutput;

	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(pinMask, pioD.reg->osr & pinMask);
	UNSIGNED_LONGS_EQUAL(pinMask, pioD.reg->owsr & pinMask);
}

/// \Given initalized Pio
///
/// \When pins are configured with debouncing
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canEnableDebouncingFilter)
{
	Pio pioA;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_A, &pioA, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_PeripheralA;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_Debounce;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	CHECK_TRUE(Pio_setPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_3 | PIO_PIN_11, pioA.reg->ifsr);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_3 | PIO_PIN_11, pioA.reg->ifscsr);

	pinConf.filter = Pio_Filter_None;

	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_3, pioA.reg->ifsr);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_3, pioA.reg->ifscsr);
}

/// \Given initialized Pio
///
/// \When pins are configured with input glitch filter
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canConfigureInputGlitchFilter)
{
	Pio pioB;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_B, &pioB, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_PeripheralA;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_Glitch;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	CHECK_TRUE(Pio_setPinsConfig(
			&pioB, PIO_PIN_9 | PIO_PIN_13, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_9 | PIO_PIN_13, pioB.reg->ifsr);
	UNSIGNED_LONGS_EQUAL(0, pioB.reg->ifscsr);

	pinConf.filter = Pio_Filter_None;

	CHECK_TRUE(Pio_setPinsConfig(&pioB, PIO_PIN_9, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_13, pioB.reg->ifsr);
	UNSIGNED_LONGS_EQUAL(0, pioB.reg->ifscsr);
}

/// @}
