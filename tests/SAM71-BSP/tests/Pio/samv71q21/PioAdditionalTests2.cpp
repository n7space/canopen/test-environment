/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PioAdditionalTests.cpp
/// \brief Pio driver test suite implementation.

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>

#include <TestsRuntime/EnumUtils.hpp>

TEST_GROUP(PioTests){ public: void setup() override{ Pmc pmc;
Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioA);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioB);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioC);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioD);
}
}
;

/// @name Pio_Init()
/// @{

/// \Given Uninitialized Pio and invalid port ID
///
/// \When Pio_Init() function is called with Pio and port ID
///
/// \Then The function returns error and register addresses are left unmodified
///
TEST(PioTests, Pio_PioInit_returnsErrorsForInvalidInput)
{
	Pio pio = {};
	ErrorCode errCode = ErrorCode_NoError;

	const auto invalidPortId = arbitraryEnumValue<Pio_Port>(0xDEADu);

	CHECK_FALSE(Pio_init(invalidPortId, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_A, 0);
	POINTERS_EQUAL(nullptr, pio.reg);
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPortId, errCode);
}

/// \Given Uninitialized Pio and valid port ID
///
/// \When Pio_Init() function is called with Pio and port ID
///
/// \Then The function returns no error and register addresses are set to correct values
///
TEST(PioTests, Pio_PioInit_initializesRegistersAddresses)
{
	Pio pio = {};
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Pio_init(Pio_Port_A, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_A, pio.port);
	POINTERS_EQUAL(PIOA_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_B, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_B, pio.port);
	POINTERS_EQUAL(PIOB_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_C, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_C, pio.port);
	POINTERS_EQUAL(PIOC_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_D, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_D, pio.port);
	POINTERS_EQUAL(PIOD_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_E, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_E, pio.port);
	POINTERS_EQUAL(PIOE_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
}

/// @}

/// @name Pio_setPortValue()
/// @{

/// \Given Initialized Pio with pins configured as synchronous outputs
///
/// \When setPortValue() is called with a mask
///
/// \Then the bits corresponding to synchronous outputs in ODSR register are set.
///
TEST(PioTests, Pio_setPortValue_setsRegisterToValue)
{
	Pio pioD = {};
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_PeripheralA;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	// Set both pins as outpts drive them low
	const uint32_t pinMask = PIO_PIN_21 | PIO_PIN_22;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_resetPins(&pioD, pinMask);
	UNSIGNED_LONGS_EQUAL(0, pioD.reg->owsr & pinMask);
	UNSIGNED_LONGS_EQUAL(0, pioD.reg->odsr & pinMask);
	UNSIGNED_LONGS_EQUAL(pinMask, pioD.reg->osr & pinMask);

	// Switch one of the pins to synchronous output
	pinConf.direction = Pio_Direction_SynchronousOutput;
	CHECK_TRUE(Pio_setPinsConfig(&pioD, PIO_PIN_21, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	// Try to drive both pins high through Pio_setPortValue
	// Only previously unmasked bit should change value
	Pio_setPortValue(&pioD, pinMask);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_21, pioD.reg->odsr & pinMask);

	Pio_setPortValue(&pioD, 0);
	UNSIGNED_LONGS_EQUAL(0, pioD.reg->odsr & pinMask);

	// Switch both of then to synchronous mode
	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_setPortValue(&pioD, pinMask);
	UNSIGNED_LONGS_EQUAL(pinMask, pioD.reg->odsr & pinMask);

	Pio_setPortValue(&pioD, 0);
	UNSIGNED_LONGS_EQUAL(0, pioD.reg->odsr & pinMask);
}

/// @}

/// @name Pio_setPins()
/// @{

/// \Given initalized Pio, with pins in output mode
///
/// \When pins are to be set or reset
///
/// \Then the pins are set or reset.
///
TEST(PioTests, Pio_setsAndResetsPins)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const uint32_t PIO_DELAY_CYCLES = 50000u;

	Pio_Pin_Config pinConf;
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;
	pinConf.irq = Pio_Irq_None;

	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_9, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_setPins(&pioC, PIO_PIN_9);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_9, Pio_getPins(&pioC) & PIO_PIN_9);
	busyWaitLoop(PIO_DELAY_CYCLES);

	Pio_resetPins(&pioC, PIO_PIN_9);
	UNSIGNED_LONGS_EQUAL(0, Pio_getPins(&pioC) & PIO_PIN_9);
	busyWaitLoop(PIO_DELAY_CYCLES);

	Pio_setPins(&pioC, PIO_PIN_9);
	UNSIGNED_LONGS_EQUAL(PIO_PIN_9, Pio_getPins(&pioC) & PIO_PIN_9);
	busyWaitLoop(PIO_DELAY_CYCLES);

	Pio_resetPins(&pioC, PIO_PIN_9);
	UNSIGNED_LONGS_EQUAL(0, Pio_getPins(&pioC) & PIO_PIN_9);
	busyWaitLoop(PIO_DELAY_CYCLES);

	pinConf.direction = Pio_Direction_Input;
	pinConf.pull = Pio_Pull_None;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, PIO_PIN_9, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
}

/// @}
