/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  PioAdditionalTests.cpp
/// \brief Pio driver test suite implementation.

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>

#include <TestsRuntime/EnumUtils.hpp>

TEST_GROUP(PioTests){ public: void setup() override{ Pmc pmc;
Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioA);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioB);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioC);
Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_PioD);
}
}
;

/// @name Pio_getIrqStatus()
/// @{

/// \Given Pio initialized with dummy registers,
///
/// \When Pio_getIrqStatus() function is called,
///
/// \Then the function returns correct register value.
///
TEST(PioTests, Pio_PiogetIrqStatus_returnsCorrectRegister)
{
	Pio pio = {};
	Pio_Registers dummyRegs = {};
	pio.reg = &dummyRegs;

	const uint32_t pattern1 = 0x1234ABCDu;
	const uint32_t pattern2 = 0xFCEA6789u;

	pio.reg->isr = pattern1;
	UNSIGNED_LONGS_EQUAL(pattern1, Pio_getIrqStatus(&pio));

	pio.reg->isr = pattern2;
	UNSIGNED_LONGS_EQUAL(pattern2, Pio_getIrqStatus(&pio));
}

/// @}

/// @name Pio_getPortConfig()
/// @{

/// \Given initialized Pio and set configuration.
///
/// \When pin configuration getter is called
///
/// \Then proper pin configuration shall be returned
TEST(PioTests, Pio_getPortConfig_canReadPortConfiguration)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_C, &pioC, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Port_Config portConfig = {};
	portConfig.pins = PIO_PIN_9 | PIO_PIN_13;
	portConfig.debounceFilterDiv = 0;

	portConfig.pinsConfig.control = Pio_Control_Pio;
	portConfig.pinsConfig.direction = Pio_Direction_Output;
	portConfig.pinsConfig.pull = Pio_Pull_Down;
	portConfig.pinsConfig.filter = Pio_Filter_None;
	portConfig.pinsConfig.isMultiDriveEnabled = false;
	portConfig.pinsConfig.isSchmittTriggerDisabled = true;
	portConfig.pinsConfig.driveStrength = Pio_Drive_Low;
	portConfig.pinsConfig.irq = Pio_Irq_None;

	CHECK_TRUE(Pio_setPortConfig(&pioC, &portConfig, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Port_Config readConfig = {};
	readConfig.pins = portConfig.pins;
	CHECK_TRUE(Pio_getPortConfig(&pioC, &readConfig, &errCode));
	MEMCMP_EQUAL(&portConfig, &readConfig, sizeof(Pio_Port_Config));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given Pio initialized with dummy registers
///
/// \When Pio_getPinsConfig() is called with pinMask not matching irq config in registers
///
/// \Then the function return error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForMismatchedPinMaskAndIrqRegisters)
{
	const uint32_t pinMask = PIO_PIN_4 | PIO_PIN_2;
	const uint32_t pinMaskSubset = PIO_PIN_2;

	Pio pio;
	Pio_Registers dummyRegs = {};
	ErrorCode errCode = ErrorCode_NoError;
	pio.reg = &dummyRegs;

	// Set pull status register to valid config to avoid triggering Pull up/down config errors
	pio.reg->pusr = pinMask;
	pio.reg->ppdsr = pinMask;

	Pio_Pin_Config readConf = {};

	pio.reg->aimmr = 0;
	pio.reg->imr = pinMaskSubset;
	pio.reg->elsr = 0;
	pio.reg->frlhsr = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);

	pio.reg->aimmr = pinMaskSubset;
	pio.reg->imr = 0;
	pio.reg->elsr = 0;
	pio.reg->frlhsr = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);

	pio.reg->aimmr = pinMaskSubset;
	pio.reg->imr = pinMask;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);

	pio.reg->aimmr = pinMask;
	pio.reg->imr = pinMask;
	pio.reg->elsr = 0;
	pio.reg->frlhsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);

	pio.reg->aimmr = pinMask;
	pio.reg->imr = pinMask;
	pio.reg->elsr = pinMaskSubset;
	pio.reg->frlhsr = pinMask;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);

	pio.reg->aimmr = pinMask;
	pio.reg->imr = pinMask;
	pio.reg->elsr = pinMask;
	pio.reg->frlhsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);
}

/// \Given Pio initialized with dummy registers
///
/// \When Pio_getPinsConfig() is called with pinMask not matching Filter config in registers
///
/// \Then the function return error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForMismatchedPinMaskAndFilterRegisters)
{
	const uint32_t pinMask = PIO_PIN_4 | PIO_PIN_2;
	const uint32_t pinMaskSubset = PIO_PIN_2;

	Pio pio;
	Pio_Registers dummyRegs = {};
	ErrorCode errCode = ErrorCode_NoError;
	pio.reg = &dummyRegs;

	// Set pull status register to valid config to avoid triggering Pull up/down config errors
	pio.reg->pusr = pinMask;
	pio.reg->ppdsr = pinMask;

	Pio_Pin_Config readConf = {};

	pio.reg->ifsr = 0;
	pio.reg->ifscsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_FilterConfigMismatch, errCode);

	pio.reg->ifsr = pinMaskSubset;
	pio.reg->ifscsr = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_FilterConfigMismatch, errCode);

	pio.reg->ifsr = pinMask;
	pio.reg->ifscsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_FilterConfigMismatch, errCode);

	pio.reg->ifsr = pinMaskSubset;
	pio.reg->ifscsr = pinMask;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_FilterConfigMismatch, errCode);
}

/// \Given Pio initialized with dummy registers
///
/// \When Pio_getPinsConfig() is called with pinMask not matching Direction config in registers
///
/// \Then the function return error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForMismatchedPinMaskAndDirectionRegisters)
{
	const uint32_t pinMask = PIO_PIN_4 | PIO_PIN_2;
	const uint32_t pinMaskSubset = PIO_PIN_2;

	Pio pio;
	Pio_Registers dummyRegs = {};
	ErrorCode errCode = ErrorCode_NoError;
	pio.reg = &dummyRegs;

	// Set pull status register to valid config to avoid triggering Pull up/down config errors
	pio.reg->pusr = pinMask;
	pio.reg->ppdsr = pinMask;

	Pio_Pin_Config readConf = {};

	pio.reg->osr = 0;
	pio.reg->owsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_DirectionConfigMismatch, errCode);

	pio.reg->osr = pinMaskSubset;
	pio.reg->owsr = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_DirectionConfigMismatch, errCode);

	pio.reg->osr = pinMaskSubset;
	pio.reg->owsr = pinMask;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_DirectionConfigMismatch, errCode);

	pio.reg->osr = pinMask;
	pio.reg->owsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_DirectionConfigMismatch, errCode);
}

/// \Given Pio initialized with dummy registers
///
/// \When Pio_getPinsConfig() is called with pinMask not matching Control config in registers
///
/// \Then the function return error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForMismatchedPinMaskAndControlRegisters)
{
	const uint32_t pinMask = PIO_PIN_4 | PIO_PIN_2;
	const uint32_t pinMaskSubset = PIO_PIN_2;

	Pio pio;
	Pio_Registers dummyRegs = {};
	ErrorCode errCode = ErrorCode_NoError;
	pio.reg = &dummyRegs;

	// Set pull status register to valid config to avoid triggering Pull up/down config errors
	dummyRegs.pusr = pinMask;
	dummyRegs.ppdsr = pinMask;

	Pio_Pin_Config readConf = {};

	pio.reg->psr = pinMaskSubset;
	pio.reg->abcdsr1 = 0;
	pio.reg->abcdsr2 = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_ControlConfigMismatch, errCode);

	pio.reg->psr = 0;
	pio.reg->abcdsr1 = pinMaskSubset;
	pio.reg->abcdsr2 = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_ControlConfigMismatch, errCode);

	pio.reg->psr = 0;
	pio.reg->abcdsr1 = 0;
	pio.reg->abcdsr2 = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_ControlConfigMismatch, errCode);

	pio.reg->psr = 0;
	pio.reg->abcdsr1 = pinMaskSubset;
	pio.reg->abcdsr2 = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_ControlConfigMismatch, errCode);

	pio.reg->psr = 0;
	pio.reg->abcdsr1 = pinMask;
	pio.reg->abcdsr2 = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_ControlConfigMismatch, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_getPinsConfig() is called pinMask set to 0,
//
/// \Then the function returns error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForInvalidPinMask)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_C, &pioC, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};

	CHECK_FALSE(Pio_getPinsConfig(&pioC, 0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);
}

/// \Given initialized Pio
///
/// \When pins are configured with flow direction
///
/// \Then proper direction configuration shall be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadPinDirectionConfiguration)
{
	const uint32_t pinMask = PIO_PIN_6;
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_C, &pioC, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.pull = Pio_Pull_None;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;

	pinConf.direction = Pio_Direction_Input;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);

	pinConf.direction = Pio_Direction_Output;
	CHECK_TRUE(Pio_setPinsConfig(&pioC, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioC, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);
}

/// \Given initialized Pio
///
/// \When pins are configured to specific peripheral mode
///
/// \Then proper peripheral mode configuration shall be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadPeripheralControlConfiguration)
{
	const uint32_t pinMask = PIO_PIN_5;
	Pio pioB;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_B, &pioB, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_None;
	pinConf.filter = Pio_Filter_None;
	pinConf.isMultiDriveEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Drive_Low;

	pinConf.control = Pio_Control_Pio;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);

	pinConf.control = Pio_Control_PeripheralA;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);

	pinConf.control = Pio_Control_PeripheralB;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);

	pinConf.control = Pio_Control_PeripheralC;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);

	pinConf.control = Pio_Control_PeripheralD;
	CHECK_TRUE(Pio_setPinsConfig(&pioB, pinMask, &pinConf, &errCode));
	CHECK_TRUE(Pio_getPinsConfig(&pioB, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);
}

/// \Given initialized Pio and
///
/// \When pins are configured differently and their config is requested
///
/// \Then function shall return false for configuration mismatch.
///
TEST(PioTests, Pio_getPinsConfig_returnsFalseOnDifferentPinConfiguration)
{
	Pio pioA;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_A, &pioA, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf1 = {};
	Pio_Pin_Config pinConf2;
	Pio_Pin_Config readConf = {};
	pinConf1.control = Pio_Control_Pio;
	pinConf1.direction = Pio_Direction_Output;
	pinConf1.pull = Pio_Pull_None;
	pinConf1.filter = Pio_Filter_None;
	pinConf1.isMultiDriveEnabled = false;
	pinConf1.isSchmittTriggerDisabled = false;
	pinConf1.driveStrength = Pio_Drive_Low;
	pinConf1.irq = Pio_Irq_None;

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.control = Pio_Control_PeripheralA;
	const bool result2 =
			Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode)
			&& Pio_setPinsConfig(
					&pioA, PIO_PIN_11, &pinConf2, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_ControlConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.direction = Pio_Direction_Input;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_DirectionConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.pull = Pio_Pull_Up;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_PullConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.filter = Pio_Filter_Glitch;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_FilterConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.isMultiDriveEnabled = true;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_MultiDriveConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.isSchmittTriggerDisabled = true;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(
			Pio_ErrorCode_SchmittTriggerConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.irq = Pio_Irq_LevelHigh;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_IrqConfigMismatch, errCode);

	errCode = ErrorCode_NoError;
	pinConf2 = pinConf1;
	pinConf2.driveStrength = Pio_Drive_High;
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_3, &pinConf1, &errCode));
	CHECK_TRUE(Pio_setPinsConfig(&pioA, PIO_PIN_11, &pinConf2, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_FALSE(Pio_getPinsConfig(
			&pioA, PIO_PIN_3 | PIO_PIN_11, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(
			Pio_ErrorCode_DriveStrengthConfigMismatch, errCode);
}

/// \Given Pio initialized with dummy registers
///
/// \When Pio_getPinsConfig() is called with pinMask not matching Pull config in registers
///
/// \Then the function return error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForMismatchedPinMaskAndPullRegisters)
{
	const uint32_t pinMask = PIO_PIN_4 | PIO_PIN_2;
	const uint32_t pinMaskSubset = PIO_PIN_2;

	Pio pio;
	Pio_Registers dummyRegs = {};
	ErrorCode errCode = ErrorCode_NoError;
	pio.reg = &dummyRegs;
	Pio_Pin_Config readConf = {};

	pio.reg->pusr = pinMaskSubset;
	pio.reg->ppdsr = pinMask;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_PullConfigMismatch, errCode);

	pio.reg->pusr = pinMaskSubset;
	pio.reg->ppdsr = 0;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_PullConfigMismatch, errCode);

	pio.reg->pusr = pinMask;
	pio.reg->ppdsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_PullConfigMismatch, errCode);

	pio.reg->pusr = 0;
	pio.reg->ppdsr = pinMaskSubset;
	CHECK_FALSE(Pio_getPinsConfig(&pio, pinMask, &readConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_PullConfigMismatch, errCode);
}

/// @}

/// @name Pio_setPortConfig()
/// @{

/// \Given initialized Pio
///
/// \When Pio_setPortConfig() is called with invalid settings for control.
///
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_portPinsConfig_returnsErrorForInvalidControlSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_C, &pioC, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	const Pio_Port_Config portConf = {
    .pins = PIO_PIN_6,
    .pinsConfig = {
      .control = arbitraryEnumValue<Pio_Control>(0xDEADu),
      .direction = Pio_Direction_Output,
      .pull = Pio_Pull_Down,
      .filter = Pio_Filter_None,
      .isMultiDriveEnabled = false,
      .irq = Pio_Irq_None,
      .driveStrength = Pio_Drive_Low,
      .isSchmittTriggerDisabled = false,
    },
    .debounceFilterDiv = 8u,
  };

	CHECK_FALSE(Pio_setPortConfig(&pioC, &portConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidControlConfig, errCode);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for control.
///
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidControlSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_C, &pioC, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	const Pio_Pin_Config pinConf = {
		.control = arbitraryEnumValue<Pio_Control>(0xDEADu),
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidControlConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for direction.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidDirectionSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = arbitraryEnumValue<Pio_Direction>(0xDEADu),
		.pull = Pio_Pull_Down,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidDirectionConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for pull-up/pull-down.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidPullSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = arbitraryEnumValue<Pio_Pull>(0xDEADu),
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPullConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for input filter.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidFilterSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.filter = arbitraryEnumValue<Pio_Filter>(0xDEADu),
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidFilterConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for interrupt event.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidIrqSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = arbitraryEnumValue<Pio_Irq>(0xDEADu),
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidIrqConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for drive strength.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidDriveStrengthSettings)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = arbitraryEnumValue<Pio_Drive>(0xDEADu),
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidDriveStrengthConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called pinMask set to 0,
///
/// \Then the function returns error.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidPinMask)
{
	Pio pioC;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_C, &pioC, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.filter = Pio_Filter_None,
		.isMultiDriveEnabled = false,
		.irq = Pio_Irq_None,
		.driveStrength = Pio_Drive_Low,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioC, 0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);
}

/// @}
