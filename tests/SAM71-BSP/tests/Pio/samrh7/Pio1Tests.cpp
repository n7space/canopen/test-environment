/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Pio1Tests.cpp
/// \brief Pio driver test suite implementation.

#include "TestUtils/TestUtils.hpp"

TEST_GROUP_BASE(PioTests, TestUtils){};

using PioTests = TEST_GROUP_CppUTestGroupPioTests;

/// @name Pio_getPortConfig()
/// @{

/// \Given initialized Pio and set configuration.
///
/// \When pin configuration getter is called
///
/// \Then proper pin configuration shall be returned
///
TEST(PioTests, Pio_getPortConfig_canReadPortConfiguration)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Port_Config portConfig = {};
	portConfig.pins = PioTests::TEST_PIN_0 | PioTests::TEST_PIN_1;
	portConfig.debounceFilterDiv = 0;
	portConfig.pinsConfig.control = Pio_Control_Pio;
	portConfig.pinsConfig.direction = Pio_Direction_Output;
	portConfig.pinsConfig.pull = Pio_Pull_Down;
	portConfig.pinsConfig.isOpenDrainEnabled = false;
	portConfig.pinsConfig.isSchmittTriggerDisabled = false;
	portConfig.pinsConfig.driveStrength = Pio_Current_2m;

	CHECK_TRUE(Pio_setPortConfig(&pioPort, &portConfig, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Port_Config readConfig = {};
	portConfig.pins = PioTests::TEST_PIN_0;
	readConfig.pins = PioTests::TEST_PIN_0;
	const bool result2 = Pio_getPortConfig(&pioPort, &readConfig, &errCode);
	MEMCMP_EQUAL(&portConfig, &readConfig, sizeof(Pio_Port_Config));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);

	portConfig.pins = PioTests::TEST_PIN_1;
	readConfig.pins = PioTests::TEST_PIN_1;
	const bool result3 = Pio_getPortConfig(&pioPort, &readConfig, &errCode);
	MEMCMP_EQUAL(&portConfig, &readConfig, sizeof(Pio_Port_Config));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result3);

	readConfig.pins = PioTests::TEST_PIN_0 | PioTests::TEST_PIN_1;
	const bool result4 = Pio_getPortConfig(&pioPort, &readConfig, &errCode);
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);
	CHECK_FALSE(result4);
}

/// @}

/// @name Pio_setPins()
/// @{

/// \Given initalized Pio, with pins in output mode
///
/// \When pins are to be set or reset
///
/// \Then the pins are set or reset.
///
TEST(PioTests, Pio_setsAndResetsPins)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	const uint32_t PIO_DELAY_CYCLES = 5000000u;

	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_setPins(&pioPort, PioTests::TEST_PIN_0);
	UNSIGNED_LONGS_EQUAL(PioTests::TEST_PIN_0,
			Pio_getPins(&pioPort) & PioTests::TEST_PIN_0);
	busyWaitLoop(PIO_DELAY_CYCLES);

	Pio_resetPins(&pioPort, PioTests::TEST_PIN_0);
	UNSIGNED_LONGS_EQUAL(0, Pio_getPins(&pioPort) & PioTests::TEST_PIN_0);
	busyWaitLoop(PIO_DELAY_CYCLES);

	Pio_setPins(&pioPort, PioTests::TEST_PIN_0);
	UNSIGNED_LONGS_EQUAL(PioTests::TEST_PIN_0,
			Pio_getPins(&pioPort) & PioTests::TEST_PIN_0);
	busyWaitLoop(PIO_DELAY_CYCLES);

	Pio_resetPins(&pioPort, PioTests::TEST_PIN_0);
	UNSIGNED_LONGS_EQUAL(0, Pio_getPins(&pioPort) & PioTests::TEST_PIN_0);
	busyWaitLoop(PIO_DELAY_CYCLES);

	pinConf.direction = Pio_Direction_Input;
	pinConf.pull = Pio_Pull_None;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pin pull configuration is set
///
/// \Then proper pin pull configuration can be read
///
TEST(PioTests, Pio_getPinsConfig_canReadPullConfiguration)
{
	const uint32_t pinMask = PioTests::TEST_PIN_0;
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.pull = Pio_Pull_Up;
	const bool result2 =
			Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.pull, readConf.pull);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);

	pinConf.pull = Pio_Pull_Down;
	const bool result3 =
			Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.pull, readConf.pull);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result3);

	pinConf.pull = Pio_Pull_None;
	const bool result4 =
			Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.pull, readConf.pull);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result4);
}

/// \Given initialized Pio,
///
/// \When pin interrupt configuration is set,
///
/// \Then proper pin interrupt configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadIrqState)
{
	const uint32_t pinMask = PioTests::TEST_PIN_0;
	Pio pio{};

	const auto validateIrqConfig = [&pio](const Pio_Pin_Config &config) {
		ErrorCode errorCode{};
		Pio_Pin_Config readConf{};

		const bool setConfigState = Pio_setPinsConfig(
				&pio, pinMask, &config, &errorCode);
		ENUMS_EQUAL_INT(ErrorCode_NoError, errorCode);
		CHECK_TRUE(setConfigState);
		const bool getConfigState = Pio_getPinsConfig(
				&pio, pinMask, &readConf, &errorCode);
		ENUMS_EQUAL_INT(ErrorCode_NoError, errorCode);
		CHECK_TRUE(getConfigState);
		CHECK_EQUAL(config.isIrqEnabled, readConf.isIrqEnabled);
		ENUMS_EQUAL_INT(config.irq, readConf.irq);
	};

	ErrorCode errCode = ErrorCode_NoError;
	const bool initState =
			Pio_init(PioTests::TEST_PIO_PORT, &pio, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(initState);

	Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Input,
		.pull = Pio_Pull_None,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	validateIrqConfig(pinConf);

	pinConf.irq = Pio_Irq_EdgeRising;
	pinConf.isIrqEnabled = true;
	validateIrqConfig(pinConf);

	pinConf.irq = Pio_Irq_EdgeBoth;
	validateIrqConfig(pinConf);

	pinConf.irq = Pio_Irq_LevelLow;
	validateIrqConfig(pinConf);

	pinConf.irq = Pio_Irq_LevelHigh;
	validateIrqConfig(pinConf);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio, with pins in output mode
///
/// \When pin pull configuration is set
///
/// \Then pin value changes accordingly.
///
TEST(PioTests, Pio_setPinsConfig_canForceResistorsIntoPullUpOrPullDownConfiguration)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	const uint32_t PIO_CONFIGURATION_DELAY = 1000u;

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Input;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.pull = Pio_Pull_Up;
	const bool result2 = Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);
	busyWaitLoop(PIO_CONFIGURATION_DELAY);

	UNSIGNED_LONGS_EQUAL(PioTests::TEST_PIN_0,
			Pio_getPins(&pioPort) & PioTests::TEST_PIN_0);

	pinConf.pull = Pio_Pull_Down;
	const bool result3 = Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result3);
	busyWaitLoop(PIO_CONFIGURATION_DELAY);

	UNSIGNED_LONGS_EQUAL(0, Pio_getPins(&pioPort) & PioTests::TEST_PIN_0);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When open drain pin configuration is set
///
/// \Then proper open drain pin configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadOpenDrainConfiguration)
{
	const uint32_t pinMask = PIO_PIN_31 | PIO_PIN_30;
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result = Pio_init(Pio_Port_D, &pioD, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.isOpenDrainEnabled = true;
	const bool result2 =
			Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioD, PIO_PIN_31, &readConf, &errCode);
	CHECK_EQUAL(pinConf.isOpenDrainEnabled, readConf.isOpenDrainEnabled);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);

	const bool result3 = Pio_getPinsConfig(
			&pioD, PIO_PIN_30, &readConf, &errCode);
	CHECK_EQUAL(pinConf.isOpenDrainEnabled, readConf.isOpenDrainEnabled);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result3);

	pinConf.isOpenDrainEnabled = false;
	const bool result4 =
			Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioD, PIO_PIN_31, &readConf, &errCode);
	CHECK_EQUAL(pinConf.isOpenDrainEnabled, readConf.isOpenDrainEnabled);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result4);

	const bool result5 = Pio_getPinsConfig(
			&pioD, PIO_PIN_30, &readConf, &errCode);
	CHECK_EQUAL(pinConf.isOpenDrainEnabled, readConf.isOpenDrainEnabled);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result5);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured in open drain mode
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canConfigureOpenDrain)
{
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.isOpenDrainEnabled = true;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	const bool result = Pio_setPinsConfig(
			&pioD, PIO_PIN_30 | PIO_PIN_31, &pinConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	CHECK_COMPARE(pioD.reg->cfgr & PIO_CFGR_OPD_MASK, !=, 0u);

	pinConf.isOpenDrainEnabled = false;

	const bool result2 = Pio_setPinsConfig(
			&pioD, PIO_PIN_30 | PIO_PIN_31, &pinConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);
	UNSIGNED_LONGS_EQUAL(0u, pioD.reg->cfgr & PIO_CFGR_OPD_MASK);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins drive current is configured
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canConfigureDriveCurrent)
{
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.isOpenDrainEnabled = true;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_16m;

	const auto result = Pio_setPinsConfig(
			&pioD, PIO_PIN_30 | PIO_PIN_31, &pinConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	ENUMS_EQUAL_INT(Pio_Current_16m,
			(pioD.reg->cfgr & PIO_CFGR_DRVSTR_MASK)
					>> PIO_CFGR_DRVSTR_OFFSET);

	pinConf.driveStrength = Pio_Current_4m;

	const auto result2 = Pio_setPinsConfig(
			&pioD, PIO_PIN_30 | PIO_PIN_31, &pinConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result2);
	ENUMS_EQUAL_INT(Pio_Current_4m,
			(pioD.reg->cfgr & PIO_CFGR_DRVSTR_MASK)
					>> PIO_CFGR_DRVSTR_OFFSET);
}

/// \Given Pio initialized with dummy registers
///
/// \When pins are configured as synchronous outputs
///
/// \Then the corresponding registers are set to correct values.
///
TEST(PioTests, Pio_setPinsConfig_canSetSynchronousOutputConfiguration)
{
	Pio pio = {};
	Pio_Registers dummyRegs = {};
	ErrorCode errCode = ErrorCode_NoError;

	pio.reg = &dummyRegs;

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_PeripheralA;
	pinConf.direction = Pio_Direction_SynchronousOutput;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_16m;

	const uint32_t pinMask = PIO_PIN_30 | PIO_PIN_31;
	CHECK_TRUE(Pio_setPinsConfig(&pio, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(
			PIO_CFGR_DIR_MASK, pio.reg->cfgr & PIO_CFGR_DIR_MASK);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins drive current configuration is set,
///
/// \Then the applied configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadDriveCurrent)
{
	const uint32_t pinMask = PIO_PIN_31 | PIO_PIN_30;
	Pio pioD;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(Pio_Port_D, &pioD, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_PeripheralC;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Up;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_8m;

	CHECK_TRUE(Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode));

	// Read configuration for each pin separately and verify it
	auto result = Pio_getPinsConfig(&pioD, PIO_PIN_31, &readConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	CHECK_EQUAL(readConf.driveStrength, pinConf.driveStrength);

	result = Pio_getPinsConfig(&pioD, PIO_PIN_30, &readConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	CHECK_EQUAL(readConf.driveStrength, pinConf.driveStrength);

	pinConf.driveStrength = Pio_Current_4m;
	result = Pio_setPinsConfig(&pioD, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioD, PIO_PIN_31, &readConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	CHECK_EQUAL(readConf.driveStrength, pinConf.driveStrength);

	result = Pio_getPinsConfig(&pioD, PIO_PIN_30, &readConf, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
	CHECK_EQUAL(readConf.driveStrength, pinConf.driveStrength);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with Schmitt trigger enabled
///
/// \Then proper Schmitt trigger configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadSchmittTriggerConfiguration)
{
	const uint32_t pinMask = PioTests::TEST_PIN_0;
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_PeripheralB;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isOpenDrainEnabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.isSchmittTriggerDisabled = true;
	bool result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.isSchmittTriggerDisabled,
			readConf.isSchmittTriggerDisabled);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	pinConf.isSchmittTriggerDisabled = false;
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.isSchmittTriggerDisabled,
			readConf.isSchmittTriggerDisabled);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with Schmitt trigger disabled
///
/// \Then the corresponding registers are set to the correct values.
///
TEST(PioTests, Pio_setPinsConfig_canDisableSchmittTrigger)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_PeripheralB;
	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isOpenDrainEnabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.isSchmittTriggerDisabled = true;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	CHECK_COMPARE(pioPort.reg->cfgr & PIO_CFGR_SCHMITT_MASK, !=, 0u);

	pinConf.isSchmittTriggerDisabled = false;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(0u, pioPort.reg->cfgr & PIO_CFGR_SCHMITT_MASK);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with interrupts enabled
///
/// \Then proper pin interrupt configuration can be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadInterruptConfiguration)
{
	const uint32_t pinMask = PIO_PIN_6;
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Input;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.irq = Pio_Irq_EdgeFalling;
	bool result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	pinConf.irq = Pio_Irq_EdgeRising;
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	pinConf.irq = Pio_Irq_LevelLow;
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	pinConf.irq = Pio_Irq_LevelHigh;
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	pinConf.irq = Pio_Irq_EdgeBoth;
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.irq, readConf.irq);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
}

/// @}
