/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Pio2Tests.cpp
/// \brief Pio driver test suite implementation.

#include "TestUtils/TestUtils.hpp"

TEST_GROUP_BASE(PioTests, TestUtils){};

using PioTests = TEST_GROUP_CppUTestGroupPioTests;

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with flow direction
///
/// \Then proper direction configuration shall be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadPinDirectionConfiguration)
{
	const uint32_t pinMask = PIO_PIN_6;
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};

	pinConf.control = Pio_Control_Pio;
	pinConf.pull = Pio_Pull_None;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.direction = Pio_Direction_Input;
	bool result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	pinConf.direction = Pio_Direction_Output;
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.direction, readConf.direction);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
}

/// \Given initialized Pio
///
/// \When pins are configured to specific peripheral mode
///
/// \Then proper peripheral mode configuration shall be read.
///
TEST(PioTests, Pio_getPinsConfig_canReadPeripheralControlConfiguration)
{
	const uint32_t pinMask = PioTests::TEST_PIN_0;
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	Pio_Pin_Config readConf = {};

	pinConf.direction = Pio_Direction_Output;
	pinConf.pull = Pio_Pull_None;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;

	pinConf.control = Pio_Control_Pio;
	bool result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

#if defined(N7S_TARGET_SAMRH71F20)
	pinConf.control = Pio_Control_PeripheralA;
#elif defined(N7S_TARGET_SAMRH707F18)
	pinConf.control = Pio_Control_PeripheralD;
#endif
	result = Pio_setPinsConfig(&pioPort, pinMask, &pinConf, &errCode)
			&& Pio_getPinsConfig(
					&pioPort, pinMask, &readConf, &errCode);
	CHECK_EQUAL(pinConf.control, readConf.control);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);
}

/// @}

/// @name Pio_setPortMaskedValue()
/// @{

/// \Given dummy PIO device descriptor with mask register (MSKR)
///        and output data status register (ODSR) set to particular values
///
/// \When Pio_setPortMaskedValue function is called with this PIO device descriptor
///       and a pin mask different than the value of mask register,
///       and a port value different than the value of data status register
///
/// \Then The function changes only the lines of the I/O port that were selected by a pin mask
///       to the correct values based on the passed port value
///
TEST(PioTests, Pio_setPortMaskedValue_setsSelectedPortLinesToCorrectValue)
{
	Pio_Registers dummyReg = {};
	dummyReg.mskr = 0xDEADBEEFu;
	dummyReg.odsr = 0xCAFEF00Du;
	Pio dummyPio = {};
	dummyPio.reg = &dummyReg;
	const uint32_t pinMask = 0xA5A5A5A5u;
	const uint32_t value = 0x12345678u;

	Pio_setPortMaskedValue(&dummyPio, pinMask, value);

	UNSIGNED_LONGS_EQUAL(0xA5A5A5A5u, dummyPio.reg->mskr);
	UNSIGNED_LONGS_EQUAL(0x12345678u, dummyPio.reg->odsr);
}

/// @}

/// @name Pio_setPortValue()
/// @{

/// \Given dummy PIO device descriptor with mask register (MSKR) with value set to zero
///        and output data status register (ODSR) set to particular value
///
/// \When Pio_setPortValue function is called with this PIO device descriptor
///       and a port value different than the value of data status register
///
/// \Then The function changes all the lines of the I/O port to the correct values based on
///       the passed port value
///
TEST(PioTests, Pio_setPortValue_setsAllPortLinesToCorrectValue)
{
	Pio_Registers dummyReg = {};
	dummyReg.mskr = 0u;
	dummyReg.odsr = 0xDEADBEEFu;
	Pio dummyPio = {};
	dummyPio.reg = &dummyReg;
	const uint32_t value = 0xCAFEF00Du;

	Pio_setPortValue(&dummyPio, value);

	UNSIGNED_LONGS_EQUAL(0xFFFFFFFFu, dummyPio.reg->mskr);
	UNSIGNED_LONGS_EQUAL(0xCAFEF00Du, dummyPio.reg->odsr);
}

/// @}

/// @name Pio_getIrqStatus()
/// @{

/// \Given dummy PIO device descriptor with interrupt status register (ISR) set to any value
///
/// \When Pio_getIrqStatus function is called with this PIO device descriptor
///
/// \Then The function returns interrupt status register value
///
TEST(PioTests, Pio_getIrqStatus_returnsIrqStatus)
{
	Pio_Registers dummyReg = {};
	dummyReg.isr = 0xA5A5A5A5u;
	Pio dummyPio = {};
	dummyPio.reg = &dummyReg;

	const uint32_t status = Pio_getIrqStatus(&dummyPio);

	UNSIGNED_LONGS_EQUAL(0xA5A5A5A5u, status);
}

/// @}

/// @name Pio_Init()
/// @{

/// \Given Uninitialized Pio and valid port ID
///
/// \When Pio_Init() function is called with Pio and port ID
///
/// \Then The function returns no error and register addresses are set to correct values
///
TEST(PioTests, Pio_PioInit_initializesRegistersAddresses)
{
	Pio pio = {};
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Pio_init(Pio_Port_A, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_A, pio.port);
	POINTERS_EQUAL(PIOA_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_B, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_B, pio.port);
	POINTERS_EQUAL(PIOB_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_C, &pio, &errCode));

	ENUMS_EQUAL_INT(Pio_Port_C, pio.port);
	POINTERS_EQUAL(PIOC_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_D, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_D, pio.port);
	POINTERS_EQUAL(PIOD_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

#if defined(N7S_TARGET_SAMRH71F20)
	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_E, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_E, pio.port);
	POINTERS_EQUAL(PIOE_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_F, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_F, pio.port);
	POINTERS_EQUAL(PIOF_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	pio = {};
	CHECK_TRUE(Pio_init(Pio_Port_G, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_G, pio.port);
	POINTERS_EQUAL(PIOG_ADDRESS_BASE, pio.reg);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
#endif
}

/// @}

/// @name Pio_Init()
/// @{

/// \Given Uninitialized Pio and invalid port ID
///
/// \When Pio_Init() function is called with Pio and port ID
///
/// \Then The function returns error and register addresses are left unmodified
///
TEST(PioTests, Pio_PioInit_returnsErrorsForInvalidInput)
{
	Pio pio = {};
	ErrorCode errCode = ErrorCode_NoError;

	const auto invalidPortId = arbitraryEnumValue<Pio_Port>(0xDEADu);

	CHECK_FALSE(Pio_init(invalidPortId, &pio, &errCode));
	ENUMS_EQUAL_INT(Pio_Port_A, 0);
	POINTERS_EQUAL(nullptr, pio.reg);
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPortId, errCode);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for control.
///
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidControlSettings)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = arbitraryEnumValue<Pio_Control>(0xDEADu),
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioPort, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidControlConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for direction.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidDirectionSettings)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = arbitraryEnumValue<Pio_Direction>(0xDEADu),
		.pull = Pio_Pull_Down,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioPort, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidDirectionConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for pull-up/pull-down.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidPullSettings)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = arbitraryEnumValue<Pio_Pull>(0xDEADu),
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioPort, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPullConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for interrupt event.
//
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidIrqSettings)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.isOpenDrainEnabled = false,
		.irq = arbitraryEnumValue<Pio_Irq>(0xDEADu),
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioPort, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidIrqConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called with invalid settings for drive strength.
///
/// \Then the corresponding registers are not modified and function returns false.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidDriveStrengthSettings)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = arbitraryEnumValue<Pio_Current>(0xDEADu),
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioPort, PIO_PIN_6, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidDriveStrengthConfig, errCode);
}

/// \Given initialized Pio
///
/// \When Pio_setPinsConfig() is called pinMask set to 0,
///
/// \Then the function returns error.
///
TEST(PioTests, Pio_setPinsConfig_returnsErrorForInvalidPinMask)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	CHECK_FALSE(Pio_setPinsConfig(&pioPort, 0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);
}

/// @}

/// @name Pio_setPortConfig()
/// @{

/// \Given initialized Pio
///
/// \When Pio_setPortConfig() is called pinMask set to 0,
///
/// \Then the function returns error.
///
TEST(PioTests, Pio_setPortConfig_returnsErrorForInvalidPinMask)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	const Pio_Pin_Config pinConf = {
		.control = Pio_Control_Pio,
		.direction = Pio_Direction_Output,
		.pull = Pio_Pull_Down,
		.isOpenDrainEnabled = false,
		.irq = Pio_Irq_EdgeFalling,
		.isIrqEnabled = false,
		.driveStrength = Pio_Current_2m,
		.isSchmittTriggerDisabled = false,
	};

	const Pio_Port_Config portConf = {
		.pins = 0, .pinsConfig = pinConf, .debounceFilterDiv = 0
	};

	CHECK_FALSE(Pio_setPortConfig(&pioPort, &portConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);
}

/// @}

/// @name Pio_getPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When Pio_getPinsConfig() is called with pinMask set to 0 or mask with multiple bits set,
///
/// \Then the function returns error.
///
TEST(PioTests, Pio_getPinsConfig_returnsErrorForInvalidPinMask)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	const bool result =
			Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode);
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	CHECK_TRUE(result);

	Pio_Pin_Config pinConf = {};

	CHECK_FALSE(Pio_getPinsConfig(&pioPort, 0, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);

	errCode = ErrorCode_NoError;
	CHECK_FALSE(Pio_getPinsConfig(
			&pioPort, PIO_PIN_11 | PIO_PIN_16, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(Pio_ErrorCode_InvalidPinMask, errCode);
}

/// \Given Pio initialized with dummy registers containing configuration for Pull up/down resistors
///
/// \When Pio_getPinsConfig() is called,
///
/// \Then the function returns field pull contains correct value .
///
TEST(PioTests, Pio_getPinsConfig_returnsCorrectPullConfig)
{
	Pio pio;
	Pio_Registers dummyRegs = {};
	pio.reg = &dummyRegs;
	ErrorCode errCode = ErrorCode_NoError;
	const uint32_t pinMask = PIO_PIN_13;
	Pio_Pin_Config pinConf = {};

	const uint32_t pull_up_mask = UINT32_C(1) << 9u;
	const uint32_t pull_down_mask = UINT32_C(1) << 10u;

	dummyRegs.cfgr = 0;

	CHECK_TRUE(Pio_getPinsConfig(&pio, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(Pio_Pull_None, pinConf.pull);

	dummyRegs.cfgr = pull_up_mask;

	CHECK_TRUE(Pio_getPinsConfig(&pio, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(Pio_Pull_Up, pinConf.pull);

	// cppcheck-suppress [redundantAssignment]
	dummyRegs.cfgr = pull_down_mask;

	CHECK_TRUE(Pio_getPinsConfig(&pio, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(Pio_Pull_Down, pinConf.pull);

	// If both are enabled, pull-up takes precedence
	// cppcheck-suppress [redundantAssignment, unreadVariable]
	dummyRegs.cfgr = pull_down_mask | pull_up_mask;

	CHECK_TRUE(Pio_getPinsConfig(&pio, pinMask, &pinConf, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(Pio_Pull_Up, pinConf.pull);
}

/// @}

/// @name Pio_setPinsConfig()
/// @{

/// \Given initialized Pio
///
/// \When pins are configured with interrupts enabled
///
/// \Then the corresponding registers are set to the correct values.
///
TEST(PioTests, Pio_setPinsConfig_canConfigureInterrupts)
{
	Pio pioPort;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pio_init(PioTests::TEST_PIO_PORT, &pioPort, &errCode));
	UNSIGNED_LONGS_EQUAL(ErrorCode_NoError, errCode);

	Pio_Pin_Config pinConf = {};
	pinConf.control = Pio_Control_Pio;
	pinConf.direction = Pio_Direction_Input;
	pinConf.pull = Pio_Pull_Down;
	pinConf.isOpenDrainEnabled = false;
	pinConf.isSchmittTriggerDisabled = false;
	pinConf.driveStrength = Pio_Current_2m;
	pinConf.isIrqEnabled = true;

	pinConf.irq = Pio_Irq_EdgeFalling;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	CHECK_COMPARE(pioPort.reg->imr & PioTests::TEST_PIN_0, !=, 0u);
	ENUMS_EQUAL_INT(Pio_Irq_EdgeFalling,
			(pioPort.reg->cfgr & PIO_CFGR_EVTSEL_MASK)
					>> PIO_CFGR_EVTSEL_OFFSET);

	pinConf.irq = Pio_Irq_EdgeRising;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	CHECK_COMPARE(pioPort.reg->imr & PioTests::TEST_PIN_0, !=, 0u);
	ENUMS_EQUAL_INT(Pio_Irq_EdgeRising,
			(pioPort.reg->cfgr & PIO_CFGR_EVTSEL_MASK)
					>> PIO_CFGR_EVTSEL_OFFSET);

	pinConf.irq = Pio_Irq_LevelLow;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	CHECK_COMPARE(pioPort.reg->imr & PioTests::TEST_PIN_0, !=, 0u);
	ENUMS_EQUAL_INT(Pio_Irq_LevelLow,
			(pioPort.reg->cfgr & PIO_CFGR_EVTSEL_MASK)
					>> PIO_CFGR_EVTSEL_OFFSET);

	pinConf.irq = Pio_Irq_LevelHigh;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	CHECK_COMPARE(pioPort.reg->imr & PioTests::TEST_PIN_0, !=, 0u);
	ENUMS_EQUAL_INT(Pio_Irq_LevelHigh,
			(pioPort.reg->cfgr & PIO_CFGR_EVTSEL_MASK)
					>> PIO_CFGR_EVTSEL_OFFSET);

	pinConf.irq = Pio_Irq_EdgeBoth;
	CHECK_TRUE(Pio_setPinsConfig(
			&pioPort, PioTests::TEST_PIN_0, &pinConf, &errCode));
	CHECK_COMPARE(pioPort.reg->imr & PioTests::TEST_PIN_0, !=, 0u);
	ENUMS_EQUAL_INT(Pio_Irq_EdgeBoth,
			(pioPort.reg->cfgr & PIO_CFGR_EVTSEL_MASK)
					>> PIO_CFGR_EVTSEL_OFFSET);
}

/// @}
