/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_PIO_TESTS_TESTUTILS_HPP
#define N7S_PIO_TESTS_TESTUTILS_HPP

/// \file  TestUtils.hpp
/// \brief PIO driver tests helper header.

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>

#include <TestsRuntime/EnumUtils.hpp>

TEST_BASE(TestUtils)
{
      public:
#if defined(N7S_TARGET_SAMRH71F20)
	constexpr static Pio_Port TEST_PIO_PORT = Pio_Port_C;
	constexpr static uint32_t TEST_PIN_0 = PIO_PIN_8;
	constexpr static uint32_t TEST_PIN_1 = PIO_PIN_7;
#elif defined(N7S_TARGET_SAMRH707F18)
	// RH707 has PIOC used for peripherals on the development board, use unused pins
	constexpr static Pio_Port TEST_PIO_PORT = Pio_Port_A;
	constexpr static uint32_t TEST_PIN_0 = PIO_PIN_5;
	constexpr static uint32_t TEST_PIN_1 = PIO_PIN_4;
#endif
	void setup() override
	{
		Pmc pmc;
		Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
		Pmc_enablePeripheralClk(&pmc,
				Pmc_PeripheralId_PioA); // enables clock for all Pio peripherals
	}
};

#endif
