/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_TIC_TESTS_TESTUTILS_HPP
#define N7S_TIC_TESTS_TESTUTILS_HPP

/// \file  TestUtils.hpp
/// \brief PMC driver tests helper header.

#include <array>
#include <atomic>
#include <cassert>
#include <cstring>
#include <initializer_list>

#include <CppUTest/TestHarness.h>

#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pio/Pio.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

inline void
initializeHardware()
{
	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc0Ch0);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc0Ch1);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc0Ch2);

	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc1Ch0);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc1Ch1);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc1Ch2);

	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc2Ch0);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc2Ch1);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc2Ch2);
#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc3Ch0);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc3Ch1);
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc3Ch2);
#endif
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel0);
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel1);
	Nvic_enableInterrupt(Nvic_Irq_Timer0_Channel2);

	Nvic_enableInterrupt(Nvic_Irq_Timer1_Channel0);
	Nvic_enableInterrupt(Nvic_Irq_Timer1_Channel1);
	Nvic_enableInterrupt(Nvic_Irq_Timer1_Channel2);

	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel0);
	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel1);
	Nvic_enableInterrupt(Nvic_Irq_Timer2_Channel2);

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	Nvic_enableInterrupt(Nvic_Irq_Timer3_Channel0);
	Nvic_enableInterrupt(Nvic_Irq_Timer3_Channel1);
	Nvic_enableInterrupt(Nvic_Irq_Timer3_Channel2);
#endif
#if defined(N7S_TARGET_SAMRH707F18)
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc0Ch0, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc0Ch1, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc0Ch2, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc1Ch0, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc1Ch1, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc1Ch2, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc2Ch0, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc2Ch1, Pmc_PckSrc_Pllack, 4);
	Pmc_enableGenericClk(
			&pmc, Pmc_PeripheralId_Tc2Ch2, Pmc_PckSrc_Pllack, 4);
#endif
}

template <typename F>
inline bool
evaluateWithTimeout(F lambda, const uint32_t timeout)
{
	for (uint32_t counter = 0; counter < timeout; ++counter)
		if (lambda()) // cppcheck-suppress [misra-c2012-14.4]
			return true;
	return false;
}

TEST_BASE(TestUtils)
{
#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	constexpr static std::array<Tic_Id, 4> TIC_ID_ENUMS = { Tic_Id_0,
		Tic_Id_1, Tic_Id_2, Tic_Id_3 };
#elif defined(N7S_TARGET_SAMRH707F18)
	constexpr static std::array<Tic_Id, 3> TIC_ID_ENUMS = { Tic_Id_0,
		Tic_Id_1, Tic_Id_2 };
#endif

	void setup() override
	{
		initializeHardware();

		Tic tic = {};
		ticForInterrupts = {};
		channelStatusForInterrupts = {};
		interruptOccurred = false;
		lastChannelThatInterrupted = Tic_Channel_Count;
		Tic_init(&tic, Tic_Id_0);
		Tic_writeProtect(&tic, false);
		Tic_init(&tic, Tic_Id_1);
		Tic_writeProtect(&tic, false);
		Tic_init(&tic, Tic_Id_2);
		Tic_writeProtect(&tic, false);
#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
		Tic_init(&tic, Tic_Id_3);
		Tic_writeProtect(&tic, false);
#endif
	}

	void teardown() override
	{
		// Disable all clocks and make sure there are no pending interrupts
		Tic tic;
		for (const auto tic_id : TIC_ID_ENUMS) {
			Tic_init(&tic, tic_id);
			Tic_writeProtect(&tic, false);

			for (const auto tic_channel : { Tic_Channel_0,
					     Tic_Channel_1, Tic_Channel_2 })
				Tic_disableChannel(&tic, tic_channel);
		}

		Nvic_clearInterruptPending(Nvic_Irq_Timer0_Channel0);
		Nvic_clearInterruptPending(Nvic_Irq_Timer0_Channel1);
		Nvic_clearInterruptPending(Nvic_Irq_Timer0_Channel2);

		Nvic_clearInterruptPending(Nvic_Irq_Timer1_Channel0);
		Nvic_clearInterruptPending(Nvic_Irq_Timer1_Channel1);
		Nvic_clearInterruptPending(Nvic_Irq_Timer1_Channel2);

		Nvic_clearInterruptPending(Nvic_Irq_Timer2_Channel0);
		Nvic_clearInterruptPending(Nvic_Irq_Timer2_Channel1);
		Nvic_clearInterruptPending(Nvic_Irq_Timer2_Channel2);

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
		Nvic_clearInterruptPending(Nvic_Irq_Timer3_Channel0);
		Nvic_clearInterruptPending(Nvic_Irq_Timer3_Channel1);
		Nvic_clearInterruptPending(Nvic_Irq_Timer3_Channel2);
#endif
	}

	static void handleInterrupt(const Tic_Id id, const Tic_Channel channel)
	{
		(void)id;
		lastChannelThatInterrupted = channel;
		interruptOccurred = true;
		Tic_ChannelConfig config;
		assert(id == ticForInterrupts.ticId);

		Tic_getChannelStatus(&ticForInterrupts, channel,
				&channelStatusForInterrupts);
		/// Ensure that interrupt is called only once.
		Tic_getChannelConfig(&ticForInterrupts, channel, &config);
		config.isEnabled = false;
		config.irqConfig.isCounterOverflowIrqEnabled = false;
		config.irqConfig.isRaCompareIrqEnabled = false;
		config.irqConfig.isRbCompareIrqEnabled = false;
		config.irqConfig.isRcCompareIrqEnabled = false;
		Tic_setChannelConfig(&ticForInterrupts, channel, &config);
	}

	static const uint32_t TIMEOUT = 1000000u;
	static const uint32_t DELAY = 10000u;
	static const uint32_t TIC_REG_COMPARE_VALUE = 2000u;

      protected:
	static inline volatile Tic_Channel lastChannelThatInterrupted;
	static inline Tic ticForInterrupts;
	static inline Tic_ChannelStatus channelStatusForInterrupts;
	static inline std::atomic<bool> interruptOccurred;

	static void prepareTimerForRaCompareIrq(
			Tic *const tic, const Tic_Channel channel)
	{
		Tic_ChannelConfig config{};
		config.isEnabled = true;
		config.clockSource = Tic_ClockSelection_MckBy8;
		config.irqConfig.isRaCompareIrqEnabled = true;
		config.modeConfig.waveformModeConfig.waveformMode =
				Tic_WaveformMode_Up;
		config.channelMode = Tic_Mode_Waveform;
		config.modeConfig.waveformModeConfig.ra = TIC_REG_COMPARE_VALUE;

		Tic_setChannelConfig(tic, channel, &config);
		lastChannelThatInterrupted = Tic_Channel_Count;
		const uint32_t raVal = Tic_getRaValue(tic, channel);
		UNSIGNED_LONGS_EQUAL(TIC_REG_COMPARE_VALUE, raVal);
	}

	static void prepareTimerForRbCompareIrq(
			Tic *const tic, const Tic_Channel channel)
	{
		Tic_ChannelConfig config{};
		config.isEnabled = true;
		config.clockSource = Tic_ClockSelection_MckBy8;
		config.irqConfig.isRbCompareIrqEnabled = true;
		config.modeConfig.waveformModeConfig.waveformMode =
				Tic_WaveformMode_Up;
		config.channelMode = Tic_Mode_Waveform;
		config.modeConfig.waveformModeConfig.rb = TIC_REG_COMPARE_VALUE;
		config.modeConfig.waveformModeConfig.externalEventSource =
				Tic_ExternalEventSelection_Xc0; // rb compare irq does not work without this.

		Tic_setChannelConfig(tic, channel, &config);

		lastChannelThatInterrupted = Tic_Channel_Count;
		const uint32_t rbVal = Tic_getRbValue(tic, channel);
		UNSIGNED_LONGS_EQUAL(TIC_REG_COMPARE_VALUE, rbVal);
	}

	static void prepareTimerForRcCompareIrq(
			Tic *const tic, const Tic_Channel channel)
	{
		Tic_ChannelConfig config{};
		config.isEnabled = true;
		config.clockSource = Tic_ClockSelection_MckBy8;
		config.modeConfig.waveformModeConfig.waveformMode =
				Tic_WaveformMode_Up_Rc;
		config.irqConfig.isRcCompareIrqEnabled = true;
		config.channelMode = Tic_Mode_Waveform;
		config.rc = TIC_REG_COMPARE_VALUE;

		Tic_setChannelConfig(tic, channel, &config);

		lastChannelThatInterrupted = Tic_Channel_Count;
		const uint32_t rcVal = Tic_getRcValue(tic, channel);
		UNSIGNED_LONGS_EQUAL(TIC_REG_COMPARE_VALUE, rcVal);
	}

	static void prepareTimerForOverflowIrq(
			Tic *const tic, const Tic_Channel channel)
	{
		Tic_ChannelConfig config{};
		config.isEnabled = true;
		config.clockSource = Tic_ClockSelection_MckBy8;
		config.irqConfig.isCounterOverflowIrqEnabled = true;

		Tic_setChannelConfig(tic, channel, &config);

		lastChannelThatInterrupted = Tic_Channel_Count;
	}

	static void verifyTimerCounts(Tic *const tic, const Tic_Channel channel)
	{
		Tic_ChannelConfig config{};
		config.isEnabled = true;
		config.clockSource = Tic_ClockSelection_MckBy8;
		Tic_setChannelConfig(tic, channel, &config);

		Tic_enableChannel(tic, channel);
		Tic_triggerChannel(tic, channel);

		const uint32_t initialValue = Tic_getCounterValue(tic, channel);

		CHECK_TRUE(evaluateWithTimeout(
				[tic, channel, initialValue]() {
					return Tic_getCounterValue(tic, channel)
							> initialValue;
				},
				TIMEOUT));
	}

	static void verifyIrqOccurs(Tic *const tic, const Tic_Channel channel)
	{
		Tic_enableChannel(tic, channel);
		Tic_triggerChannel(tic, channel);

		CHECK_TRUE(evaluateWithTimeout(
				[channel]() {
					return lastChannelThatInterrupted
							== channel;
				},
				TIMEOUT));
	}

	static void verifyThatTicChannelCounts(
			Tic *const tic, const Tic_Channel channel)
	{
		channelStatusForInterrupts = {};

		verifyTimerCounts(tic, channel);

#if defined(N7S_TARGET_SAMV71Q21)
		prepareTimerForOverflowIrq(tic, channel);
		verifyIrqOccurs(tic, channel);
		CHECK_TRUE(channelStatusForInterrupts.hasCounterOverflowed);
#endif

		prepareTimerForRaCompareIrq(tic, channel);
		verifyIrqOccurs(tic, channel);
		CHECK_TRUE(channelStatusForInterrupts.hasRaCompareOccurred);

		prepareTimerForRbCompareIrq(tic, channel);
		verifyIrqOccurs(tic, channel);
		CHECK_TRUE(channelStatusForInterrupts.hasRbCompareOccurred);

		prepareTimerForRcCompareIrq(tic, channel);
		verifyIrqOccurs(tic, channel);
		CHECK_TRUE(channelStatusForInterrupts.hasRcCompareOccurred);
	}
};

#endif
