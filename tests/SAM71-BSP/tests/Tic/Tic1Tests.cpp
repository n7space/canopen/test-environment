/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Tic1Tests.cpp
/// \brief Tic driver test suite implementation.

#include "TestUtils.hpp"

TEST_GROUP_BASE(TicTests, TestUtils){};

using TicTests = TEST_GROUP_CppUTestGroupTicTests;

/// @name Tic_setChannelConfig()
/// @{

/// \Given initialized Tic 0
///
/// \When Each channel is configured and triggered
///
/// \Then The channel timer counts and interrupt occurs.
///
TEST(TicTests, Tic_Tic0Counts)
{
	Tic_init(&ticForInterrupts, Tic_Id_0);

	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_0);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_1);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_2);
}

/// \Given initialized Tic 1
///
/// \When Each channel is configured and triggered
///
/// \Then The channel timer counts.
///
TEST(TicTests, Tic_Tic1Counts)
{
	Tic_init(&ticForInterrupts, Tic_Id_1);

	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_0);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_1);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_2);
}

/// \Given initialized Tic 2
///
/// \When Each channel is configured and triggered
///
/// \Then The channel timer counts.
///
TEST(TicTests, Tic_Tic2Counts)
{
	Tic_init(&ticForInterrupts, Tic_Id_2);

	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_0);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_1);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_2);
}

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
/// \Given initialized Tic 3
///
/// \When Each channel is configured and triggered
///
/// \Then The channel timer counts.
///
TEST(TicTests, Tic_Tic3Counts)
{
	Tic_init(&ticForInterrupts, Tic_Id_3);

	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_0);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_1);
	verifyThatTicChannelCounts(&ticForInterrupts, Tic_Channel_2);
}
#endif

/// \Given initialized Tic
///
/// \When Clock source is selected
///
/// \Then The relevant registers are set to the correct values.
///
TEST(TicTests, Tic_canSelectClockSource)
{
	Tic tic;
	Tic_ChannelConfig config{};
	Tic_init(&tic, Tic_Id_0);
#if defined(N7S_TARGET_SAMV71Q21)
	config.clockSource = Tic_ClockSelection_Pck6;
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	config.clockSource = Tic_ClockSelection_Gclk;
#else
#error "No target platform specified (missing N7S_TARGET_* macro)"
#endif
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(0u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_MckBy8;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(1u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_MckBy32;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(2u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_MckBy128;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(3u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_Slck;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(4u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_Xc0;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(5u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_Xc1;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(6u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);

	config.clockSource = Tic_ClockSelection_Xc2;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(7u,
			tic.regs->channelRegs[0].cmr & TIC_CMR_CAP_TCCLKS_MASK);
}

/// @}

/// @name Tic_isChannelEnabled()
/// @{

/// \Given initialized Tic
///
/// \When Channel is enabled or disabled
///
/// \Then The channel state changes to enabled or disabled respectively.
///
TEST(TicTests, Tic_enablesAndDisablesChannel)
{
	Tic tic;
	Tic_ChannelConfig config{};
	Tic_init(&tic, Tic_Id_0);
	config.clockSource = Tic_ClockSelection_MckBy8;
	config.isEnabled = false;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	CHECK_FALSE(Tic_isChannelEnabled(&tic, Tic_Channel_0));
	config.isEnabled = true;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	CHECK_TRUE(Tic_isChannelEnabled(&tic, Tic_Channel_0));
}

/// @}

static void
startsChannel(const Tic_ClockSelection clkSrc)
{
	Tic tic;
	Tic_ChannelConfig config{};
	Tic_init(&tic, Tic_Id_0);
	config.clockSource = clkSrc;
	config.isEnabled = true;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	Tic_triggerChannel(&tic, Tic_Channel_0);
	const uint32_t initialValue = Tic_getCounterValue(&tic, Tic_Channel_0);

	CHECK_TRUE(evaluateWithTimeout(
			[tic, initialValue]() {
				return initialValue < Tic_getCounterValue(
						       &tic, Tic_Channel_0);
			},
			TicTests::TIMEOUT));
}

/// @name startsChannel()
/// @{

#if defined(N7S_TARGET_SAMV71Q21)

/// \Given Initialized Tic and initialized PCK6
///
/// \When Channel is started with different source clocks
///
/// \Then The channel starts counting.
///
TEST(TicTests, Tic_startsChannel)
{
	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

	const Pmc_PckConfig pckConfig{
		.isEnabled = true,
		.src = Pmc_PckSrc_Slck,
		.presc = 1u,
	};

	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Pmc_setPckConfig(&pmc, Pmc_PckId_6, &pckConfig,
			PMC_DEFAULT_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	startsChannel(Tic_ClockSelection_MckBy128);
	startsChannel(Tic_ClockSelection_MckBy32);
	startsChannel(Tic_ClockSelection_MckBy8);
	startsChannel(Tic_ClockSelection_Pck6);
	startsChannel(Tic_ClockSelection_Slck);
}
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)

/// \Given Initialized Tic
///
/// \When Channel is started with different source clocks
///
/// \Then The channel starts counting.
///
TEST(TicTests, Tic_startsChannel)
{
	startsChannel(Tic_ClockSelection_MckBy128);
	startsChannel(Tic_ClockSelection_MckBy32);
	startsChannel(Tic_ClockSelection_MckBy8);
	startsChannel(Tic_ClockSelection_Gclk);
	startsChannel(Tic_ClockSelection_Slck);
}
#else
#error "No target platform specified (missing N7S_TARGET_* macro)"
#endif

/// @}
