/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Tic2Tests.cpp
/// \brief Tic driver test suite implementation.

#include <n7s/utils/Utils.h>

#include "TestUtils.hpp"

TEST_GROUP_BASE(TicTests, TestUtils){};

using TicTests = TEST_GROUP_CppUTestGroupTicTests;

#ifndef DOXYGEN

constexpr static Tic_ChannelConfig defaultWaveformChannelConfig = {
  .isEnabled = true,
  .clockSource = Tic_ClockSelection_MckBy128,
  .isClockInverted = true,
  .burst = Tic_BurstSelection_None,
  .channelMode = Tic_Mode_Waveform,
  .modeConfig = {
    .captureModeConfig = {
      .isStoppedOnRbLoading = false,
      .isDisabledOnRbLoading = false,
      .externalTriggerEdge = Tic_EdgeSelection_None,
      .triggerSource = Tic_SignalTriggerSelection_Tiob,
      .isTriggeredByRcCompare = false,
      .raLoadingEdgeSelection = Tic_EdgeSelection_None,
      .rbLoadingEdgeSelection = Tic_EdgeSelection_None,
      .loadingEdgeSubsampling = Tic_EdgeSubsampling_One,
    },
    .waveformModeConfig = {
      .isStoppedOnRcCompare = false,
      .isDisabledOnRcCompare = false,
      .externalEvent = Tic_EdgeSelection_None,
      .externalEventSource = Tic_ExternalEventSelection_Tiob,
      .isTriggeredByExternalEvent = false,
      .waveformMode = Tic_WaveformMode_UpDown,
      .raCompareEffectOnTioa = Tic_TioEffect_Clear,
      .rcCompareEffectOnTioa = Tic_TioEffect_Set,
      .externalEventEffectOnTioa = Tic_TioEffect_None,
      .triggerEffectOnTioa = Tic_TioEffect_None,
      .rbCompareEffectOnTiob = Tic_TioEffect_Clear,
      .rcCompareEffectOnTiob = Tic_TioEffect_Set,
      .externalEventEffectOnTiob = Tic_TioEffect_None,
      .triggerEffectOnTiob = Tic_TioEffect_None,
      .ra = 2048,
      .rb = 4096,
    }
  },
  .isGrayCounterEnabled = false,
  .doesGrayCounterCountDown = false,
  .irqConfig = {
    .isCounterOverflowIrqEnabled = false,
    .isLoadOverrunIrqEnabled = false,
    .isRaCompareIrqEnabled = false,
    .isRbCompareIrqEnabled = false,
    .isRcCompareIrqEnabled = false,
    .isRaLoadingIrqEnabled = false,
    .isRbLoadingIrqEnabled = false,
    .isExternalTriggerIrqEnabled = false,
  },
  .triggerSourceForInputA = Tic_TriggerSourceForInput_ExternalTio,
  .triggerSourceForInputB = Tic_TriggerSourceForInput_ExternalTio,
  .useUndividedPck = false,
  .rc = 8192,
};

#endif // DOXYGEN

/// @name Tic_setChannelConfig()
/// @{

/// \Given initialized Tic
///
/// \When Channel is configured in the waveform mode
///
/// \Then The relevant registers are set to correct values and the configuration can be read back.
///
TEST(TicTests, Tic_configuresWaveformMode)
{
	Tic tic;
	Tic_ChannelConfig config = defaultWaveformChannelConfig;
	Tic_init(&tic, Tic_Id_0);
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);

	CHECK_COMPARE((tic.regs->channelRegs[0].cmr & TIC_CMR_WVF_WAVE_MASK),
			!=, 0u);
	ENUMS_EQUAL_INT(config.modeConfig.waveformModeConfig.waveformMode,
			(tic.regs->channelRegs[0].cmr >> 13u) & 0x3u);
	ENUMS_EQUAL_INT(config.clockSource,
			(tic.regs->channelRegs[0].cmr >> 0u) & 0x7u);
	UNSIGNED_LONGS_EQUAL(config.modeConfig.waveformModeConfig.ra,
			tic.regs->channelRegs[0].ra);
	UNSIGNED_LONGS_EQUAL(config.modeConfig.waveformModeConfig.rb,
			tic.regs->channelRegs[0].rb);
	UNSIGNED_LONGS_EQUAL(config.rc, tic.regs->channelRegs[0].rc);
	ENUMS_EQUAL_INT(config.modeConfig.waveformModeConfig
					.raCompareEffectOnTioa,
			(tic.regs->channelRegs[0].cmr >> 16u) & 0x3u);
	ENUMS_EQUAL_INT(config.modeConfig.waveformModeConfig
					.rcCompareEffectOnTioa,
			(tic.regs->channelRegs[0].cmr >> 18u) & 0x3u);
	ENUMS_EQUAL_INT(config.modeConfig.waveformModeConfig
					.rbCompareEffectOnTiob,
			(tic.regs->channelRegs[0].cmr >> 24u) & 0x3u);
	ENUMS_EQUAL_INT(config.modeConfig.waveformModeConfig
					.rcCompareEffectOnTiob,
			(tic.regs->channelRegs[0].cmr >> 26u) & 0x3u);

	config.modeConfig.waveformModeConfig.isTriggeredByExternalEvent = true;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	UNSIGNED_LONGS_EQUAL(1, (tic.regs->channelRegs[0].cmr >> 12u) & 0x1u);

	Tic_ChannelConfig readConfig{};
	Tic_getChannelConfig(&tic, Tic_Channel_0, &readConfig);
	MEMCMP_EQUAL(&readConfig, &config, sizeof(Tic_ChannelConfig));
}

/// @}

/// @name Tic_setChannelConfig()
/// @{

/// \Given initialized Tic
///
/// \When Channel is configured in the capture mode
///
/// \Then The relevant registers are set to correct values and the configuration can be read back.
///
/// \Note Pulse timing, frequency, period, duty cycle, etc. are measured via a combination
///       of settings - subsampling, edge choice and consequent reads of RA and RB values.
///
/// \Note This tests does not demonstrate frequency/etc. measurement, but demonstrates that
///       the required facilities work.
///
TEST(TicTests, Tic_configuresCaptureMode)
{
	const Tic_ChannelConfig defaultCaptureChannelConfig = {
    .isEnabled = false,
    .clockSource = Tic_ClockSelection_MckBy32,
    .isClockInverted = false,
    .burst = Tic_BurstSelection_None,
    .channelMode = Tic_Mode_Capture,
    .modeConfig = {
      .captureModeConfig = {
        .isStoppedOnRbLoading = true,
        .isDisabledOnRbLoading = true,
        .externalTriggerEdge = Tic_EdgeSelection_None,
        .triggerSource = Tic_SignalTriggerSelection_Tioa,
        .isTriggeredByRcCompare = false,
        .raLoadingEdgeSelection = Tic_EdgeSelection_Rising,
        .rbLoadingEdgeSelection = Tic_EdgeSelection_Falling,
        .loadingEdgeSubsampling = Tic_EdgeSubsampling_Fourth,
      },
      .waveformModeConfig = {
        .isStoppedOnRcCompare = false,
        .isDisabledOnRcCompare = false,
        .externalEvent = Tic_EdgeSelection_None,
        .externalEventSource = Tic_ExternalEventSelection_Tiob,
        .isTriggeredByExternalEvent = false,
        .waveformMode = Tic_WaveformMode_Up,
        .raCompareEffectOnTioa = Tic_TioEffect_None,
        .rcCompareEffectOnTioa = Tic_TioEffect_None,
        .externalEventEffectOnTioa = Tic_TioEffect_None,
        .triggerEffectOnTioa = Tic_TioEffect_None,
        .rbCompareEffectOnTiob = Tic_TioEffect_None,
        .rcCompareEffectOnTiob = Tic_TioEffect_None,
        .externalEventEffectOnTiob = Tic_TioEffect_None,
        .triggerEffectOnTiob = Tic_TioEffect_None,
        .ra = 0,
        .rb = 0,
      }
    },
    .isGrayCounterEnabled = false,
    .doesGrayCounterCountDown = false,
    .irqConfig = {
      .isCounterOverflowIrqEnabled = false,
      .isLoadOverrunIrqEnabled = false,
      .isRaCompareIrqEnabled = false,
      .isRbCompareIrqEnabled = false,
      .isRcCompareIrqEnabled = false,
      .isRaLoadingIrqEnabled = false,
      .isRbLoadingIrqEnabled = false,
      .isExternalTriggerIrqEnabled = false,
    },
    .triggerSourceForInputA = Tic_TriggerSourceForInput_ExternalTio,
    .triggerSourceForInputB = Tic_TriggerSourceForInput_ExternalTio,
    .useUndividedPck = false,
    .rc = 0u,
  };

	Tic tic;
	Tic_init(&tic, Tic_Id_0);
	Tic_setChannelConfig(&tic, Tic_Channel_0, &defaultCaptureChannelConfig);
	UNSIGNED_LONGS_EQUAL(0u,
			(tic.regs->channelRegs[0].cmr & TIC_CMR_WVF_WAVE_MASK));
	ENUMS_EQUAL_INT(defaultCaptureChannelConfig.clockSource,
			(tic.regs->channelRegs[0].cmr >> 0u) & 0x7u);
	UNSIGNED_LONGS_EQUAL(defaultCaptureChannelConfig.rc,
			tic.regs->channelRegs[0].rc);
	UNSIGNED_LONGS_EQUAL(
			1u, // LDBSTOP, to avoid using TIC_CMR_LDBSTOP_MASK directly.
			(tic.regs->channelRegs[0].cmr >> 6u) & 0x1u);
	UNSIGNED_LONGS_EQUAL(
			1u, // LDBDIS, to avoid using TIC_CMR_LDBDIS_MASK directly.
			(tic.regs->channelRegs[0].cmr >> 7u) & 0x1u);
	ENUMS_EQUAL_INT(defaultCaptureChannelConfig.modeConfig.captureModeConfig
					.raLoadingEdgeSelection,
			(tic.regs->channelRegs[0].cmr >> 16u) & 0x3u);
	ENUMS_EQUAL_INT(defaultCaptureChannelConfig.modeConfig.captureModeConfig
					.rbLoadingEdgeSelection,
			(tic.regs->channelRegs[0].cmr >> 18u) & 0x3u);
	ENUMS_EQUAL_INT(defaultCaptureChannelConfig.modeConfig.captureModeConfig
					.loadingEdgeSubsampling,
			(tic.regs->channelRegs[0].cmr >> 20u) & 0x7u);

	Tic_ChannelConfig readConfig{};
	Tic_getChannelConfig(&tic, Tic_Channel_0, &readConfig);
	MEMCMP_EQUAL(&readConfig, &defaultCaptureChannelConfig,
			sizeof(Tic_ChannelConfig));
}

/// \Given initialized Tic
///
/// \When Channel is configured to produce gray count
///
/// \Then The relevant registers are set to correct values.
///
TEST(TicTests, Tic_configuresGrayCounter)
{
	Tic tic;
	Tic_ChannelConfig config = defaultWaveformChannelConfig;
	config.rc = 4;
	config.isGrayCounterEnabled = true;
	config.doesGrayCounterCountDown = true;
	Tic_init(&tic, Tic_Id_0);
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	CHECK_COMPARE((tic.regs->channelRegs[0].cmr & TIC_CMR_WVF_WAVE_MASK),
			!=, 0u);
	ENUMS_EQUAL_INT(config.clockSource,
			(tic.regs->channelRegs[0].cmr >> 0u) & 0x7u);
	UNSIGNED_LONGS_EQUAL(config.rc, tic.regs->channelRegs[0].rc);

	// GCEN, to avoid using TIC_SMMR_GCEN_MASK directly.
	CHECK_COMPARE((tic.regs->channelRegs[0].smmr & 0x1u), !=, 0u);

	// DOWN, to avoid using TIC_SMMR_DOWN_MASK directly.
	CHECK_COMPARE((tic.regs->channelRegs[0].smmr & 0x2u), !=, 0u);
}

/// @}

/// @name Tic_writeProtect()
/// @{

/// \Given initialized Tic,
///
/// \When register write protection is set to be enabled,
///
/// \Then the write protection is enabled and the registers cannot be updated.
///
TEST(TicTests, Tic_writeProtect_enablesWriteProtection)
{
	Tic tic;
	Tic_init(&tic, Tic_Id_0);

	Tic_enableChannel(&tic, Tic_Channel_0);

	Tic_ChannelConfig setConfig{};
	Tic_ChannelConfig readConfig{};

	setConfig.rc = TIC_REG_COMPARE_VALUE;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &setConfig);

	Tic_writeProtect(&tic, true);

	const uint32_t wpmr = tic.regs->wpmr;
	CHECK_COMPARE((wpmr & TIC_WPMR_WPEN_MASK), !=, 0u);

	setConfig.rc = 0u;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &setConfig);
	Tic_getChannelConfig(&tic, Tic_Channel_0, &readConfig);
	CHECK_COMPARE(setConfig.rc, !=, readConfig.rc);
}

/// \Given initialized Tic, with enabled write protection,
///
/// \When register write protection is set to be disabled,
///
/// \Then the write protection is disabled and the registers can be updated.
///
TEST(TicTests, Tic_writeProtect_disablesWriteProtection)
{
	Tic tic;
	Tic_init(&tic, Tic_Id_0);

	Tic_enableChannel(&tic, Tic_Channel_0);
	Tic_writeProtect(&tic, true);
	const Tic_ChannelConfig setConfig{};
	Tic_ChannelConfig readConfig{};

	Tic_writeProtect(&tic, false);

	Tic_setChannelConfig(&tic, Tic_Channel_0, &setConfig);
	Tic_getChannelConfig(&tic, Tic_Channel_0, &readConfig);
	UNSIGNED_LONGS_EQUAL(readConfig.rc, setConfig.rc);

	const uint32_t wpmr = tic.regs->wpmr;
	UNSIGNED_LONGS_EQUAL(0u, (wpmr & TIC_WPMR_WPEN_MASK));
}

/// @}

/// @name Tic_syncAllChannels()
/// @{

/// \Given initialized all Tic channels
///
/// \When Tic_syncAllChannels() is called
///
/// \Then All channels shall start the countdown.
///
TEST(TicTests, Tic_syncAllChannels_triggersChannelsAtOnce)
{
	Tic tic;
	Tic_ChannelConfig config{};
	Tic_init(&tic, Tic_Id_0);
	config.clockSource = Tic_ClockSelection_MckBy8;
	config.isEnabled = true;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	Tic_setChannelConfig(&tic, Tic_Channel_1, &config);
	Tic_setChannelConfig(&tic, Tic_Channel_2, &config);

	Tic_syncAllChannels(&tic);
	const uint32_t initialValueCh0 =
			Tic_getCounterValue(&tic, Tic_Channel_0);
	const uint32_t initialValueCh1 =
			Tic_getCounterValue(&tic, Tic_Channel_1);
	const uint32_t initialValueCh2 =
			Tic_getCounterValue(&tic, Tic_Channel_2);

	CHECK_TRUE(evaluateWithTimeout(
			[tic, initialValueCh0, initialValueCh1,
					initialValueCh2]() {
				return ((Tic_getCounterValue(
							 &tic, Tic_Channel_0)
							> initialValueCh0)
						&& (Tic_getCounterValue(&tic,
								    Tic_Channel_1)
								> initialValueCh1)
						&& (Tic_getCounterValue(&tic,
								    Tic_Channel_2)
								> initialValueCh2));
			},
			TIMEOUT));
}

/// @}

/// @name Tic_init()
/// @{

/// \Given uninitialized Tic
///
/// \When Tic_init() is called with each of the four valid Tic IDs
///
/// \Then register addresses are set to correct values.
///
TEST(TicTests, Tic_init_setsCorrectRegisterAddresses)
{
	Tic tic0{};
	Tic tic1{};
	Tic tic2{};

	Tic_init(&tic0, Tic_Id_0);
	Tic_init(&tic1, Tic_Id_1);
	Tic_init(&tic2, Tic_Id_2);
#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
	Tic tic3{};
	Tic_init(&tic3, Tic_Id_3);
#endif

#if defined(N7S_TARGET_SAMV71Q21)
	UNSIGNED_LONGS_EQUAL(0x4000C000u, tic0.regs);
	UNSIGNED_LONGS_EQUAL(0x40010000u, tic1.regs);
	UNSIGNED_LONGS_EQUAL(0x40014000u, tic2.regs);
	UNSIGNED_LONGS_EQUAL(0x40054000u, tic3.regs);
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	UNSIGNED_LONGS_EQUAL(0x40070000u, tic0.regs);
	UNSIGNED_LONGS_EQUAL(0x40074000u, tic1.regs);
	UNSIGNED_LONGS_EQUAL(0x40078000u, tic2.regs);
#endif
#if defined(N7S_TARGET_SAMRH71F20)
	UNSIGNED_LONGS_EQUAL(0x4007C000u, tic3.regs);
#endif
}

/// @}

/// @name Tic_configureExternalClockSignals()
/// @{

/// \Given Tic initalized with dummy registers
///
/// \When Tic_configureExternalClockSignals() is called
///
/// \Then Tic bmr register is filled correctly
///
TEST(TicTests, Tic_configureExternalClockSignals_fillsBmrRegister)
{
	Tic tic{};
	Tic_Registers dummyRegs{};
	tic.regs = &dummyRegs;

	Tic_ExternalClockSignalSelection externalClockSignals = {
		.xc0 = Tic_ExternalClock0SignalSelection_Tioa1,
		.xc1 = Tic_ExternalClock1SignalSelection_Tioa2,
		.xc2 = Tic_ExternalClock2SignalSelection_Tioa1,
	};

	Tic_configureExternalClockSignals(&tic, &externalClockSignals);

	UNSIGNED_LONGS_EQUAL(dummyRegs.bmr, (2u | (3u << 2u) | (3u << 4u)));

	externalClockSignals = {
		.xc0 = Tic_ExternalClock0SignalSelection_Tioa2,
		.xc1 = Tic_ExternalClock1SignalSelection_Tioa0,
		.xc2 = Tic_ExternalClock2SignalSelection_Tclk2,
	};

	Tic_configureExternalClockSignals(&tic, &externalClockSignals);

	UNSIGNED_LONGS_EQUAL(dummyRegs.bmr, (3u | (2u << 2u)));
}

/// @}

/// @name Tic_setChannelIrqConfig()
/// @{

/// \Given Tic initalized with dummy registers
///
/// \When Tic_setChannelIrqConfig() is called with IRQs enabled
///
/// \Then Tic channel ier and idr registers are filled correctly
///
TEST(TicTests, Tic_setChannelIrqConfig_enablesIRQs)
{
	Tic tic{};
	Tic_Registers dummyRegs{};
	tic.regs = &dummyRegs;

	const Tic_ChannelIrqConfig all_disabled = {};

	const Tic_ChannelIrqConfig all_enabled = {
		.isCounterOverflowIrqEnabled = true,
		.isLoadOverrunIrqEnabled = true,
		.isRaCompareIrqEnabled = true,
		.isRbCompareIrqEnabled = true,
		.isRcCompareIrqEnabled = true,
		.isRaLoadingIrqEnabled = true,
		.isRbLoadingIrqEnabled = true,
		.isExternalTriggerIrqEnabled = true,
	};

	const Tic_ChannelIrqConfig irq_config0 = {
		.isCounterOverflowIrqEnabled = true,
		.isLoadOverrunIrqEnabled = false,
		.isRaCompareIrqEnabled = false,
		.isRbCompareIrqEnabled = true,
		.isRcCompareIrqEnabled = false,
		.isRaLoadingIrqEnabled = true,
		.isRbLoadingIrqEnabled = true,
		.isExternalTriggerIrqEnabled = true,
	};

	const Tic_ChannelIrqConfig irq_config1 = {
		.isCounterOverflowIrqEnabled = true,
		.isLoadOverrunIrqEnabled = false,
		.isRaCompareIrqEnabled = true,
		.isRbCompareIrqEnabled = false,
		.isRcCompareIrqEnabled = true,
		.isRaLoadingIrqEnabled = true,
		.isRbLoadingIrqEnabled = false,
		.isExternalTriggerIrqEnabled = false,
	};

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[0].ier, 0u);
	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[0].idr, 0u);

	Tic_setChannelIrqConfig(&tic, Tic_Channel_0, &all_enabled);

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[0].ier, 0xFFu);
	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[0].idr, 0u);

	Tic_setChannelIrqConfig(&tic, Tic_Channel_0, &all_disabled);

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[0].idr, 0xFFu);

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[1].ier, 0u);
	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[1].idr, 0u);

	Tic_setChannelIrqConfig(&tic, Tic_Channel_1, &irq_config0);

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[1].ier, 0xE9u);
	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[1].idr, 0x16u);

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[2].ier, 0u);
	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[2].idr, 0u);

	Tic_setChannelIrqConfig(&tic, Tic_Channel_2, &irq_config1);

	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[2].ier, 0x35u);
	UNSIGNED_LONGS_EQUAL(dummyRegs.channelRegs[2].idr, 0xCAu);
}

/// @}

/// @name Tic_getChannelStatus()
/// @{

/// \Given Tic initialized with dummy registers
///
/// \When Tic_getChannelStatus() is called
///
/// \Then Returned status represents state stored in the registers
///
TEST(TicTests, Tic_getChannelStatus_correctlyRetrievesStatus)
{
	Tic tic{};
	Tic_Registers dummyRegs{};
	tic.regs = &dummyRegs;

	Tic_ChannelStatus status{};

	// set different set of status flags in SR register for each TIC channel
	// CH0: overflow, Ra & Rc compare, enabled
	dummyRegs.channelRegs[0].sr = 0x10015u;
	// CH1: overflow, Rb & Rc compare, Rb load, ext. trigger, TioB asserted
	dummyRegs.channelRegs[1].sr = 0x400D9u;
	// CH2: load overrun, Ra compare, Ra & Rb load, ext. trigger, TioA asserted
	dummyRegs.channelRegs[2].sr = 0x200E6u;

	Tic_getChannelStatus(&tic, Tic_Channel_0, &status);

	CHECK_TRUE(status.hasCounterOverflowed);
	CHECK_FALSE(status.hasLoadOverrunOccurred);
	CHECK_TRUE(status.hasRaCompareOccurred);
	CHECK_FALSE(status.hasRbCompareOccurred);
	CHECK_TRUE(status.hasRcCompareOccurred);
	CHECK_FALSE(status.hasRaLoadOccurred);
	CHECK_FALSE(status.hasRbLoadOccurred);
	CHECK_FALSE(status.hasExternalTriggerOccurred);
	CHECK_TRUE(status.isChannelEnabled);
	CHECK_FALSE(status.isTioaAsserted);
	CHECK_FALSE(status.isTiobAsserted);

	Tic_getChannelStatus(&tic, Tic_Channel_1, &status);

	CHECK_TRUE(status.hasCounterOverflowed);
	CHECK_FALSE(status.hasLoadOverrunOccurred);
	CHECK_FALSE(status.hasRaCompareOccurred);
	CHECK_TRUE(status.hasRbCompareOccurred);
	CHECK_TRUE(status.hasRcCompareOccurred);
	CHECK_FALSE(status.hasRaLoadOccurred);
	CHECK_TRUE(status.hasRbLoadOccurred);
	CHECK_TRUE(status.hasExternalTriggerOccurred);
	CHECK_FALSE(status.isChannelEnabled);
	CHECK_FALSE(status.isTioaAsserted);
	CHECK_TRUE(status.isTiobAsserted);

	Tic_getChannelStatus(&tic, Tic_Channel_2, &status);

	CHECK_FALSE(status.hasCounterOverflowed);
	CHECK_TRUE(status.hasLoadOverrunOccurred);
	CHECK_TRUE(status.hasRaCompareOccurred);
	CHECK_FALSE(status.hasRbCompareOccurred);
	CHECK_FALSE(status.hasRcCompareOccurred);
	CHECK_TRUE(status.hasRaLoadOccurred);
	CHECK_TRUE(status.hasRbLoadOccurred);
	CHECK_TRUE(status.hasExternalTriggerOccurred);
	CHECK_FALSE(status.isChannelEnabled);
	CHECK_TRUE(status.isTioaAsserted);
	CHECK_FALSE(status.isTiobAsserted);
}

/// \Given initialized Tic
///
/// \When Channel is configured to disable clock on Rc compare and triggered
///
/// \Then Clock is disabled on Rc compare event
///
TEST(TicTests, Tic_RcCompareCanDisableChannel)
{
	const uint32_t rcCompareValue = 1024;

	Tic tic;
	Tic_ChannelConfig config = defaultWaveformChannelConfig;
	config.modeConfig.waveformModeConfig.isDisabledOnRcCompare = true;
	config.rc = rcCompareValue;

	Tic_init(&tic, Tic_Id_0);
	config.clockSource = Tic_ClockSelection_MckBy8;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	CHECK_TRUE(Tic_isChannelEnabled(&tic, Tic_Channel_0));

	Tic_triggerChannel(&tic, Tic_Channel_0);

	CHECK_TRUE(evaluateWithTimeout(
			[tic]() {
				return rcCompareValue
						== Tic_getCounterValue(&tic,
								Tic_Channel_0);
			},
			TicTests::TIMEOUT));

	CHECK_FALSE(Tic_isChannelEnabled(&tic, Tic_Channel_0));

	busyWaitLoop(TicTests::DELAY);

	CHECK_FALSE(Tic_isChannelEnabled(&tic, Tic_Channel_0));
	UNSIGNED_LONGS_EQUAL(rcCompareValue,
			Tic_getCounterValue(&tic, Tic_Channel_0));
}

/// \Given initialized Tic
///
/// \When Channel is configured to stop clock on Rc compare and triggered
///
/// \Then Clock is stopped on Rc compare event
///
TEST(TicTests, Tic_RcCompareCanStopChannel)
{
	const uint32_t rcCompareValue = 1024;

	Tic tic;
	Tic_ChannelConfig config = defaultWaveformChannelConfig;
	config.modeConfig.waveformModeConfig.isStoppedOnRcCompare = true;
	config.rc = rcCompareValue;

	Tic_init(&tic, Tic_Id_0);
	config.clockSource = Tic_ClockSelection_MckBy8;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	CHECK_TRUE(Tic_isChannelEnabled(&tic, Tic_Channel_0));

	Tic_triggerChannel(&tic, Tic_Channel_0);

	CHECK_TRUE(evaluateWithTimeout(
			[tic]() {
				return rcCompareValue
						== Tic_getCounterValue(&tic,
								Tic_Channel_0);
			},
			TicTests::TIMEOUT));

	CHECK_TRUE(Tic_isChannelEnabled(&tic, Tic_Channel_0));

	busyWaitLoop(TicTests::DELAY);

	CHECK_TRUE(Tic_isChannelEnabled(&tic, Tic_Channel_0));
	UNSIGNED_LONGS_EQUAL(rcCompareValue,
			Tic_getCounterValue(&tic, Tic_Channel_0));
}

/// @}

// Tests below may assert()
#if defined(NDEBUG)

template <typename T>
static inline bool
compareContents(const T &lhs, const T &rhs)
{
	return memcmp(&lhs, &rhs, sizeof(T)) == 0;
}

/// @name Tic_init()
/// @{

/// \Given  uninitialized Tic
///
/// \When Tic_init() is called with invalid Tic ID
///
/// \Then Tic is zeroed.
///
TEST(TicTests, Tic_init_doesNotSetRegisterAddressesOnInvalidMode)
{
	Tic tic{};
	tic.regs = reinterpret_cast<Tic_Registers *>(0x12345678u);
	auto invalidId = arbitraryEnumValue<Tic_Id>(0xDEADu);

	Tic_init(&tic, invalidId);

	CHECK_TRUE(compareContents(tic, Tic{}));
}

/// @}

/// @name Tic_setChannelConfig()
/// @{

/// \Given Tic initalized with dummy registers
///
/// \When Tic_setChannelConfig() is called with invalid mode
///
/// \Then Tic registers are left unmodified.
///
TEST(TicTests, Tic_setChannelConfig_doesNotModifyRegistersOnInvalidInput)
{
	Tic tic{};
	Tic_Registers dummyRegs{};
	tic.regs = &dummyRegs;

	Tic_ChannelConfig channelConfig = defaultWaveformChannelConfig;
	channelConfig.channelMode = arbitraryEnumValue<Tic_Mode>(0xDEADu);

	Tic_setChannelConfig(&tic, Tic_Channel_0, &channelConfig);

	// All registers should still be 0
	CHECK_TRUE(compareContents(dummyRegs, Tic_Registers{}));
}

/// @}

#endif
