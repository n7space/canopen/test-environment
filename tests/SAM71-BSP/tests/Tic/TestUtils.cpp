/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  TestUtils.cpp
/// \brief Test utils containing interrupt handlers for Tic.

#include "TestUtils.hpp"

#ifndef DOXYGEN

void
TC0CH0_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_0, Tic_Channel_0);
}

void
TC0CH1_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_0, Tic_Channel_1);
}

void
TC0CH2_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_0, Tic_Channel_2);
}

void
TC1CH0_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_1, Tic_Channel_0);
}

void
TC1CH1_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_1, Tic_Channel_1);
}

void
TC1CH2_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_1, Tic_Channel_2);
}

void
TC2CH0_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_2, Tic_Channel_0);
}

void
TC2CH1_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_2, Tic_Channel_1);
}

void
TC2CH2_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_2, Tic_Channel_2);
}

#if defined(N7S_TARGET_SAMV71Q21) || defined(N7S_TARGET_SAMRH71F20)
void
TC3CH0_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_3, Tic_Channel_0);
}

void
TC3CH1_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_3, Tic_Channel_1);
}

void
TC3CH2_Handler(void)
{
	TestUtils::handleInterrupt(Tic_Id_3, Tic_Channel_2);
}
#endif

#endif // DOXYGEN
