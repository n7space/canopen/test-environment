/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  SystickTests.cpp
/// \brief Systick driver test suite implementation.

#include <algorithm>
#include <atomic>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Systick/Systick.h>
#include <n7s/bsp/Systick/SystickRegisters.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsStartup/TestsStartup.h>

TEST_GROUP(SystickTests)
{
	static inline const uint32_t SYSTICKTEST_TIMEOUT = (1024u * 1024u);
	static inline const uint64_t NANOSECONDS_IN_SECOND = 1000000000uLL;

	// allow 5 ticks of slow clock mismatch
	static inline const uint32_t ALLOWED_TIMER_MISCOUNT = (5u
			* (static_cast<uint32_t>(NANOSECONDS_IN_SECOND)
					/ PMC_SLOW_CLOCK_FREQ));

	static inline std::atomic<bool> systickHandlerCalled = false;
	static inline uint32_t SystickTests_referenceValue = 0u;

	static inline Systick systick{};

	void setup() override
	{
		systickHandlerCalled = false;
		Systick_init(&systick, Systick_getDeviceRegisterStartAddress());
	}

	void teardown() override
	{
		const Systick_Config config{
			.clockSource = Systick_ClockSource_ProcessorClock,
			.isInterruptEnabled = false,
			.isEnabled = false,
			.reloadValue = 10000u,
		};

		Systick_setConfig(&systick, &config);
		Systick_clearCurrentValue(&systick);
		systickHandlerCalled = false;
	}
};

using SystickTests = TEST_GROUP_CppUTestGroupSystickTests;

#ifndef DOXYGEN
void
SysTick_Handler(void)
{
	SystickTests::systickHandlerCalled = true;
}
#endif

/// @name Systick_init()
/// @{

/// \Given an uninitialized SysTick driver,
///
/// \When Systick_init() function is called with SysTick descriptor and driver base address,
///
/// \Then SysTick driver is initialized and registers are mapped to correct addresses in memory.
///
TEST(SystickTests, init_initsSysTickDriver)
{
	Systick systickInitTest = {};
	Systick_init(&systickInitTest, Systick_getDeviceRegisterStartAddress());
	POINTERS_EQUAL(reinterpret_cast<void *>(SYSTICK_ADDRESS_BASE),
			systickInitTest.registers);

	POINTERS_EQUAL(0xe000e010u, &systickInitTest.registers->csr);
	POINTERS_EQUAL(0xe000e014u, &systickInitTest.registers->rvr);
	POINTERS_EQUAL(0xe000e018u, &systickInitTest.registers->cvr);
	POINTERS_EQUAL(0xe000e01cu, &systickInitTest.registers->calib);
}

/// @}

/// @name Systick_getCurrentValue()
/// @{

/// \Given an initialized and configured SysTick driver,
///
/// \When Systick_getCurrentValue() function is called with SysTick descriptor, current time value
///       is queried twice,
///
/// \Then the value decreases over time.
///
TEST(SystickTests, getCurrentValue_countsDown)
{
	const Systick_Config config{
		.clockSource = Systick_ClockSource_ProcessorClock,
		.isInterruptEnabled = false,
		.isEnabled = true,
		.reloadValue = 10000u,
	};

	auto countDownLambda = []() -> bool {
		const uint32_t currentValue = Systick_getCurrentValue(&systick);
		return isBetweenUint32(
				currentValue, 0u, SystickTests_referenceValue);
	};

	auto reloadedLambda = []() -> bool {
		return Systick_getCurrentValue(&systick) > 0u;
	};

	Systick_setConfig(&systick, &config);
	Systick_clearCurrentValue(&systick);
	CHECK_TRUE(evaluateLambdaWithTimeout(
			reloadedLambda, SYSTICKTEST_TIMEOUT));

	SystickTests_referenceValue = Systick_getCurrentValue(&systick);
	CHECK_TRUE(evaluateLambdaWithTimeout(
			countDownLambda, SYSTICKTEST_TIMEOUT));
}

/// @}

/// @name Systick_setConfig(), Systick_getConfig()
/// @{

/// \Given an initialized SysTick driver,
///
/// \When Systick_setConfig() function is called with SysTick descriptor and configuration
///       structure pointer sets different SysTick periods,
///
/// \Then Systick_getConfig() function returns proper configuration of SysTick driver.
///
TEST(SystickTests, Systick_setsAndGetsConfiguration)
{
	Systick_Config setConfig = {
		.clockSource = Systick_ClockSource_ProcessorClock,
		.isInterruptEnabled = true,
		.isEnabled = true,
		.reloadValue = 1969u,
	};

	Systick_Config gotConfig{};

	Systick_setConfig(&systick, &setConfig);
	Systick_getConfig(&systick, &gotConfig);
	ENUMS_EQUAL_INT(gotConfig.clockSource, setConfig.clockSource);
	CHECK_EQUAL(setConfig.isInterruptEnabled, gotConfig.isInterruptEnabled);
	CHECK_EQUAL(setConfig.isEnabled, gotConfig.isEnabled);
	ENUMS_EQUAL_INT(gotConfig.reloadValue, setConfig.reloadValue);

	const uint32_t reloadValue = 1410u;
	setConfig.clockSource = Systick_ClockSource_ImplementationDefined;
	setConfig.isInterruptEnabled = false;
	setConfig.isEnabled = false;
	setConfig.reloadValue = reloadValue;

	Systick_setConfig(&systick, &setConfig);
	Systick_getConfig(&systick, &gotConfig);
	ENUMS_EQUAL_INT(gotConfig.clockSource, setConfig.clockSource);
	CHECK_EQUAL(setConfig.isInterruptEnabled, gotConfig.isInterruptEnabled);
	CHECK_EQUAL(setConfig.isEnabled, gotConfig.isEnabled);
	UNSIGNED_LONGS_EQUAL(gotConfig.reloadValue, setConfig.reloadValue);
}

/// @}

/// @name Systick_hasCountedToZero()
/// @{

/// \Given an initialized and configured SysTick driver,
///
/// \When Systick_hasCountedToZero() is called with SysTick descriptor to check if countdown
///       reaches zero,
///
/// \Then countdown reaches zero, interrupt and dedicated flag are asserted.
///
TEST(SystickTests, hasCountedToZero_triggersInterrupt)
{
	const Systick_Config config{
		.clockSource = Systick_ClockSource_ProcessorClock,
		.isInterruptEnabled = true,
		.isEnabled = true,
		.reloadValue = 10000u,
	};

	Systick_setConfig(&systick, &config);

	auto handlerCalledLambda = []() -> bool {
		return systickHandlerCalled;
	};

	CHECK_TRUE(evaluateLambdaWithTimeout(
			handlerCalledLambda, SYSTICKTEST_TIMEOUT));
	CHECK_TRUE(Systick_hasCountedToZero(&systick));
}

/// @}

/// @name Systick_getImplementationInformation()
/// @{

/// \Given an initialized SysTick driver,
///
/// \When Systick_getCalibrationInformation() is called with SysTick descriptor and calibration
///       information structure pointer,
///
/// \Then the calibration information is returned and matches the one in the HW manual.
///
TEST(SystickTests, getCalibartionInformation_getsCalibrationInformation)
{
	Systick_CalibrationInformation info{};

	Systick_getCalibrationInformation(&systick, &info);
	// info.isCalibrationValueExact depends on clock.
	CHECK_COMPARE(info.calibrationValue, >, 0u);
	CHECK_TRUE(info.isTheReferenceClockImplemented);
}

/// @}

/// @name SysTick period change
/// @{

/// \Given functional Systick driver and functional Tic with MCK/8 as Tic clock base,
///
/// \When SysTick is counting,
///
/// \Then countdown reaches zero and interrupt and dedicated flag are asserted and SysTick run at
///       proper frequency.
///
TEST(SystickTests, Systick_checksIfTimerPeriodIsProper)
{
	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc0Ch0);

	static const Tic_Channel channel = Tic_Channel_0;
	const uint32_t reloadValue = 10000u;

	static Tic tic;
	Tic_ChannelConfig ticConfig{};
	ticConfig.isEnabled = true;
	ticConfig.clockSource = Tic_ClockSelection_MckBy8;

	Tic_init(&tic, Tic_Id_0);
	Tic_setChannelConfig(&tic, channel, &ticConfig);
	Tic_enableChannel(&tic, channel);

	const Systick_Config config{
		.clockSource = Systick_ClockSource_ProcessorClock,
		.isInterruptEnabled = true,
		.isEnabled = true,
		.reloadValue = reloadValue,
	};

	Systick_setConfig(&systick, &config);

	Tic_triggerChannel(&tic, channel);
	Systick_clearCurrentValue(&systick);
	systickHandlerCalled = false;

	auto waitForSysTickIrqCalled = []() {
		if (systickHandlerCalled) {
			Tic_disableChannel(&tic, channel);
			return true;
		}
		return false;
	};

	CHECK_TRUE(evaluateLambdaWithTimeout(
			waitForSysTickIrqCalled, SYSTICKTEST_TIMEOUT));

	CHECK_TRUE(Systick_hasCountedToZero(&systick));

	const uint64_t ticDelay = (static_cast<uint64_t>(Tic_getCounterValue(
						   &tic, channel))
						  * NANOSECONDS_IN_SECOND)
			/ static_cast<uint64_t>(N7S_BSP_PERIPH_CLOCK / 8u);

	const uint64_t systickDelay = (static_cast<uint64_t>(reloadValue)
						      * NANOSECONDS_IN_SECOND)
			/ static_cast<uint64_t>(N7S_BSP_CORE_CLOCK);

	CHECK_COMPARE(std::max(ticDelay, systickDelay)
					- std::min(ticDelay, systickDelay),
			<=, ALLOWED_TIMER_MISCOUNT);
}

/// @}
