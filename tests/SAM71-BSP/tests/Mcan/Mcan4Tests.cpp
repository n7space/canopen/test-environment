/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Mcan4Tests.cpp
/// \brief Mcan driver test suite implementation.

#include <array>
#include <numeric>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

TEST_GROUP_BASE(MCanTests, TestUtils){};

using MCanTests = TEST_GROUP_CppUTestGroupMCanTests;

#ifndef DOXYGEN
void
MCAN1_INT0_Handler(void)
{
	Mcan_InterruptStatus status{};
	Mcan_getInterruptStatus(&MCanTests::mcan, &status);

	if (status.hasTcOccurred)
		MCanTests::txCompleteIrqCalled = true;

	if (status.hasRf0wOccurred)
		MCanTests::rxWatermarkIrqCalled = true;

	if (status.hasTooOccurred)
		MCanTests::timeoutOccurredIrqCalled = true;
}
#endif

/// @name Mcan_getConfig()
/// @{

/// \Given initialized and configured Mcan with invalid element size
///
/// \When Mcan_getConfig() is called,
///
/// \Then invalid element size is returned.
///
TEST(MCanTests, Mcan_getConfig_returnsInvalidElementSize)
{
	ErrorCode errCode = ErrorCode_NoError;
	// Apply default configuration
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// Change element size to invalid value
	mcan.tx.elementSize = 26u;
	// Read configuration and verify tx element size
	Mcan_Config readConfig{};
	Mcan_getConfig(&mcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_ElementSize_Invalid,
			readConfig.txBuffer.elementSize);
}

/// \Given initialized and configured Mcan with mock registers and
///        bits in configuration registers set to values not
///        corresponding to any valid operation mode,
///
/// \When Mcan_getConfig() is called,
///
/// \Then invalid mode is returned.
///
TEST(MCanTests, Mcan_getConfig_returnsInvalidMode)
{
	Mcan_BaseRegisters mockRegs{};
	uint32_t mockDmaBase{};
	Mcan mockMcan{};
	Mcan_init(&mockMcan, { &mockRegs, &mockDmaBase });
	Mcan_Config readConfig{};

	// Mcan is stuck in Software initialization mode if INIT bit is not cleared
	mockRegs.cccr = MCAN_CCCR_INIT_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_INIT_MASK | MCAN_CCCR_TEST_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_INIT_MASK | MCAN_CCCR_CSA_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_INIT_MASK | MCAN_CCCR_CSR_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_INIT_MASK | MCAN_CCCR_CSA_MASK
			| MCAN_CCCR_TEST_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	// Clock stop ack (CSA) is set, but not requested (CSR)
	mockRegs.cccr = MCAN_CCCR_CSA_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_CSR_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_CSA_MASK | MCAN_CCCR_TEST_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_MON_MASK | MCAN_CCCR_TEST_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	// TEST bit is set, but no loopback (LBCK) or bus monitoring (MON) is set
	mockRegs.cccr = MCAN_CCCR_TEST_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};

	mockRegs.cccr = MCAN_CCCR_TEST_MASK;
	mockRegs.test = MCAN_TEST_LBCK_MASK;
	Mcan_getConfig(&mockMcan, &readConfig);
	ENUMS_EQUAL_INT(Mcan_Mode_Invalid, readConfig.mode);
	readConfig = {};
}

/// @}

/// @name Mcan_txBufferIsTransmissionFinished()
/// @{

/// \Given initialized and configured MCAN in internal loopback mode with
///        interrupts disabled,
///
/// \When CAN message is transmitted and RX fifo is continuously polled
///
/// \Then Message is received correctly in RX fifo.
///
TEST(MCanTests, Mcan_rxAndTxWorkWithPolling)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_InternalLoopBackTest;
	conf.isFdEnabled = false;
	conf.standardIdFilter.isIdRejected = true;
	conf.extendedIdFilter.isIdRejected = true;
	conf.extendedIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo0;
	conf.extendedIdFilter.filterListSize = 0;
	conf.rxFifo1 = {};
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 8u> txData{};
	std::iota(txData.begin(), txData.end(), 7u);

	const Mcan_TxElement txElement{
		.esiFlag = Mcan_ElementEsi_Dominant,
		.idType = Mcan_IdType_Extended,
		.frameType = Mcan_FrameType_Data,
		.id = 3u,
		.marker = 1u,
		.isTxEventStored = false,
		.isCanFdFormatEnabled = true,
		.isBitRateSwitchingEnabled = false,
		.dataSize = txData.size(),
		.data = txData.data(),
		.isInterruptEnabled = false,
	};
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// Poll TX buffer status
	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	// Poll RX Fifo status
	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				Mcan_RxFifoStatus fifoStatus;
				CHECK_TRUE(Mcan_getRxFifoStatus(&mcan,
						Mcan_RxFifoId_0, &fifoStatus,
						nullptr));
				return fifoStatus.count == 1u;
			},
			MCAN_TEST_TIMEOUT));

	// cppcheck-suppress misra-c2012-17.7
	decltype(txData) rxData{};
	Mcan_RxElement rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_0, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(txElement.id, rxElement.id);
	UNSIGNED_LONGS_EQUAL(txElement.dataSize, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), txData.size());
}

/// @}

/// @name Mcan_setConfig()
/// @{

/// \Given initialized and not configured Mcan with mock registers
///
/// \When Mcan_setConfig() is called with configuration for power down
///       and hardware fails to set Clock stop acknowledge flag in CCCR register,
///
/// \Then timeout error is returned.
///
TEST(MCanTests, Mcan_setConfig_timesOutIfCSAIsNotSet)
{
	Mcan mockMcan{};
	Mcan_BaseRegisters mockRegs{};
	uint32_t mockDmaBase = 0u;
	mockMcan.reg.base = &mockRegs;
	mockMcan.reg.canDmaBase = &mockDmaBase;
	// CSA cleared
	mockRegs.cccr = 0u;
	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_PowerDown;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_FALSE(Mcan_setConfig(&mockMcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ClockStopRequestTimeout, errCode);
}

/// \Given initialized and not configured Mcan,
///
/// \When Mcan_setConfig() is called with configuration containing invalid TX element size,
///
/// \Then Invalid element size error code is returned
///
TEST(MCanTests, Mcan_setConfig_returnsErrorForInvalidTxElementSize)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.isFdEnabled = false;
	conf.txBuffer.elementSize =
			arbitraryEnumValue<Mcan_ElementSize>(0xDEADu);
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_FALSE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ElementSizeInvalid, errCode);

	errCode = ErrorCode_NoError;
	conf.txBuffer.elementSize = Mcan_ElementSize_Invalid;
	CHECK_FALSE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ElementSizeInvalid, errCode);
}

/// @}
