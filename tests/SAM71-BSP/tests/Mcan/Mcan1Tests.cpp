/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Mcan1Tests.cpp
/// \brief Mcan driver test suite implementation.

#include <array>
#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

TEST_GROUP_BASE(MCanTests, TestUtils){};

using MCanTests = TEST_GROUP_CppUTestGroupMCanTests;

#ifndef DOXYGEN
void
MCAN1_INT0_Handler(void)
{
	Mcan_InterruptStatus status{};
	Mcan_getInterruptStatus(&MCanTests::mcan, &status);

	if (status.hasTcOccurred)
		MCanTests::txCompleteIrqCalled = true;

	if (status.hasRf0wOccurred)
		MCanTests::rxWatermarkIrqCalled = true;

	if (status.hasTooOccurred)
		MCanTests::timeoutOccurredIrqCalled = true;
}
#endif

/// @name Mcan_txBufferAdd()
/// @{

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 8-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends8ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_8);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 12-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends12ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_12);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 16-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends16ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_16);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 20-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends20ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_20);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 24-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends24ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_24);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 32-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends32ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_32);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 48-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends48ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_48);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When 64-byte message is sent via TX buffer
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sends64ByteMessageFromTxBuffer)
{
	sendAndReceiveMessage(Mcan_ElementSize_64);
}

/// @}

/// @name Mcan_txQueuePush()
/// @{

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When message is sent via TX FIFO,
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sendsMessageFromTxFifo)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.standardIdFilter.isIdRejected = false;
	conf.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo1;
	conf.standardIdFilter.filterListSize = 0;
	conf.rxFifo1.isEnabled = true;
	conf.rxFifo1.startAddress = &msgRam[MSGRAM_RXFIFO1_OFFSET];
	conf.rxFifo1.size = 1;
	conf.rxFifo1.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo1.elementSize = Mcan_ElementSize_8;
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 1;
	conf.txBuffer.queueSize = 1;
	conf.txBuffer.queueType = Mcan_TxQueueType_Fifo;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 8> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Standard;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 3;
	txElement.marker = 1;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	uint8_t pushIndex = 0u;
	CHECK_TRUE(Mcan_txQueuePush(&mcan, txElement, &pushIndex, nullptr));

	Mcan_TxQueueStatus queueStatus;
	Mcan_getTxQueueStatus(&mcan, &queueStatus);
	CHECK_TRUE(queueStatus.isFull);

	CHECK_TRUE(evaluateArgLambdaWithTimeout(
			[](void *pindex) -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan,
						*static_cast<uint8_t *>(
								pindex));
			},
			&pushIndex, MCAN_TEST_TIMEOUT));

	Mcan_RxFifoStatus fifoStatus;
	CHECK_TRUE(Mcan_getRxFifoStatus(
			&mcan, Mcan_RxFifoId_1, &fifoStatus, nullptr));
	UNSIGNED_LONGS_EQUAL(1u, fifoStatus.count);
	CHECK_TRUE(fifoStatus.isFull);
	CHECK_FALSE(fifoStatus.isMessageLost);

	std::array<uint8_t, 8u> rxData = {};
	Mcan_RxElement rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_1, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(3u, rxElement.id);
	CHECK_TRUE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(8u, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 8u);
	CHECK_TRUE(Mcan_isTxFifoEmpty(&mcan));
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When message is sent via TC Id Queue
///
/// \Then transmission succeeds, message is received in RX fifo.
///
TEST(MCanTests, Mcan_sendsMessageFromTxIdQueue)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.standardIdFilter.isIdRejected = false;
	conf.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo0;
	conf.standardIdFilter.filterListSize = 0;
	conf.rxFifo0.isEnabled = true;
	conf.rxFifo0.startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET];
	conf.rxFifo0.size = 2;
	conf.rxFifo0.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo0.elementSize = Mcan_ElementSize_8;
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 0;
	conf.txBuffer.queueSize = 1;
	conf.txBuffer.queueType = Mcan_TxQueueType_Id;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 8u> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Standard;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 3;
	txElement.marker = 1;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	uint8_t pushIndex = 0u;
	CHECK_TRUE(Mcan_txQueuePush(&mcan, txElement, &pushIndex, nullptr));

	Mcan_TxQueueStatus queueStatus;
	Mcan_getTxQueueStatus(&mcan, &queueStatus);
	CHECK_TRUE(queueStatus.isFull);

	CHECK_TRUE(evaluateArgLambdaWithTimeout(
			[](void *pindex) -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan,
						*static_cast<uint8_t *>(
								pindex));
			},
			&pushIndex, MCAN_TEST_TIMEOUT));
	std::array<uint8_t, 8u> rxData = {};
	Mcan_RxElement rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_0, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(3u, rxElement.id);
	CHECK_TRUE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(8u, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 8u);
}

/// @}

/// @name Mcan_txEventFifoPull()
/// @{

/// \Given initialized and configured Mcan, with TX event FIFO enabled
///
/// \When transmission is finished
///
/// \Then TX event is present in the queue.
///
TEST(MCanTests, Mcan_storesTxEventInTxEventFifo)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.standardIdFilter.isIdRejected = false;
	conf.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo1;
	conf.standardIdFilter.filterListSize = 0;
	conf.rxFifo0.isEnabled = false;
	conf.rxFifo1.isEnabled = true;
	conf.rxFifo1.startAddress = &msgRam[MSGRAM_RXFIFO1_OFFSET];
	conf.rxFifo1.size = 2;
	conf.rxFifo1.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo1.elementSize = Mcan_ElementSize_8;
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 0;
	conf.txBuffer.queueSize = 1;
	conf.txBuffer.queueType = Mcan_TxQueueType_Id;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	conf.txEventFifo.isEnabled = true;
	conf.txEventFifo.startAddress = &msgRam[MSGRAM_TXEVENTFIFO_OFFSET];
	conf.txEventFifo.size = 1;
	conf.txEventFifo.watermark = 0;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 8> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Standard;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 5;
	txElement.marker = 3;
	txElement.isTxEventStored = true;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	uint8_t pushIndex = 0u;
	CHECK_TRUE(Mcan_txQueuePush(&mcan, txElement, &pushIndex, nullptr));

	CHECK_TRUE(evaluateArgLambdaWithTimeout(
			[](void *pindex) -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan,
						*static_cast<uint8_t *>(
								pindex));
			},
			&pushIndex, MCAN_TEST_TIMEOUT));

	Mcan_TxEventFifoStatus eventStatus;
	Mcan_getTxEventFifoStatus(&mcan, &eventStatus);
	CHECK_TRUE(eventStatus.isFull);
	CHECK_FALSE(eventStatus.isMessageLost);
	UNSIGNED_LONGS_EQUAL(1u, eventStatus.count);

	Mcan_TxEventElement txEventElement = {};
	CHECK_TRUE(Mcan_txEventFifoPull(&mcan, &txEventElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(5u, txEventElement.id);
	UNSIGNED_LONGS_EQUAL(3u, txEventElement.marker);
	ENUMS_EQUAL_INT(Mcan_TxEventType_Tx, txEventElement.eventType);
	UNSIGNED_LONGS_EQUAL(8u, txEventElement.dataSize);
}

/// @}

/// @name Mcan_setConfig()
/// @{

/// \Given initialized and configured, with TX interrupt enabled
///
/// \When transmission is completed
///
/// \Then transmission complete interrupt is generated.
///
TEST(MCanTests, Mcan_generatesTxCompleteInterrupt)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 2;
	conf.txBuffer.queueSize = 0;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	conf.interrupts[Mcan_Interrupt_Tc].isEnabled = true;
	conf.interrupts[Mcan_Interrupt_Tc].line = Mcan_InterruptLine_0;
	conf.isLine0InterruptEnabled = true;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Nvic_enableInterrupt(Nvic_Irq_Mcan1_Irq0);
	Nvic_setInterruptPriority(Nvic_Irq_Mcan1_Irq0, 0);

	std::array<uint8_t, 8> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Extended;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 11;
	txElement.marker = 12;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	txElement.isInterruptEnabled = true;
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool { return txCompleteIrqCalled; },
			MCAN_TEST_TIMEOUT));
	CHECK_TRUE(txCompleteIrqCalled);
}

/// \Given initialized and configured Mcan, with RX watermark interrupt enabled
///
/// \When transmission is completed
///
/// \Then an RX watermark interrupt is generated.
///
TEST(MCanTests, Mcan_generatesRxFifoWatermarkInterrupt)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 2;
	conf.txBuffer.queueSize = 0;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	conf.rxFifo0.isEnabled = true;
	conf.rxFifo0.startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET];
	conf.rxFifo0.size = 2;
	conf.rxFifo0.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo0.elementSize = Mcan_ElementSize_8;
	conf.rxFifo0.watermark = 1;
	conf.standardIdFilter.isIdRejected = false;
	conf.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo0;
	conf.standardIdFilter.filterListSize = 0;

	conf.interrupts[Mcan_Interrupt_Rf0w].isEnabled = true;
	conf.interrupts[Mcan_Interrupt_Rf0w].line = Mcan_InterruptLine_0;
	conf.isLine0InterruptEnabled = true;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Nvic_enableInterrupt(Nvic_Irq_Mcan1_Irq0);
	Nvic_setInterruptPriority(Nvic_Irq_Mcan1_Irq0, 0);

	std::array<uint8_t, 8> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Extended;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 11;
	txElement.marker = 12;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool { return rxWatermarkIrqCalled; },
			MCAN_TEST_TIMEOUT));
	CHECK_TRUE(rxWatermarkIrqCalled);
}

/// \Given initialized and configured Mcan in internal loopback mode
///
/// \When a response timeout is configured and a response is not received within the timeout period
///
/// \Then a timeout occurred interrupt is generated.
///
TEST(MCanTests, Mcan_generatesTimeoutOccurredInterrupt)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.timestampClk = Mcan_TimestampClk_Internal;
	conf.timestampTimeoutPrescaler = 1;
	conf.timeout.isEnabled = true;
	conf.timeout.type = Mcan_TimeoutType_TxEventFifo;
	conf.timeout.period = 100;

	conf.interrupts[Mcan_Interrupt_Too].isEnabled = true;
	conf.interrupts[Mcan_Interrupt_Too].line = Mcan_InterruptLine_0;
	conf.isLine0InterruptEnabled = true;

	conf.standardIdFilter.isIdRejected = true;
	conf.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_Rejected;
	conf.standardIdFilter.filterListSize = 0;

	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 2;
	conf.txBuffer.queueSize = 0;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	conf.txEventFifo.isEnabled = true;
	conf.txEventFifo.startAddress = &msgRam[MSGRAM_TXEVENTFIFO_OFFSET];
	conf.txEventFifo.size = 1;
	conf.txEventFifo.watermark = 0;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Nvic_enableInterrupt(Nvic_Irq_Mcan1_Irq0);
	Nvic_setInterruptPriority(Nvic_Irq_Mcan1_Irq0, 0);

	std::array<uint8_t, 8> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Standard;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 3;
	txElement.marker = 2;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	txElement.isTxEventStored = true;
	txElement.isInterruptEnabled = true;
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	// Do not pull TX event from FIFO; Placing element in FIFO starts timeout counter

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool { return timeoutOccurredIrqCalled; },
			MCAN_TEST_TIMEOUT));
	CHECK_TRUE(timeoutOccurredIrqCalled);
}

/// @}
