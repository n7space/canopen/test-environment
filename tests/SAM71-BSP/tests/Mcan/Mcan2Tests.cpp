/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Mcan2Tests.cpp
/// \brief Mcan driver test suite implementation.

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

// clang-format off
TEST_GROUP_BASE(MCanTests, TestUtils)
{
  static void setConfiguration(const Mcan_Id id)
  {
    Mcan_init(&mcan, Mcan_getDeviceRegisters(id));

    Mcan_Config conf = defaultConfig;
    conf.msgRamBaseAddress = msgRam.data();
    conf.mode = Mcan_Mode_Normal;
    conf.isFdEnabled = true;
    conf.nominalBitTiming.bitRatePrescaler = 23;
    conf.nominalBitTiming.synchronizationJump = 1;
    conf.nominalBitTiming.timeSegmentAfterSamplePoint = 2;
    conf.nominalBitTiming.timeSegmentBeforeSamplePoint = 3;
    conf.dataBitTiming.bitRatePrescaler = 1;
    conf.dataBitTiming.synchronizationJump = 1;
    conf.dataBitTiming.timeSegmentAfterSamplePoint = 2;
    conf.dataBitTiming.timeSegmentBeforeSamplePoint = 3;
    conf.transmitterDelayCompensation.isEnabled = true;
    conf.transmitterDelayCompensation.filter = 8;
    conf.transmitterDelayCompensation.offset = 9;
    conf.timestampClk = Mcan_TimestampClk_Internal;
    conf.timestampTimeoutPrescaler = 1;
    conf.timeout.isEnabled = true;
    conf.timeout.type = Mcan_TimeoutType_TxEventFifo;
    conf.timeout.period = 7;
    conf.standardIdFilter.isIdRejected = false;
    conf.standardIdFilter.nonMatchingPolicy = Mcan_NonMatchingPolicy_RxFifo1;
    conf.standardIdFilter.filterListAddress = &msgRam[MSGRAM_STDID_FILTER_OFFSET];
    conf.standardIdFilter.filterListSize = 1;
    conf.extendedIdFilter.isIdRejected = false;
    conf.extendedIdFilter.nonMatchingPolicy = Mcan_NonMatchingPolicy_Rejected;
    conf.extendedIdFilter.filterListAddress = &msgRam[MSGRAM_EXTID_FILTER_OFFSET];
    conf.extendedIdFilter.filterListSize = 2;
    conf.rxFifo0.isEnabled = true;
    conf.rxFifo0.startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET];
    conf.rxFifo0.size = 1;
    conf.rxFifo0.watermark = 2;
    conf.rxFifo0.mode = Mcan_RxFifoOperationMode_Overwrite;
    conf.rxFifo0.elementSize = Mcan_ElementSize_12;
    conf.rxFifo1.isEnabled = true;
    conf.rxFifo1.startAddress = &msgRam[MSGRAM_RXFIFO1_OFFSET];
    conf.rxFifo1.size = 2;
    conf.rxFifo1.watermark = 3;
    conf.rxFifo1.mode = Mcan_RxFifoOperationMode_Overwrite;
    conf.rxFifo1.elementSize = Mcan_ElementSize_16;
    conf.rxBuffer.startAddress = &msgRam[MSGRAM_RXBUFFER_OFFSET];
    conf.rxBuffer.elementSize = Mcan_ElementSize_8;
    conf.txBuffer.isEnabled = true;
    conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
    conf.txBuffer.bufferSize = 3;
    conf.txBuffer.queueSize = 2;
    conf.txBuffer.queueType = Mcan_TxQueueType_Id;
    conf.txBuffer.elementSize = Mcan_ElementSize_64;
    conf.txEventFifo.isEnabled = true;
    conf.txEventFifo.startAddress = &msgRam[MSGRAM_TXEVENTFIFO_OFFSET];
    conf.txEventFifo.size = 5;
    conf.txEventFifo.watermark = 3;
    conf.isLine0InterruptEnabled = true;
    conf.isLine1InterruptEnabled = true;
    for (uint32_t i = 0; i < static_cast<uint32_t>(Mcan_Interrupt_Count); i++)
    {
      if ((i == static_cast<uint32_t>(Mcan_Interrupt_Reserved1))
          || (i == static_cast<uint32_t>(Mcan_Interrupt_Reserved2)))
        continue;

      conf.interrupts[i].isEnabled = true;
      conf.interrupts[i].line = Mcan_InterruptLine_1;
    }

    CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, nullptr));
  }

  static void verifyConfiguration()
  {
    Mcan_Config conf{};

    Mcan_getConfig(&mcan, &conf);

    POINTERS_EQUAL(msgRam.data(), conf.msgRamBaseAddress);
    ENUMS_EQUAL_INT(Mcan_Mode_Normal, conf.mode);
    CHECK_TRUE(conf.isFdEnabled);
    UNSIGNED_LONGS_EQUAL(23, conf.nominalBitTiming.bitRatePrescaler);
    UNSIGNED_LONGS_EQUAL(1, conf.nominalBitTiming.synchronizationJump);
    UNSIGNED_LONGS_EQUAL(2, conf.nominalBitTiming.timeSegmentAfterSamplePoint);
    UNSIGNED_LONGS_EQUAL(3, conf.nominalBitTiming.timeSegmentBeforeSamplePoint);
    UNSIGNED_LONGS_EQUAL(1, conf.dataBitTiming.bitRatePrescaler);
    UNSIGNED_LONGS_EQUAL(1, conf.dataBitTiming.synchronizationJump);
    UNSIGNED_LONGS_EQUAL(2, conf.dataBitTiming.timeSegmentAfterSamplePoint);
    UNSIGNED_LONGS_EQUAL(3, conf.dataBitTiming.timeSegmentBeforeSamplePoint);
    CHECK_TRUE(conf.transmitterDelayCompensation.isEnabled);
    UNSIGNED_LONGS_EQUAL(8, conf.transmitterDelayCompensation.filter);
    UNSIGNED_LONGS_EQUAL(9, conf.transmitterDelayCompensation.offset);
    ENUMS_EQUAL_INT(Mcan_TimestampClk_Internal, conf.timestampClk);
    UNSIGNED_LONGS_EQUAL(1, conf.timestampTimeoutPrescaler);
    CHECK_TRUE(conf.timeout.isEnabled);
    ENUMS_EQUAL_INT(Mcan_TimeoutType_TxEventFifo, conf.timeout.type);
    UNSIGNED_LONGS_EQUAL(7, conf.timeout.period);
    CHECK_FALSE(conf.standardIdFilter.isIdRejected);
    ENUMS_EQUAL_INT(Mcan_NonMatchingPolicy_RxFifo1, conf.standardIdFilter.nonMatchingPolicy);
    POINTERS_EQUAL(&msgRam[MSGRAM_STDID_FILTER_OFFSET], conf.standardIdFilter.filterListAddress);
    UNSIGNED_LONGS_EQUAL(1, conf.standardIdFilter.filterListSize);
    CHECK_FALSE(conf.extendedIdFilter.isIdRejected);
    ENUMS_EQUAL_INT(Mcan_NonMatchingPolicy_Rejected, conf.extendedIdFilter.nonMatchingPolicy);
    POINTERS_EQUAL(&msgRam[MSGRAM_EXTID_FILTER_OFFSET], conf.extendedIdFilter.filterListAddress);
    UNSIGNED_LONGS_EQUAL(2, conf.extendedIdFilter.filterListSize);
    CHECK_TRUE(conf.rxFifo0.isEnabled);
    POINTERS_EQUAL(&msgRam[MSGRAM_RXFIFO0_OFFSET], conf.rxFifo0.startAddress);
    UNSIGNED_LONGS_EQUAL(1, conf.rxFifo0.size);
    UNSIGNED_LONGS_EQUAL(2, conf.rxFifo0.watermark);
    ENUMS_EQUAL_INT(Mcan_RxFifoOperationMode_Overwrite, conf.rxFifo0.mode);
    ENUMS_EQUAL_INT(Mcan_ElementSize_12, conf.rxFifo0.elementSize);
    CHECK_TRUE(conf.rxFifo1.isEnabled);
    POINTERS_EQUAL(&msgRam[MSGRAM_RXFIFO1_OFFSET], conf.rxFifo1.startAddress);
    UNSIGNED_LONGS_EQUAL(2, conf.rxFifo1.size);
    UNSIGNED_LONGS_EQUAL(3, conf.rxFifo1.watermark);
    ENUMS_EQUAL_INT(Mcan_RxFifoOperationMode_Overwrite, conf.rxFifo1.mode);
    ENUMS_EQUAL_INT(Mcan_ElementSize_16, conf.rxFifo1.elementSize);
    POINTERS_EQUAL(&msgRam[MSGRAM_RXBUFFER_OFFSET], conf.rxBuffer.startAddress);
    ENUMS_EQUAL_INT(Mcan_ElementSize_8, conf.rxBuffer.elementSize);
    CHECK_TRUE(conf.txBuffer.isEnabled);
    POINTERS_EQUAL(&msgRam[MSGRAM_TXBUFFER_OFFSET], conf.txBuffer.startAddress);
    UNSIGNED_LONGS_EQUAL(3, conf.txBuffer.bufferSize);
    UNSIGNED_LONGS_EQUAL(2, conf.txBuffer.queueSize);
    ENUMS_EQUAL_INT(Mcan_TxQueueType_Id, conf.txBuffer.queueType);
    ENUMS_EQUAL_INT(Mcan_ElementSize_64, conf.txBuffer.elementSize);
    CHECK_TRUE(conf.txEventFifo.isEnabled);
    POINTERS_EQUAL(&msgRam[MSGRAM_TXEVENTFIFO_OFFSET], conf.txEventFifo.startAddress);
    UNSIGNED_LONGS_EQUAL(5, conf.txEventFifo.size);
    UNSIGNED_LONGS_EQUAL(3, conf.txEventFifo.watermark);
    CHECK_TRUE(conf.isLine0InterruptEnabled);
    CHECK_TRUE(conf.isLine1InterruptEnabled);

    for (uint32_t i = 0; i < static_cast<uint32_t>(Mcan_Interrupt_Count); i++)
    {
      if ((i == static_cast<uint32_t>(Mcan_Interrupt_Reserved1))
          || (i == static_cast<uint32_t>(Mcan_Interrupt_Reserved2)))
        continue;

      CHECK_TRUE(conf.interrupts[i].isEnabled);
      ENUMS_EQUAL_INT(Mcan_InterruptLine_1, conf.interrupts[i].line);
    }
  }

  static void performFullConfigurationTest(const Mcan_Id id)
  {
    setConfiguration(id);
    verifyConfiguration();
  }
};
// clang-format on

using MCanTests = TEST_GROUP_CppUTestGroupMCanTests;

#ifndef DOXYGEN
void
MCAN1_INT0_Handler(void)
{
	Mcan_InterruptStatus status{};
	Mcan_getInterruptStatus(&MCanTests::mcan, &status);

	if (status.hasTcOccurred)
		MCanTests::txCompleteIrqCalled = true;

	if (status.hasRf0wOccurred)
		MCanTests::rxWatermarkIrqCalled = true;

	if (status.hasTooOccurred)
		MCanTests::timeoutOccurredIrqCalled = true;
}
#endif

/// @name Mcan_setConfig()
/// @{

/// \Given initialized and not configured MCAN,
///
/// \When the full configuration is set for each MCAN module,
///
/// \Then configuration is corretly written for each MCAN.
///
TEST(MCanTests, Mcan_setConfig_setsTheMcanDevicesConfiguration)
{
	performFullConfigurationTest(Mcan_Id_0);
	performFullConfigurationTest(Mcan_Id_1);
}

/// \Given initialized and not configured Mcan
///
/// \When Mcan_setConfig() is called with configuration for Internal loopback mode,
///
/// \Then TEST and MON bits are set in CCCR register, LBCK bit is set in TEST register
///
TEST(MCanTests, Mcan_setConfig_configuresInternalLoopback)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_InternalLoopBackTest;
	conf.isFdEnabled = false;
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	const uint32_t cccr = mcan.reg.base->cccr;
	const uint32_t cccrMask = MCAN_CCCR_MON_MASK | MCAN_CCCR_TEST_MASK;
	const uint32_t test = mcan.reg.base->test;
	const uint32_t testMask = MCAN_TEST_LBCK_MASK;

	UNSIGNED_LONGS_EQUAL(cccrMask, cccr);
	UNSIGNED_LONGS_EQUAL(testMask, testMask & test);
}

/// \Given initialized and not configured Mcan
///
/// \When Mcan_setConfig() is called with configuration for Bus monitoring mode,
///
/// \Then MON bit is set in CCCR register.
///
TEST(MCanTests, Mcan_setConfig_configuresBusMonitoring)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_BusMonitoring;
	conf.isFdEnabled = false;
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	const uint32_t cccr = mcan.reg.base->cccr;
	const uint32_t cccrMask = MCAN_CCCR_MON_MASK;

	UNSIGNED_LONGS_EQUAL(cccrMask, cccr);
}

/// \Given initialized and not configured Mcan
///
/// \When Mcan_setConfig() is called with configuration for Power down mode,
///
/// \Then CSR, CSA and INIT bits are set in CCCR register.
///
TEST(MCanTests, Mcan_setConfig_configuresPowerDown)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_PowerDown;
	conf.isFdEnabled = false;
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	const uint32_t cccr = mcan.reg.base->cccr;
	const uint32_t cccrMask = MCAN_CCCR_CSA_MASK | MCAN_CCCR_CSR_MASK
			| MCAN_CCCR_INIT_MASK;

	UNSIGNED_LONGS_EQUAL(cccrMask, cccr);
}

/// \Given initialized and not configured Mcan
///
/// \When Mcan_setConfig() is called with configuration for Disabled auto retransmission mode,
///
/// \Then DAR bit is set in CCCR register.
///
TEST(MCanTests, Mcan_setConfig_configuresAutoRetransmissionDisable)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_AutomaticRetransmissionDisabled;
	conf.isFdEnabled = false;
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	const uint32_t cccr = mcan.reg.base->cccr;
	const uint32_t cccrMask = MCAN_CCCR_DAR_MASK;

	UNSIGNED_LONGS_EQUAL(cccrMask, cccr);
}

/// \Given initialized and not configured Mcan
///
/// \When Mcan_setConfig() is called with configuration for Restricted mode,
///
/// \Then DAR bit is set in CCCR register.
///
TEST(MCanTests, Mcan_setConfig_configuresRestrictedMode)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_Restricted;
	conf.isFdEnabled = false;
	ErrorCode errCode = ErrorCode_NoError;

	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	const uint32_t cccr = mcan.reg.base->cccr;
	const uint32_t cccrMask = MCAN_CCCR_ASM_MASK;

	UNSIGNED_LONGS_EQUAL(cccrMask, cccr);
}

/// @}
