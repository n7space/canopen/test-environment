/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Mcan5Tests.cpp
/// \brief Mcan driver test suite implementation.

#include <array>
#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

TEST_GROUP_BASE(MCanTests, TestUtils){};

using MCanTests = TEST_GROUP_CppUTestGroupMCanTests;

#ifndef DOXYGEN
void
MCAN1_INT0_Handler(void)
{
	Mcan_InterruptStatus status{};
	Mcan_getInterruptStatus(&MCanTests::mcan, &status);

	if (status.hasTcOccurred)
		MCanTests::txCompleteIrqCalled = true;

	if (status.hasRf0wOccurred)
		MCanTests::rxWatermarkIrqCalled = true;

	if (status.hasTooOccurred)
		MCanTests::timeoutOccurredIrqCalled = true;
}
#endif

/// @name Mcan_setConfig()
/// @{

/// \Given initialized Mcan
///
/// \When Mcan_setConfig() is called with configuration that changes operation mode
///
/// \Then mode is corretly changed.
///
TEST(MCanTests, Mcan_setConfig_setsTheMcanDeviceMode)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_InternalLoopBackTest;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	Mcan_getConfig(&mcan, &conf);
	ENUMS_EQUAL_INT(Mcan_Mode_InternalLoopBackTest, conf.mode);

	conf.mode = Mcan_Mode_PowerDown;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	Mcan_getConfig(&mcan, &conf);
	ENUMS_EQUAL_INT(Mcan_Mode_PowerDown, conf.mode);

	conf.mode = Mcan_Mode_BusMonitoring;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	Mcan_getConfig(&mcan, &conf);
	ENUMS_EQUAL_INT(Mcan_Mode_BusMonitoring, conf.mode);

	conf.mode = Mcan_Mode_Restricted;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	Mcan_getConfig(&mcan, &conf);
	ENUMS_EQUAL_INT(Mcan_Mode_Restricted, conf.mode);

	conf.mode = Mcan_Mode_AutomaticRetransmissionDisabled;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	Mcan_getConfig(&mcan, &conf);
	ENUMS_EQUAL_INT(Mcan_Mode_AutomaticRetransmissionDisabled, conf.mode);

	conf.mode = Mcan_Mode_Normal;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	Mcan_getConfig(&mcan, &conf);
	ENUMS_EQUAL_INT(Mcan_Mode_Normal, conf.mode);
}

/// \Given initialized and configured Mcan
///
/// \When the timestamping clock is configured
///
/// \Then TX events and received frames are timestamped.
///
TEST(MCanTests, Mcan_setConfig_configuresTimestampGeneration)
{
	Mcan_Config conf = defaultConfig;
	conf.timestampClk = Mcan_TimestampClk_Internal;
	conf.timestampTimeoutPrescaler = 1;
	conf.standardIdFilter.isIdRejected = true;
	conf.extendedIdFilter.isIdRejected = true;
	conf.extendedIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo0;
	conf.extendedIdFilter.filterListSize = 0;
	conf.rxFifo0.isEnabled = true;
	conf.rxFifo0.startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET];
	conf.rxFifo0.size = 1;
	conf.rxFifo0.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo0.elementSize = Mcan_ElementSize_8;
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 1;
	conf.txBuffer.queueSize = 0;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	conf.txEventFifo.isEnabled = true;
	conf.txEventFifo.startAddress = &msgRam[MSGRAM_TXEVENTFIFO_OFFSET];
	conf.txEventFifo.size = 1;
	conf.txEventFifo.watermark = 0;
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 8> txData = { 0x01, 0x03, 0x07, 0x013, 0x29, 0x57,
		0x91, 0xFD };

	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Standard;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.isCanFdFormatEnabled = true;
	txElement.isTxEventStored = true;
	txElement.id = 3;
	txElement.marker = 2;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	Mcan_TxEventElement txEventElement = {};
	CHECK_TRUE(Mcan_txEventFifoPull(&mcan, &txEventElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(3, txEventElement.id);
	UNSIGNED_LONGS_EQUAL(2, txEventElement.marker);
	ENUMS_EQUAL_INT(Mcan_TxEventType_Tx, txEventElement.eventType);
	UNSIGNED_LONGS_EQUAL(8, txEventElement.dataSize);
	CHECK(txEventElement.timestamp != 0u);

	Mcan_RxFifoStatus fifoStatus;
	CHECK_TRUE(Mcan_getRxFifoStatus(
			&mcan, Mcan_RxFifoId_0, &fifoStatus, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(1u, fifoStatus.count);
	CHECK_TRUE(fifoStatus.isFull);
	CHECK_FALSE(fifoStatus.isMessageLost);

	std::array<uint8_t, 8u> rxData = {};
	Mcan_RxElement rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_0, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(3u, rxElement.id);
	CHECK_TRUE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(8u, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 8);
	CHECK(rxElement.timestamp != 0u);
	CHECK_COMPARE(static_cast<int32_t>(txEventElement.timestamp), >=,
			static_cast<int32_t>(rxElement.timestamp));
	CHECK_COMPARE(static_cast<int32_t>(txEventElement.timestamp)
					- static_cast<int32_t>(
							rxElement.timestamp),
			<, 1000);
}

/// \Given initialized and not configured Mcan with stopped peripheral clock
///
/// \When Mcan_setConfig() is called with valid configuration,
///
/// \Then initialization start timeout error is returned.
///
TEST(MCanTests, Mcan_setConfig_timesOutIfClockIsNotRunning)
{
	ErrorCode errCode = ErrorCode_NoError;
	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	Pmc_disablePeripheralClk(&pmc, Pmc_PeripheralId_Mcan0);
	Pmc_disablePeripheralClk(&pmc, Pmc_PeripheralId_Mcan1);
	CHECK_FALSE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_InitializationStartTimeout, errCode);
}

/// \Given initialized and not configured Mcan
///
/// \When Mcan_setConfig() is called with configuration for invalid mode,
///
/// \Then invalid mode error is returned.
///
TEST(MCanTests, Mcan_setConfig_returnsErrorForInvalidMode)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.mode = Mcan_Mode_Invalid;
	CHECK_FALSE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ModeInvalid, errCode);

	errCode = ErrorCode_NoError;
	conf.mode = arbitraryEnumValue<Mcan_Mode>(0xDEADu);
	CHECK_FALSE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ModeInvalid, errCode);
}

/// @}

/// Mcan_rxFifoPull()
/// @{

/// \Given initialized and configured Mcan
///
/// \When Mcan_rxFifoPull() is called with invalid RX FIFO id,
///
/// \Then invalid RX fifo id error is returned.
///
TEST(MCanTests, Mcan_rxFifoPull_returnsErrorForInvalidFifoId)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Mcan_RxElement element{};
	CHECK_FALSE(Mcan_rxFifoPull(&mcan,
			arbitraryEnumValue<Mcan_RxFifoId>(0xDEAD), &element,
			&errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_InvalidRxFifoId, errCode);
}

/// \Given initialized and configured Mcan, with empty RX Fifos
///
/// \When data is pulled from the queue
///
/// \Then appropriate error code is returned
///
TEST(MCanTests, Mcan_rxFifoPull_returnsErrorWhenFifoIsEmpty)
{
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Mcan_RxElement element;
	CHECK_FALSE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_0, &element, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_RxFifoEmpty, errCode);

	errCode = ErrorCode_NoError;
	CHECK_FALSE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_1, &element, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_RxFifoEmpty, errCode);
}

/// @}
