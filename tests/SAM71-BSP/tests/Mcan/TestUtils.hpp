/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2023-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef N7S_MCAN_TESTS_TESTUTILS_HPP
#define N7S_MCAN_TESTS_TESTUTILS_HPP

/// \file  TestUtils.hpp
/// \brief MCAN driver tests helper header.

#include <array>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <n7s/bsp/Mcan/Mcan.h>
#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Scb/Scb.h>

#if defined(N7S_TARGET_SAMRH71F20)
#include <n7s/bsp/Matrix/Matrix.h>
#endif

TEST_BASE(TestUtils)
{
	constexpr static uint32_t CONFIG_TIMEOUT = 1000u;

	constexpr static uint32_t MSGRAM_SIZE = 1024u;
	constexpr static uint32_t MSGRAM_STDID_FILTER_OFFSET = 0u;
	constexpr static uint32_t MSGRAM_EXTID_FILTER_OFFSET = 4u;
	constexpr static uint32_t MSGRAM_RXFIFO0_OFFSET = 16u;
	constexpr static uint32_t MSGRAM_RXFIFO1_OFFSET = 144u;
	constexpr static uint32_t MSGRAM_RXBUFFER_OFFSET = 272u;
	constexpr static uint32_t MSGRAM_TXEVENTFIFO_OFFSET = 400u;
	constexpr static uint32_t MSGRAM_TXBUFFER_OFFSET = 432u;
	constexpr static uint32_t MCAN_TEST_TIMEOUT = 100000u;

#if defined(N7S_TARGET_SAMRH707F18)
	alignas(4096) static inline __attribute__((section(
			".ramdata.mcan"))) std::array<uint32_t, MSGRAM_SIZE>
			msgRam{};
#else
	alignas(4096) static inline std::array<uint32_t, MSGRAM_SIZE> msgRam{};
#endif

	static inline Mcan mcan;
	static inline const Mcan_Config defaultConfig{
    .msgRamBaseAddress = msgRam.data(),
    .mode = Mcan_Mode_InternalLoopBackTest,
    .isFdEnabled = true,
    .nominalBitTiming = {
      .bitRatePrescaler = 23u,
      .synchronizationJump = 1u,
      .timeSegmentAfterSamplePoint = 1u,
      .timeSegmentBeforeSamplePoint = 1u,
    },
    .dataBitTiming = {
      .bitRatePrescaler = 1u,
      .synchronizationJump = 1u,
      .timeSegmentAfterSamplePoint = 1u,
      .timeSegmentBeforeSamplePoint = 1u,
    },
    .transmitterDelayCompensation = {
      .isEnabled = false,
      .filter = 0u,
      .offset = 0u,
    },
    .timestampClk = Mcan_TimestampClk_Internal,
    .timestampTimeoutPrescaler = 0u,
    .timeout = {
      .isEnabled = false,
      .type = Mcan_TimeoutType_Continuous,
      .period = 0u,
    },
    .standardIdFilter = {
      .isIdRejected = true,
      .nonMatchingPolicy = Mcan_NonMatchingPolicy_RxFifo0,
      .filterListAddress = nullptr,
      .filterListSize = 0u,
    },
    .extendedIdFilter = {
      .isIdRejected = true,
      .nonMatchingPolicy = Mcan_NonMatchingPolicy_RxFifo0,
      .filterListAddress = nullptr,
      .filterListSize = 0u,
    },
    .rxFifo0 = {
      .isEnabled = true,
      .startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET],
      .size = 1u,
      .watermark = 0u,
      .mode = Mcan_RxFifoOperationMode_Overwrite,
      .elementSize = Mcan_ElementSize_8,
    },
    .rxFifo1 = {
      .isEnabled = true,
      .startAddress = &msgRam[MSGRAM_RXFIFO1_OFFSET],
      .size = 1u,
      .watermark = 0u,
      .mode = Mcan_RxFifoOperationMode_Overwrite,
      .elementSize = Mcan_ElementSize_8,
    },
    .rxBuffer = {
      .startAddress = &msgRam[MSGRAM_RXBUFFER_OFFSET],
      .elementSize = Mcan_ElementSize_8,
    },
    .txBuffer = {
      .isEnabled = true,
      .startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET],
      .bufferSize = 1u,
      .queueSize = 0u,
      .queueType = Mcan_TxQueueType_Fifo,
      .elementSize = Mcan_ElementSize_8,
    },
    .txEventFifo = Mcan_TxEventFifoConfig{},
    .interrupts = {},
    .isLine0InterruptEnabled = false,
    .isLine1InterruptEnabled = false,
    .wdtCounter = 0u,
  };

	static inline volatile bool txCompleteIrqCalled = false;
	static inline volatile bool rxWatermarkIrqCalled = false;
	static inline volatile bool timeoutOccurredIrqCalled = false;

	void setup() override
	{
		Pmc pmc;
		Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());

#if defined(N7S_TARGET_SAMV71Q21)
		const Pmc_PckConfig pckConfig{
			.isEnabled = N7S_BSP_MCAN_PCK_CONFIG_IS_ENABLED,
			.src = N7S_BSP_MCAN_PCK_CONFIG_SRC,
			.presc = N7S_BSP_MCAN_PCK_CONFIG_PRESC,
		};
		CHECK_TRUE(Pmc_setPckConfig(&pmc, N7S_BSP_MCAN_PCK_INDEX,
				&pckConfig, PMC_DEFAULT_TIMEOUT, nullptr));
#endif

#if defined(N7S_TARGET_SAMRH71F20) | defined(N7S_TARGET_SAMRH707F18)
		const Pmc_PeripheralClkConfig mcanClockConfig = {
			.isPeripheralClkEnabled = true,
			.isGclkEnabled = true,
			.gclkSrc = N7S_BSP_MCAN_GCLK_SOURCE,
			.gclkPresc = N7S_BSP_MCAN_GCLK_PRESCALER,
		};
		Pmc_setPeripheralClkConfig(
				&pmc, Pmc_PeripheralId_Mcan0, &mcanClockConfig);
		Pmc_setPeripheralClkConfig(
				&pmc, Pmc_PeripheralId_Mcan1, &mcanClockConfig);
#else
		Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Mcan0);
		Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Mcan1);
#endif

		msgRam.fill(0u);
		Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));

		txCompleteIrqCalled = false;
		rxWatermarkIrqCalled = false;
		timeoutOccurredIrqCalled = false;
#if defined(N7S_TARGET_SAMRH71F20)
		initMatrix0ForMcan();
#endif
	}

	static uint8_t getElementBytesCount(const Mcan_ElementSize elementSize)
	{
		switch (elementSize) {
		case Mcan_ElementSize_8: return 8;
		case Mcan_ElementSize_12: return 12;
		case Mcan_ElementSize_16: return 16;
		case Mcan_ElementSize_20: return 20;
		case Mcan_ElementSize_24: return 24;
		case Mcan_ElementSize_32: return 32;
		case Mcan_ElementSize_48: return 48;
		case Mcan_ElementSize_64: return 64;
		default: return 0;
		}
	}

	static void sendAndReceiveMessage(const Mcan_ElementSize elementSize)
	{
		Mcan_Config conf = defaultConfig;
		conf.standardIdFilter.isIdRejected = true;
		conf.extendedIdFilter.isIdRejected = true;
		conf.extendedIdFilter.nonMatchingPolicy =
				Mcan_NonMatchingPolicy_RxFifo0;
		conf.extendedIdFilter.filterListSize = 0;
		conf.rxFifo0.elementSize = elementSize;
		conf.txBuffer.elementSize = elementSize;

		ErrorCode errCode = ErrorCode_NoError;
		CHECK_TRUE(Mcan_setConfig(
				&mcan, &conf, CONFIG_TIMEOUT, &errCode));
		ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
		Mcan_Config readConfig{};
		Mcan_getConfig(&mcan, &readConfig);
		MEMCMP_EQUAL(&conf, &readConfig, sizeof(Mcan_Config));

		const uint8_t bytesCount = getElementBytesCount(elementSize);
		CHECK(bytesCount != 0u);

		std::array<uint8_t, 64u> txData = {};
		for (uint8_t i = 0; i < bytesCount; i++)
			txData[i] = static_cast<uint8_t>(i + 1u);

		const Mcan_TxElement txElement{
			.esiFlag = Mcan_ElementEsi_Dominant,
			.idType = Mcan_IdType_Extended,
			.frameType = Mcan_FrameType_Data,
			.id = 3u,
			.marker = 1u,
			.isTxEventStored = false,
			.isCanFdFormatEnabled = true,
			.isBitRateSwitchingEnabled = false,
			.dataSize = bytesCount,
			.data = txData.data(),
			.isInterruptEnabled = false,
		};
		CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
		ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

		CHECK_TRUE(evaluateLambdaWithTimeout(
				[]() -> bool {
					return Mcan_txBufferIsTransmissionFinished(
							&mcan, 0);
				},
				MCAN_TEST_TIMEOUT));

		Mcan_RxFifoStatus fifoStatus;
		CHECK_TRUE(Mcan_getRxFifoStatus(
				&mcan, Mcan_RxFifoId_0, &fifoStatus, nullptr));
		UNSIGNED_LONGS_EQUAL(1, fifoStatus.count);
		CHECK_TRUE(fifoStatus.isFull);
		CHECK_FALSE(fifoStatus.isMessageLost);

		std::array<uint8_t, 64u> rxData = {};
		Mcan_RxElement rxElement = {};
		rxElement.data = rxData.data();
		CHECK_TRUE(Mcan_rxFifoPull(
				&mcan, Mcan_RxFifoId_0, &rxElement, &errCode));
		ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

		UNSIGNED_LONGS_EQUAL(3u, rxElement.id);
		CHECK_TRUE(rxElement.isNonMatchingFrame);
		UNSIGNED_LONGS_EQUAL(bytesCount, rxElement.dataSize);
		MEMCMP_EQUAL(rxData.data(), txData.data(), bytesCount);
	}

#if defined(N7S_TARGET_SAMRH71F20)
	// Workaround for HW bug related to MCAN/SPW/GMAC memory access on SAMRH71F20
	static void initMatrix0ForMcan()
	{
		Matrix matrix{};
		Matrix_init(&matrix, Matrix_getDeviceBaseAddress());

		Matrix_SlaveRegionProtectionConfig slaveConfig{};
		for (int32_t i = 0;
				i < static_cast<int32_t>(
						Matrix_ProtectedRegionId_Count);
				++i) {
			Matrix_getSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_Flexram0,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);
			slaveConfig.isPrivilegedRegionUserReadAllowed = true;
			slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
			slaveConfig.regionSplitOffset = Matrix_Size_128MB;
			slaveConfig.regionOrder =
					Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
			Matrix_setSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_Flexram0,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);

			Matrix_getSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_Flexram1,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);
			slaveConfig.isPrivilegedRegionUserReadAllowed = true;
			slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
			slaveConfig.regionSplitOffset = Matrix_Size_128MB;
			slaveConfig.regionOrder =
					Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
			Matrix_setSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_Flexram1,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);

			Matrix_getSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_Flexram2,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);
			slaveConfig.isPrivilegedRegionUserReadAllowed = true;
			slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
			slaveConfig.regionSplitOffset = Matrix_Size_128MB;
			slaveConfig.regionOrder =
					Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
			Matrix_setSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_Flexram2,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);

			Matrix_getSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_AhbSlave,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);
			slaveConfig.isPrivilegedRegionUserReadAllowed = true;
			slaveConfig.isPrivilegedRegionUserWriteAllowed = true;
			slaveConfig.regionSplitOffset = Matrix_Size_128MB;
			slaveConfig.regionOrder =
					Matrix_RegionSplitOrder_UpperPrivilegedLowerUser;
			Matrix_setSlaveRegionProtectionConfig(&matrix,
					Matrix_Slave_AhbSlave,
					static_cast<Matrix_ProtectedRegionId>(
							i),
					&slaveConfig);
		}
	}
#endif
};

#endif // N7S_MCAN_TESTS_TESTUTILS_HPP
