/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Mcan5Tests.cpp
/// \brief Mcan driver test suite implementation.

#include <array>
#include <cstring>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Utils.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

TEST_GROUP_BASE(MCanTests, TestUtils){};

using MCanTests = TEST_GROUP_CppUTestGroupMCanTests;

#ifndef DOXYGEN
void
MCAN1_INT0_Handler(void)
{
	Mcan_InterruptStatus status{};
	Mcan_getInterruptStatus(&MCanTests::mcan, &status);

	if (status.hasTcOccurred)
		MCanTests::txCompleteIrqCalled = true;

	if (status.hasRf0wOccurred)
		MCanTests::rxWatermarkIrqCalled = true;

	if (status.hasTooOccurred)
		MCanTests::timeoutOccurredIrqCalled = true;
}
#endif

/// @name Mcan_setStandardIdFilter()
/// @{

/// \Given initialized and configured Mcan, with standard ID filtering enabled
///
/// \When data is transmitted with matching and non-matching id
///
/// \Then data reception is successful and data is deemed matching and non-matching.
///
TEST(MCanTests, Mcan_filtersStandardIdRxElement)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.standardIdFilter.isIdRejected = false;
	conf.standardIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo1;
	conf.standardIdFilter.filterListSize = 1;
	conf.standardIdFilter.filterListAddress =
			&msgRam[MSGRAM_STDID_FILTER_OFFSET];
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 2;
	conf.txBuffer.queueSize = 0;
	conf.txBuffer.elementSize = Mcan_ElementSize_8;
	conf.rxFifo1.isEnabled = true;
	conf.rxFifo1.startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET];
	conf.rxFifo1.size = 2;
	conf.rxFifo1.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo1.elementSize = Mcan_ElementSize_16;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, nullptr));

	Mcan_RxFilterElement filterElement = {};
	filterElement.config = Mcan_RxFilterConfig_RxBuffer;
	filterElement.id1 = 5u;
	CHECK_TRUE(Mcan_setStandardIdFilter(&mcan, filterElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 8u> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Standard;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 5;
	txElement.marker = 3;
	txElement.isTxEventStored = true;
	txElement.dataSize = 8;
	txElement.data = txData.data();
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	std::array<uint8_t, 8u> rxData = {};
	Mcan_RxElement rxElement = {};
	rxElement.data = rxData.data();
	Mcan_rxBufferGet(&mcan, 0, &rxElement);

	UNSIGNED_LONGS_EQUAL(5, rxElement.id);
	CHECK_FALSE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(8, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 8);

	txElement.id = 7u;
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, nullptr));

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	rxData.fill(0u);
	rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_1, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	UNSIGNED_LONGS_EQUAL(7u, rxElement.id);
	CHECK_TRUE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(8u, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 8u);
}

/// \Given initialized and configured Mcan
///
/// \When Mcan_setStandardIdFilter() is called with filter index beyond configured range,
///
/// \Then index out of range error is returned.
///
TEST(MCanTests, Mcan_setStandardIdFilter_returnsErrorForInvalidIndex)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	const Mcan_RxFilterElement element{
		.type = Mcan_RxFilterType_Mask,
		.config = Mcan_RxFilterConfig_RxFifo0,
		.id1 = 2u,
		.id2 = 2u,
	};

	CHECK_FALSE(Mcan_setStandardIdFilter(&mcan, element,
			defaultConfig.standardIdFilter.filterListSize,
			&errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_IndexOutOfRange, errCode);
}

/// @}

/// @name Mcan_getRxFifoStatus()
/// @{

/// \Given initialized and configured Mcan
///
/// \When Mcan_getRxFifoStatus() is called with invalid RX FIFO id,
///
/// \Then invalid RX fifo id error is returned.
///
TEST(MCanTests, Mcan_getRxFifoStatus_returnsErrorForInvalidFifoId)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Mcan_RxFifoStatus status{};
	CHECK_FALSE(Mcan_getRxFifoStatus(&mcan,
			arbitraryEnumValue<Mcan_RxFifoId>(0xDEAD), &status,
			&errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_InvalidRxFifoId, errCode);
}

/// @}
