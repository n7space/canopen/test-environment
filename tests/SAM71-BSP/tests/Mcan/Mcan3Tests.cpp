/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  Mcan3Tests.cpp
/// \brief Mcan driver test suite implementation.

#include <array>
#include <numeric>
#include <string>

#include <CppUTest/TestHarness.h>

#include <n7s/utils/Bits.h>
#include <n7s/utils/Utils.h>

#include <TestsRuntime/EnumUtils.hpp>
#include <TestsStartup/TestsStartup.h>

#include "TestUtils.hpp"

TEST_GROUP_BASE(MCanTests, TestUtils){};

using MCanTests = TEST_GROUP_CppUTestGroupMCanTests;

#ifndef DOXYGEN
void
MCAN1_INT0_Handler(void)
{
	Mcan_InterruptStatus status{};
	Mcan_getInterruptStatus(&MCanTests::mcan, &status);

	if (status.hasTcOccurred)
		MCanTests::txCompleteIrqCalled = true;

	if (status.hasRf0wOccurred)
		MCanTests::rxWatermarkIrqCalled = true;

	if (status.hasTooOccurred)
		MCanTests::timeoutOccurredIrqCalled = true;
}
#endif

/// @name Mcan_setExtendedIdFilter()
/// @{

/// \Given initialized and configured Mcan, with extended ID filtering enabled
///
/// \When data is transmitted with matching and non-matching id
///
/// \Then data reception is successful and data is deemed matching and non-matching.
///
TEST(MCanTests, Mcan_filtersExtendedIdRxElement)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.extendedIdFilter.isIdRejected = false;
	conf.extendedIdFilter.nonMatchingPolicy =
			Mcan_NonMatchingPolicy_RxFifo0;
	conf.extendedIdFilter.filterListSize = 1;
	conf.extendedIdFilter.filterListAddress =
			&msgRam[MSGRAM_EXTID_FILTER_OFFSET];
	conf.txBuffer.isEnabled = true;
	conf.txBuffer.startAddress = &msgRam[MSGRAM_TXBUFFER_OFFSET];
	conf.txBuffer.bufferSize = 1;
	conf.txBuffer.queueSize = 0;
	conf.txBuffer.elementSize = Mcan_ElementSize_16;
	conf.rxFifo0.isEnabled = true;
	conf.rxFifo0.startAddress = &msgRam[MSGRAM_RXFIFO0_OFFSET];
	conf.rxFifo0.size = 2;
	conf.rxFifo0.mode = Mcan_RxFifoOperationMode_Overwrite;
	conf.rxFifo0.elementSize = Mcan_ElementSize_16;
	conf.txEventFifo.isEnabled = true;
	conf.txEventFifo.startAddress = &msgRam[MSGRAM_TXEVENTFIFO_OFFSET];
	conf.txEventFifo.size = 1;
	conf.txEventFifo.watermark = 0;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, nullptr));

	Mcan_RxFilterElement filterElement = {};
	filterElement.config = Mcan_RxFilterConfig_RxFifo0;
	filterElement.type = Mcan_RxFilterType_Dual;
	filterElement.id1 = 21;
	CHECK_TRUE(Mcan_setExtendedIdFilter(&mcan, filterElement, 0, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	std::array<uint8_t, 16> txData = { 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF,
		0x09, 0x08, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0xF, 0x09, 0x08 };
	Mcan_TxElement txElement = {};
	txElement.esiFlag = Mcan_ElementEsi_Dominant;
	txElement.idType = Mcan_IdType_Extended;
	txElement.frameType = Mcan_FrameType_Data;
	txElement.id = 21;
	txElement.marker = 13;
	txElement.isTxEventStored = true;
	txElement.isCanFdFormatEnabled = true;
	txElement.isBitRateSwitchingEnabled = true;
	txElement.dataSize = 16;
	txElement.data = txData.data();
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, nullptr));

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	Mcan_TxEventElement txEventElement = {};
	CHECK_TRUE(Mcan_txEventFifoPull(&mcan, &txEventElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
	UNSIGNED_LONGS_EQUAL(21u, txEventElement.id);
	UNSIGNED_LONGS_EQUAL(13u, txEventElement.marker);
	ENUMS_EQUAL_INT(Mcan_TxEventType_Tx, txEventElement.eventType);
	UNSIGNED_LONGS_EQUAL(16u, txEventElement.dataSize);

	std::array<uint8_t, 16> rxData = {};
	Mcan_RxElement rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_0, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_FALSE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(21u, rxElement.id);
	UNSIGNED_LONGS_EQUAL(16u, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 16);

	txElement.id = 1;
	CHECK_TRUE(Mcan_txBufferAdd(&mcan, txElement, 0, nullptr));

	CHECK_TRUE(evaluateLambdaWithTimeout(
			[]() -> bool {
				return Mcan_txBufferIsTransmissionFinished(
						&mcan, 0);
			},
			MCAN_TEST_TIMEOUT));

	rxData.fill(0);
	rxElement = {};
	rxElement.data = rxData.data();
	CHECK_TRUE(Mcan_rxFifoPull(
			&mcan, Mcan_RxFifoId_0, &rxElement, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	CHECK_TRUE(rxElement.isNonMatchingFrame);
	UNSIGNED_LONGS_EQUAL(1, rxElement.id);
	UNSIGNED_LONGS_EQUAL(16, rxElement.dataSize);
	MEMCMP_EQUAL(rxData.data(), txData.data(), 16);
}

/// \Given initialized and configured Mcan
///
/// \When Mcan_setExtendedIdFilter() is called with filter index beyond configured range,
///
/// \Then index out of range error is returned.
///
TEST(MCanTests, Mcan_setExtendedIdFilter_returnsErrorForInvalidIndex)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	const Mcan_RxFilterElement element{
		.type = Mcan_RxFilterType_Dual,
		.config = Mcan_RxFilterConfig_RxFifo1,
		.id1 = 226u,
		.id2 = 228u,
	};

	CHECK_FALSE(Mcan_setExtendedIdFilter(&mcan, element,
			defaultConfig.extendedIdFilter.filterListSize,
			&errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_IndexOutOfRange, errCode);
}

/// @}

/// @name Mcan_txBufferAdd()
/// @{

/// \Given initialized and configured Mcan
///
/// \When Mcan_txBufferAdd() is called with buffer index beyond configured range,
///
/// \Then index out of range error is returned.
///
TEST(MCanTests, Mcan_txBufferAdd_returnsErrorForInvalidIndex)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	const Mcan_TxElement element{};

	CHECK_FALSE(Mcan_txBufferAdd(&mcan, element,
			defaultConfig.txBuffer.bufferSize, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_IndexOutOfRange, errCode);
}

/// \Given initialized and configured Mcan with disabled tx buffer
///
/// \When Mcan_txBufferAdd() is called with any buffer index,
///
/// \Then index out of range error is returned.
///
TEST(MCanTests, Mcan_txBufferAdd_returnsErrorForDisabledTxBuffer)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.txBuffer.isEnabled = false;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	const Mcan_TxElement element{};

	CHECK_FALSE(Mcan_txBufferAdd(&mcan, element, 0u, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_IndexOutOfRange, errCode);
}

/// \Given initialized and configured Mcan
///
/// \When Mcan_txBufferAdd() is called with element containing invalid size,
///
/// \Then invalid element size error is returned.
///
TEST(MCanTests, Mcan_txBufferAdd_returnsErrorForInvalidElementSize)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	const std::array<uint8_t, 80u> dummy{};
	const Mcan_TxElement txElement{
		.esiFlag = Mcan_ElementEsi_Dominant,
		.idType = Mcan_IdType_Extended,
		.frameType = Mcan_FrameType_Data,
		.id = 3u,
		.marker = 1u,
		.isTxEventStored = false,
		.isCanFdFormatEnabled = true,
		.isBitRateSwitchingEnabled = false,
		.dataSize = dummy.size(),
		.data = dummy.data(),
		.isInterruptEnabled = false,
	};

	CHECK_FALSE(Mcan_txBufferAdd(&mcan, txElement, 0u, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ElementSizeInvalid, errCode);
}

/// @}

/// @name Mcan_getDeviceRegisters()
/// @{

/// \Given N/A
///
/// \When Mcan_getDeviceRegisters() is called with valid Mcan ID,
///
/// \Then Correct register addresses are returned for each supported MCAN.
///
TEST(MCanTests, Mcan_getDeviceRegisters_returnsCorrectAddresses)
{
	const Mcan_Registers mcan0Regs = Mcan_getDeviceRegisters(Mcan_Id_0);
	const Mcan_Registers mcan1Regs = Mcan_getDeviceRegisters(Mcan_Id_1);

#if defined(N7S_TARGET_SAMV71Q21)
	POINTERS_EQUAL(0x40030000u, mcan0Regs.base);
	POINTERS_EQUAL(0x40088110u, mcan0Regs.canDmaBase);

	POINTERS_EQUAL(0x40034000u, mcan1Regs.base);
	POINTERS_EQUAL(0x40088114u, mcan1Regs.canDmaBase);
#elif defined(N7S_TARGET_SAMRH71F20)
	POINTERS_EQUAL(0x40058000u, mcan0Regs.base);
	POINTERS_EQUAL(0x400A00A0u, mcan0Regs.canDmaBase);

	POINTERS_EQUAL(0x4005C000u, mcan1Regs.base);
	POINTERS_EQUAL(0x400A00A4u, mcan1Regs.canDmaBase);
#elif defined(N7S_TARGET_SAMRH707F18)
	POINTERS_EQUAL(0x40058000u, mcan0Regs.base);
	POINTERS_EQUAL(0x400A00A0u, mcan0Regs.canDmaBase);

	POINTERS_EQUAL(0x4005C000u, mcan1Regs.base);
	POINTERS_EQUAL(0x400A00A4u, mcan1Regs.canDmaBase);
#endif
}

/// \Given N/A
///
/// \When Mcan_getDeviceRegisters() is called with invalid Mcan ID,
///
/// \Then NULL pointers are returned.
///
TEST(MCanTests, Mcan_getDeviceRegisters_returnsNullForInvalidId)
{
	const Mcan_Registers mcanRegs = Mcan_getDeviceRegisters(
			arbitraryEnumValue<Mcan_Id>(0xDEADu));
	POINTERS_EQUAL(nullptr, mcanRegs.base);
	POINTERS_EQUAL(nullptr, mcanRegs.canDmaBase);
}

/// @}

/// @name Mcan_txEventFifoPull()
/// @{

/// \Given initialized and configured Mcan, with empty TX Event Fifo
///
/// \When Mcan_txEventFifoPull() is called
///
/// \Then TX event fifo empty error code is returned
///
TEST(MCanTests, Mcan_txEventFifoPull_decodesDataLengthField)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan mockMcan{};
	Mcan_BaseRegisters mockRegs{};
	uint32_t mockDmaBase = 0u;
	std::array<uint32_t, 2u> mockTxEventFifo{};
	std::array<uint32_t, 16u> expectedlengthsNonFd{};
	expectedlengthsNonFd.fill(8u);
	// For non-FD frames dlc >= 8 means 8
	std::iota(expectedlengthsNonFd.begin(),
			expectedlengthsNonFd.begin() + 8u, 0u);
	// For FD frames decoded lengths are different
	const std::array<uint32_t, 16u> expectedlengthsFd{ 0u, 1u, 2u, 3u, 4u,
		5u, 6u, 7u, 8u, 12u, 16u, 20u, 24u, 32u, 48u, 64u };

	// Replace registers with mocks
	mockMcan.reg.base = &mockRegs;
	mockMcan.reg.canDmaBase = &mockDmaBase;

	CHECK_TRUE(Mcan_setConfig(
			&mockMcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	// Replace Tx event fifo with mock
	mockMcan.txEventFifoAddress = mockTxEventFifo.data();
	mockMcan.txEventFifoSize = 1u;

	for (uint32_t dlc = 0; dlc < 16u; dlc++) {
		// set data length code (4 bits)
		mockTxEventFifo[1u] = dlc << 16u;
		// Mock presence of TX event in FIFO, set get index to 0
		mockRegs.txefs = 1u;

		// Read element from FIFO and clech decoded data length
		Mcan_TxEventElement element;
		CHECK_TRUE_TEXT(Mcan_txEventFifoPull(
						&mockMcan, &element, &errCode),
				(std::string("Non FD, DLC = ")
						+ std::to_string(dlc))
						.c_str());
		ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
		UNSIGNED_LONGS_EQUAL(
				expectedlengthsNonFd[dlc], element.dataSize);

		// Enable FD marker and pull the same element from FIFO again
		mockTxEventFifo[1u] |= UINT32_C(1) << 21u;
		(void)mockTxEventFifo;
		CHECK_TRUE_TEXT(Mcan_txEventFifoPull(
						&mockMcan, &element, &errCode),
				(std::string("FD, DLC = ")
						+ std::to_string(dlc))
						.c_str());
		ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);
		UNSIGNED_LONGS_EQUAL(expectedlengthsFd[dlc], element.dataSize);
	}
}

/// \Given initialized and configured Mcan, with empty TX Event Fifo
///
/// \When Mcan_txEventFifoPull() is called
///
/// \Then TX event fifo empty error code is returned
///
TEST(MCanTests, Mcan_txEventFifoPull_returnsErrorWhenFifoIsEmpty)
{
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	Mcan_TxEventElement element;
	CHECK_FALSE(Mcan_txEventFifoPull(&mcan, &element, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_TxEventFifoEmpty, errCode);
}

/// @}

/// @name Mcan_txQueuePush()
/// @{

/// \Given initialized and configured Mcan, with full TX queue
///
/// \When Mcan_txQueuePush() is called,
///
/// \Then TX queue full error code is returned
///
TEST(MCanTests, Mcan_txQueuePush_returnsErrorForFullQueue)
{
	ErrorCode errCode = ErrorCode_NoError;
	Mcan_Config conf = defaultConfig;
	conf.txBuffer.queueType = Mcan_TxQueueType_Fifo;
	conf.txBuffer.queueSize = 1u;
	CHECK_TRUE(Mcan_setConfig(&mcan, &conf, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	uint8_t index{};
	CHECK_TRUE(Mcan_txQueuePush(&mcan, {}, &index, &errCode));
	CHECK_FALSE(Mcan_txQueuePush(&mcan, {}, &index, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_TxFifoFull, errCode);
}

/// \Given initialized and configured Mcan
///
/// \When Mcan_txQueuePush() is called with element containing invalid size,
///
/// \Then invalid element size error is returned.
///
TEST(MCanTests, Mcan_txQueuePush_returnsErrorForInvalidElementSize)
{
	Mcan_init(&mcan, Mcan_getDeviceRegisters(Mcan_Id_1));
	ErrorCode errCode = ErrorCode_NoError;
	CHECK_TRUE(Mcan_setConfig(
			&mcan, &defaultConfig, CONFIG_TIMEOUT, &errCode));
	ENUMS_EQUAL_INT(ErrorCode_NoError, errCode);

	const std::array<uint8_t, 80u> dummy{};
	const Mcan_TxElement txElement{
		.esiFlag = Mcan_ElementEsi_Dominant,
		.idType = Mcan_IdType_Extended,
		.frameType = Mcan_FrameType_Data,
		.id = 3u,
		.marker = 1u,
		.isTxEventStored = false,
		.isCanFdFormatEnabled = true,
		.isBitRateSwitchingEnabled = false,
		.dataSize = dummy.size(),
		.data = dummy.data(),
		.isInterruptEnabled = false,
	};

	uint8_t index = 0u;
	CHECK_FALSE(Mcan_txQueuePush(&mcan, txElement, &index, &errCode));
	ENUMS_EQUAL_INT(Mcan_ErrorCode_ElementSizeInvalid, errCode);
}

/// @}
