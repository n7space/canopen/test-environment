/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file FpuTests.cpp
/// \brief Fpu driver test suite implementation.

#include <cstring>

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/OrderedTest.h>

#include <n7s/bsp/Fpu/Fpu.h>
#include <n7s/bsp/Fpu/FpuRegisters.h>
#include <n7s/bsp/Scb/Scb.h>

#include <TestsRuntime/SuiteFixture.h>
#include <TestsStartup/TestsStartup.h>

#ifdef CLANG_TIDY
// Clang-tidy assumes x86 architecture.
#define ASM_R0 "eax" // NOLINT
#define ASM_S4 "ecx" // NOLINT
#define ASM_S6 "edx" // NOLINT
#define ASM_S15 "ebx" // NOLINT
#else
#define ASM_R0 "r0" // NOLINT
#define ASM_S4 "s4" // NOLINT
#define ASM_S6 "s6" // NOLINT
#define ASM_S15 "s15" // NOLINT
#endif

TEST_GROUP(FpuTests)
{
      public:
	static inline Fpu_Config reset_config = {};

	static inline uint32_t nocpFaults = 0;
	static inline uint32_t otherFaults = 0;
};

using FpuTests = TEST_GROUP_CppUTestGroupFpuTests;

static void __attribute__((noinline))
copyMemory(volatile uint32_t *const dst, const volatile float *const src)
{
	*dst = *reinterpret_cast<const volatile uint32_t *>(src);
}

static void __attribute__((noinline))
copyMemory(volatile float *const dst, const volatile uint32_t *const src)
{
	*dst = *reinterpret_cast<const volatile float *>(src);
}

void
SuiteFixture_setup(void)
{
	// Some tests are written in assembly and will cause FPU exceptions,
	// caching can interfere with their operation
	(void)Scb_disableDCache();

	// Retrieve fresh after-reset configuration before tests are run
	Fpu fpu;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getConfig(&fpu, &FpuTests::reset_config);
}

#define FLOAT_POINT_OPERATION \
	asm volatile("vabs.f32 s4, s6" : : : ASM_S4, ASM_S6)

static uint32_t __attribute__((noinline))
asInt(const float f)
{
	uint32_t r = 0u;
	copyMemory(&r, &f);
	return r;
}

static float __attribute__((noinline))
asFloat(const uint32_t i)
{
	float r = 0.0;
	copyMemory(&r, &i);
	return r;
}

/// @name Fpu_getStatus()
/// @{

/// \Given an Fpu with dummy registers,
///
/// \When status is read,
///
/// \Then the read status match the ones in the dummy registers.
///
TEST(FpuTests, Fpu_getStatus_match)
{
	Fpu fpu;
	Fpu_Registers reg;
	Fpu_Status status;
	(void)memset(&reg, 0, sizeof(Fpu_Registers));
	fpu.registers = &reg;

	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_ASPEN_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_BFRDY_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_TRUE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_MONRDY_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_TRUE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_HFRDY_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_TRUE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_MMRDY_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_TRUE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_LSPACT_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_TRUE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_THREAD_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_TRUE(status.wasProcessorInThreadMode);
	CHECK_FALSE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_USER_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_FALSE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_FALSE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_FALSE(status.wasProcessorInThreadMode);
	CHECK_TRUE(status.wasProcessorInUserMode);
	CHECK_FALSE(status.isLazyFpStatePreservationActive);

	reg.fpccr = FPU_FPCCR_USER_MASK | FPU_FPCCR_THREAD_MASK
			| FPU_FPCCR_LSPEN_MASK | FPU_FPCCR_MMRDY_MASK
			| FPU_FPCCR_ASPEN_MASK | FPU_FPCCR_BFRDY_MASK
			| FPU_FPCCR_MONRDY_MASK | FPU_FPCCR_HFRDY_MASK
			| FPU_FPCCR_LSPACT_MASK;
	Fpu_getStatus(&fpu, &status);

	CHECK_TRUE(status.couldDebugMonitorExceptionPendingbeBeSet);
	CHECK_TRUE(status.couldBusFaultExceptionPendingbeBeSet);
	CHECK_TRUE(status.couldMemManageExceptionPendingbeBeSet);
	CHECK_TRUE(status.couldHardFaultExceptionPendingbeBeSet);
	CHECK_TRUE(status.wasProcessorInThreadMode);
	CHECK_TRUE(status.wasProcessorInUserMode);
	CHECK_TRUE(status.isLazyFpStatePreservationActive);
}

/// @}

/// @name Fpu_setConfig()
/// @{

/// \Given an initialized and started up Fpu,
///
/// \When Fpu_setConfig() function is called for this Fpu with a configuration descriptor set to a
///       nominal value,
///
/// \Then Fpu configuration descriptor retrieved using Fpu_getConfig() function is the same as the
///       one used to set the Fpu configuration.
///
TEST(FpuTests, Fpu_correctlySetsAndGetsConfig)
{
	Fpu fpu;
	Fpu_Config writtenConfig;
	Fpu_Config readConfig;
	Fpu_Config savedConfig;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	// Save old config.
	Fpu_getConfig(&fpu, &savedConfig);

	writtenConfig.defaultFlushToZeroMode = Fpu_FlushToZeroMode_Enabled;
	writtenConfig.defaultHalfPrecisionMode =
			Fpu_AlternativeHalfPrecisionMode_Alternative;
	writtenConfig.defaultNanMode = Fpu_NanMode_NanOperandsPropagate;
	writtenConfig.defaultRoundingMode =
			Fpu_RoundingMode_RoundTowardsPlusInfinity;
	writtenConfig.exceptionFpRegisterSpaceAddress = 0x1969;
	writtenConfig.isFpContextPreservedOnException = false;
	Fpu_setConfig(&fpu, &writtenConfig);
	Fpu_getConfig(&fpu, &readConfig);

	CHECK_FALSE(readConfig.isFpContextPreservedOnException);
	UNSIGNED_LONGS_EQUAL(
			0x1969, readConfig.exceptionFpRegisterSpaceAddress);
	ENUMS_EQUAL_INT(Fpu_FlushToZeroMode_Enabled,
			readConfig.defaultFlushToZeroMode);
	ENUMS_EQUAL_INT(Fpu_AlternativeHalfPrecisionMode_Alternative,
			readConfig.defaultHalfPrecisionMode);
	ENUMS_EQUAL_INT(Fpu_NanMode_NanOperandsPropagate,
			readConfig.defaultNanMode);
	ENUMS_EQUAL_INT(Fpu_RoundingMode_RoundTowardsPlusInfinity,
			readConfig.defaultRoundingMode);

	Fpu_setConfig(&fpu, &savedConfig);
}

/// \Given an initialized, started up Fpu with forced disabled lazy stacking,
///
/// \When Fpu_setConfig() function is called for this Fpu with a configuration descriptor set to a
///       nominal value,
///
/// \Then Fpu is configured and lazy stacking is enabled during settings configuration.
///
TEST(FpuTests, Fpu_correctlySetsLazyStacking)
{
	Fpu fpu;
	Fpu_Config savedConfig;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	// Save config.
	Fpu_getConfig(&fpu, &savedConfig);

	// Forcing fpccr to be zero
	fpu.registers->fpccr = 0u;

	// Lazy stacking is disabled.
	CHECK_TRUE((fpu.registers->fpccr & FPU_FPCCR_LSPEN_MASK) == 0u);

	Fpu_setConfig(&fpu, &savedConfig);

	// Lazy stacking is enabled.
	CHECK_FALSE((fpu.registers->fpccr & FPU_FPCCR_LSPEN_MASK) == 0u);

	Fpu_setConfig(&fpu, &savedConfig);
}

/// @}

/// @name Fpu_getConfig()
/// @{

/// \Given an initialized and started up Fpu,
///
/// \When default config is read,
///
/// \Then the read values match the ones in the HW manual.
///
TEST(FpuTests, Fpu_correctlyReadsResetValues)
{
	// Use reset_config acquired in suite setup
	CHECK_TRUE(FpuTests::reset_config.isFpContextPreservedOnException);
}

/// @}

/// @name Fpu_getFeatures()
/// @{

/// \Given an initialized and started up Fpu,
///
/// \When Fpu HW features are queried,
///
/// \Then the queried features match the ones described in the SOC manual.
///
TEST(FpuTests, Fpu_hasCorrectFeatures)
{
	Fpu fpu;
	Fpu_Features features;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getFeatures(&fpu, &features);
	CHECK_TRUE(features.areDivideOperationsSupported);
	CHECK_TRUE(features.areDoublePrecisionOperationsSupported);
	CHECK_TRUE(features.areFpFusedMacOperationsSupported);
	CHECK_TRUE(features.areMiscellaneousFeaturesSupported);
	CHECK_TRUE(features.areRoundingModesSupported);
	CHECK_FALSE(features.areShortVectorsSupported);
	CHECK_TRUE(features.areSinglePrecisionOperationsSupported);
	CHECK_TRUE(features.areSquareRootOperationsSupported);
	CHECK_FALSE(features.isExceptionTrappingSupported);
	CHECK_TRUE(features.isFullDenormalizedNumberArithmeticSupported);
	CHECK_TRUE(features.isNanValuePropagationSupported);
	ENUMS_EQUAL_INT(Fpu_RegisterBankSize_16, features.fpuRegisterBankSize);
}

/// @}

/// @name Fpu_getContextConfig()
/// @{

/// \Given an initialized and started up Fpu,
///
/// \When Fpu_setContextConfig() function is called with Fpu context config descriptor set to a
///       nominal value,
///
/// \Then Fpu context configuration descriptor retrieved using Fpu_getContextConfig() function is
///       the same as the one used to set the Fpu context configuration.
///
TEST(FpuTests, Fpu_correctlySetsAndGetsContextConfig)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_ContextConfig writtenConfig;
	Fpu_ContextConfig readConfig;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);

	writtenConfig.flushToZeroMode = Fpu_FlushToZeroMode_Disabled;
	writtenConfig.halfPrecisionMode =
			Fpu_AlternativeHalfPrecisionMode_Alternative;
	writtenConfig.nanMode = Fpu_NanMode_NanOperandsPropagate;
	writtenConfig.roundingMode = Fpu_RoundingMode_RoundTowardsMinusInfinity;

	Fpu_setContextConfig(&writtenConfig);
	Fpu_getContextConfig(&readConfig);

	ENUMS_EQUAL_INT(Fpu_FlushToZeroMode_Disabled,
			readConfig.flushToZeroMode);
	ENUMS_EQUAL_INT(Fpu_AlternativeHalfPrecisionMode_Alternative,
			readConfig.halfPrecisionMode);
	ENUMS_EQUAL_INT(Fpu_NanMode_NanOperandsPropagate, readConfig.nanMode);
	ENUMS_EQUAL_INT(Fpu_RoundingMode_RoundTowardsMinusInfinity,
			readConfig.roundingMode);

	writtenConfig.flushToZeroMode = Fpu_FlushToZeroMode_Enabled;
	writtenConfig.halfPrecisionMode =
			Fpu_AlternativeHalfPrecisionMode_Ieee7542008;
	writtenConfig.nanMode = Fpu_NanMode_DefaultNanReturned;
	writtenConfig.roundingMode = Fpu_RoundingMode_RoundTowardsPlusInfinity;

	Fpu_setContextConfig(&writtenConfig);
	Fpu_getContextConfig(&readConfig);

	ENUMS_EQUAL_INT(Fpu_FlushToZeroMode_Enabled,
			readConfig.flushToZeroMode);
	ENUMS_EQUAL_INT(Fpu_AlternativeHalfPrecisionMode_Ieee7542008,
			readConfig.halfPrecisionMode);
	ENUMS_EQUAL_INT(Fpu_NanMode_DefaultNanReturned, readConfig.nanMode);
	ENUMS_EQUAL_INT(Fpu_RoundingMode_RoundTowardsPlusInfinity,
			readConfig.roundingMode);

	Fpu_setContextConfig(&savedConfig);
}

namespace
{
int32_t __attribute__((noinline))
convertFloatToIntUsingFpuRoundingMode(const float f)
{
	/// Use hardware float to int conversion.
	/// GCC generates VCVT variant which rounds to 0.
	/// Setting different compiler flags may change this behaviour.
	/// However, a unit-test should be as compiler independent as possible.
	int32_t rounded{};
	asm volatile("vmov s15, %1 \n\t" /// Move f to a floating point register.
		     "vcvtr.s32.f32 s15, s15 \n\t" /// Convert using the rounding mode specified in FPSCR.
		     "vmov %0, s15 \n\t" /// Write back the result.
			: "=r"(rounded)
			: "r"(f)
			: ASM_S15, "memory");

	return rounded;
}
} // namespace

/// @}

static float __attribute__((noinline))
getDenormalizedFloat()
{
	return asFloat(0x00000001u);
}

static float __attribute__((noinline))
getSignallingNanFloat(const uint32_t bitRemainder)
{
	return asFloat(0x7fc00000u | (bitRemainder & 0x003FFFFFu));
}

/// @name Fpu_setContextConfig()
/// @{

/// \Given an initialized and started up Fpu,
///
/// \When a rounding mode is set,
///
/// \Then the floating-point operations use the set mode.
///
TEST(FpuTests, Fpu_correctlyHandlesRoundingModes)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_ContextConfig config;

	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);
	Fpu_getContextConfig(&config);

	// NOTE: Fpu_startup function used above will erase all FPU registers,
	// mark const floats as volatile to force placement in memory
	// If they are placed in registers, values will be lost.
	const volatile float almostTen = 10.4f;
	const volatile float almostEleven = 10.6f;
	const volatile float almostMinusEleven = -10.6f;

	config.roundingMode = Fpu_RoundingMode_RoundToNearest;
	Fpu_setContextConfig(&config);

	LONGS_EQUAL(10, convertFloatToIntUsingFpuRoundingMode(almostTen));
	LONGS_EQUAL(11, convertFloatToIntUsingFpuRoundingMode(almostEleven));
	LONGS_EQUAL(-11,
			convertFloatToIntUsingFpuRoundingMode(
					almostMinusEleven));

	config.roundingMode = Fpu_RoundingMode_RoundTowardsMinusInfinity;
	Fpu_setContextConfig(&config);

	LONGS_EQUAL(10, convertFloatToIntUsingFpuRoundingMode(almostTen));
	LONGS_EQUAL(10, convertFloatToIntUsingFpuRoundingMode(almostEleven));
	LONGS_EQUAL(-11,
			convertFloatToIntUsingFpuRoundingMode(
					almostMinusEleven));

	config.roundingMode = Fpu_RoundingMode_RoundTowardsPlusInfinity;
	Fpu_setContextConfig(&config);

	LONGS_EQUAL(11, convertFloatToIntUsingFpuRoundingMode(almostTen));
	LONGS_EQUAL(11, convertFloatToIntUsingFpuRoundingMode(almostEleven));
	LONGS_EQUAL(-10,
			convertFloatToIntUsingFpuRoundingMode(
					almostMinusEleven));

	config.roundingMode = Fpu_RoundingMode_RoundTowardsZero;
	Fpu_setContextConfig(&config);

	LONGS_EQUAL(10, convertFloatToIntUsingFpuRoundingMode(almostTen));
	LONGS_EQUAL(10, convertFloatToIntUsingFpuRoundingMode(almostEleven));
	LONGS_EQUAL(-10,
			convertFloatToIntUsingFpuRoundingMode(
					almostMinusEleven));

	Fpu_setContextConfig(&savedConfig);
}

/// \Given an initialized and started up Fpu,
///
/// \When Fpu_setContextConfig() function is called for this Fpu with a configuration descriptor
///       having NaN mode and flush-to-zero mode options disabled,
///
/// \Then this Fpu is in full compliance operation mode.
///
TEST(FpuTests, Fpu_enablesFullComplianceOperationMode)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_ContextConfig config;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);
	Fpu_getContextConfig(&config);
	config.nanMode = Fpu_NanMode_NanOperandsPropagate;
	config.flushToZeroMode = Fpu_FlushToZeroMode_Disabled;
	Fpu_setContextConfig(&config);
	Fpu_getContextConfig(&config);

	// According to documentation full-compliance mode (operations are done according to the
	// IEE 754 standard) is enabled when DN (NaN mode) and FZ (flush-to-zero mode) are disabled.
	CHECK_FALSE(config.nanMode);
	CHECK_FALSE(config.flushToZeroMode);

	Fpu_setContextConfig(&savedConfig);
}

/// \Given an initialized and started up Fpu,
///
/// \When flush-to-zero mode is configured to be enabled or disabled,
///
/// \Then denormalized floats behave as requested.
///
TEST(FpuTests, Fpu_correctlyHandlesFlushToZero)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_ContextConfig config;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);
	Fpu_getContextConfig(&config);
	config.flushToZeroMode = Fpu_FlushToZeroMode_Disabled;
	Fpu_setContextConfig(&config);
	CHECK_TRUE(getDenormalizedFloat() > 0.0f);
	CHECK_FALSE(getDenormalizedFloat() == 0.0f);

	config.flushToZeroMode = Fpu_FlushToZeroMode_Enabled;
	Fpu_setContextConfig(&config);
	CHECK_FALSE(getDenormalizedFloat() > 0.0f);
	CHECK_TRUE(getDenormalizedFloat() == 0.0f);

	Fpu_setContextConfig(&savedConfig);
}

/// \Given an initialized and started up Fpu,
///
/// \When NaN mode is configured to be enabled or disabled,
///
/// \Then NaN floats behave as requested.
///
TEST(FpuTests, Fpu_correctlyHandlesNans)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_ContextConfig config;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);
	Fpu_getContextConfig(&config);
	config.nanMode = Fpu_NanMode_NanOperandsPropagate;
	Fpu_setContextConfig(&config);
	/// 2.1117f is used to avoid compiler optimizations which change
	/// multiplications to movs or additions.
	const float nan1 = 2.1117f * getSignallingNanFloat(0xaaaa);
	UNSIGNED_LONGS_EQUAL(0x7fc0aaaa, asInt(nan1));

	config.nanMode = Fpu_NanMode_DefaultNanReturned;
	Fpu_setContextConfig(&config);
	const float nan2 = 2.1117f * getSignallingNanFloat(0xaaaa);
	UNSIGNED_LONGS_EQUAL(0x7fC00000, asInt(nan2));

	Fpu_setContextConfig(&savedConfig);
}

/// @}

static inline void
checkIfAllExceptionsAreCleared()
{
	Fpu_ContextState contextState;
	Fpu_clearExceptions();
	Fpu_getContextState(&contextState);
	CHECK_FALSE(contextState.isInputDenormalCumulativeExceptionBitSet);
	CHECK_FALSE(contextState.isInexactCumulativeExceptionBitSet);
	CHECK_FALSE(contextState.isUnderflowCumulativeExceptionBitSet);
	CHECK_FALSE(contextState.isOverflowCumulativeExceptionBitSet);
	CHECK_FALSE(contextState.isDivisionByZeroCumulativeExceptionBitSet);
	CHECK_FALSE(contextState.isInvalidOperationCumulativeExceptionBitSet);
}

static inline void
checkInvalidOperationException()
{
	Fpu_ContextState contextState;
	Fpu_clearExceptions();
	const volatile float a = 0.0f;
	const volatile float b = 0.0f;
	const volatile float c = a / b;
	(void)c;
	Fpu_getContextState(&contextState);
	CHECK_TRUE(contextState.isInvalidOperationCumulativeExceptionBitSet);
}

static inline void
checkDivisionByZeroException()
{
	Fpu_ContextState contextState;
	Fpu_clearExceptions();
	const volatile float a = 1.0f;
	const volatile float b = 0.0f;
	const volatile float c = a / b;
	(void)c;
	Fpu_getContextState(&contextState);
	CHECK_TRUE(contextState.isDivisionByZeroCumulativeExceptionBitSet);
}

static inline void
checkInexactAndUnderflowException()
{
	Fpu_ContextState contextState;
	Fpu_ContextConfig config;
	Fpu_getContextConfig(&config);
	config.flushToZeroMode = Fpu_FlushToZeroMode_Disabled;
	Fpu_setContextConfig(&config);
	Fpu_clearExceptions();
	const volatile float a = 0.00000000000000000000000000000001f;
	const volatile float b = 0.00000000000000000000000000000001f;
	const volatile float c = a * b;
	(void)c;
	Fpu_getContextState(&contextState);
	CHECK_TRUE(contextState.isUnderflowCumulativeExceptionBitSet);
	CHECK_TRUE(contextState.isInexactCumulativeExceptionBitSet);
}

static inline void
checkInexactAndOverflowException()
{
	Fpu_ContextState contextState;
	Fpu_clearExceptions();
	const volatile float a = 999999999999999999999999999999999.9f;
	const volatile float b = 999999999999999999999999999999999.9f;
	const volatile float c = a * b;
	(void)c;
	Fpu_getContextState(&contextState);
	CHECK_TRUE(contextState.isOverflowCumulativeExceptionBitSet);
	CHECK_TRUE(contextState.isInexactCumulativeExceptionBitSet);
}

namespace
{
void
checkInputDenormalException()
{
	Fpu_ContextState contextState;
	Fpu_ContextConfig config;
	Fpu_getContextConfig(&config);
	config.flushToZeroMode = Fpu_FlushToZeroMode_Enabled;
	Fpu_setContextConfig(&config);
	Fpu_clearExceptions();
	const float a = getDenormalizedFloat();
	asm volatile("vmov s15, %0 \n\t" /// Move the denormalized float to a floating point register.
		     "fsqrts s15, s15 \n\t" /// Get the square root of the denormalized float.
			:
			: "r"(a)
			: ASM_S15);
	Fpu_getContextState(&contextState);
	CHECK_TRUE(contextState.isInputDenormalCumulativeExceptionBitSet);
}
} // namespace

/// @name FPU errors
/// @{

/// \Given an initialized and started up Fpu,
///
/// \When following invalid operations on floating point data are used: dividing by zero, too small
///       value, too large value,
///
/// \Then correct error exceptions flags are set in a FPU context state structure provided by
///       Fpu_getContextState() function.
///
TEST(FpuTests, Fpu_correctlyDetectsErrors)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);

	checkIfAllExceptionsAreCleared();
	checkInvalidOperationException();
	checkDivisionByZeroException();
	checkInexactAndUnderflowException();
	checkInexactAndOverflowException();

	Fpu_setContextConfig(&savedConfig);
}

/// \Given an initialized and started up Fpu,
///
/// \When denormalized floating point value is used in the flush-to-zero mode,
///
/// \Then the error is signalized.
///
TEST(FpuTests, Fpu_correctlyDetectsErrorsInFlushToZeroMode)
{
	Fpu fpu;
	Fpu_ContextConfig savedConfig;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	Fpu_getContextConfig(&savedConfig);

	checkInputDenormalException();

	Fpu_setContextConfig(&savedConfig);
}

/// @}

static inline bool
isNocpRaised()
{
	// cppcheck-suppress misra-c2012-11.4
	const volatile auto *const scb =
			reinterpret_cast<volatile Scb_Registers *>(
					SCB_BASE_ADDRESS);
	return (scb->cfsr & SCB_CFSR_NOCP_MASK) != 0u;
}

static inline void
resetNocpRaised()
{
	// cppcheck-suppress misra-c2012-11.4
	auto *const scb = reinterpret_cast<volatile Scb_Registers *>(
			SCB_BASE_ADDRESS);
	// CFSR contains sticky bits: they are reset by writing 1 to them
	scb->cfsr = SCB_CFSR_NOCP_MASK;
}

static void
checkCspr()
{
	if (isNocpRaised())
		FpuTests::nocpFaults++;
	else
		FpuTests::otherFaults++;
	resetNocpRaised();
}

#ifndef DOXYGEN
void __attribute__((naked))
HardFault_Handler(void)
{
	asm volatile("ldr r0, [sp, #24]\n"
		     "add r0, #2\n" // increment PC
		     "str r0, [sp, #24]\n"
		     "push {lr}\n"
		     "bl %[checkCsprAddress]\n"
		     "pop {pc}\n"
			:
			: [checkCsprAddress] "i"(&checkCspr)
			: ASM_R0);
}
#endif

/// @name Fpu_shutdown()
/// @{

/// \Given disabled Fpu,
///
/// \When floating-point operation is executed,
///
/// \Then NOCP Usage Fault is generated.
///
TEST(FpuTests, Fpu_correctlyHandlesShutdown)
{
	checkCspr();
	UNSIGNED_LONGS_EQUAL(FpuTests::nocpFaults, 0);
	UNSIGNED_LONGS_EQUAL(FpuTests::otherFaults, 1);
	FpuTests::otherFaults = 0;

	Fpu fpu;
	Fpu_init(&fpu);
	Fpu_startup(&fpu);
	UNSIGNED_LONGS_EQUAL(0, FpuTests::nocpFaults);
	UNSIGNED_LONGS_EQUAL(0, FpuTests::otherFaults);

	FLOAT_POINT_OPERATION;
	UNSIGNED_LONGS_EQUAL(0, FpuTests::nocpFaults);
	UNSIGNED_LONGS_EQUAL(0, FpuTests::otherFaults);

	Fpu_shutdown(&fpu);

	FLOAT_POINT_OPERATION;
	FLOAT_POINT_OPERATION;

	Fpu_startup(&fpu);
	UNSIGNED_LONGS_EQUAL(2, FpuTests::nocpFaults);
	UNSIGNED_LONGS_EQUAL(0, FpuTests::otherFaults);

	FLOAT_POINT_OPERATION;
	UNSIGNED_LONGS_EQUAL(2, FpuTests::nocpFaults);
	UNSIGNED_LONGS_EQUAL(0, FpuTests::otherFaults);
}

/// @}
