/**@file
 * This file is part of the ARM BSP for the Test Environment.
 *
 * @copyright 2018-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// \file  RstcTests.cpp
/// \brief Rstc driver test suite implementation.

#include <atomic>
#include <cstring>

#include <CppUTest/TestHarness.h>

#if defined(N7S_TARGET_SAMV71Q21)
#include <n7s/utils/Utils.h>
#endif

#include <n7s/bsp/Nvic/Nvic.h>
#include <n7s/bsp/Pmc/Pmc.h>
#include <n7s/bsp/Rstc/Rstc.h>
#include <n7s/bsp/Tic/Tic.h>

#include <TestsStartup/TestsStartup.h>

TEST_GROUP(RstcTests)
{
#if defined(N7S_TARGET_SAMV71Q21)
	static inline std::atomic<bool> rstcIrq = false;
	static inline std::atomic<bool> isBusy = false;
	static inline std::atomic<bool> userResetDetected = false;
	static inline std::atomic<Rstc_ResetType> resetType;
#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
	static inline Rstc_Registers *const rstc =
			reinterpret_cast<Rstc_Registers *>(RSTC_BASE_ADDRESS);
#endif
};

using RstcTests = TEST_GROUP_CppUTestGroupRstcTests;

#if defined(N7S_TARGET_SAMV71Q21)
#ifndef DOXYGEN
void
RSTC_Handler(void)
{
	RstcTests::rstcIrq = true;
	RstcTests::userResetDetected = Rstc_wasNrstTransitionDetected();
	RstcTests::isBusy = Rstc_isBusy();
	RstcTests::resetType = Rstc_getLastResetType();
}
#endif // DOXYGEN
#endif // N7S_TARGET_SAMV71Q21

/// @name Check Idle state of microcontroller
/// @{

/// \Given Rstc driver in idle (no operations on module),
///
/// \When nothing happen,
///
/// \Then module settings and states should be default (Rstc state, reset reason and Nrst pin
/// transition).
///
TEST(RstcTests, Rstc_lastResetType)
{
	Rstc_Status status;
	Rstc_getStatus(&status);
	CHECK_TRUE(status.isNrstHigh);
	CHECK_FALSE(Rstc_isBusy());
	CHECK_TRUE(Rstc_getLastResetType());
	CHECK_FALSE(Rstc_wasNrstTransitionDetected());
}
/// @}

#if defined(N7S_TARGET_SAMV71Q21)
/// @name Rstc_setUserResetEnabled()
/// @{

/// \Given Rstc driver,
///
/// \When the user reset is commanded to be enabled and disabled,
///
/// \Then the user reset is marked as enabled and disabled respectively.
///
TEST(RstcTests, Rstc_enablesAndDisabledUserReset)
{
	Rstc_setUserResetEnabled(true);
	CHECK_TRUE(Rstc_isUserResetEnabled());
	Rstc_setUserResetEnabled(false);
	CHECK_FALSE(Rstc_isUserResetEnabled());
}

/// @}

/// @name Rstc_setExternalResetLength()
/// @{

/// \Given Rstc driver,
///
/// \When the external reset length is set and read-back,
///
/// \Then read-back value matches the set one.
///
TEST(RstcTests, Rstc_setsAndGetsExternalResetLength)
{
	const uint8_t desiredResetLength = 0x2u;
	Rstc_setExternalResetLength(desiredResetLength);
	UNSIGNED_LONGS_EQUAL(desiredResetLength, Rstc_getExternalResetLength());
}

/// @}

/// @name Rstc_setUserResetInterruptEnabled()
/// @{

/// \Given Rstc driver,
///
/// \When the user reset interrupt is commanded to be enabled and disabled,
///
/// \Then the user reset interrupt is marked as enabled and disabled respectively.
///
TEST(RstcTests, Rstc_enablesAndDisabledUserResetInterrupt)
{
	Rstc_setUserResetInterruptEnabled(true);
	CHECK_TRUE(Rstc_isUserResetInterruptEnabled());
	Rstc_setUserResetInterruptEnabled(false);
	CHECK_FALSE(Rstc_isUserResetInterruptEnabled());
}

/// \Given User reset disabled and user reset interrupt enabled,
///
/// \When external reset is triggered,
///
/// \Then reset interrupt handler is invoked, user reset is detected and correct reason is read.
///
TEST(RstcTests, Rstc_triggersResetInterrupt)
{
	Nvic_enableInterrupt(Nvic_Irq_Reset);
	Rstc_setUserResetEnabled(false);
	Rstc_setUserResetInterruptEnabled(true);
	Rstc_triggerExternalReset();

	const uint32_t timeout = 100000;
	busyWaitLoop(timeout);

	CHECK_TRUE(RstcTests::rstcIrq);
	CHECK_TRUE(RstcTests::userResetDetected);
	CHECK_TRUE(RstcTests::isBusy);
	ENUMS_EQUAL_INT(Rstc_ResetType_Software, RstcTests::resetType);
}
/// @}

#elif defined(N7S_TARGET_SAMRH71F20) || defined(N7S_TARGET_SAMRH707F18)
/// @name Rstc_triggerPeripheralReset()
/// @{

/// \Given Rstc driver and initialized peripheral,
///
/// \When Config is applied to the peripheral and Rstc resets the peripheral,
///
/// \Then The peripheral shall reset to it's initial state.
///
TEST(RstcTests, Rstc_resetsPeripheral)
{
	Tic tic;
	Tic_init(&tic, Tic_Id_2);

	Tic_ChannelConfig setConfig{};
	Tic_getChannelConfig(&tic, Tic_Channel_0, &setConfig);

	Pmc pmc;
	Pmc_init(&pmc, Pmc_getDeviceRegisterStartAddress());
	Pmc_enablePeripheralClk(&pmc, Pmc_PeripheralId_Tc2Ch0);

	Tic_ChannelConfig config = {};
	config.channelMode = Tic_Mode_Waveform;
	config.rc = 1000000u;
	config.clockSource = Tic_ClockSelection_MckBy128;
	config.modeConfig.waveformModeConfig.waveformMode =
			Tic_WaveformMode_Up_Rc;
	config.isEnabled = true;
	Tic_setChannelConfig(&tic, Tic_Channel_0, &config);
	CHECK_TRUE(Tic_isChannelEnabled(&tic, Tic_Channel_0));

	Tic_ChannelConfig readConfig = {};
	Tic_getChannelConfig(&tic, Tic_Channel_0, &readConfig);
	MEMCMP_EQUAL(&readConfig, &config, sizeof(Tic_ChannelConfig));

	Rstc_triggerPeripheralReset(Pmc_PeripheralId_Tc2Ch0);
	Tic_ChannelConfig resetConfig = {};
	Tic_getChannelConfig(&tic, Tic_Channel_0, &resetConfig);
	MEMCMP_EQUAL(&resetConfig, &setConfig, sizeof(Tic_ChannelConfig));
	CHECK_FALSE(Tic_isChannelEnabled(&tic, Tic_Channel_0));
}
/// @}

/// @name Rstc_setCpuClockFailureResetEnabled()
/// @{

/// \Given Rstc driver,
///
/// \When Cpu clock failure reset is enabled and then disabled,
///
/// \Then Rstc register shall be updated with correct values.
///
TEST(RstcTests, Rstc_enablesAndDisablesCpuClockFailureReset)
{
	Rstc_setCpuClockFailureResetEnabled(true);
	CHECK_TRUE((rstc->mr & RSTC_MR_CPUFEN_MASK) > 0u);
	Rstc_setCpuClockFailureResetEnabled(false);
	CHECK_FALSE((rstc->mr & RSTC_MR_CPUFEN_MASK) > 0u);
}
/// @}

/// @name Rstc_setSlowClockCrystalFailureResetEnabled()
/// @{

/// \Given Rstc driver,
///
/// \When Sclk failure reset is enabled and then disabled,
///
/// \Then Rstc register shall be updated with correct values.
///
TEST(RstcTests, Rstc_enablesAndDisablesSclkFailureReset)
{
	Rstc_setSlowClockCrystalFailureResetEnabled(true);
	CHECK_TRUE((rstc->mr & RSTC_MR_SCKSW_MASK) > 0u);
	Rstc_setSlowClockCrystalFailureResetEnabled(false);
	CHECK_FALSE((rstc->mr & RSTC_MR_SCKSW_MASK) > 0u);
}
/// @}

#endif // N7S_TARGET_SAMRH71F20
