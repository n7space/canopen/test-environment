# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

SAMV7 Board Support Package (BSP) tests

Given Board Support Package for HWTB
When executing its unit tests on HWTB
Then all tests execute successfuly.

SRS: SRS-CTESW-FUN-140

Note: Unit tests of BSP are documented via Doxygen comments.

Note: The test will pass only if all unit tests pass.

  $ source "${TESTDIR}/../common.sh"

Build and execute
  $ scons_wrapper bsp-unit-tests | grep "scons: done building targets."
  scons: done building targets.

