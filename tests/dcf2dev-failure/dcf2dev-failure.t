# This file is part of the Test Environment internal tests.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

SCons Dcf2Dev tool error detection tests (using dcf2dev Lely tool)

Given malformed DCF file
When checking if dcf2dev tool detects the error using SCons extensions
Then the test case passes only when dcf2dev fails
SRS: SRS-CTESW-FUN-255


  $ source "${TESTDIR}/../common.sh"

Expected dcf2dev failure
  $ rm -f "${TESTLOGPATH}/dcf/malformed.dummy"  # enforce test rerun
  $ scons_wrapper test-dcf-failure > "${TESTLOGPATH}/scons.log"
  $ grep "scons: done building targets" "${TESTLOGPATH}/scons.log"
  scons: done building targets.
  $ grep "ValueError: invalid DCF" "${TESTLOGPATH}/scons.log"
  ValueError: invalid DCF: tests/dcf2dev-failure/malformed.dcf
