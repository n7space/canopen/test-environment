# This file is part of the Test Environment internal tests.
#
# @copyright 2021-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

[DeviceInfo]
VendorName=N7 Space Sp. z o.o.
VendorNumber=0x004E3753
ProductName=
ProductNumber=0
RevisionNumber=0
OrderCode=
BaudRate_10=0
BaudRate_20=0
BaudRate_50=0
BaudRate_125=0
BaudRate_250=0
BaudRate_500=0
BaudRate_800=0
BaudRate_1000=0

[MandatoryObjects]
SupportedObjects=3
1=0x1000
2=0x1001
3=0x1018

[OptionalObjects]
SupportedObjects=2
1=0x1005
2=0x1006
3=0x1019

[ManufacturerObjects]
SupportedObjects=0

[1000]
ParameterName=Device type
DataType=0x0007
AccessType=ro

[1001]
ParameterName=Error register
DataType=0x0005
AccessType=ro

[1005]
ParameterName=COB-ID SYNC message
DataType=0x0007
AccessType=rw
DefaultValue=0x40000080

[1006]
ParameterName=Communication cycle period
DataType=0x0007
AccessType=rw
DefaultValue=100000

[1018]
SubNumber=5
ParameterName=Identity object
ObjectType=0x09

[1018sub0]
ParameterName=Highest sub-index supported
DataType=0x0005
AccessType=const
DefaultValue=0x4

[1018sub1]
ParameterName=Vendor-ID
DataType=0x0007
AccessType=ro

[1018sub2]
ParameterName=Product code
DataType=0x0007
AccessType=ro

[1018sub3]
ParameterName=Revision number
DataType=0x0007
AccessType=ro

[1018sub4]
ParameterName=Serial number
DataType=0x0007
AccessType=ro

[1019]
ParameterName=Synchronous counter overflow value
DataType=0x0005
AccessType=rw
DefaultValue=4

