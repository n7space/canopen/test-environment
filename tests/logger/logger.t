# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Loging capabilities tests

Given Test Case
When logging messages from within it
Then logs contain expected messages

SRS: SRS-CTESW-FUN-040
SRS: SRS-CTESW-FUN-045

  $ source "${TESTDIR}/../common.sh"

Building and executing
  $ scons_wrapper logger-test-case | grep "scons: done building targets."
  scons: done building targets.

HOST log
  $ cat "$TESTLOGPATH/logger-test-case.host.log"
  ============ TEST MAIN START ============
  \[.*\] - Executed on HOST (re)
  \[.*\] - uint32 value: 0x0000002A (re)
  \[.*\] - uint64 value: 0x0000000000000582 (re)
  \[.*\] - int32 value: 0xFFFFFFD6 (re)
  \[.*\] - int64 value: 0xFFFFFFFFFFFFFA7E (re)
  \[.*\] - time log: 0x000000000000000A s 0x00000014 ns (re)
  Logged raw
  \[.*\] - 32bit: expected 0x01010101 was 0xDEADBEEF (re)
  \[.*\] - 64bit: expected 0x0202020202020202 was 0xC0DE0000C0DE0000 (re)
  \[.*\] - 32bit: expected 0x01010101 was 0xDEADBEEF (re)
  \[.*\] - 64bit: expected 0x0202020202020202 was 0xC0DE0000C0DE0000 (re)
  \[.*\] - array: \[ AA BB CC \] (re)
  \[.*\] - str1: "abcd" (re)
  \[.*\] - str2: "abc" (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB log
  $ cat "$TESTLOGPATH/logger-test-case.hwtb.log"
  ============ TEST MAIN START ============
  [0x0000000000000000 s 0x00000000 ns] - Executed on HWTB
  [0x0000000000000000 s 0x00000000 ns] - uint32 value: 0x0000002A
  [0x0000000000000000 s 0x00000000 ns] - uint64 value: 0x0000000000000582
  [0x0000000000000000 s 0x00000000 ns] - int32 value: 0xFFFFFFD6
  [0x0000000000000000 s 0x00000000 ns] - int64 value: 0xFFFFFFFFFFFFFA7E
  [0x0000000000000000 s 0x00000000 ns] - time log: 0x000000000000000A s 0x00000014 ns
  Logged raw
  [0x0000000000000000 s 0x00000000 ns] - 32bit: expected 0x01010101 was 0xDEADBEEF
  [0x0000000000000000 s 0x00000000 ns] - 64bit: expected 0x0202020202020202 was 0xC0DE0000C0DE0000
  [0x0000000000000000 s 0x00000000 ns] - 32bit: expected 0x01010101 was 0xDEADBEEF
  [0x0000000000000000 s 0x00000000 ns] - 64bit: expected 0x0202020202020202 was 0xC0DE0000C0DE0000
  [0x0000000000000000 s 0x00000000 ns] - array: [ AA BB CC ]
  [0x0000000000000000 s 0x00000000 ns] - str1: "abcd"
  [0x0000000000000000 s 0x00000000 ns] - str2: "abc"
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000
