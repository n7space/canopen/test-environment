/**@file
 * This file is part of the Test Environment internal tests.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <lely/util/time.h>

#include <TestFramework/HAL/EntryPoint.h>
#include <TestFramework/Log.h>

int
EntryPoint_main(void)
{
	LOG("Executed on HWTB");
	LOG_UINT32("uint32 value", 42);
	LOG_UINT64("uint64 value", 1410);
	LOG_INT32("int32 value", -42);
	LOG_INT64("int64 value", -1410);
	const struct timespec tp = { 10, 20 };
	LOG_TIME("time log", &tp);
	LOG_RAW("Logged raw");
	LOG_UINT32_EXPECTED("32bit", 0x01010101, 0xDEADBEEF);
	LOG_UINT64_EXPECTED("64bit", 0x0202020202020202, 0XC0DE0000C0DE0000);
	LOG_INT32_EXPECTED("32bit", 0x01010101, 0xDEADBEEF);
	LOG_INT64_EXPECTED("64bit", 0x0202020202020202, 0XC0DE0000C0DE0000);
	const uint8_t array[] = { 0xAA, 0xBB, 0xCC };
	LOG_ARRAY("array", array, sizeof(array));
	LOG_STR("str1", "abcd\0efg");
	LOG_STR_N("str2", "abcdef", 3);
	return 0;
}
