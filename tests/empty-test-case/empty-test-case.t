# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Basic empty test case

Given empty Test Case defined using Test Framework and SCons extensions
When building and executing it
Then build and execution succeed and logs are created.

SRS: SRS-CTESW-FUN-010
SRS: SRS-CTESW-FUN-015
SRS: SRS-CTESW-FUN-030
SRS: SRS-CTESW-FUN-035
SRS: SRS-CTESW-FUN-040
SRS: SRS-CTESW-FUN-045
SRS: SRS-CTESW-FUN-060
SRS: SRS-CTESW-FUN-070
SRS: SRS-CTESW-FUN-080
SRS: SRS-CTESW-FUN-120
SRS: SRS-CTESW-FUN-260
SRS: SRS-CTESW-FUN-265
SRS: SRS-CTESW-IF-030
SRS: SRS-CTESW-IF-040

Note: The test contains an "empty" CANSW Test Case - it's empty because
it does not validate any CANSW features, but contains all elements
required by [SRS-CTESW-FUN-060] and to build it CTESW must provide features
validated by this test. Those include building CANSW itself and C application
using it for x86 [SRS-CTESW-FUN-010] [SRS-CTESW-FUN-030] and for HWTB
[SRS-CTESW-FUN-015] [SRS-CTESW-FUN-035].

Note: The test executes the defined Test Case [SRS-CTESW-FUN-070].

Note: The test validates, that CTESW gathers proper execution logs of the Test Case
application from x86 [SRS-CTESW-FUN-040] and HWTB [SRS-CTESW-FUN-045].

Note: Logs are used to mark the Test Case as PASSED [SRS-CTESW-FUN-080] because
it is designed to match all requirements of [SRS-CTESW-FUN-120]. As it is the only
executed Test Case, the whole execution will be marked PASSED [SRS-CTESW-FUN-265].

Note: The test uses CTESW feature [SRS-CTESW-FUN-260] to execute only the
selected "empty" Test Case.

Note: CTESW uses GDB link to execute the HWTB part of the Test Case, as
required by [SRS-CTESW-IF-030]. Logs from HWTB are downloaded using JLink
debug link [SRS-CTESW-IF-040].

Note: The Test Case is executed on hardware as described in SValP, thus validating
resource requirements for Host [SRS-CTESW-RES-010] and HWTB [SRS-CTESW-RES-020].

Note: The test will fail if the execution finishes non-nominally or logs do not
contain exactly expected messages.

  $ source "${TESTDIR}/../common.sh"

Building and executing
  $ scons_wrapper empty-test-case | grep "scons: done building targets."
  scons: done building targets.

HOST log
  $ cat "$TESTLOGPATH/empty-test-case.host.log"
  ============ TEST MAIN START ============
  Executed on HOST
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB log
  $ cat "$TESTLOGPATH/empty-test-case.hwtb.log"
  ============ TEST MAIN START ============
  Executed on HWTB
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Whole case status
  $ cat "$TESTLOGPATH/empty-test-case.log"
  empty-test-case - PASSED
