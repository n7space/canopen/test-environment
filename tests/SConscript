# This file is part of the Test Environment build system.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Import("env")

bsp_env = env.CreateEnvForBspTests()

core_tests = bsp_env.BuildModule("n7-core")
bsp_tests = bsp_env.BuildModule("SAM71-BSP")

bsp_tests_result = bsp_env.hwtb["N7_CORE_UNITTESTS"] + bsp_env.hwtb["N7_BSP_UNITTESTS"]
if bsp_env["build"] == "coverage":
    bsp_coverage = bsp_env.hwtb.GenerateCoverageHtml(
        "ARM BSP C Code coverage for $platform", bsp_tests_result, "bsp"
    )
    env.Alias("coverage-html-bsp", bsp_coverage)

can_env = env.CreateEnvForCanOpenTests()

__tests = [
    "autoreconfigure",
    "can-handshake-test",
    "dcf2dev-failure",
    "dcf2dev-generation",
    "empty-test-case",
    "empty-test-harness",
    "failing-test-cases",
    "get-time-test-case",
    "logger",
    "missing-header",
    "missing-include-directory",
    "read-memory",
    "results",
    "symmetrical-test-case",
    "timeouts",
    "traces",
    "unexpected-can-msg",
]

# Tests that are not providing any meaningful coverage, for example:
# * Performance tests
# * Intentionally failing tests
# * Tests of code generation tools
__tests_skipped_in_coverage = [
    "can-burst-send-test",
    "crashing-test-cases",
    "cpputest",
]

if can_env["build"] != "coverage":
    __tests += __tests_skipped_in_coverage


ctesw_tests_results = can_env.BuildModules(__tests)

if can_env["build"] == "coverage":
    ctesw_coverage = can_env.hwtb.GenerateCoverageHtml(
        "CTESW C Code coverage for $platform", ctesw_tests_results, "ctesw"
    )
    env.Alias("coverage-html-ctesw", ctesw_coverage)

tests_results = bsp_tests_result + ctesw_tests_results
env.Alias("tests", tests_results)

Return("tests_results")
