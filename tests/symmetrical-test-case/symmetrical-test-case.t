# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Basic empty test case

Given symmetrical Test Case defined using Test Framework and SCons extensions
When building and executing it
Then CTESW will execute it twice, exchanging roles of the HWTB and Host, and gather all necessary logs.

SRS: SRS-CTESW-FUN-070
SRS: SRS-CTESW-POR-010

Note: The test contains "symmetrical" Test Case - it's symmetrical because
the same code is to be used on Host and HWTB. The test validates CTESW
portability capabilities and hardware abstraction layer [SRS-CTESW-POR-010].

Note: The test validates that the same application is executed correctly on Host
and HWTB and its result is properly observed [SRS-CTESW-FUN-070].

Note: The test will fail if the applications do not execute correctly or reported
logs differ from expected ones.


  $ source "${TESTDIR}/../common.sh"

Build and execute
  $ scons_wrapper symmetrical-test-case | grep "scons: done building targets."
  scons: done building targets.

HOST app1 log
  $ cat "$TESTLOGPATH/symmetrical-test-case-host-to-hwtb.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - APP1 executed (re)
  \[.*\] - ON HOST (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HOST app2 log
  $ cat "$TESTLOGPATH/symmetrical-test-case-hwtb-to-host.host.log"
  ============ TEST MAIN START ============
  \[.*\] - TEST STARTED (re)
  \[.*\] - APP2 executed (re)
  \[.*\] - ON HOST (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000080 (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB app1 log
  $ cat "$TESTLOGPATH/symmetrical-test-case-hwtb-to-host.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - APP1 executed (re)
  \[.*\] - ON HWTB (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB app2 log
  $ cat "$TESTLOGPATH/symmetrical-test-case-host-to-hwtb.hwtb.log"
  ============ TEST MAIN START ============
  \[.*\] - Test start temperature: 0x.* (re)
  \[.*\] - TEST STARTED (re)
  \[.*\] - APP2 executed (re)
  \[.*\] - ON HWTB (re)
  \[.*\] - TEST FINISHED! (re)
  \[.*\] - Total allocated bytes: 0x0000000000000050 (re)
  \[.*\] - Test end temperature: 0x.* (re)
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Complete case status HWTB -> HOST
  $ cat "$TESTLOGPATH/symmetrical-test-case-hwtb-to-host.log"
  symmetrical-test-case-hwtb-to-host - PASSED

Complete case status HOST <- HWTB
  $ cat "$TESTLOGPATH/symmetrical-test-case-host-to-hwtb.log"
  symmetrical-test-case-host-to-hwtb - PASSED
