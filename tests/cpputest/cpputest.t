# This file is part of the Test Environment internal tests.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Various failing test cases

Given source files using CppUTest unit test framework
When compiling and executing those files
Then CTESW will detect success or failure of the unit tests

SRS: SRS-CTESW-FUN-030

Note: CppUTest is a C++ based library and the test builds
and executes application based on it on x86 [SRS-CTESW-FUN-030].

Note: support for CppUTest library can be used by CANSW validation tests.

Note: The test will fail if the expected failures are not detected
or an unexpected one occures.

  $ source "${TESTDIR}/../common.sh"

Skip on coverage run
  $ if [ "$CTESW_BUILD_TYPE" = "coverage" ] ; then exit 80 ; fi ;

Passing unit tests
  $ scons_wrapper cpputest-passing-unit-tests | grep "scons: done building"
  scons: done building targets.

Failing unit tests
  $ scons_wrapper cpputest-failing-unit-tests | grep "scons: \*\*\* \\["
  scons: *** [*/cpputest-failing-unit-tests-app.log] Error 1 (glob)

