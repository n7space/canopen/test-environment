# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Configuration files tests

Given configuration file with custom options
When passing it to CTESW
Then user's options are used in the build

SRS: SRS-CTESW-FUN-200
SRS: SRS-CTESW-FUN-210
SRS: SRS-CTESW-IF-055

Note: The tests validates that config files containing connection data
are read by CTESW [SRS-CTESW-FUN-200].

Note: The test modifies the default config file [SRS-CTESW-FUN-210]
using INI notation [SRS-CTESW-IF-055].

Note: The test will fail if change made in the config file will not be
reflected in CTESW build logs.

  $ source "${TESTDIR}/../common.sh"

Skip on coverage run (options are changed with coverage enabled)
  $ if [ "$CTESW_BUILD_TYPE" = "coverage" ] ; then exit 80 ; fi ;

Create custom config
  $ cp "${TESTDIR}/../../svf-configs/samv71q21/build-only.conf" "$TESTLOGPATH/custom.conf"
  $ printf "\n\n[cansw]\nconfigure_options=--customoption\n" >> "$TESTLOGPATH/custom.conf"

User provided options
  $ SCONS_OPTS="--just-print svf=$TESTLOGPATH/custom.conf" scons_wrapper LibCANopen-host | grep "lely-core/configure --prefix="
  cd .*\/resources\/LibCANopen\/lely-core && .*\/resources\/LibCANopen\/lely-core\/configure .* --customoption .* (re)

  $ cp "${TESTLOGPATH}/LibCANopen-host.scons.log" "${TESTLOGPATH}/LibCANopen-host.scons.1.log"

Python options.py (scons options)
  $ echo "svf='$TESTLOGPATH/custom.conf'" > "$TESTLOGPATH/custom.py"
  $ SCONS_OPTS="--just-print options=$TESTLOGPATH/custom.py" scons_wrapper LibCANopen-host | grep "lely-core/configure --prefix="
  cd .*\/resources\/LibCANopen\/lely-core && .*\/resources\/LibCANopen\/lely-core\/configure .* --customoption .* (re)

