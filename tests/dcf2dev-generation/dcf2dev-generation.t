# This file is part of the Test Environment internal tests.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

SCons Dcf2Dev tool tests (using dcf2dev Lely tool)

Given DCF files
When converting them to C using dcf2dev SCons extensions
Then generated C code properly compiles and links with complete project.
SRS: SRS-CTESW-FUN-250


  $ source "${TESTDIR}/../common.sh"

Build
  $ scons_wrapper test-dcf-compilation | grep "scons: done building targets."
  scons: done building targets.
