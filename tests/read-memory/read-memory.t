# This file is part of the Test Environment internal tests.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

HWTB memory access test case

Given HWTB application that sets some value under dedicated address
When executing this application and reading memory contents from given address
Then expected value is read and verified
SRS: SRS-CTESW-FUN-050

  $ source "${TESTDIR}/../common.sh"

Building and executing
  $ scons_wrapper read-memory | grep "scons: done building targets."
  scons: done building targets.

HOST log
  $ cat "$TESTLOGPATH/read-memory.host.log"
  ============ TEST MAIN START ============
  Executed on HOST
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

HWTB log
  $ cat "$TESTLOGPATH/read-memory.hwtb.log"
  ============ TEST MAIN START ============
  Executed on HWTB
  ============= TEST MAIN END =============
  
  >> EXIT STATUS: 00000000

Whole case status
  $ cat "$TESTLOGPATH/read-memory.log"
  read-memory - PASSED
