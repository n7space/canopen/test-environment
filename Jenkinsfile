/**@file
 * This file is part of the Test Environment CI configuration.
 *
 * @copyright 2020-2024 N7 Space Sp. z o.o.
 *
 * Test Environment was developed under a programme of,
 * and funded by, the European Space Agency (the "ESA").
 *
 *
 * Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
 * Version 2.4 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://essr.esa.int/license/list
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@Library("n7s-jenkins-lib@2.5.2") _

pipeline {
    agent {
        label 'docker'
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: "30", daysToKeepStr: "14"))

        timeout(time: 2, unit: "HOURS")

        timestamps()
    }

    parameters {
        booleanParam (
            name: "cleanBuild",
            defaultValue: false,
            description: "Ensures clean rebuild",
        )
        booleanParam (
            name: "resetSvf",
            defaultValue: false,
            description: "Forces SVF reset/reboot before executing tests",
        )
        choice (
            name: "selectedTarget",
            choices: ["All", "V71", "V71_XC32", "RH71", "RH71_XC32", "RH707", "RH707_XC32"],
            description: "Selects target(s) for the pipeline",
        )
        string (
            name: "sconsOptions",
            defaultValue: "",
            description: "Additional custom SCons options",
        )
    }

    triggers {
        cron(
            env.BRANCH_NAME.equals("master")
            ? "H 22 * * *"
            : "") // hack for rebuilding only master
    }

    environment {
        SCRIPTORIUM_IMAGE_URL = "gitlab-registry.n7space.com/docs-gens/scriptorium:v1.16.1"
        SHAREPOINT_LOGIN = credentials('jenkins-n7s-sharepoint')
        JOB_IMAGE_REGISTRY = "${env.JOB_NAME.toLowerCase()}"

        CORE_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:core"
        CM7_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:cortex-m7"
        XC32_BASE_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32"
        XC32_V71_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32-v71"
        XC32_RH71_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32-rh71"
        XC32_RH707_IMAGE_NAME = "${JOB_IMAGE_REGISTRY}:xc32-rh707"
    }

    stages {
        stage("Clean") {
            when {
                anyOf {
                    buildingTag()
                    triggeredBy "TimerTrigger"
                    equals expected: true, actual: params.cleanBuild
                }
            }

            steps {
                cleanRepository()
            }
        }

        stage("Docker - Core") {
            agent {
                dockerfile {
                    dir "docker/core"
                    additionalBuildArgs "-t ${CORE_IMAGE_NAME}"
                    reuseNode true
                }
            }

            steps {
                echo "Core image built - to be used by next steps"
            }
        }

        stage("Docker - Cortex-M7") {
            agent {
                dockerfile {
                    dir "docker/cortex-m7"
                    additionalBuildArgs "-t ${CM7_IMAGE_NAME} --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                    reuseNode true
                }
            }

            steps {
                echo "Cortex-M7 image built - to be used by next steps"
            }
        }

        stage("Docker - XC32 base") {
            when {
                anyOf {
                    expression { params.selectedTarget == "All" }
                    expression { params.selectedTarget.endsWith("XC32") }
                }
            }

            agent {
                dockerfile {
                    dir "docker/xc32/base"
                    additionalBuildArgs "-t ${XC32_BASE_IMAGE_NAME} --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                    reuseNode true
                }
            }

            steps {
                echo "XC32 image built - to be used by next steps"
            }
        }
        
        stage("Docker - XC32") {
            when {
                anyOf {
                    expression { params.selectedTarget == "All" }
                    expression { params.selectedTarget.endsWith("XC32") }
                }
            }

            parallel {
                stage("V71") {
                    when {
                        anyOf {
                            expression { params.selectedTarget == "All" }
                            expression { params.selectedTarget == "V71_XC32" }
                        }
                    }

                    agent {
                        dockerfile {
                            dir "docker/xc32"
                            additionalBuildArgs "-t ${XC32_V71_IMAGE_NAME} --build-arg PROCESSOR=ATSAMV71Q21RT --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                            reuseNode true
                        }
                    }

                    steps {
                        echo "XC32 SAMV71 image built - to be used by next steps"
                    }
                }

                stage("RH71") {
                    when {
                        anyOf {
                            expression { params.selectedTarget == "All" }
                            expression { params.selectedTarget == "RH71_XC32" }
                        }
                    }

                    agent {
                        dockerfile {
                            dir "docker/xc32"
                            additionalBuildArgs "-t ${XC32_RH71_IMAGE_NAME} --build-arg PROCESSOR=ATSAMRH71F20C --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                            reuseNode true
                        }
                    }

                    steps {
                        echo "XC32 SAMRH71 image built - to be used by next steps"
                    }
                }

                stage("RH707") {
                    when {
                        anyOf {
                            expression { params.selectedTarget == "All" }
                            expression { params.selectedTarget == "RH707_XC32" }
                        }
                    }

                    agent {
                        dockerfile {
                            dir "docker/xc32"
                            additionalBuildArgs "-t ${XC32_RH707_IMAGE_NAME} --build-arg PROCESSOR=ATSAMRH707F18A --build-arg REGISTRY=${JOB_IMAGE_REGISTRY}"
                            reuseNode true
                        }
                    }

                    steps {
                        echo "XC32 SAMRH707 image built - to be used by next steps"
                    }
                }
            }
        }

        stage("Prerequisites") {
            parallel {
                stage("clang-format check") {
                    agent {
                        docker {
                            image env.CORE_IMAGE_NAME
                            reuseNode true
                        }
                    }
                    steps {
                        sh '''
                           clang-format -n --Werror \
                                 $(find resources/bsp -name *.c -o -name *.h) \
                                 $(find resources/n7-core -name *.c -o -name *.h) \
                                 $(find resources/TestFramework -name *.c -o -name *.h) \
                                 $(find tests -name *.c -o -name *.h -o -name *.cpp -o -name *.hpp)
                           '''
                    }
                }

                stage("Python code checks") {
                    agent {
                        docker {
                            image env.CORE_IMAGE_NAME
                            reuseNode true
                        }
                    }
                    steps {
                        sh 'black --diff --check site_scons/'
                        sh 'black --diff --check $(find . -name SCons\\*)'
                        sh 'flake8 site_scons/'
                        sh 'flake8 $(find . -name SCons\\*)'
                    }
                }

                stage("Sonar setup") {
                    agent {
                        docker {
                            image env.CORE_IMAGE_NAME
                            reuseNode true
                        }
                    }
                    steps {
                        withSonarQubeEnv("SonarQube N7S") {
                            sonar action: "downloadScanner",
                                force: params.cleanBuild,
                                sonarUrl: env.SONAR_HOST_URL
                        }
                    }
                }
            }
        }

        stage("SRS") {
            agent {
                docker {
                    image env.SCRIPTORIUM_IMAGE_URL
                    args "--entrypoint="
                    reuseNode true
                }
            }

            steps {
                sh "mkdir -p artifacts"

                scriptoriumArtifact(
                    "SRS.xlsx",
                    "download-from-sharepoint",
                    "--sharepoint ${N7S_SHAREPOINT}" as String,
                    '--user $SHAREPOINT_LOGIN_USR',
                    '--password $SHAREPOINT_LOGIN_PSW',
                    "\"CANopen9/Shared Documents/02 Deliverables/CTSDP/SRS/CAN-N7S-CTSDP-SRS-A Requirements.xlsx\"",
                )

                scriptorium(
                    "check-srs",
                    "--input artifacts/SRS.xlsx", // new one
                )

                scriptorium(
                    "check-srs",
                    "--input ctsdp-srs.json", // old one
                )

                scriptoriumArtifact(
                    "srs-current.json",
                    "extract-srs",
                    "--input artifacts/SRS.xlsx",
                )

                scriptorium(
                    "srs-changelog",
                    "ctsdp-srs.json",
                    "artifacts/srs-current.json",
                    "> artifacts/srs-changelog.txt",
                )

                archiveArtifacts "artifacts/srs-changelog.txt"

                scriptoriumArtifact(
                    "CTSDP-SDD-Forward-Trace.html",
                    "generate-sdd-forward",
                    "artifacts/srs-current.json",
                    "ctsdp-sdd.json",
                )

                scriptoriumArtifact(
                    "CTSDP-SDD-Backward-Trace.html",
                    "generate-sdd-backward",
                    "ctsdp-sdd.json",
                    "artifacts/srs-current.json",
                )
            }
        }

        stage("Building") {
            matrix {
                axes {
                    axis {
                        name 'N7S_CI_TARGET'
                        values 'V71', 'V71_XC32', 'RH71', 'RH71_XC32', 'RH707', 'RH707_XC32'
                    }
                }

                when {
                    anyOf {
                        expression { params.selectedTarget == "All" }
                        expression { params.selectedTarget == env.N7S_CI_TARGET }
                    }
                }

                agent {
                    docker {
                        image getDockerImageNameForTarget(env.N7S_CI_TARGET)
                        reuseNode true
                    }
                }

                environment {
                    N7S_CI_PLATFORM = getCurrentPlatformName()
                }

                stages {
                    stage("Build dependencies") {
                        steps {
                            debugBuild("LibCANopen libs", parallelBuildFlag())
                            releaseBuild("LibCANopen libs", parallelBuildFlag())
                            coverageBuild("LibCANopen libs", parallelBuildFlag())
                        }
                    }

                    stage("Build test binaries") {
                        steps {
                            debugBuild("test-cases-apps unit-tests-apps", parallelBuildFlag())
                            releaseBuild("test-cases-apps unit-tests-apps", parallelBuildFlag())
                            coverageBuild("test-cases-apps unit-tests-apps", parallelBuildFlag())

                            copyCompilationDatabaseForSonar()
                        }
                    }

                    stage("Doxygen") {
                        steps {
                            releaseBuild("doc")
                            publishDoxygenDocs("doxygen-scons", "SCons utilities")
                            publishDoxygenDocs("doxygen-framework", "Test Framework")
                            publishDoxygenDocs("doxygen-bsp", "ARM BSP")
                            publishDoxygenDocs("doxygen-bsp-unit-tests", "ARM BSP unit tests")
                        }
                    }
                }
            }
        }

        stage("Sonar") {
            agent {
                docker {
                    image env.CORE_IMAGE_NAME
                    reuseNode true
                }
            }

            environment {
                SONAR_PROJECT_KEY = "canopen_test-environment"
            }

            steps {
                withSonarQubeEnv("SonarQube N7S") {
                    sonar action: "scan",
                        args: ("-Dsonar.projectKey=${SONAR_PROJECT_KEY}"
                               + " -Dsonar.cfamily.variants.names=" + getSonarVariants()
                               + " -Dsonar.projectVersion=\$(git describe --exact-match --tags || echo '')"
                               + " --debug"
                    )

                    gatherSonarLogs()
                }
            }
        }

        stage("Testing") {
            matrix {
                axes {
                    axis {
                        name 'N7S_CI_TARGET'
                        values 'V71', 'V71_XC32', 'RH71', 'RH71_XC32', 'RH707', 'RH707_XC32'
                    }
                }

                when {
                    anyOf {
                        expression { params.selectedTarget == "All" }
                        expression { params.selectedTarget == env.N7S_CI_TARGET }
                    }
                }

                environment {
                    N7S_CI_PLATFORM = getCurrentPlatformName()
                }

                stages {
                    stage("SVF") {

                        agent {
                            docker {
                                image getDockerImageNameForTarget(env.N7S_CI_TARGET)
                                reuseNode true
                            }
                        }

                        stages {
                            stage("Reset SVF") {
                                when {
                                    anyOf {
                                        buildingTag()
                                        triggeredBy "TimerTrigger"
                                        equals expected: true, actual: params.resetSvf
                                    }
                                }

                                steps {
                                    lock(resource: null,
                                         label: getSvfLabelForTarget(env.N7S_CI_TARGET),
                                         variable: "SELECTED_SVF",
                                         quantity: 1) {
                                        sh "PYTHONPATH=./site_scons/site_tools python3 ./resources/n7-core/environment/ci/reset-svf.py " + getCurrentPlatformSvf()
                                        sleep(time: 40, unit: "SECONDS")
                                    }
                                }
                            }

                            stage("Cram Tests") {
                                steps {
                                    lock(resource: null,
                                         label: getSvfLabelForTarget(env.N7S_CI_TARGET),
                                         variable: "SELECTED_SVF",
                                         quantity: 1) {
                                        execCram()
                                    }
                                }
                                post {
                                    always {
                                        zip zipFile: "artifacts/cram-logs-release-${N7S_CI_PLATFORM}.zip",
                                            archive: true, dir: getCurrentPlatformBuildDir() + "/release/tests",
                                            glob: "**/*.log",
                                            overwrite: true
                                        zip zipFile: "artifacts/integration-tests-logs-release-${N7S_CI_PLATFORM}.zip",
                                            archive: true, dir: getCurrentPlatformBuildDir() + "/release",
                                            glob: "**/*-report.xml",
                                            overwrite: true
                                    }
                                }
                            }

                            stage("Cram Coverage Tests") {
                                when {
                                    // XC32 doesn't support current coverage download mechanism
                                    expression { !env.N7S_CI_TARGET.endsWith("XC32") }
                                }

                                steps {
                                    lock(resource: null,
                                         label: getSvfLabelForTarget(env.N7S_CI_TARGET),
                                         variable: "SELECTED_SVF",
                                         quantity: 1) {
                                        execCram("CTESW_BUILD_TYPE=coverage")
                                        coverageBuild("coverage-html")
                                    }
                                    publishCoverageReports()
                                }

                                post {
                                    always {
                                        zip zipFile: "artifacts/cram-logs-coverage-${N7S_CI_PLATFORM}.zip",
                                            archive: true,
                                            dir: getCurrentPlatformBuildDir() + "/coverage",
                                            glob: "**/*.log",
                                            overwrite: true
                                        zip zipFile: "artifacts/integration-tests-logs-coverage-${N7S_CI_PLATFORM}.zip",
                                            archive: true, dir: getCurrentPlatformBuildDir() + "/coverage",
                                            glob: "**/*-report.xml",
                                            overwrite: true
                                    }
                                }
                            }
                        }
                    }

                    stage("Scriptorium") {
                        agent {
                            docker {
                                image env.SCRIPTORIUM_IMAGE_URL
                                args "--entrypoint="
                                reuseNode true
                            }
                        }

                        environment {
                            SCRIPTORIUM_PROJECT_NAME = "CANopen Library Toolset - CTESW (${env.N7S_CI_PLATFORM.toUpperCase()})"
                        }

                        stages {
                            stage("Specifications") {
                                steps {
                                    scriptoriumArtifact(
                                        "unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "extract-cpputests-specs",
                                        "${docDir('release')}doxygen-bsp-unit-tests/xml" as String,
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SUITP-unit-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitp",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Unit Tests'" as String,
                                        "artifacts/unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/srs-current.json",
                                    )

                                    scriptoriumArtifact(
                                        "integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "extract-cram-specs",
                                        "tests",
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SUITP-integration-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitp",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Integration Tests'" as String,
                                        "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/srs-current.json",
                                    )

                                    scriptoriumArtifact(
                                        "validation-spec-${N7S_CI_PLATFORM}.json",
                                        "combine-validation-spec",
                                        "--name '${SCRIPTORIUM_PROJECT_NAME}'" as String,
                                        "--tests",
                                        "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        // TODO ? "validation/specifications/tests-extra.json",
                                        // TODO ? "validation/specifications/tests-extra-${N7S_CI_PLATFORM}.json",
                                        "--reviews",
                                        "validation/specifications/design-reviews.json",
                                        "validation/specifications/design-reviews-${N7S_CI_PLATFORM}.json",
                                        "--inspections",
                                        "validation/specifications/inspections.json",
                                        "validation/specifications/inspections-${N7S_CI_PLATFORM}.json",
                                        "--overrides",
                                        "validation/overrides.json",
                                        "validation/overrides-${N7S_CI_PLATFORM}.json",
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SVS-Backward-Trace-${N7S_CI_PLATFORM}.html",
                                        "generate-svs-backward",
                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json" as String,
                                        "artifacts/srs-current.json",
                                    )
                                    scriptoriumArtifact(
                                        "CTSDP-SVS-Forward-Trace-${N7S_CI_PLATFORM}.html",
                                        "generate-svs-forward",
                                        "artifacts/srs-current.json",
                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json" as String,
                                    )

                                }
                            }

                            stage("Reports") {
                                steps {
                                    scriptoriumArtifact(
                                        "unit-tests-results-${N7S_CI_PLATFORM}.json",
                                        "extract-cpputests-results",
                                        unitTestsLogDir('release') as String,
                                    )
                                    scriptoriumArtifact(
                                        "integration-tests-results-${N7S_CI_PLATFORM}.json",
                                        "extract-xunit-results",
                                        "--input-filter *-report.xml",
                                        "--cram",
                                        "${getCurrentPlatformBuildDir()}/release/tests" as String,
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SUITR-unit-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitr",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Unit Tests Results'" as String,
                                        "artifacts/unit-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/unit-tests-results-${N7S_CI_PLATFORM}.json",
                                    )
                                    scriptoriumArtifact(
                                        "CTSDP-SUITR-integration-tests-${N7S_CI_PLATFORM}.html",
                                        "generate-suitr",
                                        "--title '${SCRIPTORIUM_PROJECT_NAME} Integration Tests Results'" as String,
                                        "artifacts/integration-tests-specs-${N7S_CI_PLATFORM}.json",
                                        "artifacts/integration-tests-results-${N7S_CI_PLATFORM}.json",
                                    )

                                    scriptoriumArtifact(
                                        "validation-report-${N7S_CI_PLATFORM}.json",
                                        "combine-validation-report",
                                        "--name '${SCRIPTORIUM_PROJECT_NAME}'" as String,
                                        "--tests",
                                        "artifacts/unit-tests-results-${N7S_CI_PLATFORM}.json",
                                        "artifacts/integration-tests-results-${N7S_CI_PLATFORM}.json",
                                        // TODO "validation/results/tests-extra.json",
                                        // TODO "validation/results/tests-extra-${N7S_CI_PLATFORM}.json",
                                        "--reviews",
                                        "validation/results/design-reviews.json",
                                        "validation/results/design-reviews-${N7S_CI_PLATFORM}.json",
                                        "--inspections",
                                        "validation/results/inspections-results.json",
                                        "validation/results/inspections-results-${N7S_CI_PLATFORM}.json",
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SValR-item-results-${N7S_CI_PLATFORM}.html",
                                        "generate-svalr-items-results",
                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json" as String,
                                        "artifacts/validation-report-${N7S_CI_PLATFORM}.json",
                                    )

                                    scriptoriumArtifact(
                                        "CTSDP-SValR-${N7S_CI_PLATFORM}.html",
                                        "generate-svalr",
                                        "artifacts/srs-current.json",
                                        "artifacts/validation-spec-${N7S_CI_PLATFORM}.json",
                                        "artifacts/validation-report-${N7S_CI_PLATFORM}.json",
                                        "validation/close-outs/ts-${N7S_CI_PLATFORM}.json",
                                    )

                                    publishHtmlSummary("Validation Reports", [
                                        "SValR",
                                        "SUITR-unit-tests",
                                        "SUITR-integration-tests",
                                    ])
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

def getPlatformName(String target) {
    return [
        "V71": "samv71q21",
        "V71_XC32": "samv71q21_xc32",
        "RH71": "samrh71f20",
        "RH71_XC32": "samrh71f20_xc32",
        "RH707": "samrh707f18",
        "RH707_XC32": "samrh707f18_xc32",
    ].get(target)
}

def getPlatformBaseName(String target) {
    return [
        "V71": "samv71q21",
        "V71_XC32": "samv71q21",
        "RH71": "samrh71f20",
        "RH71_XC32": "samrh71f20",
        "RH707": "samrh707f18",
        "RH707_XC32": "samrh707f18",
    ].get(target)
}

def getDockerImageNameForTarget(String target) {
    return [
        "V71": env.CM7_IMAGE_NAME,
        "V71_XC32": env.XC32_V71_IMAGE_NAME,
        "RH71": env.CM7_IMAGE_NAME,
        "RH71_XC32": env.XC32_RH71_IMAGE_NAME,
        "RH707": env.CM7_IMAGE_NAME,
        "RH707_XC32": env.XC32_RH707_IMAGE_NAME,
    ].get(target)
}

def getSvfLabelForTarget(String target) {
    return [
        "V71": "CAN2-V71",
        "V71_XC32": "CAN2-V71",
        "RH71": "CAN2-RH71",
        "RH71_XC32": "CAN2-RH71",
        "RH707": "CAN2-RH707",
        "RH707_XC32": "CAN2-RH707",
    ].get(target)
}

def getPlatformList() {
    return [
        "samv71q21",
        "samv71q21_xc32",
        "samrh71f20",
        "samrh71f20_xc32",
        "samrh707f18",
        "samrh707f18_xc32",
    ]
}

def getCurrentPlatformName() {
    return getPlatformName(env.N7S_CI_TARGET)
}

def getCurrentPlatformBaseName() {
    return getPlatformBaseName(env.N7S_CI_TARGET)
}

def getCurrentPlatformBuildDir() {
    return "build-${env.N7S_CI_TARGET}"
}

def getSelectedSvfName() {
    return env.SELECTED_SVF.split("-")[2].toLowerCase()
}

def getCurrentPlatformSvf() {
    if (env.SELECTED_SVF) {
        return "svf-configs/${getCurrentPlatformBaseName()}/${getSelectedSvfName()}.conf"
    }
    if (env.N7S_CI_PLATFORM) {
        return "svf-configs/${getCurrentPlatformBaseName()}/build-only.conf"
    }
    return "svf-configs/localhost.conf"
}

def parallelBuildFlag() {
    return "-j3 "
}

def execCram(String opts="", String targets="") {
    sh "SCONS_BUILD_DIR=" + getCurrentPlatformBuildDir() + " " + opts + " ./run-cram.sh " + getCurrentPlatformName() + " " + getSelectedSvfName() + " " + targets
}

def baseSconsOpts() {
    "buildDirName=${getCurrentPlatformBuildDir()} hwtbPlatformName=${getCurrentPlatformName()} svf=${getCurrentPlatformSvf()}"
}

def coverageSconsOpts() {
    return "build=coverage ${baseSconsOpts()}"
}

def releaseSconsOpts() {
    return "build=release ${baseSconsOpts()}"
}

def debugSconsOpts() {
    return "build=debug ${baseSconsOpts()}"
}

def releaseBuild(String target, String additionalOpts = "") {
    scons targets: target, opts: releaseSconsOpts() + " " + additionalOpts
}

def debugBuild(String target, String additionalOpts = "") {
    scons targets: target, opts: debugSconsOpts() + " " + additionalOpts
}

def coverageBuild(String target, String additionalOpts = "") {
    scons targets: target, opts: coverageSconsOpts() + " " + additionalOpts
}

def publishCoverageReports() {
    publishHtmlReport(
        "${getCurrentPlatformBuildDir()}/coverage/${getCurrentPlatformName()}/install_root/doc/coverage/bsp",
        "ARM BSP C Coverage Report",
    )

    publishHtmlReport(
        "${getCurrentPlatformBuildDir()}/coverage/${getCurrentPlatformName()}/install_root/doc/coverage/ctesw",
        "CTESW C Coverage Report",
    )

    publishHtmlReport(
        "${getCurrentPlatformBuildDir()}/coverage/scons-coverage/html",
        "Python Coverage Report",
    )
}

def docDir(String type) {
    return "${getCurrentPlatformBuildDir()}/${type}/${getCurrentPlatformName()}/install_root/doc/"
}

def unitTestsLogDir(String type) {
    return "${getCurrentPlatformBuildDir()}/${type}/${getCurrentPlatformName()}/install_root/tests"
}

def publishHtmlReport(String dir,
                      String title,
                      String reportFiles = "index.html") {
    title = title + " (" + env.N7S_CI_PLATFORM.toUpperCase() + ")"
    publishHTML target: [
        allowMissing: false,
        alwaysLinkToLastBuild: false,
        keepAll: true,
        reportDir: dir,
        reportFiles: reportFiles,
        reportName: title,
    ]
    // separate zip for copying artifacts
    archiveDirAsZip(title.replaceAll(" ", "-").replaceAll("/", "-") + ".zip", dir)
}

def publishHtmlSummary(String title, List<String> files) {
    title = title + " (" + env.N7S_CI_PLATFORM.toUpperCase() + ")"
    pattern = files.collect{ "CTSDP-" + it + "-" + env.N7S_CI_PLATFORM + ".html" }.join(",")
    publishHTML target: [
        allowMissing: false,
        alwaysLinkToLastBuild: false,
        keepAll: true,
        reportDir: "artifacts",
        reportName: title,
        reportFiles: pattern,
        includes: pattern,
    ]
}

def publishDoxygenDocs(String location, String name) {
    publishHtmlReport(docDir("release") + "${location}/html", "Doxygen " + name)
}

def gatherSonarLogs() {
    sh "./sonar/rules_report.py '' ${SONAR_PROJECT_KEY} artifacts/ctesw-sonar-rules.html"
    archiveArtifacts artifacts: "artifacts/ctesw-sonar-rules.html"

    sh "./sonar/issues_report.py '' ${SONAR_PROJECT_KEY} artifacts/ctesw-sonar-issues.html"
    archiveArtifacts artifacts: "artifacts/ctesw-sonar-issues.html"
}

def sonarVariantsForPlatform(String platform) {
    return "${platform},gcc_host_for_${platform}"
}

def getSonarVariants() {
    if (params.selectedTarget == "All")
        return getPlatformList().collect {
            sonarVariantsForPlatform(it)
        }.join(",")
    return sonarVariantsForPlatform(getCurrentPlatformName())
}

def copyCompilationDatabaseForSonar() {
    final host_dir = "sonar-variants/gcc_host_for_${getCurrentPlatformName()}"
    final dir = "sonar-variants/${getCurrentPlatformName()}"
    sh "mkdir -p ${host_dir}"
    sh "mkdir -p ${dir}"
    sh "cp ${getCurrentPlatformBuildDir()}/release/${getCurrentPlatformName()}/compile_commands.json ${dir}"
    sh "cp ${getCurrentPlatformBuildDir()}/release/gcc_host/compile_commands.json ${host_dir}"
}

def archiveDirAsZip(String file, String dir) {
    zip zipFile: "artifacts/" + file, archive: true, dir: dir, overwrite: true
}
