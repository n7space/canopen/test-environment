# This file is part of the Test Environment build system.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import re

from SCons.Action import Action
from SCons.Builder import Builder
from SCons.Script import WhereIs

__LCOV_EXT = ".info"


def exists(env) -> bool:
    return True


def __extractMetrics(env, source, target):
    data = {}
    checks = {
        "lineCoverage": re.compile(r"lines\.+: (\d+\.\d+)% \((\d+) of (\d+) lines\)"),
        "decisionCoverage": re.compile(
            r"branches\.+: (\d+\.\d+)% \((\d+) of (\d+) branches\)"
        ),
    }
    with open(str(source[0]), "r", encoding="utf-8") as summary:
        for line in summary.readlines():
            for name, regex in checks.items():
                if m := regex.search(line):
                    data[name] = {
                        "percent": float(m[1]),
                        "covered": int(m[2]),
                        "total": int(m[3]),
                    }
    report = {
        "title": env.subst("${LCOV_METRICS_TITLE}"),
        "metrics": data,
    }
    with open(str(target[0]), "w", encoding="utf-8") as metrics:
        json.dump(report, metrics, indent=2)


def __lcovAddModules(env, modules: list[str]):
    build_dir_path = env.Dir(env["buildDir"]).path
    for dir in modules:
        env.Append(LCOV_DIRS=["--directory", f"{build_dir_path}/{dir}"])


def __lcovExcludeModules(env, modules: list[str]):
    for dir in modules:
        env.Append(LCOV_REMOVE_PATTERNS=[f"{dir}\\*"])


def __lcovAddFlags(env, flags: list[str]):
    env.Prepend(LCOV_FLAGS=flags)


def __lcovCoverageReport(env, title: str, target: str, sources: list[str]):
    lcov_env = env.Clone()
    lcov_env.Append(GENHTML_FLAGS=["--title", f"'{title.strip()}'"])

    infos = [
        lcov_env.LcovRemove(info)
        for info in lcov_env.Flatten(sources)
        if info.path.endswith(__LCOV_EXT)
    ]

    html = lcov_env.GenHtml(target, infos)
    summary = lcov_env.LcovSummary(str(target) + "/summary.txt", infos)
    metrics = lcov_env.LcovMetrics(str(target) + "/metrics.json", summary)
    reports = [html, metrics]

    env.Alias("coverage-html", reports)
    if platform := env.get("platform"):
        env.Alias(f"coverage-html-{platform}", reports)
    return reports


def __summary_generator(source, target, env, for_signature):
    summaries = " ".join(f"--summary {s}" for s in source)
    return "${LCOV} ${LCOV_FLAGS} " + summaries + " > ${TARGET}"


def generate(env):
    env.SetDefault(LCOV=WhereIs("lcov"))
    env.SetDefault(GENHTML=WhereIs("genhtml"))
    env.SetDefault(LCOV_DIRS=[])
    env.SetDefault(
        LCOV_METRICS_TITLE="${N7S_SW_PLATFORM} ${N7S_SW_TITLE_SHORT} (${N7S_SW_VERSION})"
    )
    env.SetDefault(
        LCOV_REMOVE_PATTERNS=[
            "/usr/\\*",
            "/opt/\\*",
        ]
    )
    env.SetDefault(
        GENHTML_FLAGS=[
            "--rc",
            "genhtml_hi_limit=100",
            "--rc",
            "genhtml_med_limit=80",
            "--branch-coverage",
            "--function-coverage",
            "--show-details",
            "--legend",
            "-p",
            env.Dir(".").path,  # The automatic prefix search can yield weird results.
        ]
    )
    # note: `--ignore-errors` reduces an error to a warning, if it was passed once.
    # to completely ignore an error, it must appear twice, for example: "unused,unused"
    env.SetDefault(
        LCOV_FLAGS=[
            # missing remove pattern should not cause build to stop, not every
            # test will use code from paths masked by them, and setting them
            # separately for every test would be a headache.
            "--ignore-errors",
            "unused",
            "--compat-libtool",
            "--rc",
            "branch_coverage=1",
            "--rc",
            "geninfo_unexecuted_blocks=1",
            "--gcov-tool",
            "${GCOV}",
            "--follow",
        ]
    )

    capCmd = [
        "${LCOV} --capture ${LCOV_DIRS} ${LCOV_FLAGS} --output-file ${TARGET}.captured --test-name ${SOURCE.filebase}",
        "${LCOV} --zerocounters ${LCOV_FLAGS} ${LCOV_DIRS}",
        "${LCOV} --capture --initial ${LCOV_FLAGS} ${LCOV_DIRS} --output-file ${TARGET}.initial",
        "${LCOV} --add-tracefile ${TARGET}.captured --add-tracefile ${TARGET}.initial ${LCOV_DIRS} ${LCOV_FLAGS} --output-file ${TARGET}",
    ]
    capBld = Builder(action=capCmd, src_suffix=".log", suffix=__LCOV_EXT)

    rmCmd = "${LCOV} --remove ${SOURCE} ${LCOV_REMOVE_PATTERNS} ${LCOV_FLAGS} --output-file ${TARGET}"
    rmBld = Builder(
        action=rmCmd, single_source=1, src_suffix=__LCOV_EXT, suffix=".stripped.info"
    )

    genCmd = "${GENHTML} ${GENHTML_FLAGS} -o ${TARGET} ${SOURCES}"
    genBld = Builder(action=genCmd)

    summaryBld = Builder(generator=__summary_generator)

    metricsBld = Builder(
        action=Action(__extractMetrics, "Extracting metrics ${SOURCE} -> ${TARGET}")
    )

    env.Append(
        BUILDERS={
            "LcovCapture": capBld,
            "LcovRemove": rmBld,
            "LcovSummary": summaryBld,
            "LcovMetrics": metricsBld,
            "GenHtml": genBld,
        }
    )

    env.AddMethod(__lcovAddModules, "LcovAddModules")
    env.AddMethod(__lcovExcludeModules, "LcovExcludeModules")
    env.AddMethod(__lcovAddFlags, "LcovAddFlags")
    env.AddMethod(__lcovCoverageReport, "LcovCoverageReport")
