# This file is part of the Test Environment build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


def exists(env) -> bool:
    return True


def generate(env):
    def printSymbolAddressAction(self, symbol: str):
        return (
            "${OBJDUMP} -t ${TARGET} | grep \\\\s"
            + symbol
            + "\\$$ | awk '{print \"SYMBOLS(${TARGET}) - "
            + symbol
            + ": 0x\" $1}'"
        )

    def logSectionsAction(self):
        return "${OBJDUMP} -x ${TARGET} > ${TARGET}-sections.txt"

    def readSymbolAddress(env, binary: list, name: str):
        source = binary[0].path
        target = binary[0].path + "." + name
        env.Execute(
            "${OBJDUMP} -t "
            + source
            + " | grep \\\\s"
            + name
            + "\\$$ | cut -c 1-8 > "
            + target
        )
        with open(target) as f:
            return int("0x" + f.read(), base=16)

    env.AddMethod(printSymbolAddressAction, "PrintSymbolAddressAction")
    env.AddMethod(logSectionsAction, "LogSectionsAction")
    env.AddMethod(readSymbolAddress, "ReadSymbolAddress")
