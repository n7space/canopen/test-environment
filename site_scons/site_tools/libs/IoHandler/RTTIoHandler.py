# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..ConnectionConfig import ConnectionConfig
from .SocatIoHandler import SocatIoHandler


class RTTIoHandler(SocatIoHandler):
    """
    Class responsible for gathering the output of the application executed on HWTB,
    when the output is transimtted over SEGGER RTT (forwarded via `socat`).
    """

    def __init__(
        self,
        connection_config,
        rtt_local_port,
        debug=False,
    ):
        super().__init__(connection_config, "RTT", debug)

        self.rtt_local_port = rtt_local_port

        self.socatArgs = (
            " -b1048576"
            + f" tcp-l:{self.connection_config.socket.port},reuseaddr"
            + f" tcp:127.0.0.1:{self.rtt_local_port}"
        )

    @staticmethod
    def from_config(config, section):
        return RTTIoHandler(
            ConnectionConfig.from_config(config[section]),
            rtt_local_port=config.getint(section, "rtt_local_port", fallback=19021),
            debug=config.getboolean(section, "verbose", fallback=True),
        )
