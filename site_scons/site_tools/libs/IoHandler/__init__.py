# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from abc import ABC, abstractmethod


class IoHandler(ABC):
    """
    Interface for handling output of the application executed in SVF.
    """

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def getOptimalReadSize(self):
        pass

    @abstractmethod
    def open(self):
        pass

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def send(self, data, timeout):
        pass

    @abstractmethod
    def receive(self, maxlen, timeout, blockUntilMaxlenReceived):
        pass

    @abstractmethod
    def flush(self):
        pass

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def enable_debug_log_file(self, path: str):
        pass

    @staticmethod
    def from_type(io_type):
        if io_type == "rtt":
            from .RTTIoHandler import RTTIoHandler

            return RTTIoHandler

        raise RuntimeError(f"Unknown IO handler: {io_type}")

    @staticmethod
    def get_selected_type_from(config):
        return config.get("ioHandler", "type")

    @staticmethod
    def get_subsection_for(io_type):
        return f"ioHandler.{io_type}"

    @staticmethod
    def get_selected_config(config):
        io_type = IoHandler.get_selected_type_from(config)
        return config[IoHandler.get_subsection_for(io_type)]

    @staticmethod
    def from_config(config):
        """
        Designed to construct from ConfigParser files like:

        [ioHandler]
        type = XYZ

        [ioHandler.XYZ]
        option1 = 1
        option2 = 3
        """
        io_type = IoHandler.get_selected_type_from(config)

        io = IoHandler.from_type(io_type)

        return io.from_config(config, IoHandler.get_subsection_for(io_type))
