# This file is part of the Test Environment build system.
#
# @copyright 2022-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import select
import socket
import time

from ..RemoteAppInvoker.SocatInvoker import SocatInvoker
from . import IoHandler


class SocatIoHandler(IoHandler):
    """
    Base Class for serial-like IO handlers responsible for gathering the
    output of the application executed on HWTB, when the output is forwarded via `socat`.
    """

    OPTIMAL_READ_SIZE = 4096

    def getOptimalReadSize(self):
        return self.OPTIMAL_READ_SIZE

    def __init__(
        self,
        connection_config,
        name,
        log_debug=False,
        log_traffic=False,
    ):
        self.name = name
        self.connection_config = connection_config
        self.log_debug = log_debug
        self.log_traffic = log_traffic

        self.ioSocket = None
        self.opened = False

        self.socatArgs = None

        self.socatInvoker = None
        self.setup_commands = None

    def get_name(self):
        return self.name

    def _openSocket(self, timeout=1):
        self.ioSocket = socket.socket()
        self.ioSocket.setblocking(False)
        location = (
            self.connection_config.socket.address,
            self.connection_config.socket.port,
        )
        elapsed = 0
        while elapsed < timeout:
            checkresult = self.ioSocket.connect_ex(location)
            if checkresult == 0:
                return
            else:
                time.sleep(0.1)
                elapsed += 0.1
        raise RuntimeError("Wait for IO Handler (socat) timed out")

    def _spawnRemoteSocat(self):
        if self.socatInvoker is None:
            self.socatInvoker = SocatInvoker(
                self.connection_config,
                self.name,
                self.socatArgs,
                setup_commands=self.setup_commands,
                log_debug=self.log_debug,
                log_traffic=self.log_traffic,
            )
        self.socatInvoker.open()

    def __checkIfLaunched(self):
        if self.socatInvoker is not None:
            return self.socatInvoker.running
        else:
            return False

    def open(self):
        super().open()
        if self.opened:
            raise RuntimeError("IoHandler has to be closed before opening it again!")

        if self.__checkIfLaunched():
            raise RuntimeError("IoHandler is already running elsewhere.")
        self._spawnRemoteSocat()
        self._openSocket()

        self.opened = True

    def checkAndForceClose(self):
        if self.__checkIfLaunched():
            self.socatInvoker.close()

    def close(self):
        if not self.opened:
            return

        self.opened = False

        self.ioSocket.shutdown(socket.SHUT_RDWR)
        self.ioSocket.close()
        self.ioSocket = None

        self.checkAndForceClose()
        super().close()

    def send(self, data, timeout):
        if self.ioSocket is None:
            raise RuntimeError("The IO handler has not been open()'d!")

        start_time = time.time()
        time_elapsed = 0

        while (len(data) > 0) and (time_elapsed < timeout):
            _, writable, _ = select.select(
                [], [self.ioSocket], [], timeout - time_elapsed
            )
            if writable:
                sent = self.ioSocket.send(data)
                if sent <= 0:
                    raise RuntimeError("IO handler socket connection broken")
                data = data[sent:]
            time_elapsed = time.time() - start_time

        if len(data) > 0:
            raise TimeoutError(
                "IO handler did not manage to sent all data in requested time"
            )

    def _receive_anything_block(self, maxlen, timeout):
        data = b""
        try:
            readable, _, _ = select.select([self.ioSocket], [], [], timeout)
            if readable:
                data = self.ioSocket.recv(maxlen)
        except OSError:
            pass
        return data

    def _receive_exact_block(self, maxlen, timeout):
        start_time = time.time()
        time_elapsed = 0
        data = b""
        while len(data) < maxlen and time_elapsed < timeout:
            data_left = maxlen - len(data)
            try:
                readable, _, _ = select.select(
                    [self.ioSocket], [], [], timeout - time_elapsed
                )
                time_elapsed = time.time() - start_time
                if readable:
                    data += self.ioSocket.recv(data_left)
            except OSError:
                pass
        if len(data) != maxlen:
            raise TimeoutError(
                f"IO handler: Did not receive requested data length in time, requested: {maxlen}, received: {len(data)}"
            )
        return data

    def _receive_noblock(self, maxlen):
        data = b""
        try:
            readable, _, _ = select.select([self.ioSocket], [], [], 0)
            if readable:
                data = self.ioSocket.recv(maxlen)
        except OSError:
            pass
        return data

    def receive(self, maxlen=1, timeout=1, blockUntilMaxlenReceived=True):
        if self.ioSocket is None:
            raise RuntimeError("The IO handler has not been open()'d!")

        data = b""
        if timeout > 0 and not blockUntilMaxlenReceived:
            data = self._receive_anything_block(maxlen, timeout)
        elif timeout > 0 and blockUntilMaxlenReceived:
            data = self._receive_exact_block(maxlen, timeout)
        else:
            data = self._receive_noblock(maxlen)

        return data

    def flush(self):
        while (
            len(self.receive(8 * 1024, timeout=0, blockUntilMaxlenReceived=False)) > 0
        ):
            pass  # read all cached incoming bytes

    def reset(self):
        self.flush()
        super().reset()

    def enable_debug_log_file(self, path: str):
        if self.socatInvoker:
            self.socatInvoker.enable_debug_log_file(path)
