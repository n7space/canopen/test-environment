# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import threading

from ..Logger import get_logger
from . import IoHandler


class Reader:
    def __init__(self, io: IoHandler, tee_stdout: bool = True):
        self.io = io
        self.tee_stdout = tee_stdout
        self.thread = None
        self.finish = threading.Event()
        self.terminate = threading.Event()
        self.end_marker = None
        self.end_marker_found = False
        self.logger = get_logger(f"IO-{self.io.get_name()}")

    def is_running(self) -> bool:
        return self.thread is not None and self.thread.is_alive()

    def start(self, log_path: str = None, end_marker: bytes = None) -> None:
        name = "ReaderThread"
        self.logger.info(f"Starting {name}")
        log_path = log_path if log_path else "io_handler.log"
        self.io.enable_debug_log_file(log_path + ".io-debug")
        self.end_marker_found = False
        self.end_marker = end_marker
        self.thread = threading.Thread(
            target=self._download_io, args=(log_path,), name=name
        )
        self.finish.clear()
        self.terminate.clear()
        self.thread.start()

    def stop(self, timeout: float = None) -> None:
        self.finish.set()
        self.thread.join(timeout)
        if self.thread.is_alive():
            self.terminate.set()
            self.thread.join(0.1)
            raise TimeoutError(f"Downloading output took longer than {timeout}")
        self.logger.info("Reader thread stopped")

    def _log_line(self, line) -> None:
        try:
            self.logger.info(line.decode("utf-8").rstrip())
        except UnicodeDecodeError:
            self.logger.info(
                f"Not showing binary data ({len(line)} bytes) - see log file."
            )

    def _process_line(self, line: bytearray) -> bytearray:
        if line.endswith(b"\n"):
            self._check_if_end_marker_found(line)
            if self.tee_stdout:
                self._log_line(line)
            return bytearray()
        return line

    def _tee_remaining(self, line: bytearray) -> None:
        if self.tee_stdout and line:
            self._log_line(line)

    def _check_if_end_marker_found(self, line: bytearray) -> None:
        if not self.end_marker_found:
            self.end_marker_found = line == self.end_marker

    def _process_data(self, out, line: bytearray, data: bytearray) -> bytearray:
        out.write(data)
        for datum in data:
            line.append(datum)
            line = self._process_line(line)
        return line

    def _receive(self, timeout) -> bytes:
        return self.io.receive(
            timeout=timeout,
            maxlen=self.io.getOptimalReadSize(),
            blockUntilMaxlenReceived=False,
        )

    def _download_output_until_stop(self, out) -> None:
        line = bytearray()
        while not self.finish.is_set():
            data = self._receive(timeout=1)
            if not data:
                continue
            line = self._process_data(out, line, data)
        self._tee_remaining(line)

    def _output_end_was_reached(self, data) -> bool:
        if not self.end_marker:
            return not bool(data)
        return self.end_marker_found

    def _download_remaining_output(self, out) -> None:
        line = bytearray()
        end_reached = False
        while not end_reached and not self.terminate.is_set():
            data = self._receive(timeout=0.1)
            line = self._process_data(out, line, data)
            end_reached = self._output_end_was_reached(data)
        self._tee_remaining(line)

    def _download_io(self, out_path) -> None:
        self.logger.info(
            f"output file set to {out_path}, tee to stdout: {self.tee_stdout}"
        )
        with open(out_path, "wb") as out:
            self._download_output_until_stop(out)
            self.logger.info("Received stop request, downloading remaining output")
            self._download_remaining_output(out)
        self.logger.info("Finished")
