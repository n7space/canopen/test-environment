# This file is part of the Test Environment build system.
#
# @copyright 2022-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time

import paramiko
from scp import SCPClient


class SshBackend:
    def __init__(self, parent, force_kill=False):
        self.parent = parent

        self.handle = None
        self.port_forwarder = None
        self.streams = None
        self.pid = None
        self.force_kill = force_kill

    def open(self):
        self.__openSsh()
        if self.parent.setup_commands:
            for cmd, args in self.parent.setup_commands:
                self.parent.logger.info(f"Executing setup command: {cmd} {args}")
                _, stdout, _ = self.handle.exec_command(f"{cmd} {args}")
                stdout.channel.recv_exit_status()

        command = f"echo $$; exec {self.parent.path} {self.parent.args}"
        self.parent.logger.info(f"Executing remotely: {command}")
        self.streams = self.handle.exec_command(
            command, environment=self.parent.environment
        )
        self.pid = int(self.stdout().readline())
        self.parent.logger.info(f"Started remotely: {self.pid}")

    def close(self):
        if self.handle:
            kill_command = (
                "kill "
                + ("-9 " if self.force_kill else "")
                + "--verbose "
                + str(self.pid)
            )
            self.parent.logger.info(f"Killing remotely: '{kill_command}'")
            _, stdout, _ = self.handle.exec_command(kill_command)
            while not stdout.channel.exit_status_ready:
                # Loop until command executes
                pass
            self.parent.logger.info("Kill done")
        if self.port_forwarder:
            self.parent.logger.info("Stopping port forwarder")
            self.port_forwarder.stop()
        if self.handle:
            self.handle.close()
        self.handle = None
        self.port_forwarder = None

    def wait_for_exit(self, timeout):
        if self.handle:
            # NOTE: paramiko ignores timeouts set with channel.settimeout() when waiting for exit
            t0 = time.time()
            time_elapsed = 0
            while time_elapsed < timeout:
                if self.stdout().channel.exit_status_ready():
                    return self.stdout().channel.recv_exit_status()

                time.sleep(0.05)
                time_elapsed = time.time() - t0

            self.close()
            raise RuntimeError(f"Timeout, {self.pid}")

    def signal(self, sig):
        if self.handle:
            self.parent.logger.info(f"Sending signal {sig.name} to {self.pid}")
            inp, _, _ = self.handle.exec_command(f"kill -{sig.name} {self.pid}")
            inp.channel.recv_exit_status()
        else:
            raise RuntimeError("Tried to send signal with no active ssh connection")

    def get_pid(self):
        return self.pid

    def download(self, remote_path, local_path):
        self.parent.logger.info(
            f"Downloading from remote: '{remote_path}' into '{local_path}'."
        )
        with SCPClient(self.handle.get_transport()) as scp:
            scp.get(remote_path, local_path)

    def __openSsh(self):
        try:
            self.handle = paramiko.SSHClient()
            self.handle.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.handle.connect(
                self.parent.connection_config.ssh.host(),
                self.parent.connection_config.ssh.port(22),
                self.parent.connection_config.ssh.username,
                self.parent.connection_config.ssh.password,
            )
        except Exception:
            if self.handle is not None and self.handle.get_transport() is not None:
                self.handle.close()
            self.handle = None
            raise

    def stdout(self):
        return self.streams[1]

    def stdin(self):
        return self.streams[0]

    def stderr(self):
        return self.streams[2]
