# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time

from ..Logger import get_logger
from .Reader import DefaultReader


class RemoteAppInvoker:
    """
    Utility class for executing custom applications either locally or via SSH,
    depending on configuration.
    """

    def __init__(
        self,
        path,
        args="",
        connection_config=None,
        init_delay=None,
        log_prefix=None,
        reader=DefaultReader,
        setup_commands=None,
        environment=None,
        force_kill=False,
    ):
        backend_class = self.__get_backend_class(connection_config)
        self.backend = backend_class(self, force_kill)

        self.path = path
        self.args = args
        self.connection_config = connection_config
        self.init_delay = init_delay
        self.setup_commands = setup_commands
        self.environment = environment

        self.running = False

        self.logger = get_logger(log_prefix if log_prefix else f"RemoteApp({path})")

        # lambdas to delay stream retrieving
        streams = {
            "stderr": lambda: self.backend.stderr(),
            "stdout": lambda: self.backend.stdout(),
            "stdin": lambda: self.backend.stdin(),
        }

        self.reader = reader.from_streams(streams, self.logger)

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def __get_backend_class(self, connection_config):
        if connection_config.ssh is None:
            from .LocalBackend import LocalBackend

            return LocalBackend
        from .SshBackend import SshBackend

        return SshBackend

    def open(self):
        if self.running:
            self.logger.warrning("already running")
            return

        self.logger.info("open")
        self.backend.open()
        self.logger.info("backend started")
        self.reader.start()
        self.logger.info("reader started")

        if self.init_delay is not None:
            time.sleep(self.init_delay)
            self.logger.info(f"init_delay complete ({self.init_delay})")

        self.running = True

    def close(self):
        self.logger.info("close")
        self.backend.close()
        self.logger.info("backend closed")
        self.reader.stop()
        self.logger.info("reader stopped")
        self.running = False

    def wait_for_exit(self, timeout):
        self.logger.info(f"waiting for remote exit with timeout: {timeout}")
        return self.backend.wait_for_exit(timeout)

    def signal(self, sig):
        self.backend.signal(sig)

    def get_pid(self):
        return self.backend.get_pid()

    def download_file(self, remote_path, local_path=""):
        self.backend.download(remote_path, local_path)

    def get_reader(self):
        return self.reader

    def stdout(self):
        return self.backend.stdout()

    def stdin(self):
        return self.backend.stdin()

    def stderr(self):
        return self.backend.stderr()
