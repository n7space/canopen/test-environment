# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..ConnectionConfig import ConnectionConfig
from . import RemoteAppInvoker


class SocatInvoker(RemoteAppInvoker):
    """
    Invoker for spawning Socat with specified arguments
    and logging it's traffic to a file
    """

    def __init__(
        self,
        connection_config: ConnectionConfig,
        name: str,
        socat_arguments: str,
        setup_commands: str | None = None,
        log_traffic: bool = False,
        log_debug: bool = False,
        force_kill: bool = False,
    ):
        self.name = name
        self.socat_arguments = socat_arguments

        socat_args = (
            ("-x " if log_traffic else "")
            + ("-ddd " if log_debug else "")
            + "-lf/dev/stdout "  # messages go to stdout
            + socat_arguments.strip()
            + " 3>&2 2>&1 1>&3"  # swap stderr with stdout
        )

        super().__init__(
            "socat",
            args=socat_args,
            connection_config=connection_config,
            log_prefix=f"{name}-SOCAT",
            setup_commands=setup_commands,
            force_kill=force_kill,
        )

    def enable_debug_log_file(self, path: str):
        self.get_reader().enable_log_to_file(path)

    def enable_raw_stdout_log_file(self, path: str):
        self.get_reader().enable_raw_stdout_log_to_file(path)

    def enable_raw_stderr_log_file(self, path: str):
        self.get_reader().enable_raw_stderr_log_to_file(path)
