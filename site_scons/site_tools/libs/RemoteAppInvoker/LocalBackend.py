# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import shutil
import subprocess


class LocalBackend:
    def __init__(self, parent, force_kill=False):
        self.parent = parent
        self.handle = None
        self.force_kill = force_kill

    def _prepareCommand(self, path, arg_string):
        bin_path = (
            os.path.realpath(path)
            if os.path.isfile(os.path.realpath(path))
            else shutil.which(path)
        )
        cmd = [bin_path] + (
            [arg.strip() for arg in arg_string.split()]
            if arg_string is not None
            else []
        )
        return cmd

    def open(self):
        if self.parent.setup_commands:
            for cmd, args in self.parent.setup_commands:
                cmd_list = self._prepareCommand(cmd, args)
                self.parent.logger.info(
                    f"Executing setup command: {' '.join(cmd_list)}"
                )
                subprocess.run(cmd_list)

        cmd_list = self._prepareCommand(self.parent.path, self.parent.args)
        self.parent.logger.info(f"Executing locally: {' '.join(cmd_list)}")
        self.handle = subprocess.Popen(
            cmd_list,
            shell=False,
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            encoding="utf-8",
            env=self.parent.environment,
        )

    def close(self):
        if self.handle:
            if self.force_kill:
                self.handle.kill()
            else:
                self.handle.terminate()
        self.handle = None

    def wait_for_exit(self, timeout):
        if self.handle:
            status = self.handle.wait(timeout)
            self.handle = None
            return status

    def signal(self, sig):
        if self.handle:
            self.parent.logger.info(f"Sending signal {sig.name} to {self.handle.pid}")
            self.handle.send_signal(sig)
        else:
            raise RuntimeError("Tried to send signal with no process open")

    def get_pid(self):
        return self.handle.pid

    def download(self, remote_path, local_path):
        if local_path:
            self.parent.logger.info(f"Copying {remote_path} into {local_path}")
            shutil.copy(remote_path, local_path)

    def stdout(self):
        return self.handle.stdout

    def stdin(self):
        return self.handle.stdin

    def stderr(self):
        return self.handle.stderr
