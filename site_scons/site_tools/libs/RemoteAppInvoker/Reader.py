# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import threading
import time
from abc import ABC, abstractmethod

from ..Logger import get_file_logger


class Reader(ABC):
    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def stop(self):
        pass

    @abstractmethod
    def enable_log_to_file(self, path: str):
        pass

    @classmethod
    def from_streams(cls, streams, logger):
        return cls(streams, logger)


class NoneReader(Reader):
    def __init__(self, streams, logger):
        logger.info("NoneReader selected.")

    def start(self):
        """noop"""

    def stop(self):
        """noop"""


class DefaultReader(Reader):
    def __init__(self, streams, logger):
        logger.info("DefaultReader selected.")
        self.streams = streams
        self.kill = threading.Event()
        self.threads = []
        self.logger = logger
        self.line_flags = {}
        self.file_log = None
        self.raw_stdout_file_log = None
        self.raw_stderr_file_log = None

    def start(self):
        # one stream per thread - ugly, but Paramiko does not properly support 'select'...
        self.threads = [
            threading.Thread(
                name="stderr",
                target=self.__read,
                kwargs={
                    "stream": self.streams["stderr"](),
                    "line_handler": self.__log_stderr,
                },
            ),
            threading.Thread(
                name="stdout",
                target=self.__read,
                kwargs={
                    "stream": self.streams["stdout"](),
                    "line_handler": self.__log_stdout,
                },
            ),
        ]

        self.kill.clear()

        for thread in self.threads:
            thread.start()

    def stop(self):
        self.logger.info("Stopping reader threads")

        self.kill.set()

        for thread in self.threads:
            thread.join()

        self.logger.info("Reader threads stopped")

        self.__cleanup_log(self.file_log)
        self.__cleanup_log(self.raw_stdout_file_log)
        self.__cleanup_log(self.raw_stderr_file_log)

        self.file_log = None
        self.raw_stdout_file_log = None
        self.raw_stderr_file_log = None

        self.threads = []

    def add_line_flag(self, line):
        flag = threading.Event()
        self.line_flags[line] = flag
        return flag

    def enable_log_to_file(self, path: str):
        self.file_log = get_file_logger(self.logger.name + "-fd", path)

    def enable_raw_stdout_log_to_file(self, path: str):
        self.raw_stdout_file_log = get_file_logger(
            self.logger.name + "-fd-raw-stdout", path, log_raw_data=True
        )

    def enable_raw_stderr_log_to_file(self, path: str):
        self.raw_stderr_file_log = get_file_logger(
            self.logger.name + "-fd-raw-stderr", path, log_raw_data=True
        )

    @staticmethod
    def __cleanup_log(file_log):
        if file_log:
            for handler in file_log.handlers:
                file_log.removeHandler(handler)
                handler.close()

    def __log_stderr(self, line):
        line = line.rstrip()
        self.logger.info(line)
        if self.file_log:
            self.file_log.info(line)

        if self.raw_stderr_file_log:
            self.raw_stderr_file_log.critical(line)

    def __log_stdout(self, line):
        line = line.rstrip()
        if self.file_log:
            self.file_log.info(line)
        else:
            self.logger.info(line)

        if self.raw_stdout_file_log:
            self.raw_stdout_file_log.critical(line)

    def __read(self, stream, line_handler):
        while not self.kill.is_set():
            try:
                line = stream.readline()
            except ValueError:
                continue
            if line:
                for key, flag in self.line_flags.items():
                    if line.strip().startswith(key):
                        flag.set()

                line_handler(line)
            else:
                time.sleep(0.01)
