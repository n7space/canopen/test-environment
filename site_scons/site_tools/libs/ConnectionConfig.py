# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

from configparser import SectionProxy
from typing import NamedTuple, Optional, cast


class SocketConnectionConfig(NamedTuple):
    address: str
    port: int


class SshConnectionConfig(NamedTuple):
    """
    Tuple representing SSH connection configuration.
    """

    address: str
    ssh_port: str
    username: str
    password: str

    def host(self) -> str:
        return self.address.split(":")[0]

    def port(self, fallback: int = 22) -> int:
        if self.ssh_port is not None:
            return int(self.ssh_port)
        pair = self.address.split(":")
        return int(pair[1]) if len(pair) > 1 else fallback

    @staticmethod
    def from_config(config: SectionProxy) -> Optional[SshConnectionConfig]:
        if "address" not in config or config["address"] == "localhost":
            return None
        return SshConnectionConfig(
            **{k: config.get(k) for k in SshConnectionConfig._fields}
        )


class ConnectionConfig(NamedTuple):
    ssh: Optional[SshConnectionConfig]
    socket: SocketConnectionConfig

    @staticmethod
    def from_config(config: SectionProxy) -> Optional[ConnectionConfig]:
        port = config.getint("port")
        ssh = SshConnectionConfig.from_config(config)
        address = "localhost" if not ssh else cast(SshConnectionConfig, ssh).host()
        return ConnectionConfig(ssh, SocketConnectionConfig(address, port))
