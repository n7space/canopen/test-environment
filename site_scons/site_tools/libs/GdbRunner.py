# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import threading
import time

from . import Debugger
from .ConfigParserWrapper import ConfigParserWrapper
from .ConnectionConfig import ConnectionConfig
from .EgseControl import EgseControl
from .GdbInterface import GdbInterface
from .GdbServerInvoker import GdbServerInvoker
from .IoHandler import IoHandler
from .IoHandler.Reader import Reader
from .Logger import get_logger

logger = get_logger("GDB-Runner")


class GdbRunner(object):
    def __init__(self, hwconfig: ConfigParserWrapper):
        self.hwconfig = hwconfig
        self.connection_config = ConnectionConfig.from_config(
            self.hwconfig["gdbServer"]
        )

        self.egse: EgseControl = self._initEgse()
        self.gdbSrv = self._invokeGdbServer()
        self.io: IoHandler = self._openIoHandler()
        self.io.reset()
        self.gdb: GdbInterface = self._invokeGdb()
        self.debugger: (
            Debugger.JLinkDebugger | Debugger.OpenocdDebugger | Debugger.TsimDebugger
        )

        gdb_server = hwconfig.get("gdbServer", "path")
        if "JLinkGDBServer" in gdb_server:
            self.debugger = Debugger.JLinkDebugger(self.gdb)
        elif "openocd" in gdb_server:
            self.debugger = Debugger.OpenocdDebugger(self.gdb)
        elif "tsim" in gdb_server:
            self.debugger = Debugger.TsimDebugger(self.gdb)
        else:
            self.egse.disablePowerSupply()
            raise NotImplementedError(f"Debugger unknown - {gdb_server}")

        self.io_reader = Reader(
            self.io,
            tee_stdout=self.hwconfig.getboolean("ioHandler", "verbose", fallback=True),
        )

    def _initEgse(self) -> EgseControl:
        egse = EgseControl.from_config(self.hwconfig)
        logger.info("Turning board ON")
        egse.enablePowerSupply()

        return egse

    def _prepareEnv(self) -> dict[str, str] | None:
        env = self.hwconfig.get("gdbServer", "environment", fallback="")
        if not env:
            return None
        return os.environ | dict([item.strip().split("=") for item in env.split(";")])

    def _startGdbServer(self, additionalArgs: str | None) -> GdbServerInvoker | None:
        serverProcessName = os.path.basename(self.hwconfig.get("gdbServer", "path"))
        setup_commands = (
            [("killall", serverProcessName), ("sleep", "1")]
            if self.hwconfig.getboolean(
                "gdbServer", "killallBeforeLaunch", fallback=True
            )
            else None
        )
        logger.info("Starting GDB server...")
        srv = GdbServerInvoker(
            self.hwconfig.get("gdbServer", "path"),
            self.hwconfig.get("gdbServer", "args")
            + ((" " + additionalArgs) if additionalArgs else ""),
            self.connection_config,
            verbosity=self.hwconfig.getboolean("gdbServer", "verbose", fallback=False),
            setup_commands=setup_commands,
            environment=self._prepareEnv(),
        )
        reader = srv.get_reader()
        connected_flag = reader.add_line_flag(
            self.hwconfig.get("gdbServer", "connectedFlag")
        )
        failed_flag = reader.add_line_flag(self.hwconfig.get("gdbServer", "failedFlag"))

        srv.open()
        if self._waitForServerStartup(connected_flag, failed_flag):
            return srv
        else:
            logger.warning("GDB server did not start correctly - cleaning up")
            srv.close()
            return None

    def _invokeGdbServer(self, additionalArgs: str | None = None):
        for _ in range(3):
            if srv := self._startGdbServer(additionalArgs):
                return srv
        msg = "Failed to launch GDB server - check debugger connection"
        logger.critical(msg)
        raise RuntimeError(msg)

    def _waitForServerStartup(
        self,
        connected_flag: threading.Event,
        failed_flag: threading.Event,
        timeout: int = 15,
    ):
        time_elapsed = 0
        while time_elapsed < timeout:
            if connected_flag.is_set():
                return True
            elif failed_flag.is_set():
                return False
            else:
                time.sleep(0.1)
                time_elapsed += 0.1
        raise RuntimeError("Wait for GDB server timed out")

    def _invokeGdb(self) -> GdbInterface:
        logger.info("Staring GDB...")
        gdb = GdbInterface(
            f"{self.connection_config.socket.address}:{self.connection_config.socket.port}",
            self.hwconfig.get("gdb", "path"),
            self.hwconfig.get("gdb", "architecture", fallback="auto"),
            self.hwconfig.get("gdb", "customResetCmd", fallback=None),
            verbose=self.hwconfig.getboolean("gdb", "verbose", fallback=False),
            delayStartupCommands=self.hwconfig.getboolean(
                "gdb", "delayStartupCommands", fallback=False
            ),
            timeout=self.hwconfig.getint("gdb", "timeout"),
        )
        gdb.launch()
        return gdb

    def _openIoHandler(self) -> IoHandler:
        logger.info("Starting IO Handler...")
        handler = IoHandler.from_config(self.hwconfig)
        handler.open()
        return handler

    def cleanup(self):
        logger.info("Turning board OFF")
        self.egse.disablePowerSupply()
        logger.info("Cleaning IO Handler...")
        self.io.close()
        logger.info("Cleaning GDB...")
        self.gdb.shutdown()
        logger.info("Cleaning GDB Server...")
        self.gdbSrv.close()

    def executePreInitProgram(self, preInitProgramPath: str):
        logger.info("Initializing pre-init binary...")
        self.gdb.load(preInitProgramPath)
        if "postLoadGdbCommands" in self.hwconfig["gdb"]:
            post_load_commands = self.hwconfig["gdb"]["postLoadGdbCommands"].split("\n")
            for cmd in post_load_commands:
                self.gdb.execCmd(cmd)
        logger.info("Starting pre-init program...")
        self.gdb.start()
        self.waitForProgramExit(False)

        logger.info("Pre-init program finished execution.")

        self.gdb.monitor("file")  # discards current file

    def cleanupPreviousPreStartHook(self):
        self.gdb.installPreStartHook(None)

    def startOnGdb(
        self,
        binaryPath: str,
        logPath: str | None = None,
        reset: bool = True,
        outputEndMarker: bytes | None = None,
        preInitProgramPath: str | None = None,
    ):
        self.cleanupPreviousPreStartHook()
        self.gdb.startupComplete = False
        if reset:
            self.gdb.reset()
        if preInitProgramPath:
            self.executePreInitProgram(preInitProgramPath)
        self.gdb.load(binaryPath)

        if "postLoadGdbCommands" in self.hwconfig["gdb"]:
            post_load_commands = self.hwconfig["gdb"]["postLoadGdbCommands"].split("\n")
            for cmd in post_load_commands:
                self.gdb.execCmd(cmd)

        if self.getIoType() == "rtt":
            self.debugger.setupRTTaddress()

        if self.hwconfig.getboolean(
            "gdb", "enableLowPowerHandlingMode", fallback=False
        ):
            self.debugger.enableLowPowerHandling()

        def preStartHook():
            logger.info("Flushing I/O")
            self.io.flush()
            logger.info("Flush complete")
            if logPath:
                logger.info("Starting I/O reader")
                self.io_reader.start(logPath, end_marker=outputEndMarker)

        self.gdb.installPreStartHook(preStartHook)

        logger.info("Starting execution...")
        self.gdb.start()
        logger.info("Execution started.")

    def waitForProgramExit(self, interruptible: bool):
        gdbConnectionGood = True
        if self.gdb.isRunning():
            try:
                logger.info("Waiting for GDB to finish...")
                response = self.gdb.waitForFinish(
                    timeout=self.hwconfig.getint("gdbServer", "timeout"),
                    interruptible=interruptible,
                )
                if self.debugger.checkProgramExitCorrectly(response):
                    logger.info("Program exited correctly")
                else:
                    logger.info("Program finished with error")
            except Exception as e:
                logger.info("Wait for finish timed out or failed")
                logger.info(e)
                try:
                    self.gdb.stop()
                except Exception as e:
                    logger.info("GDB stop timed out or failed")
                    logger.info(e)
                    gdbConnectionGood = False

        logger.info("Execution finished.")
        if gdbConnectionGood:
            self.gdb.backtrace()
            self.gdb.infoRegisters()

    def waitToFinishOnGdb(
        self,
        logPath: str | None = None,
        interruptible: bool = False,
        outputEndMarker: bytes | None = None,
    ):
        self.waitForProgramExit(interruptible)
        logger.info("Downloading logs...")
        if not self.io_reader.is_running():
            assert logPath
            self.io_reader.start(logPath, end_marker=outputEndMarker)  # type: ignore
        self.io_reader.stop(self.hwconfig.getint("gdbServer", "timeout"))
        logger.info("Log dumped.")

    def readMemoryFromGdb(self, address: int, size: int = 4) -> bytes:
        logger.info(f"Reading from 0x{address:08x}")
        value = self.gdb.rmem(address, size)
        logger.info(f"0x{address:08x}: {value.hex()}")
        return value

    def runOnGdb(self, binaryPath: str, logPath: str | None):
        self.startOnGdb(binaryPath, logPath)
        self.waitToFinishOnGdb()

    def getIoType(self):
        return IoHandler.get_selected_type_from(self.hwconfig)

    def getIoConfig(self):
        return IoHandler.get_selected_config(self.hwconfig)

    def restartServer(self):
        logger.info("Restarting GDB server")
        if self.gdbSrv:
            logger.info("Clenup old GDB server")
            self.gdbSrv.close()
        self.gdbSrv = self._invokeGdbServer()
        logger.info("New server started - reconnecting")
        self.gdb.connect()
