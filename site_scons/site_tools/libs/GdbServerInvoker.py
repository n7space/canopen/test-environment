# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .ConnectionConfig import ConnectionConfig
from .RemoteAppInvoker import RemoteAppInvoker


class GdbServerInvoker(RemoteAppInvoker):
    """
    Class responsible for invoking GDB server.
    It can invoke it locally or remotely via SSH.
    """

    def __init__(
        self,
        path: str,
        args: str = "",
        config: ConnectionConfig = None,
        initDelay: int | None = None,
        verbosity: bool = False,
        setup_commands: str | None = None,
        environment=None,
    ):
        super().__init__(
            path,
            args + (" -silent" if not verbosity else ""),
            config,
            initDelay,
            "GDB-SERVER",
            setup_commands=setup_commands,
            environment=environment,
        )
