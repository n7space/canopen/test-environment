# This file is part of the Test Environment build system.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import paramiko
import scp

from ..ConnectionConfig import ConnectionConfig
from ..Logger import get_logger
from .PowerSupplyControl import PowerSupplyControl

logger = get_logger("EGSE")


class EgseControl:
    @staticmethod
    def from_config(config):
        return EgseControl(
            ConnectionConfig.from_config(config["egse"]),
            PowerSupplyControl.from_config(config),
        )

    def __init__(
        self,
        connection_config,
        power_control,
    ):
        self.connection_config = connection_config
        self.power_control = power_control
        self.logger = logger

        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    def connectClient(self):
        self.client.connect(
            self.connection_config.ssh.host(),
            self.connection_config.ssh.port(),
            self.connection_config.ssh.username,
            self.connection_config.ssh.password,
        )

    def closeClient(self):
        self.client.close()

    def putFileOnEgse(self, localFilePath, remoteFilePath):
        logger.info(f"Uploading '{localFilePath}' -> '{remoteFilePath}'")
        self.connectClient()

        def progress(filename, size, sent):
            logger.info(f"Progress: {float(sent) / float(size) * 100:.2f}")

        with scp.SCPClient(
            self.client.get_transport(), socket_timeout=30.0, progress=progress
        ) as scpClient:
            scpClient.put(localFilePath, remoteFilePath)
            logger.info("Uploading done")
            self.closeClient()

    def executeOnEgse(self, command):
        logger.info(f"Executing on EGSE: '{command}'")
        self.connectClient()
        _, stdout, _ = self.client.exec_command(command)
        while not stdout.channel.exit_status_ready:
            # Loop until command executes
            pass
        output = stdout.read().decode("ascii").strip("\n")
        logger.info(f"'{command}' -> '{output}'")
        self.closeClient()
        return output

    def setEgsePin(self, pin, high=True):
        command = f"sudo raspi-gpio set {pin} op {'dh' if high else 'dl'}"
        self.executeOnEgse(command)

    def pulseEgsePin(self, pin, width=0.05, delay=0):
        low = f"sudo raspi-gpio set {pin} op dl"
        high = f"sudo raspi-gpio set {pin} op dh"
        hold = f"sleep {width}"
        setup = f"{low} && sleep {delay}"
        command = f"{setup} && {high} && {hold} && {low}"
        self.executeOnEgse(command)

    def configureEgsePinAsInput(self, pin):
        self.executeOnEgse(f"sudo raspi-gpio set {pin} ip pd")

    def configureEgsePinAsInputNoPull(self, pin):
        self.executeOnEgse(f"sudo raspi-gpio set {pin} ip pn")

    def getEgsePin(self, pin):
        output = self.executeOnEgse(f"sudo raspi-gpio get {pin}")
        return "level=1" in output

    def enablePowerSupply(self):
        logger.info("Powering ON")
        self.power_control.on(self)
        logger.info("Power is ON")

    def disablePowerSupply(self):
        logger.info("Powering OFF")
        self.power_control.off(self)
        logger.info("Power is OFF")
