# This file is part of the Test Environment build system.
#
# @copyright 2023-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from abc import ABC, abstractmethod


class PowerSupplyControl(ABC):
    @abstractmethod
    def on(self, egse):
        pass

    @abstractmethod
    def off(self, egse):
        pass

    @staticmethod
    def get_type_class(type_name):
        if type_name == "pin":
            from .PinControl import PinControl

            return PinControl

        raise ValueError(f"Unknown power control {type_name}")

    @classmethod
    def from_config(cls, config):
        power_control = config.get("egse", "power_control", fallback="none")
        type_class = cls.get_type_class(power_control)
        return type_class.from_config(config, f"egse.power.{power_control}")
