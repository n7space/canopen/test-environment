# This file is part of the Test Environment build system.
#
# @copyright 2023-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from . import PowerSupplyControl


class PinControl(PowerSupplyControl):
    @staticmethod
    def from_config(config, section):
        return PinControl(
            config.getint(section, "pin"),
            config.getboolean(section, "onIsHigh", fallback=True),
        )

    def __init__(self, power_pin, on_is_high):
        self.power_pin = power_pin
        self.on_is_high = on_is_high

    def on(self, egse):
        egse.setEgsePin(self.power_pin, high=self.on_is_high)

    def off(self, egse):
        egse.setEgsePin(self.power_pin, high=not self.on_is_high)
