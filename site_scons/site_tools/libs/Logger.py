# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging

__DEFAULT_LEVEL = logging.DEBUG
__DEFAULT_FORMATTER = logging.Formatter(
    "%(asctime)s %(name)-10s [%(levelname)-8s](%(threadName)-10s): %(message)s"
)
__RAW_LOG_FORMATTER = logging.Formatter("%(message)s")


def __make_console_handler():
    handler = logging.StreamHandler()
    handler.setLevel(__DEFAULT_LEVEL)
    handler.setFormatter(__DEFAULT_FORMATTER)
    return handler


__CONSOLE_HANDLER = __make_console_handler()
__DEFAULT_HANDLERS = [__CONSOLE_HANDLER]


def get_logger(name: str):
    logger = logging.getLogger(name)
    logger.setLevel(__DEFAULT_LEVEL)

    for handler in __DEFAULT_HANDLERS:
        logger.addHandler(handler)

    return logger


def get_file_logger(name: str, path: str, log_raw_data: bool = False):
    logger = logging.getLogger(name)
    logger.setLevel(__DEFAULT_LEVEL)

    handler = logging.FileHandler(path, mode="w")
    handler.setLevel(__DEFAULT_LEVEL)
    handler.setFormatter(__RAW_LOG_FORMATTER if log_raw_data else __DEFAULT_FORMATTER)

    logger.addHandler(handler)

    return logger


def set_console_level(level: int):
    __CONSOLE_HANDLER.setLevel(level)


def set_default_level(level: int):
    """should be called before any `get_logger`"""
    global __DEFAULT_LEVEL
    __DEFAULT_LEVEL = level
