# This file is part of the Test Environment build system.
#
# @copyright 2022-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from __future__ import annotations

import os
import struct
from dataclasses import dataclass

from .Logger import get_logger

logger = get_logger("Bin-Log")


class BinaryLogDecoder(object):
    CMD_OPEN = ord("O")
    CMD_BYTE = ord("B")
    CMD_DATA = ord("W")

    @dataclass
    class LogBlock:
        cmd: int
        fileno: int
        data: bytes

    def __init__(self, basePath: str | None = None):
        self.parsers = {
            self.CMD_OPEN: self._parseCmdOpen,
            self.CMD_BYTE: self._parseCmdByte,
            self.CMD_DATA: self._parseCmdData,
        }

        self.executors = {
            self.CMD_OPEN: self._execCmdOpen,
            self.CMD_BYTE: self._execCmdWrite,
            self.CMD_DATA: self._execCmdWrite,
        }

        self.basePath = basePath
        self.openFiles = dict()

    def _close_files(self):
        for f in self.openFiles.values():
            f.close()

    def _parseCmdOpen(self, stream) -> LogBlock:
        blockFileno, blockLength = struct.unpack("<ii", stream.read(8))
        blockData = stream.read(blockLength)

        return self.LogBlock(self.CMD_OPEN, blockFileno, blockData)

    def _parseCmdData(self, stream) -> LogBlock:
        blockFileno, blockLength = struct.unpack("<ii", stream.read(8))
        blockData = stream.read(blockLength)

        return self.LogBlock(self.CMD_DATA, blockFileno, blockData)

    def _parseCmdByte(self, stream) -> LogBlock:
        blockFileno, blockData = struct.unpack("<ic", stream.read(5))

        return self.LogBlock(self.CMD_BYTE, blockFileno, blockData)

    def _getNextBlock(self, stream) -> LogBlock | None:
        blockCmd = stream.read(1)
        if len(blockCmd) == 0:
            return None

        return self.parsers[blockCmd[0]](stream)

    def _execCmdOpen(self, block):
        path = block.data.decode("utf-8")
        if self.basePath is not None and not os.path.isabs(path):
            path = os.path.join(self.basePath, path)

        logger.info(f"LOG-DECODER: open file {block.fileno}: {path}")
        if block.fileno in self.openFiles:
            raise RuntimeError(f"File {block.fileno} opened twice")

        self.openFiles[block.fileno] = open(path, "wb")

    def _execCmdWrite(self, block):
        logger.info(
            f"LOG-DECODER: write to file {block.fileno}, {len(block.data)} bytes"
        )
        if block.fileno not in self.openFiles:
            raise RuntimeError(f"File {block.fileno} has not been opened before write")

        self.openFiles[block.fileno].write(block.data)

    def decode(self, stream):
        try:
            while block := self._getNextBlock(stream):
                self.executors[block.cmd](block)
        finally:
            self._close_files()
