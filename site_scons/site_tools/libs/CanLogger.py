# This file is part of the Test Environment build system.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .ConnectionConfig import ConnectionConfig
from .Logger import get_logger
from .RemoteAppInvoker.SocatInvoker import SocatInvoker


class CanLogger:
    """
    Class responsible for providing Socat bridge between test app on host and HWTB,
    and also for logging all CAN traffic.
    """

    def __init__(
        self,
        connection_config: ConnectionConfig,
        can_interface: str,
        can_parameters: str,
        log_socat_debug: bool = False,
    ):
        self.logger = get_logger(f"CanLogger-{can_interface}")

        socat_arguments = (
            " -b16"
            + f" INTERFACE:{can_interface},pf=29,type=3,prototype=1"
            + f" tcp-l:{connection_config.socket.port},reuseaddr"
        )

        ip_link = "sudo ip link"
        setup_commands = [
            (ip_link, f"set {can_interface} down"),
            (ip_link, f"set {can_interface} up type can {can_parameters}"),
            (ip_link, f"set {can_interface} txqueuelen {8 * 1024}"),
        ]

        self.socat_invoker = SocatInvoker(
            connection_config,
            can_interface,
            socat_arguments,
            setup_commands=setup_commands,
            log_traffic=True,
            log_debug=log_socat_debug,
            force_kill=True,
        )

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def open(self):
        self.logger.info("Configuring and opening connection to CAN interface")
        self.socat_invoker.open()

    def close(self):
        self.logger.info("Shutting down CAN connection and interface")
        self.socat_invoker.close()

    def set_data_log_path(self, logFile):
        self.socat_invoker.enable_raw_stdout_log_file(logFile)
