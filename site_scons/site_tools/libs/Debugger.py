# This file is part of the Test Environment build system.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .GdbInterface import GdbInterface


class Debugger(object):
    def __init__(self, gdb: GdbInterface):
        self.gdb = gdb

    def waitForProcessorResetAndContinue(self, timeout: int | None = None):
        """empty generic implementation"""

    def resetHalt(self):
        raise NotImplementedError(
            "Implement debugger-specific function to reset MCU and stop program execution\n"
        )

    def turnFlashProtectionOff(self):
        raise NotImplementedError(
            "Implement debugger-specific function to turn off flash protection\n"
        )

    def excludeFlashCacheRange(self, startAddress: int, endAddress: int):
        raise NotImplementedError(
            "Implement debugger-specific function to exclude flash memory cache range"
        )

    def setupRTTaddress(self):
        raise NotImplementedError(
            "Implement debugger-specific function to set Address of RTT structure"
        )

    def enableLowPowerHandling(self):
        raise NotImplementedError(
            "Implement debugger-specific function to enable low-power mode handling"
        )

    def writeBytes(self, address: int, value: int):
        raise NotImplementedError("Implement debugger-specific function to write bytes")

    def clearBreakpoints(self):
        raise NotImplementedError(
            "Implement debugger-specific function to clear breakpoints"
        )

    def checkProgramExitCorrectly(self, response: list):
        raise NotImplementedError(
            "Implement debugger-specific function to check if program exited correctly"
        )


class JLinkDebugger(Debugger):
    def resetHalt(self):
        self.gdb.execCmd("mon reset")
        self.gdb.waitForDone()
        self.gdb.execCmd("set $pc = &Reset_Handler")
        self.gdb.waitForDone()

    def excludeFlashCacheRange(self, startAddress: int, endAddress: int):
        """
        This is poorly documented JLinkGDBServer option that disables flash memory
        caching on JLink/JLinkGDBServer side (note that this is NOT the built-in GDB dcache)
        Enable this if JLink is used and debugged program modifies flash memory
        that is then read using debugger (without cache exclusion GDB reads old data)
        """
        self.gdb.execCmd(
            f"mon exec ExcludeFlashCacheRange 0x{format(startAddress, '08x')}-0x{format(endAddress, '08x')}"
        )

    def setupRTTaddress(self):
        """
        If jlink and SEGGER RTT are used, we need to tell the JLinkGDBServer
        address of _SEGGER_RTT structure to avoid non-deterministic automatic serach
        mon exec SetRTTAddr &_SEGGER_RTT won't work (address is not evaluated, gdb sends the string)
        """
        self.gdb.execCmd('eval "mon exec SetRTTAddr %p", &_SEGGER_RTT')

    def enableLowPowerHandling(self):
        """
        This is JLink SEGGER specific option that causes debugger to treat
        communication errors less strictly (SEGGER documentation does not mention specifics).
        Micocontroller resets no longer break communication with GDB in this mode.
        """
        self.gdb.execCmd("mon exec EnableLowPowerHandlingMode")

    def clearBreakpoints(self):
        """
        This is workaround for GDB/JLinkGDBServer issue with Cortex-M7
        debugging. Gdb does not read DWT/FPB registers during connect,
        if there are watchpoints (DWT) or breakpoints (FPB) present
        from previous crashed test executions they will not be reported
        (-break-delete) or del commands do not chear them properly.
        Clear relevant DWT and FPB registers manually to avoid
        spurious program halts in unexpected places
        """
        dwt_function_registers = [
            0xE0001028,
            0xE0001038,
            0xE0001048,
            0xE0001058,
        ]
        fpb_comp_registers = [
            0xE0002008,
            0xE000200C,
            0xE0002010,
            0xE0002014,
            0xE0002018,
            0xE000201C,
            0xE0002020,
            0xE0002024,
        ]

        registers_to_clear = dwt_function_registers + fpb_comp_registers
        for reg in registers_to_clear:
            self.gdb.execCmd(f"set *(0x{reg:08X}) = 0")

    def writeBytes(self, address: int, value: int):
        assert len(value) % 4 == 0
        for i in range(0, len(value), 4):
            data_word = value[i : i + 4][::-1].hex()
            location = address + i
            self.gdb.execCmd(f"mon memU32 0x{location:08x} = 0x{data_word}")

    def checkProgramExitCorrectly(self, response: list):
        for payload in (entry["payload"] for entry in response if "payload" in entry):
            if "BKPT #0" in payload:
                return True
        return False


class OpenocdDebugger(Debugger):
    def resetHalt(self):
        self.gdb.execCmd("mon reset halt")
        self.gdb.waitForStop()

    def turnFlashProtectionOff(self):
        self.gdb.execCmd("mon flash protect 0 0 32 off")
        self.gdb.waitForDone()

    def clearBreakpoints(self):
        """not implemented on openocd"""


class TsimDebugger(Debugger):
    def resetHalt(self):
        """not implemented on TSIM"""

    def turnFlashProtectionOff(self):
        """not implemented on TSIM"""

    def clearBreakpoints(self):
        """not implemented on TSIM"""

    def checkProgramExitCorrectly(self, response: list):
        """not implemented on TSIM"""
