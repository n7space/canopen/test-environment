# This file is part of the Test Environment build system.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import binascii
import signal
import time
from collections import deque
from tempfile import NamedTemporaryFile

from pygdbmi.constants import GdbTimeoutError
from pygdbmi.gdbcontroller import GdbController

from .Logger import get_logger

logger = get_logger("GDB-Iface")


class GdbInvalidCommandError(ValueError):
    """
    Exception indicating invalid GDB command.
    """

    pass


class GdbUnexpectedOutputError(ValueError):
    """
    Exception indicating unexpected output from GDB.
    """

    pass


class GdbRuntimeError(RuntimeError):
    """
    Exception indicating GDB runtime error.
    """

    pass


class GdbCommunicationError(EnvironmentError):
    """
    Exception indicating error while trying to communicate with GDB server.
    """

    pass


class GdbInterruptible:
    """
    Class handling user requested interruption by detecting it and passing it
    to gdbmi
    """

    def __init__(self, interruptible, gdbmi):
        self.interruptible = interruptible
        self.gdbmi = gdbmi

    def __enter__(self) -> GdbInterruptible:
        if self.interruptible:
            self._setupSigIntHandler()
        return self

    def __exit__(self, ext_type, ext_value, traceback):
        if self.interruptible:
            self._restoreSigIntHandler()

    def _setupSigIntHandler(self):
        def handler(signum, stack, self=self):
            self.gdbmi.gdb_process.send_signal(signal.SIGINT)

            # call SCons' default behavior to stop other jobs
            baseHandler = (
                self.sconsSigInt if self.sconsSigInt is not None else signal.SIG_DFL
            )
            baseHandler(signum, stack)

        self.sconsSigInt = signal.signal(signal.SIGINT, handler)

    def _restoreSigIntHandler(self):
        baseHandler = (
            self.sconsSigInt if self.sconsSigInt is not None else signal.SIG_DFL
        )
        signal.signal(signal.SIGINT, baseHandler)


class GdbInterface:
    """
    Class representing connection to the GDB server.
    """

    def __init__(
        self,
        address: str,
        path: str = "gdb",
        architecture: str = "auto",
        customResetCmd: str | None = None,
        verbose: bool = False,
        delayStartupCommands: bool = False,
        timeout: int = 60,
    ):
        self.address = address
        self.verbose = verbose
        self.path = path
        self.architecture = architecture
        self.timeout = timeout

        if customResetCmd is not None:
            self.resetCmdList = customResetCmd.split("\n")
        else:
            self.resetCmdList = ["monitor reset"]

        self.gdbmi = None
        self.running = False
        self.connected = False

        self.delayStartupCommands = delayStartupCommands
        self.startupComplete = False
        self.startupCommands = []
        self.preStartHook = None

        self.gdbResponses = deque()

    def _requires_connected(func):
        def _wrapper_connected(self, *args, **kwargs):
            if self.connected:
                return func(self, *args, **kwargs)
            else:
                raise RuntimeError(
                    f"Invalid invocation: {func.__name__}, reason: GDB not connected"
                )

        return _wrapper_connected

    def _requires_running(func):
        def _wrapper_running(self, *args, **kwargs):
            if self.running:
                return func(self, *args, **kwargs)
            else:
                raise RuntimeError(
                    f"Invalid invocation: {func.__name__}, reason: program not running"
                )

        return _wrapper_running

    def _requires_stopped(func):
        def _wrapper_stopped(self, *args, **kwargs):
            if not self.running:
                return func(self, *args, **kwargs)
            else:
                raise RuntimeError(
                    f"Invalid invocation: {func.__name__}, reason: program not stopped"
                )

        return _wrapper_stopped

    def printv(self, *args):
        if self.verbose:
            message = " ".join([str(arg) for arg in args])
            message = message.replace("\\t", "\t")
            message = message.replace("\\n", "\n")
            for msg in message.split("\n"):
                if msg:
                    logger.info(msg)

    def printGdbResponse(self, response):
        console_msgs = [
            entry["payload"] for entry in response if entry["type"] == "console"
        ]
        for msg in console_msgs:
            self.printv(msg)
        errors = [
            entry["payload"]["msg"] for entry in response if entry["message"] == "error"
        ]
        if errors:
            raise GdbRuntimeError(errors[0])

    def execCmd(
        self,
        command: str,
        expectedResponsePatterns: list | None = None,
        timeout: int | None = None,
    ) -> list | None:
        if self.delayStartupCommands and not self.startupComplete:
            if expectedResponsePatterns:
                raise RuntimeError(
                    "Issued GDB command with expected response pattern before startup is complete"
                )
            logger.info(f"delayed GDB command: {command}")
            self.startupCommands.append(command)
            return None
        else:
            timeout = timeout if timeout is not None else self.timeout
            try:
                logger.info(f"GDB command: {command}")
                self.gdbmi.write(
                    command,
                    read_response=False,
                    timeout_sec=timeout,
                )
            except TypeError:
                raise GdbInvalidCommandError(
                    "Invalid command supplied to execCmd: " + str(command)
                )
            if expectedResponsePatterns:
                return self.waitForMatchingGdbResponse(
                    expectedResponsePatterns, timeout=timeout
                )
            return None

    def monitor(
        self,
        command: str,
        expectedResponsePatterns: list = None,
        timeout: int | None = None,
    ) -> list | None:
        return self.execCmd(command, expectedResponsePatterns, timeout)

    def connect(self):
        self.execCmd(f"target extended-remote {self.address}")
        self.connected = True

    @_requires_connected
    @_requires_stopped
    def disconnect(self):
        self.connected = False
        return self.execCmd("-target-disconnect")

    def launch(self, resetStartupCommands: bool = True):
        if resetStartupCommands:
            self.resetStartupCommands()

        self.gdbmi = GdbController([self.path, "--interpreter=mi2"])

        if not self.delayStartupCommands:
            time.sleep(1)

        self.monitor("set architecture " + self.architecture)
        self.connect()
        self.monitor("set remotetimeout 30")

    def readProgram(self, path: str):
        self.monitor("file " + path)

    @_requires_connected
    def loadCurrentProgram(self):
        if self.startupComplete:
            self.monitor("-target-download")
            self.waitForDone()
        else:  # delayed load
            self.monitor("load")

    def load(self, path: str):
        self.readProgram(path)
        self.loadCurrentProgram()

    @_requires_connected
    def reset(self):
        for cmd in self.resetCmdList:
            self.monitor(cmd)

    def installPreStartHook(self, hook):
        self.preStartHook = hook

    def _responseMatch(self, response, pattern) -> bool:
        if not pattern.keys() <= response.keys():
            return False

        for k in pattern:
            if isinstance(pattern[k], dict) and isinstance(response[k], dict):
                if not self._responseMatch(response[k], pattern[k]):
                    return False
            elif pattern[k] != response[k]:
                return False
        return True

    def getGdbResponse(self, interruptible, timeout: int | None = None):
        timeout = timeout if timeout is not None else self.timeout
        if not self.gdbResponses:
            with GdbInterruptible(interruptible, self.gdbmi):
                responses = self.gdbmi.get_gdb_response(
                    timeout_sec=timeout,
                    raise_error_on_timeout=True,
                )
                logger.info(f"GDB response: {responses}")
                self.printGdbResponse(responses)
                self.gdbResponses.extend(responses)
        return self.gdbResponses.popleft()

    def waitForMatchingGdbResponse(
        self, patterns: list, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        timeout = timeout if timeout is not None else self.timeout
        responses = []
        start_time = time.time()
        elapsed_time = 0
        while response := self.getGdbResponse(interruptible, timeout - elapsed_time):
            responses.append(response)
            for pattern in patterns:
                if self._responseMatch(response, pattern):
                    return responses
            elapsed_time = time.time() - start_time
        return []

    def waitForDoneOrError(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        return self.waitForMatchingGdbResponse(
            [
                {"type": "result", "message": "done"},
                {"type": "result", "message": "error"},
            ],
            interruptible,
            timeout,
        )

    def waitForDone(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        return self.waitForMatchingGdbResponse(
            [{"type": "result", "message": "done"}],
            interruptible,
            timeout,
        )

    def waitForBreakpointSet(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        return self.waitForMatchingGdbResponse(
            [
                {
                    "type": "result",
                    "message": "done",
                    "payload": {"bkpt": {}},
                }
            ],
            interruptible,
            timeout,
        )

    def waitForBreakpointSetOrError(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        return self.waitForMatchingGdbResponse(
            [
                {
                    "type": "result",
                    "message": "done",
                    "payload": {"bkpt": {}},
                },
                {"type": "result", "message": "error"},
            ],
            interruptible,
            timeout,
        )

    @_requires_connected
    @_requires_running
    def waitForStop(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        result = self.waitForMatchingGdbResponse(
            [{"type": "notify", "message": "stopped"}],
            interruptible,
            timeout,
        )
        self.running = False
        return result

    @_requires_connected
    def waitForDoneOrStop(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        return self.waitForMatchingGdbResponse(
            [
                {"type": "result", "message": "done"},
                {"type": "notify", "message": "stopped"},
            ],
            interruptible,
            timeout,
        )

    @_requires_connected
    @_requires_running
    def waitForBreakpointHit(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        self.running = False
        return self.waitForMatchingGdbResponse(
            [
                {
                    "type": "notify",
                    "message": "stopped",
                    "payload": {"reason": "breakpoint-hit"},
                },
                {
                    # Workaround for gdb with TSIM: Breakpoints are sometimes reported as SIGINT
                    "type": "notify",
                    "message": "stopped",
                    "payload": {"reason": "signal-received"},
                },
            ],
            interruptible,
            timeout,
        )

    @_requires_connected
    @_requires_running
    def waitForWatchpointHit(
        self,
        interruptible: bool = True,
        watch_read: bool = False,
        watch_access: bool = False,
        timeout: int | None = None,
    ) -> list:
        self.running = False
        stop_reason = "watchpoint-trigger"
        if watch_read:
            stop_reason = "read-watchpoint-trigger"
        if watch_access:
            stop_reason = "access-watchpoint-trigger"

        return self.waitForMatchingGdbResponse(
            [
                {
                    "type": "notify",
                    "message": "stopped",
                    "payload": {"reason": stop_reason},
                }
            ],
            interruptible,
            timeout,
        )

    @_requires_connected
    def waitForBreakpointDeleted(
        self, interruptible: bool = True, timeout: int | None = None
    ) -> list:
        return self.waitForMatchingGdbResponse(
            [{"type": "notify", "message": "breakpoint-deleted"}],
            interruptible,
            timeout,
        )

    @_requires_connected
    @_requires_stopped
    def continueExecution(self) -> list | None:
        self.running = True
        return self.execCmd(
            "-exec-continue", [{"type": "result", "message": "running"}]
        )

    @_requires_connected
    @_requires_stopped
    def deleteBreakpoints(self) -> list:
        self.execCmd("-break-delete")
        return self.waitForDone()

    @_requires_connected
    @_requires_stopped
    def deleteBreakpoint(self, breakpointId: int) -> list:
        self.execCmd(f"-break-delete {breakpointId}")
        return self.waitForDone()

    @_requires_connected
    @_requires_stopped
    def backtrace(self) -> list:
        self.execCmd("bt")
        return self.waitForDone()

    @_requires_connected
    @_requires_stopped
    def infoRegisters(self) -> list:
        self.execCmd("info reg")
        return self.waitForDone()

    @_requires_connected
    @_requires_stopped
    def start(self):
        self.pushStartupCommands()

        self.breakpoint("main", temporary=False)
        self.continueExecution()

        self.waitForBreakpointHit()
        self.deleteBreakpoints()

        if self.preStartHook:
            logger.info("Running pre startup hook")
            self.preStartHook()

        self.continueExecution()

    def pushStartupCommands(self, clearStartupCommands: bool = True):
        self.startupComplete = True
        if self.delayStartupCommands:
            self._executeStartupCommands(clearStartupCommands)

    def _executeStartupCommands(self, clearStartupCommands: bool = True):
        if self.startupCommands:
            with NamedTemporaryFile("wt") as startupFile:
                startupFile.write("\n".join(self.startupCommands) + "\n")
                startupFile.flush()
                logger.info(
                    f"Executing temporary file (GDB script) {startupFile.name} with contents:"
                )
                for cmd in self.startupCommands:
                    logger.info("    " + cmd)

                self.execCmd("source " + startupFile.name)
                sync_marker = "N7S: GDB setup complete"
                self.execCmd(f"echo {sync_marker}")
                self.waitForMatchingGdbResponse(
                    [
                        {"type": "console", "payload": sync_marker},
                    ]
                )

            if clearStartupCommands:
                self.startupCommands = []

    def resetStartupCommands(self):
        self.startupComplete = False
        self.startupCommands = []

    @_requires_connected
    def waitForFinish(self, timeout: int | None = 0, interruptible: bool = True):
        response = self.waitForDoneOrStop(interruptible, timeout)
        self.running = False
        return response

    @_requires_connected
    def stop(self):
        if self.running:
            self.gdbmi.gdb_process.send_signal(signal.SIGINT)
            self.waitForStop()
            self.running = False

    def shutdown(self):
        if self.connected:
            try:
                self.stop()
            except GdbTimeoutError as err:
                logger.warning("Unable to stop execution, GDB will disconnect anyway")
                logger.warning(err)
                self.running = False

            if self.gdbmi is not None:
                self.gdbmi.exit()
                self.gdbmi = None

            self.connected = False

    @_requires_connected
    @_requires_stopped
    def rmem(self, address: int, count: int = 4) -> bytes:
        if count == 0:
            return b""

        self.execCmd(f"-data-read-memory-bytes {hex(address)} {count}")
        # There can be spurious '^done' messages in the queue,
        # wait for one with existing payload which contains key 'memory'
        while response := self.waitForDone()[-1]:
            if (
                "payload" in response
                and response["payload"] is not None
                and "memory" in response["payload"]
            ):
                break

        return binascii.a2b_hex(response["payload"]["memory"][0]["contents"])

    @_requires_connected
    @_requires_stopped
    def wmem(self, address: int, value: int | bytes | bytearray):
        logger.info(f"wmem to 0x{address:08x}")
        if isinstance(value, int):
            logger.info(f"wmem value: {value}")
            value = [value]
        elif isinstance(value, (bytes, bytearray)):
            logger.info(f"wmem byte count: {len(value)}")
            tmpvalue = []
            while value:
                tmpvalue.append(int.from_bytes(value[:4], byteorder="big"))
                value = value[4:]
            value = tmpvalue

        for offset, val in enumerate(value):
            self.monitor(
                "set *((unsigned int*) " + hex(address + offset * 4) + ") = " + str(val)
            )

        if isinstance(value, int) and self.rmem(address) != value:
            raise GdbRuntimeError("Writing memory failed (write-protect?).")

    @_requires_connected
    @_requires_stopped
    def breakpoint(self, location: str, temporary: bool = False) -> int:
        breakArgs = "-t -h" if temporary else "-h"
        self.monitor(f"-break-insert {breakArgs} {str(location)}")
        responses = self.waitForBreakpointSetOrError()
        logger.info(f"GDBMI: {responses}")
        errors = [x for x in responses if x["message"] == "error"]
        if errors:
            self.printv(
                "Setting a hardware breakpoint failed: " + str(errors[0]["payload"])
            )
            self.printv("Setting a software breakpoint at " + str(location) + ".")
            breakArgs = "-t" if temporary else ""
            self.monitor(f"-break-insert {breakArgs} {str(location)}")
            responses = self.waitForBreakpointSet()
        breakpointId = int(responses[-1]["payload"]["bkpt"]["number"])
        logger.info(f"GDBMI: breakpoint ID {breakpointId}")
        return breakpointId

    @_requires_connected
    @_requires_stopped
    def watchpoint(
        self, location: str, watch_read: bool = False, watch_access: bool = False
    ) -> list:
        """Creates a watchpoint at specified location.
        Location can be either an address (usually prefixed with `*`, for example `*0xF00BA442`) or name.
        By default, watchpoint breaks on write. `watch_read` and `watch_access` flags are exclusive.
        """
        watchArgs = ""
        if watch_read:
            watchArgs = "-r"
        elif watch_access:
            watchArgs = "-a"

        self.monitor(f"-break-watch {watchArgs} {str(location)}")
        responses = self.waitForDoneOrError()
        logger.info(f"GDBMI: {responses}")
        return responses

    @_requires_connected
    @_requires_stopped
    def rmemb(self, address: int) -> int:
        output = self.rmem(address, 1)
        return output[0]

    @_requires_connected
    @_requires_stopped
    def wmemb(self, address: int, value: int):
        self.monitor("set *((unsigned char*) " + hex(address) + ") = " + hex(value))
        if self.rmemb(address) != value:
            raise GdbRuntimeError("Writing memory failed (write-protect?).")

    def getEntryPoint(self) -> int:
        self.execCmd("info file")
        messages = self.waitForDone()

        entry_point_lst = [
            msg["payload"].strip().split(":")[-1]
            for msg in messages
            if msg["type"] == "console"
            and msg["payload"] is not None
            and msg["payload"].strip().startswith("Entry point:")
        ]

        if len(entry_point_lst) != 1:
            raise GdbUnexpectedOutputError("Entry point not found in GDB output")

        return int(entry_point_lst[0], base=0)

    def isRunning(self) -> bool:
        return self.running

    def isConnected(self) -> bool:
        return self.connected
