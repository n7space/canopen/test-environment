CTESW (Test Environment) SCons utilities
========================================

This module contains Python utility classes
used by the SCons tool to provide test building and execution features.
