# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pathlib import Path
from typing import Callable

from libs.ConsoleFormatter import green, red
from SCons.Action import Action


def __check_log_action(env, test_name: str, check_contents: Callable[[bytes], bool]):
    def check_log(path: Path):
        with path.open("rb") as log:
            return check_contents(log.read())

    def check_result(path: str):
        success_log = Path(path)
        failure_log = Path(path[:-3] + "failed.log")

        if check_log(success_log):
            print(green(f"PASS: {test_name}"))
            failure_log.unlink(missing_ok=True)
        else:
            print(red(f"FAIL: {test_name}"))
            success_log.rename(failure_log)
            raise Exception(f"{test_name} failed (while checking {path})")

    def action(target, source, env):
        path = str(target[0])
        if path.endswith(".log"):
            check_result(path)
        else:
            print(f"Skipping {path}.")

    return Action(action, f"Checking {test_name} ($TARGET)")


def __alias_unit_test_result(env, program, result, name, **kwargs):
    suffix = kwargs.get("name_suffix", "-unit-tests")
    group = kwargs.get("test_group")
    platform = env.get("platform")

    if suffix:
        name += suffix
        if group:
            group += suffix

    env.Alias("unit-tests-apps", program)
    env.Alias("unit-tests", result)
    env.Alias(name, result)

    if platform:
        env.Alias(f"unit-tests-apps-{platform}", program)
        env.Alias(f"unit-tests-{platform}", result)
        env.Alias(f"{name}-{platform}", result)

    if group:
        env.Alias(group, result)
        env.Alias(f"{group}-apps", program)
        if platform:
            env.Alias(f"{group}-{platform}", result)
            env.Alias(f"{group}-apps-{platform}", program)


def __clone_env_for_unit_tests(env):
    env = env.Clone()
    env.Append(CXXFLAGS=env.get("UNIT_TESTS_CXXFLAGS", []))
    env.Append(CFLAGS=env.get("UNIT_TESTS_CFLAGS", []))
    env.Append(CCFLAGS=env.get("UNIT_TESTS_CCFLAGS", []))
    env.Append(CPPPATH=env.get("UNIT_TESTS_CPPPATH", []))
    env.Append(CPPDEFINES=env.get("UNIT_TESTS_CPPDEFINES", []))
    env.Append(LIBPATH=env.get("UNIT_TESTS_LIBPATH", []))
    env.Append(LINKFLAGS=env.get("UNIT_TESTS_LINKFLAGS", []))
    env.Append(LIBS=env.get("UNIT_TESTS_LIBS", []))
    return env


def exists(env):
    return True


def generate(env):
    env.AddMethod(__check_log_action, "CheckUnitTestsLogAction")
    env.AddMethod(__alias_unit_test_result, "AliasUnitTestsResult")
    env.AddMethod(__clone_env_for_unit_tests, "CloneForUnitTests")

    env.Append(UNIT_TESTS_CPPDEFINES=["N7S_UNIT_TESTS"])
