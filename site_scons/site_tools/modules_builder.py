# This file is part of the Test Environment build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities for building SCons modules"""

import os

from SCons.Node import Node
from SCons.Script import Dir


def __buildModule(env, modulePath: str) -> Node | list[Node]:
    """Builds module (in buildDir) and returns it's result"""
    if env.Dir(".").abspath.startswith(env.Dir(env["buildDir"]).abspath):
        # already in variant dir
        variant_dir = None
    else:
        # top level sconscript
        variant_dir = os.path.join(env["buildDir"], modulePath)
    return env.SConscript(
        modulePath + "/SConscript",
        exports="env",
        variant_dir=variant_dir,
        duplicate=0,
    )


def __buildModules(env, modules: list[str]) -> list[Node | list[Node]]:
    res = [env.BuildModule(m) for m in modules]
    return [r for r in res if r is not None]


def __findUnitTestsModules(modules: list[str], subdir: str) -> list[str]:
    """Looks for tests in `subdir` subdirectory of each module and builds them, returning their results"""
    testModules = []
    moduleTestDirs = [m + subdir for m in modules]
    for testDir in moduleTestDirs:
        sconscript = os.path.join(Dir(".").srcnode().abspath, testDir, "SConscript")
        if os.path.isfile(sconscript):
            testModules += [testDir]
    return testModules


def __buildUnitTestsForModules(
    env, modules: list[str], subdir: str = "/tests"
) -> list[Node | list[Node]]:
    modules = __findUnitTestsModules(modules, subdir)
    return env.BuildModules(modules)


def exists(env) -> bool:
    return True


def generate(env):
    env.AddMethod(__buildModules, "BuildModules")
    env.AddMethod(__buildModule, "BuildModule")
    env.AddMethod(__buildUnitTestsForModules, "BuildUnitTestsFor")
