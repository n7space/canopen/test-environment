# This file is part of the Test Environment build system.
#
# @copyright 2020-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from SCons.Action import Action

from libs.CanLogger import CanLogger
from libs.ConnectionConfig import ConnectionConfig
from libs.Logger import get_logger

logger = get_logger("ctesw_test_case")


def __invokeCanServer(env, bus, canLogPath):
    hwconfig = env["SVF_CONFIG"]
    can = CanLogger(
        ConnectionConfig.from_config(hwconfig[bus]),
        hwconfig.get(bus, "ipLinkCanId"),
        hwconfig.get(bus, "ipLinkCanConfig"),
        hwconfig.getboolean(bus, "verbose"),
    )
    can.set_data_log_path(canLogPath)
    return can


def exists(env) -> bool:
    return True


def __validateLog(logPath):
    startMarker = "============ TEST MAIN START ============"
    endMarker = "============= TEST MAIN END ============="
    exitStatusPrefix = ">> EXIT STATUS: "

    startReached = False
    endReached = False

    logger.info(f"Validating {logPath}")

    with open(logPath) as log:
        for line in log.readlines():
            if line.startswith(startMarker):
                startReached = True
            if line.startswith(endMarker):
                if not startReached:
                    logger.error("FAIL: End marker reached before start marker!")
                    return -3
                endReached = True
            if line.startswith(exitStatusPrefix):
                if not endReached:
                    logger.error("FAIL: Exit code found before end marker!")
                    return -2
                return int(line[len(exitStatusPrefix) :], base=16)
    logger.error("FAIL: Application exit code unknown!")
    return -1


def __executeTest(env, source, target):
    with __invokeCanServer(env, "canBusA", target[2].path):
        with __invokeCanServer(env, "canBusB", target[3].path):
            env.StartOnGdb(source[0].path)
            env.Execute(f"{env.host.TestEnvCmd()} {source[1].path} > {target[1].path}")
            env.WaitToFinishOnGdb(target[0].path)


def __combineResults(env, name, case1, case2):
    def log_test(env, source, target):
        # if we got here, everything must have passed
        env.AddResult(name, "Successful", target[0].path)

    return env.Command(
        [name + ".log"],
        [case1[0], case2[0]],
        [
            "cat $SOURCES > $TARGET",
            log_test,
        ],
    )


def __makeSymmetricalTestCase(
    env, name, app1src, app2src, additional_asserts=None, trace=None
):
    app1_hwtb = env.hwtb.MakeTestCaseApp(
        name + "-app1-hwtb",
        [env.hwtb.MakeObject(src) for src in app1src],
    )
    app2_hwtb = env.hwtb.MakeTestCaseApp(
        name + "-app2-hwtb",
        [env.hwtb.MakeObject(src) for src in app2src],
    )
    app1_host = env.host.MakeTestCaseApp(
        name + "-app1-host",
        [env.host.MakeObject(src) for src in app1src],
    )
    app2_host = env.host.MakeTestCaseApp(
        name + "-app2-host",
        [env.host.MakeObject(src) for src in app2src],
    )

    env.Alias(name + "-apps", [app1_host, app1_hwtb, app2_host, app2_hwtb])

    if trace is not None:
        trace = dict(trace)
        trace["title"] = trace.get("title", f"{name} Test Case (Symmetrical)")
        env.AddTraces(name, trace)

    case1 = env.MakeTestCase(
        name + "-hwtb-to-host", app1_hwtb, app2_host, additional_asserts
    )
    case2 = env.MakeTestCase(
        name + "-host-to-hwtb", app2_hwtb, app1_host, additional_asserts
    )

    result = __combineResults(env, name, case1, case2)
    env.Alias(name, result)

    return result


def __defaultAsserts(**kwrags):
    return True


def __makeTestCase(env, name, hwtbApp, hostApp, additional_asserts=None, trace=None):
    if additional_asserts is None:
        additional_asserts = __defaultAsserts

    env.AddTraces(name, trace)

    def verify_test(env, source, target):
        env.Execute(
            [
                f"cat {source[0].path}",
                f"cat {source[1].path}",
            ]
        )
        hwtbResult = __validateLog(source[0].path)
        if hwtbResult != 0:
            env.AddResult(name, "Failed", source[0].path)
            return hwtbResult
        hostResult = __validateLog(source[1].path)
        if hostResult != 0:
            env.AddResult(name, "Failed", source[1].path)
            return hostResult
        if not additional_asserts(
            env=env, hwtbLog=source[0].path, hostLog=source[1].path
        ):
            logger.error(f"FAIL: {name} - additonal assertions failed")
            env.AddResult(name, "Failed", target[0].path)
            return -1
        env.AddResult(name, "Successful", target[0].path)
        return env.Execute(f"echo '{name} - PASSED' > {target[0].path}")

    env.Alias("test-cases-hwtb-apps", hwtbApp)
    env.Alias("test-cases-host-apps", hostApp)
    env.Alias("test-cases-apps", [hwtbApp, hostApp])

    coverageSuffix = ".coverage" if env["build"] == "coverage" else ""

    logs = env.Command(
        [
            name + coverageSuffix + ".hwtb.log",
            name + ".host.log",
            name + ".can-a.log",
            name + ".can-b.log",
        ],
        [hwtbApp, hostApp],
        Action(__executeTest, f"HWTB: executing {name}"),
    )

    if env["build"] == "coverage":
        hostCov = env.host.LcovCapture(logs[1])
        logs[0] = env.ConvertCoverageLog(sources=logs[0], targets=name + ".hwtb.log")
        hwtbCov = env.hwtb.LcovCapture(logs[0])
        # HOST and HWTB coverage has to be gathered one by one and separately,
        # so the gcda extraction has to be performed _after_ processing HOST coverage files
        env.Depends(logs[0], hostCov)
        logs += [hostCov, hwtbCov]

    cmd = env.Command(
        [name + ".log"],
        logs,
        Action(verify_test, f"HWTB: verifying {name} results"),
    )

    env.Alias(name, cmd)

    return cmd + logs


def generate(env):
    env.Tool("gdb_runner")
    env.Tool("traces")
    env.Tool("results")

    env.AddMethod(__makeTestCase, "MakeTestCase")
    env.AddMethod(__makeSymmetricalTestCase, "MakeSymmetricalTestCase")
