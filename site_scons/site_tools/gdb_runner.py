# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import binascii
import threading
import traceback
from io import BytesIO

from libs.BinaryLogDecoder import BinaryLogDecoder
from libs.GdbRunner import GdbRunner
from libs.Logger import get_logger
from SCons.Action import Action
from SCons.Node import Node

__GDB_RUNNER = None

logger = get_logger("gdb_runner")


def __cleanupThread():
    threading.main_thread().join()
    global __GDB_RUNNER
    if __GDB_RUNNER is not None:
        __GDB_RUNNER.cleanup()
        __GDB_RUNNER = None


def getGdbRunner(env):
    global __GDB_RUNNER
    if __GDB_RUNNER is None:
        config = env.get("GDB_RUNNER_CONFIG")
        if not config:
            config = env["SVF_CONFIG"]
        __GDB_RUNNER = GdbRunner(config)
        logger.info("Initialized GDB-Runner")
        monitor = threading.Thread(target=__cleanupThread, name="GDB Runner Cleanup")
        monitor.start()
    return __GDB_RUNNER


def __startOnGdb(env, executablePath: str):
    getGdbRunner(env).startOnGdb(executablePath)


def __waitToFinishOnGdb(env, logPath: str, interruptible: bool = False):
    getGdbRunner(env).waitToFinishOnGdb(logPath, interruptible)


def __readMemoryFromGdb(env, address: int, size: int = 4):
    return getGdbRunner(env).readMemoryFromGdb(address, size)


def __runOnGdb(env, executablePath: str, logPath: str):
    getGdbRunner(env).startOnGdb(
        executablePath,
        logPath,
        outputEndMarker=env["GDB_RUNNER_OUTPUT_END_MARKER"],
        preInitProgramPath=env["RUNNER_BOOTSTRAPPER"],
    )
    getGdbRunner(env).waitToFinishOnGdb(
        logPath, interruptible=True, outputEndMarker=env["GDB_RUNNER_OUTPUT_END_MARKER"]
    )


def __convertCoverageLogCommand(env, targets: list, sources: list) -> Node:
    def action(target, source, env):
        __convertCoverageLog(env, source[0].path)
        __removeCoverageFromLog(env, source[0].path, target[0].path)

    return env.Command(
        source=sources,
        target=targets,
        action=Action(action, "Converting $TARGET into coverage logs"),
    )


def __convertCoverageLog(env, logFileName: str):
    """Extracts and converts ASCII-encoded coverage log to it's original, binary form"""
    with open(logFileName, "rb") as log:
        logdata = log.read()
        startMarker = b">> COVERAGE RESULT - BEGIN <<"
        endMarker = b"\n>> COVERAGE RESULT - END <<\n"

        startMarkerPosition = logdata.find(startMarker)
        endMarkerPosition = logdata.find(endMarker, startMarkerPosition)

        if startMarkerPosition != -1 and endMarkerPosition != -1:
            data = logdata[startMarkerPosition + len(startMarker) : endMarkerPosition]
            unescaped = binascii.a2b_hex(data.replace(b"\n", b""))

            decoder = BinaryLogDecoder()
            decoder.decode(BytesIO(unescaped))
        else:
            logger.info(f"No binary coverage in file {logFileName}")
    logger.info(f"Done extracting coverage from {logFileName}")


def __removeCoverageFromLog(env, source: str, target: str):
    with open(source, "rb") as log:
        logdata = log.read()
        markerPosition = logdata.find(b">> COVERAGE RESULT - BEGIN <<")
        if markerPosition != -1:
            logdata = logdata[:markerPosition]

        with open(target, "wb") as logOutput:
            logOutput.write(logdata)


def __gdb_runner_action(target: list, source: list, env):
    program = source[0].path
    result = target[0].path
    output = result + ".org"
    try:
        __runOnGdb(env, program, output)
        __convertCoverageLog(env, output)
        __removeCoverageFromLog(env, output, result)
    except Exception:
        # sometimes SCons can "loose" exceptions
        logger.error(f"FATAL GDB RUNNER ERROR: {traceback.format_exc()}")
        raise


def exists(env):
    return True


def generate(env, **kwargs):
    env.Tool("runner")

    env.SetDefault(GDB_RUNNER_OUTPUT_END_MARKER=b">> OUTPUT END MARKER <<\n")

    action = Action(
        __gdb_runner_action, "Executing '$SOURCE' on GDB (logging to '$TARGET')"
    )
    bld = env.Builder(action=action, suffix=".log")

    env.Append(BUILDERS={"GdbRunner": bld})

    env.AddMethod(__convertCoverageLogCommand, "ConvertCoverageLog")
    env.AddMethod(__startOnGdb, "StartOnGdb")
    env.AddMethod(__waitToFinishOnGdb, "WaitToFinishOnGdb")
    env.AddMethod(__readMemoryFromGdb, "ReadMemoryFromGdb")

    if kwargs.get("nodefault", True):
        env.InstallDefaultRunner(bld)
