# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import SCons.Script
from SCons.Node import Node


def exists(env) -> bool:
    return True


def __setup_compilation_db(env) -> Node:
    env["COMPILATIONDB_PATH_FILTER"] = env["buildDir"][2:] + "/*"
    db = env.CompilationDatabase("$buildDir/compile_commands.json")
    env.Alias("compilation_db", db)
    return db


def __generate_compilation_db(env):
    SCons.Script.BUILD_TARGETS += ["compilation_db"]


def generate(env):
    env.Tool("compilation_db")  # The SCons base tool

    env.AddMethod(__setup_compilation_db, "SetupCompilationDb")
    env.AddMethod(__generate_compilation_db, "GenerateCompilationDb")
