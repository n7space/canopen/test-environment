# This file is part of the Test Environment build system.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import json
from pathlib import Path


def __add_result(self, spec_id, status, log):
    result = {
        "specificationId": spec_id,
        "logPath": str(Path(log).relative_to(self.Dir("$buildDir").path)),
        "status": status,
        "date": datetime.datetime.now().astimezone().isoformat(),
    }

    report = self.File("$RESULTS_JSON_PATH").path

    if Path(report).exists():
        with open(report, "r", encoding="utf-8") as f:
            current = {item["specificationId"]: item for item in json.load(f)}
    else:
        current = {}

    current[spec_id] = result

    with open(report, "w", encoding="utf-8") as f:
        json.dump(list(current.values()), f, indent=2)


def exists(env):
    return True


def generate(env):
    env.SetDefault(RESULTS_JSON_PATH=env["buildDir"] + "/tests/report.json")
    env.AddMethod(__add_result, "AddResult")
