# This file is part of the Test Environment build system.
#
# @copyright 2023-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Core utilities for creating SCons environments.
This is the "main" module of N7-Core SCons toolset.
It's responsible for setting up SCons command-line arguments, tools, compilation environment and flags.

Example usage:

    env = Environment()
    env.Tool("n7s.core-std-env", additional_variables=additional_variables)
    env.CreateStdSubEnvs(["samv71q21", "samrh71f20", "samrh707f18"])
    for subenv in env.AllEnvs.Items():
        __setup_environment(subenv)
    return env

where __setup_environment(subenv) would add custom tools and configuration for each build env.
For more details, see README.md and `env_builder`/`multi_platform` tools documentation.

For list of provided environment methods, see `generate()`.
"""

import os

from SCons.Script import ARGUMENTS, BoolVariable, EnumVariable, PathVariable, Variables
from SCons.Script.Main import SetOption
from SCons.Script.SConscript import SConsEnvironment
from SCons.Warnings import warningAsException


def exists(env) -> bool:
    return True


def __build_variables(additional_variables: list) -> Variables:
    variables = Variables(ARGUMENTS.get("options", "options.py"), ARGUMENTS)

    variables.Add(
        EnumVariable(
            "build",
            "Defines build type",
            "release",
            allowed_values=("release", "debug", "coverage"),
        )
    )

    variables.Add(
        PathVariable(
            "svf",
            "SVF hardware configuration file",
            "svf-configs/localhost.conf",
            PathVariable.PathIsFile,
        )
    )

    variables.Add(
        BoolVariable("checkCode", "Set to enable additional code checks", True)
    )

    variables.Add(
        EnumVariable(
            "optimization",
            "Optimization option used by release build "
            "(g can be pased to debug and coverage builds also)",
            "2",
            allowed_values=("g", "0", "1", "2", "3", "s"),
        )
    )

    variables.Add(
        BoolVariable(
            "disableAsserts",
            "Disables assertions in the code "
            "(smaller binary but reduced memory corruption detection)",
            False,
        )
    )

    variables.Add(
        "buildDirName",
        "Name of the main build directory (usefull for parallel CI builds)",
        "build",
    )

    if additional_variables:
        variables.AddVariables(*additional_variables)

    return variables


def __handle_unknown_variables(variables: list):
    unknown = variables.UnknownVariables()
    if unknown:
        # "options" is used to load options themselves
        names = [name for name in unknown.keys() if name != "options"]
        if names:
            raise Exception(f"Unknown variables: {names}")


def __get_optimization_flag(env) -> str:
    if env["build"] in ("debug", "coverage"):
        if env["optimization"] in ("0", "g"):
            return env["optimization"]
        return "0"
    return env["optimization"]


def __configure_ndebug_flag(env):
    if env["disableAsserts"] or (env["build"] == "coverage"):
        env.Append(CPPDEFINES=["NDEBUG"])


def __configure_base_flags(env):
    env.SetDefault(LIBS=[])
    env.Append(
        CPPPATH=[
            p
            for p in [
                env.GetTestEnvSubDir("resources/n7-core/lib"),
            ]
            if env.Dir(p).exists()
        ]
    )

    opt_flag = f"-O{__get_optimization_flag(env)}"
    env.Append(CCFLAGS=opt_flag)
    env.Append(LINKFLAGS=[opt_flag])

    if env["build"] == "coverage":
        env.Append(
            CCFLAGS=[
                "--coverage",
                "-fprofile-update=prefer-atomic",
            ]
        )
        env.Append(
            LINKFLAGS=[
                "--coverage",
                "-fprofile-update=prefer-atomic",
            ]
        )
        env.Append(CPPDEFINES=["N7S_ENABLE_COVERAGE"])
        env.Append(LIBS=["stdc++"])

    __configure_ndebug_flag(env)


def __configure_base_path(env):
    env.Append(ENV={"PATH": os.environ["PATH"]})


def __add_base_tools(env):
    env.Tool("env_builder")
    env.Tool("modules_builder")
    env.Tool("multi_platform")
    env.Tool("compiledb")
    env.Tool("config_loader")
    env.Tool("version_from_git")
    env.Tool("autotools")
    env.Tool("objdump")


def __generate_coverage_html(env, title: str, modules: list, subdir: str):
    env.LcovCoverageReport(
        title, env.Dir(f"$installDir/doc/coverage/{subdir}/"), modules
    )


def __add_lcov_tool(env):
    env.Tool("lcov")
    env.AddMethod(__generate_coverage_html, "GenerateCoverageHtml")


def __add_common_subenv_tools(env):
    env.Tool("cpputest")
    env.Append(
        UNIT_TESTS_CPPPATH=[
            m
            for m in [
                env.GetTestEnvSubDir("resources/bsp/lib"),
                env.GetTestEnvSubDir("resources/n7-core/environment/lib"),
            ]
            if env.Dir(m).exists()
        ]
    )

    env.Tool("doxygen")
    if env["build"] == "coverage":
        __add_lcov_tool(env)

    if runner_tool := env.get("DefaultRunnerTool"):
        env.Tool(runner_tool)


def __configure_unit_tests_vars_for_subenv(env):
    if env["build"] == "coverage":
        env.Append(UNIT_TESTS_LIBS=["gcov"])
        if env["platform"] not in ["gcc_host", "mingw_w64"]:
            env.Append(CCFLAGS=["-fprofile-info-section"])
            env.Append(LINKFLAGS=["-fprofile-info-section"])
    env.Append(UNIT_TESTS_LIBS=env["N7S_TESTS_RUNTIME_LIBS"])
    env.Append(UNIT_TESTS_LINKFLAGS=env["N7S_TESTS_RUNTIME_LINKFLAGS"])

    env["CPPUTEST_MAIN_CONTENTS"] = "#include <TestsRuntime/TestMain.h>\n"


def __create_std_subenv_base(env, platform: str) -> SConsEnvironment:
    env = env.CreateSubEnvFor(platform)
    env.SetupCompilationDb()
    __add_common_subenv_tools(env)
    return env


def __create_std_subenv(env, platform: str) -> SConsEnvironment:
    """Creates an environment for provided platform based on it's configuration
    files, and adds it to environments list.
    For details, see `multi_platform` tool."""
    env = __create_std_subenv_base(env, platform)
    __configure_unit_tests_vars_for_subenv(env)
    return env


def __create_std_subenvs(env, platforms: list[str]) -> list[SConsEnvironment]:
    return [__create_std_subenv(env, platform) for platform in platforms]


def __setup_scons_warnings():
    SetOption("warn", "all")
    SetOption("warn", "no-dependency")
    warningAsException()


def generate(env, **kwargs):
    __setup_scons_warnings()

    variables = __build_variables(kwargs.get("additional_variables"))
    variables.Update(env)
    env.Help(variables.GenerateHelpText(env))
    __handle_unknown_variables(variables)

    __configure_base_path(env)
    __configure_base_flags(env)
    __add_base_tools(env)

    env["SCONS_C_STANDARD"] = kwargs.get("c_standard", "c11")
    env["SCONS_CXX_STANDARD"] = kwargs.get("cxx_standard", "c++2a")

    env["SVF_CONFIG"] = env.LoadConfig([env["svf"]])
    env["SKIP_CODE_CHECKS"] = not env["checkCode"]

    env.SetDefault(N7S_SW_TITLE_SHORT=kwargs.get("short_title", ""))

    env.AddMethod(__create_std_subenv, "StdSubEnvFor")
    env.AddMethod(__create_std_subenvs, "CreateStdSubEnvs")

    env.Host = __create_std_subenv_base(env, kwargs.get("host_platform", "gcc_host"))

    env.GenerateCompilationDb()
