# This file is part of the Test Environment build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from libs.ConfigParserWrapper import ConfigParserWrapper
from SCons.Action import Action
from SCons.Node import Node


def exists(env) -> bool:
    return True


def __load_config(env, paths) -> ConfigParserWrapper:
    config = ConfigParserWrapper()
    config.optionxform = lambda option: option
    config.configpaths = [env.File(p).abspath for p in paths]
    if len(config.read(config.configpaths)) != len(config.configpaths):
        raise Exception(f"Error while reading config files: {config.configpaths}")
    return config


def __dump_config_action(env, source, target):
    with open(target[0].abspath, "w", encoding="utf-8") as configfile:
        env["CONFIG_TO_DUMP"].write(configfile)


def __dump_config(env, config, path):
    return env.Command(
        source=config.configpaths,
        target=path,
        action=Action(
            __dump_config_action,
            "Dumping configuration to $TARGET, read from ($SOURCES)",
        ),
        CONFIG_TO_DUMP=config,
    )


def __add_defines_from(env, config):
    env.Append(CPPDEFINES=[f"{key}={value}" for key, value in config.items()])


def __create_config_header(env, target, config) -> Node:
    contents = "// GENERATED FILE DO NOT EDIT\n"
    contents += "\n".join(f"#define {key} {value}" for key, value in config.items())
    contents += "\n"
    return env.CreateFileForContents(target, contents)


def generate(env):
    env.AddMethod(__load_config, "LoadConfig")
    env.AddMethod(__dump_config, "DumpConfig")
    env.AddMethod(__add_defines_from, "AddDefinesFrom")
    env.AddMethod(__create_config_header, "CreateConfigHeader")
