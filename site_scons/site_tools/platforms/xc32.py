# This file is part of the Test Environment build system.
#
# @copyright 2024-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any

from . import Operation
from .gcc import Gcc


class XC32(Gcc):
    def __init__(self, platform: str):
        super().__init__(platform)

    def env(self) -> dict[Operation, dict[str, Any]]:
        return self._merge_env(
            super().env(),
            {
                Operation.replace: {
                    "CC": "xc32-gcc",
                    "CXX": "xc32-g++",
                    "LD": "xc32-ld",
                    "AR": "xc32-ar",
                    "RANLIB": "xc32-ranlib",
                    "STRIP": "xc32-strip",
                    "GCOV": "pic32c-gcov",
                    "OBJDUMP": "xc32-objdump",
                    "OBJCOPY": "xc32-objcopy",
                    "GDB": "arm-none-eabi-gdb",
                    "CPPCHECK_PLATFORM": "arm32-wchar_t2",
                    "DefaultRunnerTool": "gdb_runner",
                },
                Operation.append: {
                    "LINKFLAGS": [
                        "-ffunction-sections",
                        "-ffreestanding",
                        "-Wl,--gc-sections",
                        "-mno-device-startup-code",
                        "-Wl,--print-memory-usage",
                        "-Wl,--defsym=exit=_exit",
                        "-Wl,--defsym=_Exit=_exit",
                        "-Wl,--no-cpp",
                    ],
                    "CCFLAGS": [
                        "-ffunction-sections",
                        "-ffreestanding",
                        "-Wno-cast-qual",
                    ],
                    "CPPPATH": [
                        "/opt/RTT/xc32/RTT",
                    ],
                    "LIBPATH": [
                        "/opt/RTT/xc32",
                    ],
                    "CPPUTEST_CXXFLAGS": [
                        "-I/opt/cpputest/xc32/include",
                    ],
                    "CPPUTEST_LIBPATH": [
                        "/opt/cpputest/xc32/lib",
                    ],
                    "N7S_TESTS_RUNTIME_LINKFLAGS": [],
                    "N7S_TESTS_RUNTIME_LIBS": [],
                },
            },
        )
