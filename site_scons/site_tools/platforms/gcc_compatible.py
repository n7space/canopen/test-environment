# This file is part of the Test Environment build system.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any

from . import Configuration, Operation


class GccCompatible(Configuration):
    def __init__(self, platform: str):
        super().__init__(platform)

    def env(self) -> dict[Operation, dict[str, Any]]:
        return self._merge_env(
            super().env(),
            {
                Operation.replace: {},
                Operation.prepend: {},
                Operation.append: {
                    "CFLAGS": [
                        "--std=${SCONS_C_STANDARD}",
                        "-Wbad-function-cast",
                        "-Wmissing-prototypes",
                        "-Wnested-externs",
                        "-Wold-style-definition",
                        "-Wstrict-prototypes",
                    ],
                    "CXXFLAGS": [
                        "--std=${SCONS_CXX_STANDARD}",
                        "-fno-rtti",
                        "-fcheck-new",
                    ],
                    "CCFLAGS": [
                        "-ggdb3",
                        "-Werror",
                        "-Wall",
                        "-Wextra",
                        "-pedantic",
                        "-pedantic-errors",
                        "-Wcast-align",
                        "-Wcast-qual",
                        # TODO: this should be disabled for lely-core-related
                        # builds only
                        # "-Wconversion",
                        "-Wformat=2",
                        "-Winit-self",
                        "-Wmissing-declarations",
                        "-Wmissing-field-initializers",
                        "-Wredundant-decls",
                        "-Wshadow",
                        # TODO: this should be disabled for lely-core-related
                        # builds only
                        # "-Wstrict-overflow=5",
                        # "-Wundef",
                        "-Wuninitialized",
                        "-Wvla",
                        "-Wwrite-strings",
                    ],
                },
            },
        )
