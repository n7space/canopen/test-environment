# This file is part of the Test Environment build system.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any

from . import Operation
from .sam7_xc32 import SAM7_XC32


class SAMRH707F18_XC32(SAM7_XC32):
    def __init__(self):
        super().__init__("samrh707f18_xc32")

    def env(self) -> dict[Operation, dict[str, Any]]:
        return self._merge_env(
            super().env(),
            {
                Operation.append: {
                    "CTESW_TESTS_RUNTIME_LIBS": [
                        "n7s-bsp-matrix",
                    ],
                    "N7S_TESTS_RUNTIME_LIBS": [
                        "n7s-bsp-matrix",
                    ],
                    "CCFLAGS": [
                        "-mprocessor=ATSAMRH707F18A",
                        "-mitcm",
                        "-mdtcm",
                    ],
                    "LINKFLAGS": [
                        "-mprocessor=ATSAMRH707F18A",
                        "-mitcm",
                        "-mdtcm",
                    ],
                },
            },
        )


PLATFORM = SAMRH707F18_XC32
