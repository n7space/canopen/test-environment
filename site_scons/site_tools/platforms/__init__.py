# This file is part of the Test Environment build system.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import importlib
from enum import Enum
from typing import Any


class Operation(str, Enum):
    replace: str = "replace"
    prepend: str = "prepend"
    append: str = "append"


class Configuration:
    def __init__(self, platform: str):
        self.__platform = platform

    def platform(self) -> str:
        return self.__platform

    def env(self) -> dict[Operation, dict[str, Any]]:
        return {}

    @staticmethod
    def _merge_env(
        parent_env: dict[Operation, dict[str, Any]],
        src_env_dict: dict[Operation, dict[str, Any]],
    ) -> dict[Operation, dict[str, Any]]:
        merged_dict: dict[Operation, dict[str, Any]] = {
            Operation.replace: {},
            Operation.prepend: {},
            Operation.append: {},
        }
        merged_dict.update(parent_env)

        if Operation.replace in src_env_dict.keys():
            merged_dict[Operation.replace].update(src_env_dict[Operation.replace])

        if Operation.prepend in src_env_dict.keys():
            for key, value in src_env_dict[Operation.prepend].items():
                merged_dict[Operation.prepend][key] = value + merged_dict[
                    Operation.prepend
                ].get(key, [])

        if Operation.append in src_env_dict.keys():
            for key, value in src_env_dict[Operation.append].items():
                merged_dict[Operation.append][key] = (
                    merged_dict[Operation.append].get(key, []) + value
                )

        return merged_dict


def load_configuration_for(platform: str) -> Configuration:
    module_name = f".{platform}"
    module = importlib.import_module(module_name, package=__package__)
    conf_class = vars(module)["PLATFORM"]
    return conf_class()
