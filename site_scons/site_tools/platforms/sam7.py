# This file is part of the Test Environment build system.
#
# @copyright 2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any

from . import Operation
from .cortex_m7 import CortexM7


class SAM7(CortexM7):
    def __init__(self, platform):
        super().__init__(platform)

    def env(self) -> dict[Operation, dict[str, Any]]:
        return self._merge_env(
            super().env(),
            {
                Operation.replace: {
                    "PlatformEndianness": "little",
                },
                Operation.append: {
                    "LINKFLAGS": [
                        "-nodefaultlibs",
                        "-mfloat-abi=hard",
                        "-mfpu=fpv5-d16",
                    ],
                    "LIBPATH": [
                        "$installDir/environment/lib",
                    ],
                    "CCFLAGS": [
                        "-mfloat-abi=hard",
                        "-mfpu=fpv5-d16",
                    ],
                    "N7S_TESTS_RUNTIME_LIBS": [
                        "tests-startup",
                        "c",
                        "tests-runtime-sam7-libc",
                        "tests-runtime",
                        "tests-runtime-sam7",
                        "stdc++",
                        "c",  # must be twice as tests-runtime use it
                        "gcc",
                        "segger_rtt",
                        "n7s-utils",
                        "n7s-bsp-nvic",
                        "n7s-bsp-wdt",
                        "n7s-bsp-pmc",
                        "n7s-bsp-fpu",
                        "n7s-bsp-mpu",
                    ],
                    "CTESW_TESTS_RUNTIME_LIBS": [
                        "tests-startup",
                        "TestFramework-hwtb",
                        "gcc",
                        "segger_rtt",
                        "n7s-bsp-fpu",
                        "n7s-bsp-mcan",
                        "n7s-bsp-mpu",
                        "n7s-bsp-nvic",
                        "n7s-bsp-pio",
                        "n7s-bsp-pmc",
                        "n7s-bsp-systick",
                        "n7s-bsp-wdt",
                    ],
                },
            },
        )
