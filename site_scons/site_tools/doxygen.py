# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re

from SCons.Defaults import Delete
from SCons.Node import Node
from SCons.Node.FS import File
from SCons.Script import Flatten

__include_re = re.compile(r"^@INCLUDE\s*=\s*(\S+)$", re.M)


def __scan(env, doxyfile) -> list[File]:
    contents = doxyfile.get_text_contents()
    files = [env.File("#/" + f) for f in __include_re.findall(contents)]
    files = env.Flatten(files + [__scan(env, f) for f in files])
    return files


def __find_sources(directory: str, extensions: list[str]) -> list[str]:
    result = []

    def check(filename):
        return any(filename.endswith(ext) for ext in extensions)

    for root, _, filenames in os.walk(directory):
        result += [os.path.join(root, f) for f in filenames if check(f)]
    return result


def __generate_doxygen_exclude_patterns(env) -> list[str]:
    platforms = env["ALL_BUILD_PLATFORMS"]
    return [f"*/{p}/*" for p in platforms if p != env["platform"]]


def __doxygen_env(env) -> str:
    return " ".join([f'{k}="{env.subst(v)}"' for k, v in env["DOXYGEN_ENV"].items()])


def exists(env) -> bool:
    return True


def doxygen(
    env,
    name: str,
    doxyfile: str,
    output: str,
    directories: list[str],
    extensions: list[str] | None = None,
) -> Node:
    extensions = extensions if extensions is not None else [".h", ".hpp", ".c", ".cpp"]
    directories = Flatten([directories])  # accept single dir or list
    inputs = " ".join(directory.abspath for directory in directories)
    includes = [env.Dir(d).path for d in env["CPPPATH"]]
    includes = [d for d in includes if not d.startswith("build")]

    env = env.Clone()

    env["DOXYGEN_ENV"]["N7S_DOXYGEN_PROJECT_NAME"] = name
    env["DOXYGEN_ENV"]["N7S_DOXYGEN_INPUT_DIRS"] = inputs
    env["DOXYGEN_ENV"]["N7S_DOXYGEN_OUTPUT_DIR"] = env.Dir(output).abspath
    env["DOXYGEN_ENV"]["N7S_DOXYGEN_PREDEFINED"] = " ".join(env["CPPDEFINES"])
    env["DOXYGEN_ENV"]["N7S_DOXYGEN_INCLUDE_PATH"] = " ".join(includes)

    env["DOXYGEN_ENV"]["N7S_DOXYGEN_PLATFORM"] = env.get("N7S_SW_PLATFORM")
    env["DOXYGEN_ENV"]["N7S_DOXYGEN_PROJECT_NUMBER"] = env.get("N7S_SW_VERSION")

    env["DOXYGEN_ENV"]["N7S_DOXYGEN_EXCLUDE_PATTERNS"] = " ".join(
        env["DOXYGEN_EXCLUDE_PATTERNS"]
    )

    if env["DOXYGEN_EXCLUDE_OTHER_PLATFORMS"]:
        env["DOXYGEN_ENV"]["N7S_DOXYGEN_EXCLUDE_PATTERNS"] += " " + " ".join(
            __generate_doxygen_exclude_patterns(env)
        )

    output_dir = env.Dir(output).abspath

    cmd = env.Command(
        [os.path.join(output_dir, "html", "index.html")],
        [doxyfile]
        + Flatten(
            [__find_sources(directory.abspath, extensions) for directory in directories]
        ),
        [
            Delete(os.path.join(output_dir, "html")),
            Delete(os.path.join(output_dir, "xml")),
            Delete(os.path.join(output_dir, "pdf")),
            "${doxygen_env(__env__)} $DOXYGEN $SOURCE.abspath",
        ],
    )

    env.Depends(cmd, __scan(env, env.File(doxyfile)))
    env.Clean(cmd, output_dir)

    return cmd


def generate(env):
    env.SetDefault(DOXYGEN="doxygen")
    env.SetDefault(DOXYGEN_EXCLUDE_OTHER_PLATFORMS=True)
    env.SetDefault(DOXYGEN_EXCLUDE_PATTERNS=[])
    env.SetDefault(DOXYGEN_ENV={})
    env["doxygen_env"] = __doxygen_env

    env.AddMethod(doxygen, "Doxygen")
