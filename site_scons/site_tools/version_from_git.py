# This file is part of the Test Environment build system.
#
# @copyright 2023-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess


def __read_version_from_git():
    command = (
        'GIT_TOP_PROJECT_DIR="$(git rev-parse --show-superproject-working-tree)"; '
        + 'git -C "${GIT_TOP_PROJECT_DIR:-./}" describe --tags --dirty --always --abbrev=6'
    )
    version = subprocess.check_output(command, shell=True, encoding="utf-8").strip()
    print("Detected SW version from git:", version)
    return version


def __read_version_from_file(file):
    with open(file.abspath, encoding="utf-8") as version_file:
        version = version_file.read().strip()
        print("Detected SW version from file:", version)
        return version


def __read_version(env):
    version_file = env.File("#/VERSION")
    if version_file.exists():
        return __read_version_from_file(version_file)
    return __read_version_from_git()


def exists(env):
    return True


def generate(env):
    env["N7S_SW_VERSION"] = __read_version(env)
