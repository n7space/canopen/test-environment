# This file is part of the Test Environment build system.
#
# @copyright 2024-2025 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__io_handler_defines = {
    "rtt": "USE_RTT_IO",
}


def exists(env) -> bool:
    return True


def __configure_test_io(env):
    io_type = env["SVF_CONFIG"].get("ioHandler", "type")
    env["IO_TYPE"] = io_type
    env.Append(CPPDEFINES=__io_handler_defines[io_type])


def __configure_can_interfaces(env):
    can_a_id = env["SVF_CONFIG"]["canBusA"]["mcanId"]
    can_b_id = env["SVF_CONFIG"]["canBusB"]["mcanId"]
    env.Append(
        CPPDEFINES=[f"CTESW_CAN_A_MCAN={can_a_id}", f"CTESW_CAN_B_MCAN={can_b_id}"]
    )


def __configure_subenv_for_hwtb(env):
    __configure_test_io(env)
    __configure_can_interfaces(env)
    env.Append(CPPDEFINES=["CAN_TEST_ENV_HWTB"])
    env["TEST_ROLE"] = "HWTB"


def __configure_subenv_for_host(env):
    def ctesw_host_env(env):
        return " ".join(
            [f'{k}="{env.subst(v)}"' for k, v in env["CTESW_HOST_ENV"].items()]
        )

    env.AddMethod(ctesw_host_env, "TestEnvCmd")

    env.Append(
        CTESW_HOST_ENV={
            "CTESW_CAN_A_ADDRESS": env["SVF_CONFIG"]["canBusA"]["address"],
            "CTESW_CAN_A_PORT": env["SVF_CONFIG"]["canBusA"]["port"],
            "CTESW_CAN_B_ADDRESS": env["SVF_CONFIG"]["canBusB"]["address"],
            "CTESW_CAN_B_PORT": env["SVF_CONFIG"]["canBusB"]["port"],
        }
    )
    env.Append(CPPDEFINES=["CAN_TEST_ENV_HOST"])
    env["TEST_ROLE"] = "HOST"


def __makeTestCaseApp(env, name, sources, libs=[], **kwargs):
    program = env.MakeProgram(
        name,
        sources,
        env["CTESW_TESTS_RUNTIME_LIBS"] + env["LELYCORE_LIBS"] + libs,
        **kwargs,
    )
    env.AddPostAction(
        program,
        [
            env.LogSectionsAction(),
            env.PrintSymbolAddressAction("_end"),
        ],
    )
    env.Alias(name, program)
    return program


def __depends_on_lely_config(env, item):
    env.Depends(item, env["lelyConfigFile"])


def configure_lely_config(env):
    env["lelyConfigDir"] = env["buildDir"] + "/resources/LibCANopen/lely-core"
    env["lelyConfigFile"] = env.File(env["lelyConfigDir"] + "/config.h").abspath
    env.Append(CPPPATH=[env["lelyConfigDir"]])


def generate(env):
    configure_lely_config(env)

    env.AddMethod(__configure_subenv_for_hwtb, "ConfigureAsHwtbEnv")
    env.AddMethod(__configure_subenv_for_host, "ConfigureAsHostEnv")
    env.AddMethod(__depends_on_lely_config, "DependsOnLelyConfig")
    env.AddMethod(__makeTestCaseApp, "MakeTestCaseApp")
