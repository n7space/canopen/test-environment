# This file is part of the Test Environment build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json

from SCons.Action import Action


def __addTraces(self, name, trace=None):
    if trace is None:
        return

    trace = dict(trace)
    trace["id"] = trace.get("id", name)
    trace["title"] = trace.get("title", f"{name} Test Case")
    trace["path"] = self.Dir(".").abspath.replace(
        self.Dir(self["buildDir"]).abspath + "/", ""
    )

    self.GetTraces().append(trace)


def __dumpTraces(self, output):
    def action(env, source, target):
        with open(str(target[0]), "w") as f:
            json.dump(env.GetTraces(), f, indent=2)

    cmd = self.Command(
        output,
        self.Value(self.GetTraces()),
        Action(action, "Dumping test traces to $TARGET"),
    )
    self.AlwaysBuild(cmd)
    return cmd


def exists(env):
    return True


def generate(env):
    traces = []  # global per tool instantiation

    def localTraces(self):
        return traces

    env.AddMethod(localTraces, "GetTraces")
    env.AddMethod(__addTraces, "AddTraces")
    env.AddMethod(__dumpTraces, "DumpTraces")
