# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities for building SCons environments for different platforms/toolchains.

Platform support is provided via `Configuration` class. Every supported platform must have
an instance (containing build environment configuration) in `platforms` sub-package.

After providing a platform instance, it must be configured via `ConfigureFor` method.
This is automatically done via `CreateSubEnvFor` method of `multi_platform` tool.
See `Configurator.configure()` method for list of build sub-environment methods.

This tool also provides methods for code validation (per extension) and main environment
initialization. See `generate()` function for list of provided methods.
"""

import os
from typing import Any

from SCons.Defaults import Mkdir
from SCons.Node import Node
from SCons.Script import Flatten, Glob, WhereIs

from platforms import Configuration, Operation, load_configuration_for


def _resolve_alias(env, alias: str) -> list:
    # works only if alias was defined before (in terms of order of SConscript loading)
    return env.Alias(alias)[0].all_children()


def _find_dirs(sources: list[str]) -> set[str]:
    dirs = [os.path.dirname(str(s)) for s in sources]
    return set(dirs)


def _filter_c_sources(sources: list[str]):
    suffixes = {
        ".c",
        ".cpp",
        ".cxx",
        ".h,",
        ".hpp",
        ".hh",
    }
    return [s for s in sources if os.path.splitext(str(s))[1] in suffixes]


def _find_header_files(env, sources: list[str]) -> list[str]:
    """Accepts a list of source C/C++ files and returns list of headers for those source files,
    assuming that they are in the same directories, next to each other."""
    return Flatten(
        [
            [
                Glob(os.path.join(d, "*.h")),
                Glob(os.path.join(d, "*.hpp")),
                Glob(os.path.join(d, "*.H")),
                Glob(os.path.join(d, "*.hh")),
            ]
            for d in _find_dirs(_filter_c_sources(sources))
        ]
    )


def _apply_code_checks(env, target: str, sources: list[str]):
    env.Depends(target, env.CheckCode(sources, search_for_headers=True))


def _get_file_ext(file: str) -> str:
    return os.path.splitext(str(file))[1]


def _check_code(
    env, sources: list[str] | str, search_for_headers: bool = False
) -> list:
    """Processes the list of sources using previously configured code check tools.
    Returns the list of results from the callback for each provided file.
    Setting `search_for_headers = True` adds header files (searched via FindHeadersFor)
    to the list of checked sources."""
    if env["SKIP_CODE_CHECKS"]:
        return []

    if not isinstance(sources, list):
        sources = [sources]

    if search_for_headers:
        # deep copy
        sources = list(sources) + _find_header_files(env, sources)

    return Flatten(
        [
            [
                callback(env, src)
                for callback in env["CODE_CHECK_TOOLS"].get(_get_file_ext(src), [])
            ]
            for src in sources
        ]
    )


def _install(env, item, args: dict[str, str], default_suffix: str) -> Node:
    install_suffix = args.get("install_suffix", default_suffix)
    if install_suffix:
        return env.Install(os.path.join(env["installDir"], install_suffix), item)
    return item


def _install_in_subdir(env, subdir: str, item) -> Node:
    return _install(env, item, {}, subdir)


def _add_cwd_to_cpppath(env, kwargs: dict[str, Any]) -> dict[str, Any]:
    if "CPPPATH" not in kwargs:
        kwargs["CPPPATH"] = env["CPPPATH"] + [env.Dir(".")]
    else:
        kwargs["CPPPATH"].append(env.Dir("."))
    return kwargs


def _make_object(env, sources: str, name: str | None = None, **kwargs) -> Node:
    # Generate the name (explicitly define the platform to prevent issues in CAN tests)
    if "OBJSUFFIX" not in kwargs:
        kwargs["OBJSUFFIX"] = f".{env['platform']}.o"

    kwargs = _add_cwd_to_cpppath(env, kwargs)

    obj = env.Object(name, sources, **kwargs)
    env.Depends(obj, env.CheckCode(sources))
    return obj


def _make_library(env, name: str, sources: list[str], **kwargs) -> Node:
    kwargs = _add_cwd_to_cpppath(env, kwargs)
    lib = env.Library(name, sources, **kwargs)
    _apply_code_checks(env, lib, sources)

    lib = _install(env, lib, kwargs, env["INSTALL_SUFFIX_LIB"])

    env.Alias(name, lib)
    env.Alias("libs", lib)
    env.Alias("static-libs", lib)
    env.Alias(f"{name}-{env['platform']}", lib)
    env.Alias(f"libs-{env['platform']}", lib)
    env.Alias(f"static-libs-{env['platform']}", lib)
    return lib


def _make_shared_library(env, name: str, sources: list[str], **kwargs) -> Node:
    kwargs = _add_cwd_to_cpppath(env, kwargs)
    lib = env.SharedLibrary(name, sources, **kwargs)

    _apply_code_checks(env, lib, sources)

    lib = _install(env, lib, kwargs, env["INSTALL_SUFFIX_LIB"])

    env.Alias(name, lib)
    env.Alias("libs", lib)
    env.Alias("shared-libs", lib)
    env.Alias(f"{name}-{env['platform']}", lib)
    env.Alias(f"libs-{env['platform']}", lib)
    env.Alias(f"shared-libs-{env['platform']}", lib)
    return lib


def _build_program(
    env, name: str, sources: list[str], libs: list[str], **kwargs
) -> Node:
    env = env.Clone()
    env.Prepend(LIBS=libs)
    kwargs = _add_cwd_to_cpppath(env, kwargs)
    linker_script = kwargs.get("LDSCRIPT", None)
    if not linker_script:
        linker_script = env.get("LDSCRIPT", None)
    if linker_script:
        env.Append(LINKFLAGS=[f"-T{linker_script}"])

    # workaround issue with executable filename being equal to alias name
    target_name = env.File(name)

    program = env.Program(target_name, sources, **kwargs)

    if linker_script:
        env.Depends(program, linker_script)

    return program


def _make_program(
    env, name: str, sources: list[str], libs: list[str], **kwargs
) -> Node:
    program = _build_program(env, name, sources, libs, **kwargs)

    _apply_code_checks(env, program, sources)

    program = _install(env, program, kwargs, env["INSTALL_SUFFIX_BIN"])

    env.Alias(name, program)
    env.Alias("apps", program)
    env.Alias(f"{name}-{env['platform']}", program)
    env.Alias(f"apps-{env['platform']}", program)
    return program


def _configure_environment(env, env_dict: dict[Operation, dict[str, Any]]):
    if Operation.replace in env_dict.keys():
        for key, value in env_dict[Operation.replace].items():
            env[key] = value

    if Operation.prepend in env_dict.keys():
        env.Prepend(**env_dict[Operation.prepend])

    if Operation.append in env_dict.keys():
        env.Append(**env_dict[Operation.append])


def _merge_env(
    dst_env_dict, src_env_dict: dict[Operation, dict[str, Any]]
) -> dict[Operation, dict[str, Any]]:
    merged_dict = {
        Operation.replace: {},
        Operation.prepend: {},
        Operation.append: {},
    }
    merged_dict.update(dst_env_dict)

    if Operation.replace in src_env_dict.keys():
        merged_dict[Operation.replace].update(src_env_dict[Operation.replace])

    if Operation.prepend in src_env_dict.keys():
        for key, value in src_env_dict[Operation.prepend].items():
            merged_dict[Operation.prepend][key] = value + merged_dict[
                Operation.prepend
            ].get(key, [])

    if Operation.append in src_env_dict.keys():
        for key, value in src_env_dict[Operation.append].items():
            merged_dict[Operation.append][key] = (
                merged_dict[Operation.append].get(key, []) + value
            )

    return merged_dict


def _configure_builddir(env, platform: str):
    env["platform"] = platform

    env["buildDir"] = os.path.join(
        "#", env.get("buildDirName", "build"), env.get("build", ""), platform
    )
    env["installDir"] = os.path.join(env["buildDir"], "install_root")

    # the buildDir's value is sliced ([2:]) to get rid of leading '#/'.
    env.Execute(Mkdir(env["buildDir"][2:]))


def _configure_sconssign(env):
    # the buildDir's value is sliced ([2:]) to get rid of leading '#/'.
    env.SConsignFile(env["buildDir"][2:] + "/sconsign.dblite")


def _find_exec(tool: str):
    executable = WhereIs(tool)
    if not executable:
        raise Exception(f"Missing required '{tool}' in PATH")
    return executable


def _resolve_absolute_paths_for_tools(env):
    known_tools = ["CC", "CXX", "LD", "AR", "RANLIB", "STRIP", "GCOV", "OBJDUMP"]
    for k in known_tools:
        env[k] = _find_exec(env[k])


def _apply_configuration(env, configuration: Configuration):
    env.AddMethod(_make_object, "MakeObject")
    env.AddMethod(_make_program, "MakeProgram")
    env.AddMethod(_make_library, "MakeLibrary")
    env.AddMethod(_make_shared_library, "MakeSharedLibrary")

    env.AddMethod(_install_in_subdir, "InstallInSubdir")

    env.SetDefault(SCONS_C_STANDARD="c99")
    env.SetDefault(SCONS_CXX_STANDARD="c++17")

    env.SetDefault(SKIP_CODE_CHECKS=False)

    env.SetDefault(INSTALL_SUFFIX_LIB="lib")
    env.SetDefault(INSTALL_SUFFIX_BIN="bin")
    env.SetDefault(INSTALL_SUFFIX_TESTS="tests")

    env.SetDefault(N7S_SW_PLATFORM=configuration.platform().upper())

    _configure_builddir(env, configuration.platform())

    _configure_environment(env, configuration.env())

    _resolve_absolute_paths_for_tools(env)

    target_platform = configuration.platform().upper().removesuffix("_XC32")
    env.Append(CPPDEFINES=["N7S_TARGET_" + target_platform])
    env.Append(LIBPATH=os.path.join("$installDir", "$INSTALL_SUFFIX_LIB"))


def _configure_for(env, platform: str):
    """Configures current environment for specified platform.
    Should be called once per platform, after cloning the environment."""
    _apply_configuration(env, load_configuration_for(platform))


def _add_src_validation_callback(self, callback, extensions: list[str] | None = None):
    """Adds global validation callback for specified file extensions.
    Those validation callbacks can be later used via CheckCode."""
    extensions = extensions if extensions else [".c", ".cpp", ".h", ".hpp", ".cc"]
    for ext in extensions:
        # ugly, but defaultdict + SCons.Environment somehow duplicates entries
        items = self["CODE_CHECK_TOOLS"].get(ext, [])
        items.append(callback)
        self["CODE_CHECK_TOOLS"][ext] = items


def _configure_main_env(env):
    """Configures main build environment.
    Should only be called once."""
    _configure_builddir(env, "")
    _configure_sconssign(env)


def exists(env):
    return True


def generate(env):
    env.SetDefault(CODE_CHECK_TOOLS={})

    env.AddMethod(_find_header_files, "FindHeadersFor")
    env.AddMethod(_configure_environment, "ConfigureEnvironment")
    env.AddMethod(_configure_for, "ConfigureFor")
    env.AddMethod(_add_src_validation_callback, "AddSrcValidationCallback")
    env.AddMethod(_configure_main_env, "ConfigureMainEnv")
    env.AddMethod(_check_code, "CheckCode")
    env.AddMethod(_resolve_alias, "ResolveAlias")
