# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


def exists(env) -> bool:
    return True


def __create_file_for_contents(env, file_name: str, contents: str):
    def create_file(env, source, target):
        with open(target[0].path, "w", encoding="ascii") as output:
            output.write(env.subst(contents))

    return env.Command(
        [file_name], [], env.Action(create_file, f"Creating {file_name}")
    )


def generate(env):
    env.AddMethod(__create_file_for_contents, "CreateFileForContents")
