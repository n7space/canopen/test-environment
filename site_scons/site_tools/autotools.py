# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from pathlib import Path

import SCons
from SCons.Action import Action
from SCons.Node import Node
from SCons.Script import Dir


def exists(env) -> bool:
    return env.Detect("autoreconf")


def __make_symlink(
    target: list[SCons.Node.FS.File], source: list[SCons.Node.FS.File], env
):
    for source, target in zip(source, target):
        Path(target.get_abspath()).symlink_to(source.get_abspath())


def __link_directory_contents(env, source: Dir, target: Dir):
    source_path = Path(source.abspath)
    target_path = Path(target.abspath)

    symlinks = [
        env.Command(
            source=str(f.resolve().absolute()),
            target=str(target_path / f.relative_to(source_path)),
            action=Action(__make_symlink, "Symlinking $SOURCE -> $TARGET"),
        )
        for f in source_path.rglob("*")
        if f.is_file()
    ]
    return symlinks


def __refreshAutotools(env, source: str) -> Node:
    """Runs `autoreconf -i` in source directory"""
    source = env.File(source + "/configure.ac")
    return env.Command(
        source=source.abspath,
        target=f"{source.dir.abspath}/configure",
        action=f"cd {source.dir.path} && autoreconf -i",
    )


def __symlinkAndRefreshAutotools(env, source: str) -> Node:
    """Clones the source directory using symlinks and runs `autoreconf -i` in cloned directory"""
    # Note: `source` will usually be a relative path from SConscript that executes this function,
    # and will point to a directory with project's sources.
    # Treating it with env.Dir here will create a path starting with variant's build directory.

    target_dir = env.Dir(source)
    source_dir = target_dir.srcnode()

    if not target_dir.path.startswith(env.Dir(env["buildDir"]).path):
        raise RuntimeError("symlinkAndRefreshAutotools called from non-variant build")

    symlinks = __link_directory_contents(
        env,
        source_dir,
        target_dir,
    )

    autotools_command = __refreshAutotools(env, target_dir.abspath)

    env.Depends(autotools_command, symlinks)

    return autotools_command


def __autotools(
    env,
    target: str,
    source: str,
    librariesHint: list[str] = [],
    applicationsHint: list[str] = [],
) -> tuple[Node, Node]:
    """Configures the source project, builds it and installs it in target directory.
    Returns pair of nodes, first for `configure` and second for `make && make install`.
    Install target can be defined via `AUTOTOOLS_MAKE_INSTALL_COMMAND` env variable.
    `configure` options can be defined via `CONFIGURE_OPTIONS` env variable."""

    def factory(target):
        librariesTargets = [
            env.File(os.path.join(env["installDir"], "lib", lib))
            for lib in librariesHint
        ]

        applicationsTargets = [
            env.File(os.path.join(env["installDir"], "bin", bin))
            for bin in applicationsHint
        ]

        explicitTargets = librariesTargets + applicationsTargets

        if len(explicitTargets) > 0:
            return explicitTargets
        return env.Dir(target)

    config = env.Command(
        source=source,
        target=env.File(os.path.join(env["buildDir"], target, "config.h")),
        action="cd $TARGET.dir && $SOURCE.dir.abspath/configure --prefix=$INSTALL_DIR.abspath $CONFIGURE_OPTIONS",
        INSTALL_DIR=env.Dir(env["installDir"]),
    )

    result = env.Command(
        source=config,
        target=factory(target),
        action="cd $SOURCE.dir && bear -- make && make $AUTOTOOLS_MAKE_INSTALL_COMMAND",
    )
    return config, result


def generate(env):
    env["AUTOTOOLS_MAKE_INSTALL_COMMAND"] = "install-exec"

    env.AddMethod(__symlinkAndRefreshAutotools, "SymlinkAndRefreshAutotools")
    env.AddMethod(__autotools, "Autotools")
