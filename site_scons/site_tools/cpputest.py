# This file is part of the Test Environment build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from SCons.Action import Action
from SCons.Node import Node
from SCons.Script.SConscript import SConsEnvironment


def __verify_log_action(env, test_name: str) -> Action:
    def check_log_contents(contents: bytes) -> bool:
        return b"Errors (" not in contents and b"OK (" in contents

    return env.CheckUnitTestsLogAction(test_name, check_log_contents)


def exists(env) -> bool:
    return True


__DEFAULT_MAIN_CONTENTS = """
#include <CppUTest/CommandLineTestRunner.h>

int main(int ac, char** av) {
  return RUN_ALL_TESTS(ac, av);
}
"""


def __clone_for_cpputest(env) -> SConsEnvironment:
    env = env.CloneForUnitTests()
    env.Append(CXXFLAGS=env.get("CPPUTEST_CXXFLAGS", []))
    env.Append(CPPDEFINES=env.get("CPPUTEST_CPPDEFINES", []))
    env.Append(LIBPATH=env.get("CPPUTEST_LIBPATH", []))
    env.Append(RUNNER_ARGS=env.get("CPPUTEST_RUNNER_ARGS", []))
    return env


def __make_cpputest(
    env,
    name: str,
    sources: list[str],
    libs: list[str] = [],
    generate_main: bool = True,
    **kwargs,
) -> Node:
    if kwargs.get("clone_env", True):
        env = env.CloneForCppUTest()

    main = (
        [env.CreateFileForContents(name + "-main.cpp", env["CPPUTEST_MAIN_CONTENTS"])]
        if generate_main
        else []
    )
    kwargs["install_suffix"] = kwargs.get("install_suffix", env["INSTALL_SUFFIX_TESTS"])
    program = env.MakeProgram(
        name + kwargs.get("name_suffix", "-unit-tests") + "-app",
        sources + main,
        libs + env["CPPUTEST_LIBS"],
        **kwargs,
    )

    result = env.DefaultRunner(program)

    env.AddPostAction(result, env.VerifyCppUTestLogResultAction(name))

    env.AliasUnitTestsResult(program, result, name, **kwargs)

    return result


def generate(env):
    env.Tool("unit_tests")
    env.Tool("file_creator")

    preInitProgram = env["SVF_CONFIG"].get(
        "cpputestUnitTests", "preInitProgram", fallback=None
    )
    if preInitProgram is not None:
        preInitProgram = env.File(
            f'{env["installDir"]}/environment/bin/{preInitProgram}'
        ).abspath
    env.Append(CPPUTEST_UNIT_TESTS_PREINIT_PROGRAM_PATH=preInitProgram)

    env.SetDefault(
        CPPUTEST_LIBS=[
            "CppUTestExt",
            "CppUTest",
            "m",
            "cpputest-main",
        ]
    )
    env.SetDefault(
        CPPUTEST_CPPDEFINES=[
            "N7S_CPPUTEST_TESTS",
            "CPPUTEST_USE_MEM_LEAK_DETECTION=0",
        ]
    )
    env.SetDefault(
        CPPUTEST_RUNNER_ARGS=[
            "-c",
            "-v",
        ]
    )
    env.SetDefault(CPPUTEST_MAIN_CONTENTS=__DEFAULT_MAIN_CONTENTS)

    env.AddMethod(__make_cpputest, "MakeCppUTest")
    env.AddMethod(__clone_for_cpputest, "CloneForCppUTest")
    env.AddMethod(__verify_log_action, "VerifyCppUTestLogResultAction")
