# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


def exists(env) -> bool:
    return True


def __install_default(env, runner):
    if env.get_builder("DefaultRunnerBld") is not None:
        raise Exception("Trying to override configured DefaultRunner")
    env.Append(BUILDERS={"DefaultRunnerBld": runner})


def __default_runner(env, *args, **kwargs):
    result = env.DefaultRunnerBld(*args, **kwargs)

    if bootstrapper := env["RUNNER_BOOTSTRAPPER"]:
        env.Depends(result, bootstrapper)

    if "lcov" in env["TOOLS"]:
        return result, env.LcovCapture(result)

    return result


def generate(env):
    env.SetDefault(RUNNER_BOOTSTRAPPER=None)

    env.AddMethod(__install_default, "InstallDefaultRunner")
    env.AddMethod(__default_runner, "DefaultRunner")
