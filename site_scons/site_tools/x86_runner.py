# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from SCons.Action import Action


def exists(env) -> bool:
    return True


def generate(env, **kwargs):
    env.Tool("runner")

    actions = []

    if bootstrapper := env["RUNNER_BOOTSTRAPPER"]:
        actions.append(Action(bootstrapper, f"Bootstrapping using {bootstrapper}"))

    actions.append(
        Action(
            "bash -c 'set -o pipefail ; ${SOURCES} ${RUNNER_ARGS} | tee ${TARGETS}'",
            "Executing ${SOURCES}...",
        )
    )

    bld = env.Builder(action=actions, suffix=".log")
    env.Append(BUILDERS={"X86Runner": bld})

    if kwargs.get("nodefault", True):
        env.InstallDefaultRunner(bld)
