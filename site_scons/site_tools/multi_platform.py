# This file is part of the Test Environment build system.
#
# @copyright 2022-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities for managing environments for multiple platforms.

This tool adds `AllEnvs` property to the environment, and provides
CreateSubEnvFor method for creating subenvironments for supported platforms.
Those subenvironments then can be used via `AllEnvs` property, or accessed via
`GetSubEnvFor` method.

For list of supported platforms and details about the subenvironments, see `env_builder`
"""

from SCons.Node import Node
from SCons.Script import Flatten
from SCons.Script.SConscript import SConsEnvironment


class Envs:
    def __init__(self):
        self.__envs: dict[str, SConsEnvironment] = {}

    def Items(self) -> tuple[str, SConsEnvironment]:
        return self.__envs.values()

    def BuildModules(self, modules: list[str]) -> list[Node]:
        return Flatten([env.BuildModules(modules) for env in self.Items()])

    def BuildModule(self, module: str) -> list[Node]:
        return Flatten([env.BuildModule(module) for env in self.Items()])

    def BuildUnitTestsFor(self, modules: list[str]) -> list[Node]:
        return Flatten([env.BuildUnitTestsFor(modules) for env in self.Items()])

    def Register(self, platform: str, env: SConsEnvironment):
        self.__envs[platform] = env

    def GetFor(self, platform: str):
        return self.__envs[platform]


def __init_multiplatform(env):
    if hasattr(env, "AllEnvs"):
        return
    env.ConfigureMainEnv()
    env.AllEnvs = Envs()


def __create_subenv_for(self, platform: str) -> SConsEnvironment:
    __init_multiplatform(self)

    env = self.Clone()
    env.ConfigureFor(platform)
    env.Parent = self

    self.AllEnvs.Register(platform, env)

    return env


def __get_subenv_for(self, platform: str) -> SConsEnvironment:
    return self.AllEnvs.GetFor(platform)


def exists(env) -> bool:
    return True


def generate(env):
    env.Tool("env_builder")
    env.Tool("modules_builder")

    env.AddMethod(__create_subenv_for, "CreateSubEnvFor")
    env.AddMethod(__get_subenv_for, "GetSubEnvFor")
