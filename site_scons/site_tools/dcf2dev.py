# This file is part of the Test Environment build system.
#
# @copyright 2020-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os.path

from SCons.Builder import Builder
from SCons.Node import Node
from SCons.Script.SConscript import SConsEnvironment


def exists(env) -> bool:
    return True


def generate_name(env, source) -> str:
    name = os.path.basename(source)[:-4].replace("-", "_")
    return env["DCF2DEV_NAME_PREFIX"] + name


def get_build_dir_root(env) -> str:
    # build dir is expected to look like this:
    # #/build_dir_root/some/platform/subdir
    # we want the "build_dir_root" part.
    return env["buildDir"].split("/")[1]


def generate_actions(source, target, env, for_signature) -> str:
    source = str(source[0])
    target = str(target[0])
    name = generate_name(env, source)
    buildDirRoot = get_build_dir_root(env)
    return f"BUILD_DIR_ROOT={buildDirRoot} $DCF2DEV $DCF2DEV_FLAGS -o {target} {source} {name}"


def generate_actions_for_fail_test(source, target, env, for_signature) -> str:
    source = str(source[0])
    target = str(target[0])
    name = generate_name(env, source)
    buildDirRoot = get_build_dir_root(env)
    return f"BUILD_DIR_ROOT={buildDirRoot} $DCF2DEV $DCF2DEV_FLAGS -o {target} {source} {name} ; test $$? -ne 0 && touch {target}"


def dcf2dev(env, sources) -> tuple[Node, SConsEnvironment]:
    envH = env.Clone()
    envH.Append(DCF2DEV_FLAGS="--header")
    hFile = envH.Dcf2DevH(sources)
    cFile = env.Dcf2DevC(sources)
    env.Depends(cFile, hFile)

    return cFile


def generate(env):
    env["DCF2DEV"] = env.GetBuildPath(
        env.GetTestEnvSubDir("resources/LibCANopen/dcf2dev.sh")
    )
    env["DCF2DEV_FLAGS"] = ["--include-config"]
    env["DCF2DEV_FLAGS"] += ["--gen-coverage"] if env["build"] == "coverage" else []
    env["DCF2DEV_NAME_PREFIX"] = "dcf_"

    bldC = Builder(
        generator=generate_actions,
        single_source=1,
        src_suffix=".dcf",
        prefix="dcf/",
        suffix=".c",
    )
    bldH = Builder(
        generator=generate_actions,
        single_source=1,
        src_suffix=".dcf",
        prefix="dcf/",
        suffix=".h",
    )
    bldFail = Builder(
        generator=generate_actions_for_fail_test,
        single_source=1,
        src_suffix=".dcf",
        prefix="dcf/",
        suffix=".dummy",
    )

    env.Append(
        BUILDERS={
            "Dcf2DevC": bldC,
            "Dcf2DevH": bldH,
            "Dcf2DevFailTest": bldFail,
        }
    )

    env.AddMethod(dcf2dev, "Dcf2Dev")
