# This file is part of the Test Environment build system.
#
# @copyright 2021-2024 N7 Space Sp. z o.o.
#
# Test Environment was developed under a programme of,
# and funded by, the European Space Agency (the "ESA").
#
#
# Licensed under the ESA Public License (ESA-PL) Permissive (Type 3),
# Version 2.4 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://essr.esa.int/license/list
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from SCons.Script import EnumVariable, Environment, PathVariable, Exit

__HWTB_PLATFORMS = (
    "samv71q21",
    "samv71q21_xc32",
    "samrh71f20",
    "samrh71f20_xc32",
    "samrh707f18",
    "samrh707f18_xc32",
)
__BUILD_PLATFORMS = __HWTB_PLATFORMS + ("gcc_host",)


def __test_env_root(env):
    src_root = env.Dir("#/").abspath
    if test_env_dir := env.get("CANOPEN_TEST_ENVIRONMENT_ROOT_DIR"):
        return os.path.join(src_root, test_env_dir)
    return src_root


def __test_env_subdir(env, *subdirs):
    subdirs = list(subdirs)
    return os.path.join(env.GetTestEnvSrcRoot(), *subdirs)


def __default_chip_config(env):
    return env.subst("resources/build-configs/default-${platform}.conf")


def __get_ldscript(env, name):
    return env.GetTestEnvSubDir("ld/", env["platform"], name + ".ld")


def __add_bsp_specific_opts(env):
    if env["platform"] != "gcc_host":
        env["LDSCRIPT"] = __get_ldscript(env, "tests")

    env.LoadChipConfig([__default_chip_config(env), env["chipConfig"]])
    env.Append(CPPPATH=env.GetTestEnvSubDir("resources/bsp"))

    if env["platform"] not in ["samrh71f20", "samrh707f18"]:
        env.Append(
            DOXYGEN_EXCLUDE_PATTERNS=[
                "*/samrh7/*",  # common base for RH71 and RH707
            ]
        )


def __add_test_env_specific_opts(env):
    env.Append(
        CPPPATH=[
            env.GetTestEnvSubDir("resources/LibCANopen/lely-core/include"),
            env.GetTestEnvSubDir("resources/"),
        ]
    )


def __setup_subenvironment(env):
    env.Tool("chip_config")
    env.Tool("ctesw_test_config")
    env.Tool("compiledb")

    __add_bsp_specific_opts(env)
    __add_test_env_specific_opts(env)


def __setup_environment_for_ctesw(env):
    env.Tool("ctesw_test_case")


def __prepare_subenv_for_canopen_tests(subenv):
    subenv = subenv.Clone()

    # CANOpen binaries built with XC32 require it's standard library to work
    if subenv["platform"].endswith("xc32") and "-nodefaultlibs" in subenv["LINKFLAGS"]:
        subenv["LINKFLAGS"].remove("-nodefaultlibs")

    # Use default libs in coverage builds (if some other tools disabled them previously)
    if subenv["build"] == "coverage":
        if subenv["platform"].endswith("xc32"):
            print("Coverage is not supported with XC32!")
            Exit(0)

        if "-nodefaultlibs" in subenv["LINKFLAGS"]:
            subenv["LINKFLAGS"].remove("-nodefaultlibs")

        subenv.LcovAddModules(subenv["TEST_SUITE_LCOV_MODULES"])
        subenv.LcovExcludeModules(subenv["TEST_SUITE_LCOV_EXCLUSIONS"])
        subenv.LcovAddFlags(
            [
                # some platforms don't support atomic profiling, leading to negative amount of executions.
                # it's unavoidable and rare, so it should be treated as a warning instead of an error.
                "--ignore-errors",
                "negative",
                # generating code from macros sometimes causes lcov to detect end-of-line mismatches.
                # nothing we can do about it.
                "--ignore-errors",
                "mismatch",
            ]
        )

    # Add ./include to C/C++ include paths as some CAN tests use it directory
    subenv.Append(
        CPPPATH=[
            "${SOURCE.dir.abspath}/include",
        ]
    )
    return subenv


def __create_env_for_canopen_tests(env):
    env = env.Clone()
    env.Tool("dcf2dev")

    env.host = __prepare_subenv_for_canopen_tests(env.host)
    env.hwtb = __prepare_subenv_for_canopen_tests(env.hwtb)

    return env


def __prepare_subenv_for_bsp_tests(subenv):
    subenv = subenv.Clone()
    if subenv["build"] == "coverage":
        subenv.LcovAddModules(["resources/bsp", "resources/n7-core"])
        subenv.LcovExcludeModules(["n7-core/environment/", "bsp/lib/"])
    return subenv


def __create_env_for_bsp_tests(env):
    env = env.Clone()

    env.host = __prepare_subenv_for_bsp_tests(env.host)
    env.hwtb = __prepare_subenv_for_bsp_tests(env.hwtb)

    return env


def CreateCANOpenEnvironments(
    additional_variables,
    root_dir: str | None,
    test_suite_lcov_modules: list[str] | None,
    test_suite_lcov_exclusions: list[str] | None,
):
    env = Environment()

    env["ALL_HWTB_PLATFORMS"] = __HWTB_PLATFORMS
    env["ALL_BUILD_PLATFORMS"] = __BUILD_PLATFORMS

    if test_suite_lcov_modules is None:
        test_suite_lcov_modules = ["resources/TestFramework"]

    if test_suite_lcov_exclusions is None:
        test_suite_lcov_exclusions = [
            "resources/LibCANopen/",
            "resources/bsp/",
            "resources/n7-core/",
        ]

    env.SetDefault(CANOPEN_TEST_ENVIRONMENT_ROOT_DIR=root_dir)
    env.SetDefault(TEST_SUITE_LCOV_MODULES=test_suite_lcov_modules)
    env.SetDefault(TEST_SUITE_LCOV_EXCLUSIONS=test_suite_lcov_exclusions)

    env.AddMethod(__test_env_root, "GetTestEnvSrcRoot")
    env.AddMethod(__test_env_subdir, "GetTestEnvSubDir")

    env.Tool(
        "core-std-env",
        additional_variables=additional_variables,
    )

    env.host = env.GetSubEnvFor("gcc_host")
    env.hwtb = env.StdSubEnvFor(env["hwtbPlatformName"])

    __setup_environment_for_ctesw(env)
    __setup_subenvironment(env.host)
    __setup_subenvironment(env.hwtb)

    env.host.ConfigureAsHostEnv()
    env.hwtb.ConfigureAsHwtbEnv()

    env.AddMethod(__create_env_for_canopen_tests, "CreateEnvForCanOpenTests")
    env.AddMethod(__create_env_for_bsp_tests, "CreateEnvForBspTests")

    return env


def CreateEnvironment(
    root_dir: str | None = None,
    test_suite_lcov_modules: list[str] | None = None,
    test_suite_lcov_exclusions: list[str] | None = None,
):
    bsp_variables = (
        PathVariable(
            "chipConfig",
            "MCU configuration file",
            "resources/build-configs/default.conf",
            PathVariable.PathIsFile,
        ),
    )
    test_env_variables = (
        EnumVariable(
            "hwtbPlatformName",
            "Defines HWTB platform name",
            "samv71q21",
            allowed_values=__HWTB_PLATFORMS,
        ),
    )
    return CreateCANOpenEnvironments(
        bsp_variables + test_env_variables,
        root_dir,
        test_suite_lcov_modules,
        test_suite_lcov_exclusions,
    )
